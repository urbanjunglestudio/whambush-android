package com.whambush.android.client.config;

public class AppConfig {
	
    public static final String URL = "http://api.whambush.net/";

    public static final String GOOGLE_ANALYST_ID = "UA-49596640-3";

    public static final String GCM_SENDER_ID = "927763095093";

    public static final String API_VERSION = "2.1";
    
}
