package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import roboguice.util.Ln;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.whambush.android.client.R;
import com.whambush.android.client.Selector;
import com.whambush.android.client.domain.tv.Channel;
import com.whambush.android.client.service.VideoFeedService;
import com.whambush.android.client.util.Fonts;
import com.whambush.android.client.util.StringHelper;
import com.whambush.android.client.view.list.VideoFeedListView;
import com.whambush.android.client.view.listener.PaginateListListener;

public class TVFeedActivity extends AbstractFeedActivity implements PaginateListListener {

	private static final String WHAMBUSH_TV = "WhambushTV";

	private static final String ACTIVITY_ID = "TvFeed";

	private Channel channel;

	private View header;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		super.onCreate(savedInstanceState, R.layout.activity_tv);

		showBackButton();

		showProgressBar();
		getList().setActivity(this);

		channel = (Channel)getParameter(Channel.KEY);

		getList().selectWithRequest(WHAMBUSH_TV, new VideoFeedService(this).parse(channel.getEndpoint(), false));
		getList().hideIcons();
		getList().setListener(this);
		getList().showFooter();
		View header = getHeader();
		if (header != null)
			getList().addHeaderView(header);
	}


	@SuppressLint("InflateParams")
	private View getHeader() {
		if (channel.getPictureUrl() != null && !channel.getPictureUrl().isEmpty()) {

			LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			header = (View)inflater.inflate(R.layout.list_channel_row, null);
			setTitle(header);
			final ImageView banner = (ImageView)header.findViewById(R.id.banner);

			ImageLoader.getInstance().displayImage(channel.getPictureUrl(), banner, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View v, Bitmap loadedImage) {
					Ln.d("loaded image for channel " + channel.getName());
					banner.setImageBitmap(loadedImage);
				}
			});

			return header;
		}
		return null;
	}


	private void setTitle(View header) {
		TextView title = (TextView)header.findViewById(R.id.title);
		if (title != null) {
			title.setTypeface(new Fonts().getTitleFont());
			title.setText(StringHelper.getEpisodeString(channel.getName(), "" + getList().listItemCount));
		}
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(Channel.KEY, channel);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		if (savedInstanceState != null)
			channel = (Channel)savedInstanceState.getSerializable(Channel.KEY);
	}

	@Override
	protected void onResume() {
		setCurrentSelector(Selector.TV);
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		overridePendingTransition(R.anim.none, R.anim.none);
	}

	public void tvButtonPressed(View view) {
	}


	@Override
	protected VideoFeedListView getList() {
		return (VideoFeedListView)super.getList();
	}

	public boolean isTv() {
		return true;
	}


	@Override
	public void listDataReceived() {
		super.listDataReceived();
		getList().removeHeaderView(getList().getHeader());
		setTitle(header);
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return ACTIVITY_ID;
	}

	public void currentClicked(View view) {
		;
	}

	@Override
	public void reload(View view) {
		startReloadAnimation();
		getList().reload();
	}

	@Override
	public void loaded() {
		stopReloadAnimation();
	}

	public void selectCountry(View view) {
		Map<String, Serializable> map = new HashMap<String, Serializable>();
		map.put(RootChannelActivity.SELECT_COUNTRY, true);

		startActivity(RootChannelActivity.class, map);
	}

}

