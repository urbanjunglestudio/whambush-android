package com.whambush.android.client.service;

import android.content.Intent;
import android.net.Uri;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.device.DeviceInfo;

public class MailSendingService {

	private final WhambushActivity activity;

	public MailSendingService(WhambushActivity activity) {
		this.activity = activity;
	}

	public EMail createMailSender() {
		return new EMail();
	}


	public class EMail {
		private final String BASE_SUBJECT = "---info to support don't remove---\n%s\n%s\n%s\n%s\n%s\n%s";


		private EMail() {

		}

		public void send() {
			Intent email = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
			email.setType("message/rfc822");

			email.putExtra(Intent.EXTRA_EMAIL, new String[] {activity.getString(R.string.SETTINGS_SUPPORT_MAIL)});
			email.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.SETTINGS_SUPPORT_MAIL_SUBJECT));
			email.putExtra(Intent.EXTRA_TEXT, getBaseSubject());

			try {
				activity.startActivity(Intent.createChooser(email, activity.getString(R.string.SETTINGS_SUPPORT_LABEL)));
			} catch (android.content.ActivityNotFoundException ex) {
				activity.getDialogHelper().displayDialog(activity.getDialogHelper().createWarningDialog(activity.getString(R.string.SETTINGS_SUPPORT_MAIL_FAIL)));
			}
		}

		private String getBaseSubject() {
			DeviceInfo info = new DeviceInfoService().getInfo();
			return String.format(BASE_SUBJECT, getUsername(), info.getManufacture(), info.getDevice(), info.getOs(), info.getVersion(), new SoftwareVersionService(activity).getVersion());
		}

		private Object getUsername() {
			return Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getUsername();
		}
	}
}
