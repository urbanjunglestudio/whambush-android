package com.whambush.android.client.domain;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.user.User;

public class WhambushObject implements Serializable {

	private static final long serialVersionUID = 7059322168560304085L;

	private String id;

	@SerializedName("hashtag")
	private String hash;
	
	private String name;
	
	private String description;
	
	@SerializedName("added_by")
	private User addedBy;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("modified_at")
	private String modifiedAt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(User addedBy) {
		this.addedBy = addedBy;
	}

	public String getCreated_at() {
		return createdAt;
	}

	public void setCreated_at(String created_at) {
		this.createdAt = created_at;
	}

	public String getModified_at() {
		return modifiedAt;
	}

	public void setModified_at(String modified_at) {
		this.modifiedAt = modified_at;
	}

	@Override
	public String toString() {
		return "WhambushObject [id=" + id + ", name=" + name + ", description="
				+ description + ", addedBy=" + addedBy + ", created_at="
				+ createdAt + ", modified_at=" + modifiedAt + ", toString()="
				+ super.toString() + "]";
	}

}
