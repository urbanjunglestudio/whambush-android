package com.whambush.android.client.view.fragment;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.AbstractWhambushActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.activity.ActivityFeedEvent;
import com.whambush.android.client.domain.user.LoginInfo;
import com.whambush.android.client.service.ActivityReadService;
import com.whambush.android.client.service.events.ActivitiesRead;
import com.whambush.android.client.service.events.ChangedFollow;
import com.whambush.android.client.service.events.ConnectionError;
import com.whambush.android.client.service.events.WBError;
import com.whambush.android.client.view.list.UserActivityListView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class ActivityFragment extends RoboFragment {

	@InjectView(R.id.list)
	UserActivityListView list;

	private View view;

	private CountListener listener;

	public ActivityFragment() {
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);
		view = inflater.inflate(R.layout.view_whambush_activity, container, false);

		return view;
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setup();
	}	

	private void setup() {
		list.setActivity((WhambushActivity)getActivity());
		list.setListener(listener);
		list.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				int lastPos = list.getLastVisiblePosition();

				if (isLastUnreadVisible(lastPos)) {
					if (list.getUnreadCount() > 0) {
						Ln.d("Send Unread");
						new ActivityReadService((AbstractActivity)getActivity()).markAllAsUnread();
						
					}
				}
			}
		});	
	}

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ActivitiesRead follow) {
        list.setAllUnread();
    }

	protected boolean isLastUnreadVisible(int index) {
		int unreadCount = list.getUnreadCount();
		LoginInfo info = Whambush.get(ConfigImpl.class).getLoginInfo();
		return (unreadCount >= info.getUnreadActivities());
	}

	public void refresh() {
		if (list == null)
			return;
		list.reload();
	}


	public void setListener(CountListener countListener) {
		listener = countListener;
	}


	public int getUnreadCount() {
		return list.getUnreadCount();
	}

}
