package com.whambush.android.client.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ProfilePicture extends ImageView {

	private Drawable defaultPicture;

	Bitmap roundBitmap = null;
	
	public ProfilePicture(Context context) {
		this(context, null);
	}

	public ProfilePicture(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public ProfilePicture(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
		setup();
	}


	private void setup() {
		defaultPicture = getDrawable();
	}

	public void setDefaultPicture() {
		setImageDrawable(defaultPicture);
	}
	
	@Override
	public void setImageResource(int resId) {
		roundBitmap = null;
		super.setImageResource(resId);
	}
	
	@Override
	public void setImageDrawable(Drawable drawable) {
		roundBitmap = null;
		super.setImageDrawable(drawable);
	}
	
	@Override
	public void setImageBitmap(Bitmap bm) {
		roundBitmap = null;
		super.setImageBitmap(bm);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {

		if (getDrawable() == null || getWidth() == 0 || getHeight() == 0)
			return; 

		if (roundBitmap == null)
			roundBitmap = create();
		
		canvas.drawBitmap(roundBitmap, 0,0, null);
	}

	private Bitmap create() {
		Drawable drawable = getDrawable();
		
		Bitmap b =  ((BitmapDrawable)drawable).getBitmap();
		Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);

		Bitmap roundBitmap =  getCroppedBitmap(bitmap);
		return roundBitmap;
	}


	public Bitmap getCroppedBitmap(Bitmap bmp) {
		Bitmap sbmp;

		int min = Math.min(getWidth(), getHeight());

		if(bmp.getWidth() != min || bmp.getHeight() != min)
			sbmp = Bitmap.createScaledBitmap(bmp, min, min, false);
		else
			sbmp = bmp;

		Bitmap output = Bitmap.createBitmap(getWidth(), getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final Paint paint = new Paint();
		
		int diff = getWidth() - min;
		
		final Rect dst = new Rect(diff/2, 0, min + diff/2, min);
		final Rect src = new Rect(0, 0, min, min);

		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		paint.setDither(true);
		canvas.drawARGB(0, 0, 0, 0);
		canvas.drawCircle(min/2 + diff/2, min / 2,
				min / 2, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(sbmp, src, dst, paint);

		return output;
	}
}
