package com.whambush.android.client.parser;

import com.whambush.android.client.domain.follow.FollowList;

public class FollowListParser extends AbstractParser {
	
	public FollowList create(String json) {		
		return parse(json, FollowList.class);
	}
}
