package com.whambush.android.client.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.HttpURLConnection;

import org.apache.http.client.methods.HttpPost;
import org.greenrobot.eventbus.EventBus;

import roboguice.RoboGuice;
import roboguice.util.Ln;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Build;

import com.vzaar.ProgressListener;
import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.user.LoginInfo;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.domain.video.VideoType;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.service.events.CannotCreateVideo;
import com.whambush.android.client.service.events.UploadError;
import com.whambush.android.client.service.events.UploadStatus;
import com.whambush.android.client.service.events.VideoCreated;
import com.whambush.android.client.util.StringHelper;

public class VideoCreationService extends AbstractHTTPResultCallback {

	private static final String TITLE_POSTFIX = " - WHAMBUSH";
	private static final String CREATE_URL = "videos/";
	private static final String VIDEOID = "external_id";
	private static final String NAME = "name";
//	private static final String ADDED_BY = "added_by";
	private static final String DESCRIPTION = "description";
	private static final String TAGS = "tags";

	private static final String WHAMBUSH_WWW_WHAMBUSH_COM = " \n\nwww.WHAMBUSH.com";
	private static final String MISSION_ID = "mission_id";
	private static final String VIDEO_TYPE = "video_type";

	private BackgroundTask task;
	private Mission mission;

	public void uploadVideo(AbstractActivity context, String filename, VideoEntry entry, VideoType videoType, Mission mission, String username) throws FileNotFoundException {

		RoboGuice.getInjector(context.getApplicationContext()).injectMembers(this);

		this.setMission(mission);

		task = new BackgroundTask(context, filename, entry, videoType, username);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO)
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		else
			task.execute();
	}

	public Status getStatus() {
		if (task != null)
			return task.getStatus();
		else 
			return Status.FINISHED;
	}

	public void cancelUpload() {
		task.doCancel();
	}

	private String getTitle(String title, String username) {
		return String.format("(%s) %s %s", StringHelper.normalize(username), StringHelper.normalize(title), TITLE_POSTFIX);
	}

	private void sendToWhambush(final AbstractActivity context, final String id, final VideoEntry entry, final VideoType videoType) {

		context.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				PostRequest request = new PostRequest(context, CREATE_URL, false);

				//	String user = Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getUrl();

				request.addParameter(VIDEOID, id);
				request.addParameter(NAME, entry.getName());
				//request.addParameter(ADDED_BY, user);
				request.addParameter(MISSION_ID, entry.getMissionId());
				request.addParameter(DESCRIPTION, entry.getDescription());
				request.addParameter(TAGS, entry.getTags());
				request.addParameter(VIDEO_TYPE, "" + videoType.getValue());

				send(context, request);
			}
		});
	}

	private String getDescription(String description) {
		return WHAMBUSH_WWW_WHAMBUSH_COM; //description + 
	}

	@Override
	public void result(int httpResultCode, String result) {

		if (httpResultCode == HttpURLConnection.HTTP_CREATED) {
			EventBus.getDefault().post(new VideoCreated());
		} else if (httpResultCode == HttpURLConnection.HTTP_BAD_REQUEST) {
			EventBus.getDefault().post(new CannotCreateVideo());
		}
	}

	public Mission getMission() {
		return mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}

	private class BackgroundTask extends AsyncTask<Void, Void, Void> implements ProgressListener {

		private String filename;
		private String username;
		private VideoEntry entry;
		private long fileSize;
		private File f;
		private Vzaar vzaar;
		private int lastValue = -1;

		private AbstractActivity context;
		private VideoType videoType;

		BackgroundTask(AbstractActivity context, String filename, VideoEntry entry, VideoType videoType, String username) {
			this.context = context;
			this.filename = filename;
			this.entry = entry;
			this.videoType = videoType;
			this.username = username;

			LoginInfo info = Whambush.get(ConfigImpl.class).getLoginInfo();

			vzaar = new Vzaar(info.getSettings().getVzaarKey(), info.getSettings().getVzaarSecret());
			f = new File(filename);
			fileSize = f.length();
		}

		void doCancel() {
			new Thread(new Runnable() {

				@Override
				public void run() {
					HttpPost request = vzaar.getRequest();
					if (request != null) 
						request.abort();
					BackgroundTask.this.cancel(true);
				}
			}).start();
		}

		@Override
		protected Void doInBackground(Void ... args) {
			lastValue = -1;

			try {
				String guid = vzaar.uploadVideo(new FileInputStream(f), filename, fileSize, "whambush.com", this);
				String id = vzaar.processVideo(guid, getTitle(entry.getName(), username), getDescription(entry.getDescription()), "", true);
				sendToWhambush(context, id, entry, videoType);

			} catch (Exception ex) {
				Ln.e(ex);
				EventBus.getDefault().post(new UploadError());
			}



			return null;
		}

		@Override
		public void update(long sentBytes) {
			int newValue = calculatePresentage(sentBytes);
			if (newValue != lastValue) {
				lastValue = newValue;
				sendValue();
			}
		}

		void sendValue() {
			EventBus.getDefault().post(new UploadStatus(lastValue));
		}

		private int calculatePresentage(long sent) {
			return Math.round((((float)sent) / ((float)fileSize)) * 100.0f);
		}

	}

}
