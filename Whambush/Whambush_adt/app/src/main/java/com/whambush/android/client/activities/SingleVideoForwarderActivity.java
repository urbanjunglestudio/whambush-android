package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.whambush.android.client.Whambush;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.VideoService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class SingleVideoForwarderActivity extends ForwarderActivity {

	public static final String TO_THE_VIDEO_ID = "toVideoId";

	@Override
	protected void startLoading() {
		new VideoService(this).load((String)getParameter(TO_THE_VIDEO_ID));
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(VideoEntry video) {
		Map<String, Serializable> params = new HashMap<>();
		params.put(VideoEntry.KEY, video.getId());
		Whambush.get(VideoEntryDao.class).add(video);
		initBackToMainInActivity(params);

		startActivity(SingleVideoActivity.class, params);
	}


	@Override
	protected void getParameters(Map<String, Serializable> params) {
		params.put(TO_THE_VIDEO_ID, (String)getParameter(TO_THE_VIDEO_ID));
	}
}
