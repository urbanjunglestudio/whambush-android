package com.whambush.android.client.service;

import java.net.HttpURLConnection;
import java.util.HashMap;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.parser.UserParser;
import com.whambush.android.client.service.events.WBError;

import org.greenrobot.eventbus.EventBus;

public class UserService extends AbstractHTTPResultCallback {
	
	private static final String USER_URL = "users/";
	private AbstractActivity context;
	
	private static final HashMap<String, User> users = new HashMap<>();

	public UserService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}
	
	public void createLoadUserRequest(String userid) {
		send(context, new GetRequest(context, USER_URL+userid));
	}

	public void createLoadUserWithFullURL(String url, boolean forceLoad) {

		if (!forceLoad && users.containsKey(url)) {
			User user = users.get(url);
			EventBus.getDefault().post(user);
			return;
		}

		send(context, new GetRequest(context, url, true));
	}

	public void findUserSlug(String slug) {
		GetRequest request = new GetRequest(context, USER_URL, true);
		request.addParameter("fields", "username");
		request.addParameter("query", slug);
		send(context, request);
	}
	
	
	@Override
	public void result(int httpResultCode, String result) {
		
		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			User parseUser = parseUser(result);
			users.put(parseUser.getUrl(), parseUser);
			EventBus.getDefault().post(parseUser);
		} else
			EventBus.getDefault().post(new WBError(httpResultCode));
	}

	private User parseUser(String result) {
		return new UserParser().create(result);
	}

}
