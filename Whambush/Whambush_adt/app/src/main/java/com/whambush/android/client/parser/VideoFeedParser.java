package com.whambush.android.client.parser;

import com.whambush.android.client.domain.feed.Feeds;


public class VideoFeedParser extends AbstractParser {

	public Feeds create(String result) {
		return parse(result, Feeds.class);
	}
}
