package com.whambush.android.client.domain.tv;

import com.whambush.android.client.domain.WhambushList;


public class Channels extends WhambushList<Channel> {

	private static final long serialVersionUID = 3488199346523826844L;
	
	public static final String KEY = "Channels";
	
}
