package com.whambush.android.client.camera;

public interface VideoCombinerListener {

	public void processingDone(int id, String path);
	
}
