package com.whambush.android.client.adapter;

import java.util.ArrayList;
import java.util.List;

import roboguice.util.Ln;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.util.Fonts;
import com.whambush.android.client.view.ProfilePicture;

public abstract class WhambushAdapter<T> extends BaseAdapter {

	private Fonts fonts;
	
	private List<T> items = new ArrayList<T>();
	
	private final LayoutInflater inflater;
	
	private Context context;
	
	public WhambushAdapter(Context context) {
		super();
		
		this.context = context;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public T getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return items.get(position).toString().hashCode();
	}

	public List<T> getAll() {
		return items;
	}
	
	public void add(T item) {
		this.items.add(item);
	}
	
	public void addAll(List<T> items) {
		this.items.addAll(items);
		notifyDataSetChanged();
	}

	public void clear() {
		this.items.clear();
		notifyDataSetChanged();
	}
	
	protected void setProfilePicture(final ProfilePicture view, final User user) {

		view.setDefaultPicture();
		ImageLoader.getInstance().cancelDisplayTask(view);

		if (isValidUser(user)) {
			Ln.d("valid user " + user);//ResourceHelper.parseImageUrl(
			ImageLoader.getInstance().displayImage(user.getPicture(), view, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View v, Bitmap loadedImage) {
					Ln.d("loaded image for user " + user);
					view.setImageBitmap(loadedImage);
				}
			});
		} else {
			Ln.d("not valid user " + user);
		}
	}

	protected void setImageView(final ImageView imageView, String pictureUrl, int defaultImage) {
		
		ImageLoader.getInstance().cancelDisplayTask(imageView);
		imageView.setImageResource(defaultImage);
		
		if (pictureUrl == null || pictureUrl.isEmpty())
			return;

		ImageLoader.getInstance().displayImage(pictureUrl, imageView, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				imageView.setImageBitmap(loadedImage);
			}
		});
	}

	
	protected void setImageView(final ImageView imageView, String pictureUrl) {

		imageView.setVisibility(View.INVISIBLE);

		ImageLoader.getInstance().cancelDisplayTask(imageView);

		ImageLoader.getInstance().displayImage(pictureUrl, imageView, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				imageView.setImageBitmap(loadedImage);
				imageView.setVisibility(View.VISIBLE);
			}
		});
	}
	
	private boolean isValidUser(User user) {
		return user != null && user.getPicture() != null && !user.getPicture().isEmpty();
	}
	
	protected Context getContext() {
		return context;
	}
	
	@SuppressLint("InflateParams")
	protected View inflate(int id, ViewGroup parent, boolean b) {
		return inflater.inflate(id, parent, b);
	}

	public Fonts getFonts() {
		if (fonts == null)
			fonts = new Fonts();
		
		return fonts;
	}
}
