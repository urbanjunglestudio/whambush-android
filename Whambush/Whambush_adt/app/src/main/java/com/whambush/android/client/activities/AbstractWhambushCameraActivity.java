package com.whambush.android.client.activities;

import java.io.IOException;

import android.os.Bundle;
import android.view.View;

import com.commonsware.cwac.camera.CameraHostProvider;
import com.commonsware.cwac.camera.PictureTransaction;
import com.whambush.android.client.AbstractWhambushActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.camera.AbstractCameraFragment;
import com.whambush.android.client.camera.CameraListener;
import com.whambush.android.client.camera.WBCameraFragment;
import com.whambush.android.client.camera.WBCameraHost;

public abstract class AbstractWhambushCameraActivity extends AbstractWhambushActivity implements CameraHostProvider, CameraListener {
	
	private static final int BACK_CAMERA = 1;
	private static final int FRONT_CAMERA = 2;
	
	private AbstractCameraFragment std = null;
	private AbstractCameraFragment ffc = null;
	private AbstractCameraFragment current = null;
	
	protected WBCameraHost wbCameraHost;
		
	private static final String TAG_CAMERA_FRAGMENT = "cameraFragment";

	protected void onCreate(Bundle savedInstanceState, int id) {
		super.onCreate(savedInstanceState, id, true);

		
		current = getCamera(BACK_CAMERA);
		getFragmentManager().beginTransaction().add(R.id.container, current, TAG_CAMERA_FRAGMENT).commit();
	}

	@Override
	public WBCameraHost getCameraHost() {
		if (wbCameraHost == null)
			wbCameraHost = new WBCameraHost(this, this);
		
		return wbCameraHost;
	}

	@Override
	protected void onResume() {
		super.onResume();
		current.autoFocus();
	}
	
	private AbstractCameraFragment getCamera(int camera) {
		if (camera == BACK_CAMERA) {
			if (std == null)
				std = getNewCameraFragment(false);

			getCameraHost().setFrontCamera(false);
			return std;
		} else if (camera == FRONT_CAMERA) {
			if (ffc == null)
				ffc = getNewCameraFragment(true);

			getCameraHost().setFrontCamera(true);
			return ffc;
		} else
			throw new RuntimeException("Invalid Camera");
	}

	protected AbstractCameraFragment getCurrentCameraView() {
		return current;
	}
	
	protected AbstractCameraFragment getNewCameraFragment(boolean b) {
		 return new WBCameraFragment(b);
	}
	
	public void switchCamera(View view) {
		current = getCamera(current == std?FRONT_CAMERA:BACK_CAMERA);

		getFragmentManager().beginTransaction().replace(R.id.container, current).commit();
	}

	protected void takePicture() {
		PictureTransaction xact = new PictureTransaction(getCameraHost());
		xact.needBitmap(true);
		showProgressBar();
		current.takePicture(xact);
	}
	
	@Override
	public void pictureTaken(final String path) {
		
	}
	
	@Override
	public void pictureTakeFailed() {
		getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(""));
	}

	@Override
	public void cameraInitializationFailure() {
		getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getString(R.string.CAMERA_ERROR_NO_CAMERA)));
		
	}
	
	protected boolean isRecording() {
		return current.isRecording();
	}

	protected void stopRecording() {
		try {
			current.stopRecording();
		} catch (IOException e) {
			recordingFailed();	
		}
	}
	
	protected void startRecording() {
		try {
			current.record();
		} catch (Exception e) {
			recordingFailed();
		}		
	}
	
	protected void updateView(int duration) {
		if (duration > 0)
			current.getSwitchIcon().setVisibility(View.GONE);
	}
	
}
