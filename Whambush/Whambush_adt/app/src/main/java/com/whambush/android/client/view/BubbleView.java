package com.whambush.android.client.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import com.whambush.android.client.R;

public class BubbleView extends View {

	private Bubble bubble;

	
	public BubbleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		createBubble();
	}

	public BubbleView(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
		createBubble();
	}

	public BubbleView(Context context) {
		super(context);
		createBubble();
	}
	
	private void createBubble() {
		bubble = new Bubble(getResources().getColor(R.color.whambushBottomBarWithTransparency));		
	}

	
	@Override
	protected void onDraw(Canvas canvas) {
	
		bubble.drawBubble(getWidth(), getHeight(), canvas);
		super.onDraw(canvas);
	}

}
