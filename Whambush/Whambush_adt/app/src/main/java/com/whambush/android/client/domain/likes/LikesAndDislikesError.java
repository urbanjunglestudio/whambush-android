package com.whambush.android.client.domain.likes;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class LikesAndDislikesError implements Serializable {

	private static final long serialVersionUID = 5976931276409225111L;

	@SerializedName("success")
	private boolean success;
	
	@SerializedName("detail")
	private int detail;

	public int getDetail() {
		return detail;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setDetail(int detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		return "LikesAndDislikesError [success=" + success + ", detail="
				+ detail + "]";
	}	
}
