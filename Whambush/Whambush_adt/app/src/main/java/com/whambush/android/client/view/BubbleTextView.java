package com.whambush.android.client.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

import com.whambush.android.client.R;

public class BubbleTextView extends TextView {

	private Bubble bubble;

	public BubbleTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		createBubble();
	}

	public BubbleTextView(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
		createBubble();
	}

	public BubbleTextView(Context context) {
		super(context);
		createBubble();
	}	
	
	private void createBubble() {
		bubble = new Bubble(getResources().getColor(R.color.bubble));		
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		super.setText(text, type);
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {		

		bubble.drawBubble(getWidth(), getHeight(), canvas);
		super.onDraw(canvas);
	}

	public boolean isLeftBubble() {
		return bubble.isLeftBubble();
	}

	public void setLeftBubble(boolean leftBubble) {
		bubble.setLeftBubble(leftBubble);
	}

}
