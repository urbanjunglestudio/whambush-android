package com.whambush.android.client.util;

import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;

import com.whambush.android.client.R;

public class ShaderHelper {

	private Context context;

	public ShaderHelper(Context context) {
		this.context = context;
	}

	public Shader getHeaderShader() {

		Shader textShader=new LinearGradient(0, 0, 0, 48,
				new int[]{context.getResources().getColor(R.color.top_color), context.getResources().getColor(R.color.bottom_color)},
				null, TileMode.CLAMP);
		return textShader;
	}
}
