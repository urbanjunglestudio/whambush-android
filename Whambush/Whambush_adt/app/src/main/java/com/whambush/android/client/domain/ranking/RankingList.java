package com.whambush.android.client.domain.ranking;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.WhambushList;

public class RankingList extends WhambushList<RankedUser> {

	private static final long serialVersionUID = -573509126423503689L;

	private String nextPage;
	
	private String previousPage;
	
	@SerializedName("time")
	private String time;
	
	@SerializedName("user")
	private RankedUser user;
	
	public String getNextPage() {
		return nextPage;
	}
	
	public void setNextPage(String next) {
		this.nextPage = next;
	}
	
	public String getPreviousPage() {
		return previousPage;
	}
	
	public void setPreviousPage(String previous) {
		this.previousPage = previous;

	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public RankedUser getUser() {
		return user;
	}

	public void setUser(RankedUser user) {
		this.user = user;
	}
}
