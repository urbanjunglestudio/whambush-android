package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.service.UserService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class UserFeedForwarderActivity extends ForwarderActivity {

	@Override
	protected void startLoading() {
		new UserService(this).createLoadUserRequest((String)getParameter(User.KEY));
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(User user) {
        Map<String, Serializable> params = new HashMap<String, Serializable>();
        if (isSelf(user)) {
            startActivity(UserActivity.class, params);
        } else {
            params.put(User.KEY, user);
            startActivity(OtherUserActivity.class, params);
        }
    }

	@Override
	protected void getParameters(Map<String, Serializable> params) {
		params.put(User.KEY, (String)getParameter(User.KEY));
	}

}
