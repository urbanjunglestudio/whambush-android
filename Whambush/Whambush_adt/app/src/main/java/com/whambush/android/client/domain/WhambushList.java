package com.whambush.android.client.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class WhambushList<T> implements Serializable {

	private static final long serialVersionUID = -573509126423503689L;

	@SerializedName("count")
	private int count;
	
	@SerializedName("next")
	private String next;
	
	@SerializedName("previous")
	private String previous;
	
	@SerializedName("results")
	private List<T> results = new ArrayList<T>();
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	public String getNext() {
		return next;
	}
	
	public void setNext(String next) {
		this.next = next;
	}
	
	public String getPrevious() {
		return previous;
	}
	
	public void setPrevious(String previous) {
		this.previous = previous;

	}
	
	public List<T> getResults() {
		return results;
	}
	
	public void setResults(List<T> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		return "WhambushList [count=" + count + ", next=" + next
				+ ", previous=" + previous + ", results=" + results + "]";
	}
}
