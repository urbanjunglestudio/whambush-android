package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.parser.MissionParser;
import com.whambush.android.client.service.events.WBError;

import org.greenrobot.eventbus.EventBus;

public class MissionsSlugService extends AbstractHTTPResultCallback {


	private static final String MISSION_SLUG_URL = "missions/slug/%s/";

	private AbstractActivity context;

	public MissionsSlugService(AbstractActivity context) {
		super();
		this.context = context;
	}

	public void findMissionSlug(String slug) {
		send(context, new GetRequest(context, String.format(MISSION_SLUG_URL, slug)));
	}

	@Override
	public void result(int httpResultCode, String result) {
		super.result(httpResultCode, result);

		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			EventBus.getDefault().post(parseMission(result));
		} else
			EventBus.getDefault().post(new WBError(httpResultCode));
	}

	private Mission parseMission(String result) {
		return new MissionParser().create(result);
	}

}
