package com.whambush.android.client.view.list;

import roboguice.util.Ln;
import android.content.Context;
import android.util.AttributeSet;

import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.activities.AbstractFeedActivity;
import com.whambush.android.client.adapter.VideoListAdapter;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.feed.Feed;
import com.whambush.android.client.domain.feed.Feeds;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.domain.video.Videos;
import com.whambush.android.client.service.FeedService;
import com.whambush.android.client.service.VideoFeedService;
import com.whambush.android.client.view.listener.PaginateListListener;

public class VideoFeedListView extends FeedListView<VideoEntry> {	

	private VideoListAdapter adapter;
	private PaginateListListener listListener;

	public VideoFeedListView(Context context) {
		super(context);
	}

	public VideoFeedListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public VideoFeedListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void setActivity(WhambushActivity activity) {
		super.setActivity(activity);
		addPaginateLoader(new VideoFeedService(activity));
		
		boolean tv = false;
		if (activity instanceof AbstractFeedActivity)
			 tv = ((AbstractFeedActivity)activity).isTv();
		
		setAdapter(adapter = new VideoListAdapter(activity, tv));
	}

	@Override
	public void paginateItems(final WhambushList<VideoEntry> items) {
		VideoFeedListView.super.paginateItems(items);
		Ln.d("Got items %d", items.getResults().size());
		Whambush.get(VideoEntryDao.class).addVideoEntries((Videos)items);
		adapter.addVideos(items.getResults());
		if (listListener != null)
			listListener.loaded();

	}

	@Override
	protected void setFeeds(final Feeds feeds, boolean updateFeedsView) {
		this.feeds = feeds;
		Ln.d("Got feeds %s", feeds.toString());

		FeedService feedService = new FeedService(activity);

		ConfigImpl config = Whambush.get(ConfigImpl.class);
		if (feedService.findFeed(feeds, config.getSelectedMainFeed()) != null) {
			Ln.d("Find Feed");
			Feed feed = feedService.findFeed(feeds, config.getSelectedMainFeed());
			currentFeed = feed;
			if (updateFeedsView)
				setFeeds(feeds, feed);

			setBaseRequest(service.parse(feed.getUrl(), false));
			loadBase();
		} else if (config.getSelectedSearchMainFeed() != null) {
			Ln.d("Selected Search Main Feed");
			Feed feed = feedService.findDefaultFeed(feeds);
			setFeeds(feeds, feed);
			setCurrentToSearch(config.getSelectedSearchMainFeed());
			setSearchText(config.getSelectedSearchMainFeed());
			search(config.getSelectedSearchMainFeed());
		} else {
			Ln.d("Else Feed");
			selectedFeed = feedService.findDefaultFeed(feeds);
			currentFeed = selectedFeed;
			if (updateFeedsView)
				setFeeds(feeds, selectedFeed);
			Ln.d("default feed URL : %s", selectedFeed.getUrl());
			setBaseRequest(service.parse(selectedFeed.getUrl(), false));
			loadBase();
		}

		activity.viewChanged();
	}

	@Override
	public void endOfListReached() {
		Ln.d("End of list reached");
		if (getFeeds() != null)
			super.endOfListReached();
	}

	@Override
	public void loadFeeds() {
		Feeds feeds = getPreloadedFeeds();
		if (feeds == null)
			new FeedService(activity).createLoadVideoRequest();
		else
			onMessageEvent(feeds);
	}

	private Feeds getPreloadedFeeds() {
		return Whambush.get(ConfigImpl.class).getFeeds();
	}

	@Override
	public void selected(Feed feed) {
		super.selected(feed);
		adapter.clear();
		adapter.notifyDataSetChanged();
	}

	@Override
	public void search(String text) {
		super.search(text);
		adapter.clear();
		adapter.notifyDataSetChanged();
	}

	@Override
	public void reload() {
		adapter.clear();
		adapter.notifyDataSetChanged();
		super.reload();
	}

	
	public VideoListAdapter getVideoListAdapter() {
		return adapter;
	}
	
	public void setListener(PaginateListListener listListener) {
		this.listListener = listListener;
	}
}
