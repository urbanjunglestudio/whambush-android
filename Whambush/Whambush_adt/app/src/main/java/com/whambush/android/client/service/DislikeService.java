package com.whambush.android.client.service;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.video.VideoEntry;

import org.greenrobot.eventbus.EventBus;

public class DislikeService extends LikeAndDislikeAbstractService {

	private static final String DISLIKE_URL = "dislike/video/";
	private static final String DISLIKE_MISSION_URL = "dislike/mission/";

	public DislikeService(AbstractActivity context) {
		super(context, DISLIKE_URL, DISLIKE_MISSION_URL);
	}

	public void dislike(VideoEntry video) {
		doCall(video);
	}
	
	public void dislike(Mission mission) {
		doCall(mission);
	}

	@Override
	protected void result(LikesAndDislikes result) {
		EventBus.getDefault().post(result);
	}
}
