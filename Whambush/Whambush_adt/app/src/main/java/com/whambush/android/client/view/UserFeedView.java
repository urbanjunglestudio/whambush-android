package com.whambush.android.client.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.whambush.android.client.R;
import com.whambush.android.client.view.list.FeedListView;

public class UserFeedView extends FeedListView {

	private RelativeLayout feedView;

	public UserFeedView(Context context) {
		super(context);

	}

	public UserFeedView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public UserFeedView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void initHeader() {
		feedView = (RelativeLayout) getInflater().inflate(R.layout.view_user_details_feed_header, this, false);
		addHeaderView(feedView);
	}
}
