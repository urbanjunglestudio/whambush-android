package com.whambush.android.client.view.listener;

import com.whambush.android.client.domain.country.Country;

public interface CountrySelectionListener {

	void selected(Country item);

}
