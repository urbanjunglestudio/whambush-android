package com.whambush.android.client.parser;

import com.whambush.android.client.domain.video.VideoEntry;

public class VideoFeedEntryParser extends AbstractParser {

	public VideoEntry create(String result) {
		return parse(result, VideoEntry.class);
	}
}
