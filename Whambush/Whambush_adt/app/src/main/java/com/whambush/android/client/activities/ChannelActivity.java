package com.whambush.android.client.activities;

import android.os.Bundle;

import com.whambush.android.client.Selector;
import com.whambush.android.client.domain.tv.Channel;

public class ChannelActivity extends RootChannelActivity {

	private Channel channel;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void loadChannels() {
		channel = (Channel)getParameter(Channel.KEY);
		setChannels(channel.getSubChannels());
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(Channel.KEY, channel);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState != null && savedInstanceState.containsKey(Channel.KEY))
			channel = (Channel)savedInstanceState.getSerializable(Channel.KEY);
	}
	
	@Override
	protected void onResume() {
		setCurrentSelector(Selector.TV);
		super.onResume();
	}
}
