package com.whambush.android.client.view.list;

import android.content.Context;
import android.util.AttributeSet;

import com.whambush.android.client.adapter.FollowAdapter;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.http.requests.Request;

public class FollowListView extends PaginatedListView<User> {	

	private FollowAdapter adapter;
	private User user;

	public FollowListView(Context context) {
		super(context);
	}

	public FollowListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FollowListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void setAdapter(FollowAdapter adapter) {
		super.setAdapter(adapter);
		this.adapter = adapter;
	}

	@Override
	public void paginateItems(final WhambushList<User> items) {
		FollowListView.super.paginateItems(items);
		adapter.addAll(items.getResults());
		requestLayout();

	}

	@Override
	public void endOfListReached() {
		super.endOfListReached();
	}

	@Override
	public void reload() {
		adapter.clear();
		super.reload();
		requestLayout();
	}

	@Override
	protected Request getNext(WhambushList<User> items) {
		return service.parse(items.getNext(), true, user.getId());
	}
	
	public void setUser(User user) {
		this.user = user;
	}
}
