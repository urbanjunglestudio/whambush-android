package com.whambush.android.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import roboguice.activity.RoboFragmentActivity;
import roboguice.util.Ln;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.whambush.android.client.config.AppConfig;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.http.RequestProxy;
import com.whambush.android.client.service.AbstractHTTPResultCallback;
import com.whambush.android.client.util.TrackerName;

import org.greenrobot.eventbus.EventBus;

public abstract class AbstractActivity extends RoboFragmentActivity {

	protected static final String TO_ACTIVITY = "TO_ACTIVITY";

	private List<AbstractHTTPResultCallback> callbacks = new ArrayList<>();

	private static final HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


	protected void onCreate(Bundle savedInstanceState, int layout, boolean fullScreen) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		if (fullScreen) {
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
		setContentView(layout);
	}

	protected void onCreate(Bundle savedInstanceState, boolean fullScreen) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		if (fullScreen) {
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
	}

	protected boolean isValidLogin() {
		return (Whambush.get(ConfigImpl.class).getAuthenticationToken() != null &&
				!Whambush.get(ConfigImpl.class).getAuthenticationToken().isEmpty()) &&
				Whambush.get(ConfigImpl.class).getLoginInfo() != null;
	}

	synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			Tracker t = analytics.getTracker(Whambush.get(ConfigImpl.class).getStatisticID());
			mTrackers.put(trackerId, t);

		}
		return mTrackers.get(trackerId);
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
		viewChanged();
	}

	protected boolean isStatisticActivity() {
		return true;
	}

	public void viewChanged() {
		if (allowStatistics() && isStatisticActivity())
			getTracker(TrackerName.APP_TRACKER).send(MapBuilder
					.createAppView()
					.set(Fields.SCREEN_NAME, getDetailedActivityNameForAnalysis())
					.build());
	}

	protected abstract String getDetailedActivityNameForAnalysis();

	@Override
	public void onStop() {
		cancelCallbacks();

		super.onStop();
		EventBus.getDefault().unregister(this);

		if (allowStatistics())
			getTracker(TrackerName.APP_TRACKER).send(MapBuilder
					.createAppView()
					.set(Fields.SCREEN_NAME, null)
					.build());
	}

	private void cancelCallbacks() {
		for (AbstractHTTPResultCallback callback : callbacks) {
			callback.cancel();
		}
	}

	public void registerCallback(AbstractHTTPResultCallback callback) {
		callbacks.add(callback);
	}


	private boolean allowStatistics() {
		return Whambush.get(ConfigImpl.class) != null && Whambush.get(ConfigImpl.class).getAllowStatistics();
	}

	public void startActivity(final Class<? extends Activity> activity) {
		startActivity(activity, null, 0);
	}

	public void startActivity(final Class<? extends Activity> activity, int flags) {
		startActivity(activity, null, flags);
	}

	protected void startActivity(final Class<? extends Activity> activity, final boolean finish) {
		startActivity(activity, (Bundle)null, finish);
	}
	public void startActivity(Class<? extends Activity> activity, Map<String, Serializable> params) {
		startActivity(activity, params, 0);
	}

	public void startActivity(Class<? extends Activity> activity, Map<String, Serializable> params, int flags) {

		cancelRequests();
		Intent intent = new Intent(this, activity);
		if (params != null)
			for (String key : params.keySet())
				intent.putExtra(key, params.get(key));

		if (flags > 0)
			intent.addFlags(flags);

		startActivity(intent);
	}

	protected void redirectToActivity(Class<? extends Activity> viaActivity, Class<? extends Activity> toActivity, Map<String, Serializable> params) {
		params.put(TO_ACTIVITY, toActivity);

		cancelRequests();
		Intent intent = new Intent(this, viaActivity);

		for (String key : params.keySet())
			intent.putExtra(key, params.get(key));

		startActivity(intent);
	}

	private void cancelRequests() {
		if (Whambush.get(RequestProxy.class) != null)
			Whambush.get(RequestProxy.class).cancelRequests(this);
	}

	public void startActivity(Class<? extends Activity> activity, Map<String, Serializable> params, boolean finish) {
		cancelRequests();

		if (finish)
			finish();
		Intent intent = new Intent(this, activity);

		if (params != null)
			for (String key : params.keySet())
				intent.putExtra(key, params.get(key));
		startActivity(intent);
	}

	public void startActivity(Class<? extends Activity> activity, Bundle params, boolean finish) {
		cancelRequests();

		if (finish)
			finish();
		Intent intent = new Intent(this, activity);

		if (params != null)
			intent.putExtras(params);
		startActivity(intent);
	}

	protected void printIntentParams() {
		if (!BuildConfig.DEBUG)
			return;

		Ln.d("Printing Params for activity " + this.getClass().getName());
		for (String key : getIntent().getExtras().keySet()) {
			Ln.d(key + " = " + getIntent().getExtras().get(key));
		}

	}

	protected Object getParameter(String key) {
		if (getIntent() != null && getIntent().getExtras() != null)	
			return getIntent().getExtras().get(key);
		return null;
	}

	protected boolean getBooleanParameter(String key) {
		if (getIntent() != null && getIntent().getExtras() != null)	
			return getIntent().getExtras().getBoolean(key);

		return false;
	}

	protected ProgressBar getProgressBarView(int id) {
		return (ProgressBar) findViewById(id);
	}

	public ImageView getImageView(int id) {
		return (ImageView) findViewById(id);
	}


	protected ImageButton getImageButton(int buttonID) {
		return (ImageButton) findViewById(buttonID);
	}

	protected class EmptyButtonListener implements DialogInterface.OnClickListener {
		public void onClick(DialogInterface dialog, int which) {
			//Empty on purpose
		}
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	public boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				finish();
			}
			return false;
		}
		return true;
	}
}
