package com.whambush.android.client;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import roboguice.inject.InjectResource;
import roboguice.util.Ln;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.whambush.android.client.activities.SplashScreen;
import com.whambush.android.client.adapter.CountryAdapter;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.dialog.DialogBuilderHelper;
import com.whambush.android.client.domain.country.Country;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.http.RequestProxy;
import com.whambush.android.client.service.VideoCreationService;
import com.whambush.android.client.service.events.ConnectionError;
import com.whambush.android.client.service.events.WBError;
import com.whambush.android.client.util.Fonts;
import com.whambush.android.client.util.TextInputParameters;
import com.whambush.android.client.view.listener.CountrySelectionListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public abstract class AbstractWhambushActivity extends AbstractActivity {

	private static final String EXIT_TIME = "EXIT_TIME";
	private static final int EXIT_TIME_AFTER = 2000;

	private boolean progressbarVisible = false;
	private boolean textInputVisible = false;
	private boolean doubleBackToExitPressedOnce = false;

	@InjectResource(R.anim.fadein)
	protected Animation fadeIn;

	@InjectResource(R.anim.fadeout)
	protected Animation fadeOut;

	@InjectResource(R.anim.from_down_in)
	protected Animation inDown;

	@InjectResource(R.anim.to_down_out)
	protected Animation outDown;

	@InjectResource(R.anim.collapse)
	public Animation collapse;

	@InjectResource(R.anim.expand)
	public Animation expand;

	private Fonts fonts;

	private DialogBuilderHelper dialogHelper;

	private static final int COUNTRY_LOAD_INTERVAL = 1000;

	//	private boolean paused = false;

	@Override
	protected void onCreate(Bundle savedInstanceState, int layout,
			boolean fullScreen) {
		super.onCreate(savedInstanceState, layout, fullScreen);

		setPendingTransition();
		isExit();
	}

	protected void setPendingTransition() {
		overridePendingTransition(R.anim.none, R.anim.none);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		Ln.d("** New intent **");
		super.onNewIntent(intent);
		overridePendingTransition(R.anim.none, R.anim.none);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState,
			boolean fullScreen) {
		super.onCreate(savedInstanceState, fullScreen);
		setPendingTransition();
		isExit();
	}

	private void isExit() {
		if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(EXIT_TIME)) {
			long exit = getIntent().getExtras().getLong(EXIT_TIME);
			if ((new Date().getTime() - exit) < EXIT_TIME_AFTER) {
				Intent startMain = new Intent(Intent.ACTION_MAIN);
				startMain.addCategory(Intent.CATEGORY_HOME);
				startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(startMain);

				finish();
				android.os.Process.killProcess(android.os.Process.myPid());
				System.exit(0);
				throw new RuntimeException();

			}
		}
	}

	protected boolean isValidServiceAndLoggedIn() {
		if (isValidServices() && isValidLogin())
			return true;

		Ln.w("Not valid service");
		return false;
	}

	public void showProgressBar() {
		if (progressbarVisible)
			return;
		progressbarVisible = true;
		FrameLayout rootLayout = (FrameLayout)findViewById(android.R.id.content);
		View.inflate(this, R.layout.progressbar, rootLayout);
		findViewById(R.id.progressbar_main_layout).setVisibility(View.INVISIBLE);
		findViewById(R.id.progressbar_main_layout).startAnimation(fadeIn);
		findViewById(R.id.progressbar_main_layout).setVisibility(View.VISIBLE);

	}

	protected boolean isValidServices() {
		if (Whambush.get(ConfigImpl.class) == null ||  
				Whambush.get(RequestProxy.class) == null ||
				Whambush.get(VideoCreationService.class) == null ||
				Whambush.get(VideoEntryDao.class) == null) {

			Ln.e("Is Config Null " + Whambush.get(ConfigImpl.class) == null);
			Ln.e("Is RequestProxy Null " + Whambush.get(RequestProxy.class) == null);
			Ln.e("Is VideoCreationService Null " + Whambush.get(VideoCreationService.class) == null);
			Ln.e("Is VideoEntryDao Null " + Whambush.get(VideoEntryDao.class) == null);
			return false;
		}

		return true;
	}

	protected void showTextInput() {
		showTextInput(new TextInputParameters());
	}


	protected void showTextInput(TextInputParameters params) {
		if (textInputVisible)
			return;
		textInputVisible = true;
		FrameLayout rootLayout = (FrameLayout)findViewById(android.R.id.content);
		View.inflate(this, R.layout.view_textinput, rootLayout);

		TextView view = getTextView(R.id.newComment);
		TextView commentText = (TextView)findViewById(R.id.commentView);
		commentText.setTypeface(getFonts().getTitleFont());
		commentText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT);
		commentText.setText(params.getTitle());
		view.requestFocus();

		InputMethodManager imm = (InputMethodManager)getSystemService(
				Context.INPUT_METHOD_SERVICE);

		setTextViewParams(params, view);

		imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
		rootLayout.findViewById(R.id.commentMainView).startAnimation(inDown);
	}

	private void setTextViewParams(TextInputParameters params, TextView view) {
		if (params.getDefaultText() != null) {
			view.setText(params.getDefaultText());
		}
		if (params.getMaxLines() > 0) {
			view.setMaxLines(params.getMaxLines());
		}

		if (params.getMaxChars() > 0) {
			InputFilter[] filterArray = new InputFilter[1];
			filterArray[0] = new InputFilter.LengthFilter(params.getMaxChars());
			view.setFilters(filterArray);
		}

		view.setAllCaps(params.isAllCaps());
	}

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ConnectionError connectionError) {
		hideProgressBar();
		getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_NO_NETWORK)));
	}

	public void removeViewFromContent(View view) {
		FrameLayout rootLayout = (FrameLayout)findViewById(android.R.id.content);
		rootLayout.removeViewAt(rootLayout.getChildCount()-1);
	}

	protected void handleDoubleClickToExit() {
		if (doubleBackToExitPressedOnce) {
			Intent intent = new Intent(this, SplashScreen.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(EXIT_TIME, new Date().getTime());
			finish();
			startActivity(intent);
			setPendingTransition();
			return;
		}

		this.doubleBackToExitPressedOnce = true;
		Toast.makeText(this, R.string.GENERAL_CLICK_AGAIN_TO_EXIT, Toast.LENGTH_SHORT).show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				doubleBackToExitPressedOnce=false;                       
			}
		}, 2000);
	}

	public void enableAll() {

	}

	protected EditText getEditText(int id) {
		EditText editText = (EditText) findViewById(id);
		editText.setTypeface(getFonts().getDescriptionFont());
		return editText;
	}

	protected void setCheckBoxDefaultTypeface(CheckBox checkbox) {
		checkbox.setTypeface(getFonts().getDescriptionFont());
	}

	protected void setDefaultTypeface(TextView view) {
		view.setTypeface(getFonts().getDescriptionFont());
	}

	public TextView getTextView(int id) {
		TextView editText = (TextView) findViewById(id);
		editText.setTypeface(getFonts().getDescriptionFont());
		return editText;
	}

	protected void setTitleFont(TextView view) {
		view.setTypeface(getFonts().getTitleFont());
	}

	protected TextView getTitleTextView(int id) {
		TextView editText = (TextView) findViewById(id);
		editText.setTypeface(getFonts().getTitleFont());
		editText.setText(editText.getText().toString().toUpperCase(getResources().getConfiguration().locale));
		return editText;
	}


	protected Button getButton(int buttonID) {
		Button button = (Button) findViewById(buttonID);
		button.setTypeface(getFonts().getTitleFont());
		return button;
	}


	public Fonts getFonts() {		
		if (fonts == null)
			fonts = new Fonts();

		return fonts;
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(WBError httpResultCode) {
		getDialogHelper().displayDialog(new DialogBuilderHelper(this).createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_UNKNOWN)));
	}

	public void hideProgressBar() {
		hideProgressBar(null);
	}

	public void hideProgressBar(final AnimationListener listener) {
		if (progressbarVisible) {
			progressbarVisible = false;
			final FrameLayout rootLayout = (FrameLayout)findViewById(android.R.id.content);

				fadeOut.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						if (listener != null)
							listener.onAnimationStart(animation);
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						if (listener != null)
							listener.onAnimationRepeat(animation);
					}

					@Override
					public void onAnimationEnd(Animation animation) {
						rootLayout.removeViewAt(rootLayout.getChildCount()-1);
						if (listener != null)
							listener.onAnimationEnd(animation);
					}
				});


			View findViewById = findViewById(R.id.progressbar_main_layout);
			if (findViewById == null)
				return;

			findViewById.startAnimation(fadeOut);
			findViewById.setVisibility(View.INVISIBLE);
		}
	}


	protected void hideTextInput() {
		if (textInputVisible) {			
			hideKeyboard();
			textInputVisible = false;
		}
	}

	protected void hideKeyboard() {
		FrameLayout rootLayout = (FrameLayout)findViewById(android.R.id.content);
		InputMethodManager imm = (InputMethodManager)getSystemService(
				Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(rootLayout.getWindowToken(), 0);
		rootLayout.removeViewAt(rootLayout.getChildCount()-1);
	}

	public DialogBuilderHelper getDialogHelper() {
		if (dialogHelper == null)
			dialogHelper = new DialogBuilderHelper(this);

		return dialogHelper;
	}

	public boolean isSelf(User user) {
		if (Whambush.get(ConfigImpl.class).getLoginInfo() == null || Whambush.get(ConfigImpl.class).getLoginInfo().getUser() == null)
			return false;

		return user != null && user.getId().equals(Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getId());
	}

	public void showCountrySelection(final CountrySelectionListener listener) {
		showCountrySelection(listener,  (FrameLayout)findViewById(android.R.id.content));
	}

	public void hideCountry(final View view) {

		outDown.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				removeViewFromContent(view);
			}
		});

		view.startAnimation(outDown);
		view.setVisibility(View.INVISIBLE);
	}

	public void showCountrySelection(final CountrySelectionListener listener, final ViewGroup rootLayout) {
		if (Whambush.get(ConfigImpl.class).getCountries() == null) {
			new Timer().schedule(new TimerTask() {

				@Override
				public void run() {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							showCountrySelection(listener, rootLayout);
						}
					});

				}
			}, COUNTRY_LOAD_INTERVAL);
		}
		else
			doShowCountrySelection(listener, rootLayout);

	}

	private void doShowCountrySelection(final CountrySelectionListener listener, final ViewGroup rootLayout) {
		if (progressbarVisible) {
			hideProgressBar(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					doShowCountrySelectionNow(listener, rootLayout);
				}
			});
		} else
			doShowCountrySelectionNow(listener, rootLayout);
	}

	private void doShowCountrySelectionNow(final CountrySelectionListener listener,
			final ViewGroup rootLayout) {
		Ln.i("Starting to show countries");

		View.inflate(AbstractWhambushActivity.this, R.layout.view_country_chooser, rootLayout);

		final View view = rootLayout.findViewById(R.id.country_chooser);
		view.setVisibility(View.INVISIBLE);
		final ListView list = (ListView)rootLayout.findViewById(R.id.countryList);


		final CountryAdapter adapter = new CountryAdapter(AbstractWhambushActivity.this);
		list.setAdapter(adapter);

		view.startAnimation(inDown);
		view.setVisibility(View.VISIBLE);

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Country item = adapter.getItem(position);
				if (item.isSeparator())
					return;

				removeViewFromContent(view);
				listener.selected(item);
			}
		});
	}
}
