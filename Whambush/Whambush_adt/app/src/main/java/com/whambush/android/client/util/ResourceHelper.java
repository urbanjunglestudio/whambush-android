package com.whambush.android.client.util;

import com.whambush.android.client.Whambush;
import com.whambush.android.client.config.ConfigImpl;

public class ResourceHelper {
	
	private static final String PREFIX = "media/";

	public static String parseImageUrl(String picture) {
		 return Whambush.get(ConfigImpl.class).getBaseURL() + PREFIX + picture;
	}

	
}
