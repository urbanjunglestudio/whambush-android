package com.whambush.android.client.domain.tv;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.WhambushObject;

public class Channel extends WhambushObject implements Serializable {

	private static final long serialVersionUID = -5118102293366464416L;

	public static final String KEY = "Channel";

	@SerializedName("channel_type")
	private int channelType;
	
	@SerializedName("picture_url")
	private String pictureUrl;
	
	@SerializedName("country")
	private String country;
	
	@SerializedName("children")
	private List<Channel> subChannels;
	
	@SerializedName("videos_endpoint")
	private String endpoint;

	public int getChannelType() {
		return channelType;
	}

	public void setChannelType(int channelType) {
		this.channelType = channelType;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<Channel> getSubChannels() {
		return subChannels;
	}

	public void setSubChannels(List<Channel> subChannels) {
		this.subChannels = subChannels;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	
	
}
