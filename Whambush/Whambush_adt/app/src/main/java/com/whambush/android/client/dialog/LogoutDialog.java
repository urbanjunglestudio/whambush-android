package com.whambush.android.client.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.activities.SplashScreen;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.service.GCMService;
import com.whambush.android.client.service.events.ConnectionError;
import com.whambush.android.client.service.events.Registered;
import com.whambush.android.client.service.events.Unregistered;
import com.whambush.android.client.service.events.WBError;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

@SuppressLint("InflateParams")
public class LogoutDialog extends WhambushDialogFragment {

	private WhambushActivity activity;

	public LogoutDialog() {

	}

	public void setActivity(WhambushActivity activity) {
		this.activity = activity;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setPositiveButton(R.string.GENERAL_OK, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				activity.showProgressBar();
				new GCMService(activity).unregisterDevice();
			}
		})
		.setNegativeButton(R.string.GENERAL_CANCEL, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			}
		});
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_logout, null);
		builder.setView(view);

		
		AlertDialog dialog = builder.create();
		setAnimation(dialog);
		
		dialog.show();
		setButton(dialog.getButton(DialogInterface.BUTTON_POSITIVE));
		setButton(dialog.getButton(DialogInterface.BUTTON_NEGATIVE));
		
		((TextView)view.findViewById(R.id.logout_title)).setTypeface(getFonts().getTitleFont());
		
		return dialog;
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(WBError error) {
		doLogout();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(ConnectionError comErro) {
		doLogout();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(Registered reg) {
		doLogout();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(Unregistered unreg) {
		doLogout();
	}

	protected void doLogout() {
		Whambush.get(ConfigImpl.class).setPassword("");
		Whambush.get(ConfigImpl.class).setUsername("");
		Whambush.get(ConfigImpl.class).setAuthenticationToken("");
		activity.startActivity(SplashScreen.class);
	}
	
	
}
