package com.whambush.android.client.domain.ranking;

import com.google.gson.annotations.SerializedName;

public class RankResult {

	@SerializedName("result")
	private RankingList result;

	public RankingList getResult() {
		return result;
	}

	public void setResult(RankingList result) {
		this.result = result;
	}
}
