package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.service.events.Subscribed;
import com.whambush.android.client.service.events.WBError;

import org.greenrobot.eventbus.EventBus;

public class SubscribeService extends AbstractHTTPResultCallback {

	private static final String SUBSCRIBE = "subscribe/waitinglist/";
	private static final String EMAIL = "email";

	private AbstractActivity context;

	public SubscribeService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}
	
	public void createSubscribeRequest(String email) {
		PostRequest request = new PostRequest(context, SUBSCRIBE, false, false);
		
		request.addParameter(EMAIL, email);
		send(context, request);
	}
	
	@Override
	public void result(int httpResultCode, String result) {		
		if (httpResultCode == HttpURLConnection.HTTP_OK)
			EventBus.getDefault().post(new Subscribed());
		else
			EventBus.getDefault().post(new WBError(-1));
	}
}
