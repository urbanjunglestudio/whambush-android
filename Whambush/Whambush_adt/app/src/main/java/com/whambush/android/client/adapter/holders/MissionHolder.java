package com.whambush.android.client.adapter.holders;

import android.widget.ImageView;
import android.widget.TextView;

import com.whambush.android.client.view.ProfilePicture;

public class MissionHolder {

	public String mission;
	public TextView title;
	public TextView time;
	public TextView separator;
	public TextView username;
	public ImageView bkgImg;
	public ProfilePicture profilePicture;
	public TextView likeCount;
	public TextView videoCount;
	public ImageView likeImage;
	public ImageView accessImage;
}