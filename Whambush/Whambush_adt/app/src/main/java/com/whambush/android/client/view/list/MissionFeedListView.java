package com.whambush.android.client.view.list;

import roboguice.util.Ln;
import android.content.Context;
import android.util.AttributeSet;

import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.adapter.MissionListAdapter;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.feed.Feed;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.service.MissionsService;
import com.whambush.android.client.view.listener.PaginateListListener;

public class MissionFeedListView extends FeedListView<Mission> {	

	private static final String GLOBAL = "ZZ";
	private MissionListAdapter adapter;
	private PaginateListListener listListener;
	private MissionsService missionsService;
	private String country;

	public MissionFeedListView(Context context) {
		super(context);
	}

	public MissionFeedListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MissionFeedListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void setActivity(WhambushActivity activity) {
		super.setActivity(activity);		
		enableSearch(false);
		addPaginateLoader(missionsService = new MissionsService(activity));
		setAdapter(adapter = new MissionListAdapter(activity));
		adapter.setCountry(country);
	}

	public void setUserCountry(String country) {
		Ln.d("*** country " + country);
		Ln.d("*** adapter " + adapter);
		adapter.setUserCountry(country);
	}

	@Override
	public void paginateItems(final WhambushList<Mission> items) {
		MissionFeedListView.super.paginateItems(items);
		adapter.addAll(items.getResults());

		if (listListener != null)
			listListener.loaded();
	}

	@Override
	public void endOfListReached() {
		if (getFeeds() != null)
			super.endOfListReached();
	}

	@Override
	public void loadFeeds() {
		onMessageEvent(missionsService.getMissionFeeds());
	}

	@Override
	public void selected(Feed feed) {
		super.selected(feed);
		adapter.clear();
	}

	@Override
	protected Request getNext(WhambushList<Mission> items) {
		return service.parse(items.getNext(), true, getExtras());
	}


	protected String[] getExtras() {
		return new String[] {country + "," + GLOBAL};
	}

	@Override
	public void search(String text) {
		super.search(text);
		adapter.clear();
	}

	@Override
	public void reload() {
		adapter.clear();
		super.reload();
	}

	public void setListener(PaginateListListener listListener) {
		this.listListener = listListener;
	}

	public MissionListAdapter getMissionAdapter() {
		return adapter;
	}
	
	public void setCountry(String country) {
		this.country = country;
		if (adapter != null)
			adapter.setCountry(country);
	}

	public String getCountry() {
		return country;
	}

	public void clear() {
		adapter.clear();
	}

	public boolean isWrongCountry() {
		return adapter.wrongCountry();
	}

}
