package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.follow.FollowList;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.http.requests.DeleteRequest;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.parser.FollowListParser;
import com.whambush.android.client.service.events.ChangedFollow;
import com.whambush.android.client.service.events.ChangedUnFollow;
import com.whambush.android.client.service.events.WBError;

import org.greenrobot.eventbus.EventBus;

public class ChangeFollowService extends AbstractHTTPResultCallback {

	private static final String URL = "follows/?type=";
	private static final String FOLLOW_URL = "follows/";
	private static final String UNFOLLOW_URL = "follows/%s/";
	
	private static final String USER = "user";

	private static final String FOLLOWERS = "followers";
	private static final String FOLLOWING = "following";
	private static final String PAGE_SIZE_KEY = "page_size";
	private static final String NUMBER_OF_ENTRIES_IN_QUERY = "10000";

	private static final String FOLLOW_PARAM = "to_user";

	private AbstractActivity context;

	public ChangeFollowService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}
	
	public void getFollowers(String userId) {
		
		GetRequest request = get(FOLLOWERS);
		request.addParameter(USER, userId);
		send(context, request);
	}
	
	public void getFollowing(String userId) {
		
		GetRequest request = get(FOLLOWING);
		request.addParameter(USER, userId);
		send(context, request);
	}
	
	public void getFollowers() {
		send(context, get(FOLLOWERS));
	}

	public void follow(User user) {

		PostRequest request = new PostRequest(context, FOLLOW_URL);
		request.addParameter(FOLLOW_PARAM, user.getId());
		send(context, request);
	}

	public void unfollow(User user) {

		send(context, new DeleteRequest(context, String.format(UNFOLLOW_URL, user.getId())));
	}


	public void getFollowing() {
		send(context, get(FOLLOWING));
		
	}

	private GetRequest get(String type) {

		GetRequest request = new GetRequest(context, URL + type);
		
		request.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
		return request;
	}

	@Override
	public void result(int httpResultCode, String result) {

		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			EventBus.getDefault().post(parseList(result));
		} else if (httpResultCode == HttpURLConnection.HTTP_CREATED) {
			EventBus.getDefault().post(new ChangedFollow());
		} else if (httpResultCode == HttpURLConnection.HTTP_NO_CONTENT) {
			EventBus.getDefault().post(new ChangedUnFollow());

		} else
			EventBus.getDefault().post(new WBError(httpResultCode));

	}

	private FollowList parseList(String result) {
		return new FollowListParser().create(result);
	}
	

}
