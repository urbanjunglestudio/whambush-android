package com.whambush.android.client.service;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.service.events.Flagged;
import com.whambush.android.client.service.events.WBError;

public class FlagService extends AbstractHTTPResultCallback {

	private static final String URL = "flags/video/%s/";
	private static final String REASON = "reason_code";
	private AbstractActivity context;

	public FlagService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}

	
	public void flag(String videoId, int reason) {

		String url = String.format(URL, videoId);
		PostRequest request = new  PostRequest(context, url);
		
		try {
			JSONObject obj = new JSONObject();
			obj.put(REASON, reason);
			request.setJson(obj);
			send(context, request);
		} catch (JSONException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void result(int httpResultCode, String result) {
		
		if (httpResultCode == HttpURLConnection.HTTP_CREATED)
			EventBus.getDefault().post(new Flagged());
		else
			EventBus.getDefault().post(new WBError(httpResultCode));
	}

}
