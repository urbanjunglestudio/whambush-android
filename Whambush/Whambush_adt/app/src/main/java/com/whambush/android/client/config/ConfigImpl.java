package com.whambush.android.client.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import roboguice.RoboGuice;
import roboguice.util.Ln;
import android.content.SharedPreferences;
import android.os.Environment;

import com.google.inject.Inject;
import com.whambush.android.client.BuildConfig;
import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.domain.country.Countries;
import com.whambush.android.client.domain.feed.Feeds;
import com.whambush.android.client.domain.user.LoginInfo;
import com.whambush.android.client.util.FileUtil;

import org.greenrobot.eventbus.EventBus;

public class ConfigImpl {

	private static final String ALLOW_STATISTIC = "allow_statistics";
	private static final String USERNAME_KEY = "username_key";
	private static final String PASSWORD_KEY = "password_key";

	private static final String DEBUG_AUTH_TOKEN_KEY = "debug_auth_token_key";
	private static final String PROD_AUTH_TOKEN_KEY = "prod_auth_token_key";

	private static final String GCM_REQ_ID = "GCM_REQ_ID";
	private static final String APP_VERSION = "APP_VERSION";

	private static final String OLD_GUEST_ID = "GUEST_ID";
	private static final String DEBUG_GUEST_ID = "GUEST_ID";
	private static final String PROD_GUEST_ID = "GUEST_ID";

	private static final String SELECTED_MAIN_FEED = "selected_main_feed";
	private static final String SELECTED_SEARCH_MAIN_FEED = "selected_search_main_feed";
	private static final String GUEST_COUNTRY = "guest_country";

	@Inject private SharedPreferences preferences;

	private LoginInfo loginInfo;
	private Feeds feeds;

	private boolean triedToLogin = false;

	private final Properties externalProperties;
	private Countries countries = null;

	public ConfigImpl() {
		RoboGuice.getInjector(Whambush.getContext()).injectMembers(this);

		externalProperties = readProps();
	}

	private String get(int id) {
		return Whambush.getContext().getString(id);
	}

	public String getBaseURL() {
		return AppConfig.URL;
	}

	private String getAPIVersion() {
		return AppConfig.API_VERSION;
	}

	public String getURL() {
		return getBaseURL() + "v" + getAPIVersion() + "/";
	}

	public String getStatisticID() {
		return AppConfig.GOOGLE_ANALYST_ID;
	}

	public String getGCMSenderID() {
		return AppConfig.GCM_SENDER_ID;
	}

	private void update(String key, String value) {
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(key, value);
		editor.apply();
	}

	private void update(String key, int value) {
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(key, value);
		editor.apply();
	}

	public void setUsername(String username) {
		update(USERNAME_KEY, username);
	}

	public void setPassword(String password) {
		update(PASSWORD_KEY, password);
	}

	public String getUsername() {
		return preferences.getString(USERNAME_KEY, "");
	}

	public String getPassword() {
		return preferences.getString(PASSWORD_KEY, "");
	}

	public void setAuthenticationToken(String authToken) {
		if (BuildConfig.DEBUG)
			update(DEBUG_AUTH_TOKEN_KEY, authToken);
		else
			update(PROD_AUTH_TOKEN_KEY, authToken);
	}

	private void set(String prodAuthTokenKey, String debugAuthTokenKey,
			String oldAuthTokenKey, String authToken) {
		
		boolean externalStorageWritable = isExternalStorageWritable();		
		if (externalProperties != null && externalStorageWritable) {
			if (BuildConfig.DEBUG)
				externalProperties.setProperty(debugAuthTokenKey, authToken);
			else
				externalProperties.setProperty(prodAuthTokenKey, authToken);

			try {
				commitExternal();
				return;
			} catch (IOException e) {
				Ln.e(e);
			}
		}

		update(oldAuthTokenKey, authToken);
	}

	private void commitExternal() throws IOException {
		File file = getExternalProperties();
		externalProperties.store(new FileOutputStream(file), "props for WB");
	}

	private File getExternalProperties() {
		return new File(new FileUtil().getBasePath(), "conf.prop");
	}

	public String getAuthenticationToken() {
		if (BuildConfig.DEBUG)
			return preferences.getString(DEBUG_AUTH_TOKEN_KEY, "");
		else
			return preferences.getString(PROD_AUTH_TOKEN_KEY, "");
	}

	private String get(String prodKey, String debugKey, String oldKey) {
		if (externalProperties != null) {
			String key;
			
			if (BuildConfig.DEBUG)
				key = externalProperties.getProperty(debugKey);
			else
				key = externalProperties.getProperty(prodKey);
			
			if (key != null)
				return key;
		}
		return preferences.getString(oldKey, "");
	}

	private Properties readProps() {
		File file = getExternalProperties();
		Properties externalProperties = new Properties();
		try {
			externalProperties.load(new FileReader(file));
			externalProperties = removeInvalidKeys(externalProperties); // Remove this in future!!!!
		} catch (IOException e) {
			Ln.e("Error while reading the props");
		}

		return externalProperties;
	}

	private Properties removeInvalidKeys(Properties externalProperties) {
		Properties props = externalProperties;
		if (externalProperties.keySet().size() > 2) {
			String guest = getGuestId();
			props = new Properties();
			setGuestId(guest);
		}
		return props;
	}

	public void setLoginInfo(LoginInfo loginInfo) {
		this.loginInfo = loginInfo;
	}

	public LoginInfo getLoginInfo() {
		return loginInfo;
	}

	public Feeds getFeeds() {
		return feeds;
	}

	public void setFeeds(Feeds feeds) {
		this.feeds = feeds;
	}

	public boolean getAllowStatistics() {
		return preferences.getBoolean(ALLOW_STATISTIC, true);
	}

	public void setAllowStatistics(boolean allow) {
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(ALLOW_STATISTIC, allow);
		editor.apply();
	}

	public void setSelectedMainFeed(String id) {
		update(SELECTED_MAIN_FEED, id);
	}

	public String getSelectedMainFeed() {
		return preferences.getString(SELECTED_MAIN_FEED, null);
	}

	public void setSelectedSearchMainFeed(String text) {
		update(SELECTED_SEARCH_MAIN_FEED, text);
	}

	public String getSelectedSearchMainFeed() {
		return preferences.getString(SELECTED_SEARCH_MAIN_FEED, null);
	}

	public String getGCMRequestID() {
		return preferences.getString(GCM_REQ_ID, "");
	}

	public void setGCMRequestID(String id) {
		update(GCM_REQ_ID, id);
	}

	public void setApplicationVersionForGCM(int appVersion) {
		update(APP_VERSION, appVersion);	
	}

	public int getApplicationVersionForGCM() {
		return preferences.getInt(APP_VERSION, Integer.MIN_VALUE);
	}

	public void setGuestId(String guestId) {
		set(PROD_GUEST_ID, DEBUG_GUEST_ID, OLD_GUEST_ID, guestId);
	}

	public String getGuestId() {
		return get(PROD_GUEST_ID, DEBUG_GUEST_ID, OLD_GUEST_ID);
	}

	public boolean isTriedToLogin() {
		return triedToLogin;
	}

	public void setTriedToLogin(boolean triedToLogin) {
		this.triedToLogin = triedToLogin;
	}

	/* Checks if external storage is available for read and write */
	private boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state);
	}

	/* Checks if external storage is available to at least read */
	public boolean isExternalStorageReadable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state) ||
				Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
	}

	public void setCountries(Countries countries) {
		Ln.d("Received new set of countries : " + countries);
		this.countries = countries;
	}
	
	public Countries getCountries() {
		if (countries == null)
			countries = EventBus.getDefault().getStickyEvent(Countries.class);
		return countries;
	}

	public void setGuestCountry(String value) {
		update(GUEST_COUNTRY, value);
	}
	
	public String getGuestCountry() {
		return preferences.getString(GUEST_COUNTRY, "");
	}

}
