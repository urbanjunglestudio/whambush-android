package com.whambush.android.client.video;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import roboguice.util.Ln;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.whambush.android.client.R;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.util.Fonts;

public class VideoPlayer extends RelativeLayout implements Callback, OnPreparedListener, MediaPlayerControl, OnVideoSizeChangedListener, OnInfoListener {

	private static final String VZAAR_HTTP = "http://view.vzaar.com/%s/video";

	private List<View> hideWhenFullScreen = new ArrayList<View>();

	private int bufferStatusInPercentage = 0;
	private SurfaceView videoSurface;
	private MediaPlayer player;
	private VideoControllerView controller;
	private ImageView image;
	private VideoEntry video;

	private boolean readyToPlay = false;
	private boolean prepared = false;
	private boolean sizeChanged = false;

	private View child;

	private Uri localURL;

	private boolean visibleBuffer = true;

	private boolean fullScreen;
	private boolean released = false;
	
	int count = 0;

	private int playButton = R.drawable.play;

	private int pauseButton = R.drawable.pause;

	private boolean shareVisible = true;

	public VideoPlayer(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public VideoPlayer(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
	}

	public VideoPlayer(Context context) {
		super(context);
	}

	public void inflate(Activity activity) {
		LayoutInflater inflater = activity.getLayoutInflater();
		child = inflater.inflate(R.layout.videoplayer, null);
		addView(child);
		child.findViewById(R.id.bufferStatus).setVisibility(View.VISIBLE);
		child.findViewById(R.id.videoControl).setVisibility(View.GONE);
		child.findViewById(R.id.shareButton).setVisibility(View.GONE);
		registerClickListeners();
		setBuffer();
	}

	private void registerClickListeners() {
		child.findViewById(R.id.videoViewThumbnail).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (readyToPlay) {
					image.setVisibility(View.GONE);
					child.findViewById(R.id.videoSurfaceContainer).setVisibility(View.VISIBLE);
					start();
				}
			}
		});

	}

	public void setVideo(VideoEntry video) {
		this.video = video;
		image = setImageView(R.id.videoViewThumbnail, video.getThumbnail_url());
	}

	public void setVideoURI(Uri url) {
		this.localURL = url;
	} 

	public void setThumbnail(Bitmap bmThumbnail) {
		image = (ImageView)child.findViewById(R.id.videoViewThumbnail);
		image.setImageBitmap(bmThumbnail);
	}

	public void addHideableView(View view) {
		hideWhenFullScreen.add(view);
	}

	protected void setBufferStatus(int percent) {
		bufferStatusInPercentage = percent;
	}

	public void setup(VideoEntry videoEntry) {
		setVideo(videoEntry);
		setup();
	}

	public void setup() {
		String stringPath = getVideoPath();
		videoSurface = (SurfaceView) child.findViewById(R.id.videoSurface);
		SurfaceHolder videoHolder = videoSurface.getHolder();
		videoHolder.addCallback(this);
		released = false;
		player = new MediaPlayer();

		updateBufferStatus();

		player.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {
			@Override
			public void onBufferingUpdate(MediaPlayer mp, int percent) {
				setBufferStatus(percent);
				if (percent < 100)
					updateBufferStatus();
				else {
					child.findViewById(R.id.bufferStatus).setVisibility(View.GONE);
				}
			}
		});

		controller = new VideoControllerView(this.getContext());
		try { 
			player.setAudioStreamType(AudioManager.STREAM_MUSIC);
			player.setDataSource(this.getContext(), Uri.parse(stringPath));
			player.setOnPreparedListener(this);
			player.setOnVideoSizeChangedListener(this);
			player.setOnInfoListener(this);
		} catch (IllegalArgumentException e) { 
			e.printStackTrace();
		} catch (SecurityException e) { 
			e.printStackTrace(); 
		} catch (IllegalStateException e) {
			e.printStackTrace(); 
		} catch (IOException e) {
			e.printStackTrace(); 
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	private String getVideoPath() {
		if (video != null)
			return String.format(VZAAR_HTTP, video.getExternal_id());
		else
			return localURL.getPath();
	}

	public ImageView setImageView(int videoviewthumbnail, String url) {

		final ImageView image = (ImageView)child.findViewById(videoviewthumbnail);

		ImageLoader.getInstance().displayImage(url, image, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				image.setImageBitmap(loadedImage);
				image.setVisibility(View.VISIBLE);
			}
		});

		return image;
	}

	public ImageView setImageView(int videoviewthumbnail, int resource) {
		final ImageView image = (ImageView)child.findViewById(videoviewthumbnail);
		image.setImageResource(resource);
		image.setVisibility(View.VISIBLE);
		return image;
	}
	
	private void setBuffer() {
		((TextView)child.findViewById(R.id.bufferStatus)).setTypeface(new Fonts().getTitleFont());
	}

	private void updateBufferStatus() {
		((TextView)child.findViewById(R.id.bufferStatus)).setText(bufferStatusInPercentage + "%");
	}

	@Override 
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_UP) {
			if (readyToPlay) {
				toFullScreen(fullScreen);
				controller.show();
				if (isPlaying())
					pause();
				else
					start();
			}
		}
		return true;
	}

	@Override 
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

	}

	@Override 
	public void surfaceCreated(SurfaceHolder holder) {
		try {
			player.setDisplay(holder);
			playerPrepare();
		} catch (java.lang.IllegalArgumentException ex) {
			;
		}
	}
	
	public void playerPrepare() {
		try {
			released = false;
			player.prepareAsync();
		} catch (IllegalStateException e) {
			Ln.d(e);
			count++;
			if (count < 3)
				playerPrepare();
		} 
		controller.pause();
	}

	@Override 
	public void surfaceDestroyed(SurfaceHolder holder) {

	} 

	@Override 
	public void onPrepared(MediaPlayer mp) {
		prepared = true;
		setReadines();
	} 

	public void setSurfaceSize() {

		if (player == null)
			return;
		
		View view = child.findViewById(R.id.videoSurface);
		ViewGroup.LayoutParams parentSize = getLayoutParams();
		ViewGroup.LayoutParams params = view.getLayoutParams();

		float aspectRatio = ((float)player.getVideoWidth()) / ((float)player.getVideoHeight());

		if (player.getVideoWidth() > player.getVideoHeight()) {
			calculatePortrait(parentSize, params, aspectRatio);

			if (params.width > parentSize.width || params.height > parentSize.height)
				calculateLandscape(parentSize, params, aspectRatio);
		} else {
			calculateLandscape(parentSize, params, aspectRatio);

			if (params.width > parentSize.width || params.height > parentSize.height)
				calculatePortrait(parentSize, params, aspectRatio);
		}

		view.setLayoutParams(params);
	}

	private void calculateLandscape(ViewGroup.LayoutParams parentSize,
			ViewGroup.LayoutParams params, float aspectRatio) {
		params.width = (int)(((float)parentSize.height) * aspectRatio);
		params.height = parentSize.height;
	}

	private void calculatePortrait(ViewGroup.LayoutParams parentSize,
			ViewGroup.LayoutParams params, float aspectRatio) {
		params.width = parentSize.width;
		params.height = (int)(((float)parentSize.width) / aspectRatio);
	}

	@Override 
	public boolean canPause() { 
		return true; 
	} 

	@Override 
	public boolean canSeekBackward() { 
		return true; 
	}

	@Override 
	public boolean canSeekForward() {
		return true;
	} 

	@Override 
	public int getBufferPercentage() {

		return 0; 
	} 

	@Override 
	public int getCurrentPosition() {
		try {
			return player.getCurrentPosition();
		} catch (IllegalStateException ex) {
			return 0;
		}
	}

	@Override 
	public int getDuration() {
		try {
			return player.getDuration();
		} catch (IllegalStateException ex) {
			return 0;
		}
	} 

	@Override 
	public boolean isPlaying() {
		try {
			return player.isPlaying();
		} catch (IllegalStateException e) {
			return false;
		}
	} 

	@Override 
	public void pause() {
		if (isPlaying()) {
			((ImageView)child.findViewById(R.id.videoControl)).setImageDrawable(getResources().getDrawable(playButton));
			controller.updatePausePlayIcon(playButton);
		} else {
			controller.updatePausePlayIcon(pauseButton);
		}

		if (bufferStatusInPercentage < 100 && visibleBuffer)
			child.findViewById(R.id.bufferStatus).setVisibility(View.VISIBLE);

		player.pause();
		updateCenterController();
	} 

	@Override 
	public void seekTo(int i) { 
		player.seekTo(i); 
	}

	@Override 
	public void start() {
		controller.updatePausePlayIcon(pauseButton);
		child.findViewById(R.id.videoControl).setVisibility(View.GONE);
		child.findViewById(R.id.shareButton).setVisibility(View.GONE);
		child.findViewById(R.id.bufferStatus).setVisibility(View.GONE);
		player.start(); 
	} 

	@Override 
	public boolean isFullScreen() {
		return false; 
	} 

	@Override
	public void toFullScreen() {	
	}

	public void toFullScreen(boolean fullScreen) {
		this.fullScreen = fullScreen;
		if (controller != null) {
			controller.setVisiblePause(fullScreen);
			controller.toFullScreenController(fullScreen); 
			updateCenterController();
		}
	}

	private void updateCenterController() {
		if (!isPlaying()) {
			if (fullScreen) {
				child.findViewById(R.id.videoControl).setVisibility(View.GONE);
				child.findViewById(R.id.shareButton).setVisibility(View.GONE);
			}
			else {
				if (readyToPlay) {
					child.findViewById(R.id.videoControl).setVisibility(View.VISIBLE);
					if (shareVisible)
						child.findViewById(R.id.shareButton).setVisibility(View.VISIBLE);
				} else {
					child.findViewById(R.id.videoControl).setVisibility(View.GONE);
					if (shareVisible)
						child.findViewById(R.id.shareButton).setVisibility(View.VISIBLE);
				}
			}
		}
	}

	public void setVisibleBufferStatus(boolean visible) {
		this.visibleBuffer = visible;
		updateBufferVisibility();
	}

	private void updateBufferVisibility() {
		if (visibleBuffer)
			child.findViewById(R.id.bufferStatus).setVisibility(View.VISIBLE);
		else
			child.findViewById(R.id.bufferStatus).setVisibility(View.GONE);
	}

	public MediaPlayer getPlayer() {
		return player;

	}

	public void readyToPlay() {
		child.findViewById(R.id.videoControl).setVisibility(View.VISIBLE);
		if (shareVisible)
			child.findViewById(R.id.shareButton).setVisibility(View.VISIBLE);
		
		if (isPlaying()) {
			((ImageView)child.findViewById(R.id.videoControl)).setImageDrawable(getResources().getDrawable(playButton));
			controller.updatePausePlayIcon(playButton);	
		} else {
			controller.updatePausePlayIcon(pauseButton);
		}
		readyToPlay = true;
	}

	public void release() {
		released = true;
		player.release();
	}

	public void stop() {
		try {
			player.stop();
		} catch (IllegalStateException ex) {
			Ln.e(ex);
		}
	}

	public boolean isReleased() {
		return released;
	}

	@Override
	public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
		if (sizeChanged)
			return;
		
		sizeChanged = true;
		setReadines();
	}

	private void setReadines() {
		if (sizeChanged && prepared) {
			controller.setMediaPlayer(this); 
			controller.setAnchorView((FrameLayout) child.findViewById(R.id.videoSurfaceContainer));
			child.findViewById(R.id.videoControl).setVisibility(View.VISIBLE);
			
			controller.updatePausePlayIcon(playButton);
			
			if (shareVisible)
				child.findViewById(R.id.shareButton).setVisibility(View.VISIBLE);

			setSurfaceSize();
			
			readyToPlay = true;
		}
		
		
	}

	@Override
	public boolean onInfo(MediaPlayer mp, int what, int extra) {
		if (extra == 35) {
			setup();
		}
		return true;
	}

	public void setShareVisible(boolean visibility) {
		this.shareVisible = visibility;
		updateShare();
	}

	private void updateShare() {
		if (shareVisible)
			child.findViewById(R.id.shareButton).setVisibility(View.VISIBLE);
		else
			child.findViewById(R.id.shareButton).setVisibility(View.GONE);
	}

	public void setPlayButton(int playButton) {
		this.playButton = playButton;
		controller.setPlayButton(playButton);
		controller.updatePausePlayIcon(playButton);
		((ImageView)child.findViewById(R.id.videoControl)).setImageDrawable(getResources().getDrawable(playButton));
	}
	
	public void setPauseButton(int pauseButton) {
		this.pauseButton = pauseButton;
		controller.setPauseButton(pauseButton);
		((ImageView)child.findViewById(R.id.videoControl)).setImageDrawable(getResources().getDrawable(pauseButton));
	}

	public int getPlayButton() {
		return playButton;
	}
	
	public int getPauseButton() {
		return pauseButton;
	}
}
