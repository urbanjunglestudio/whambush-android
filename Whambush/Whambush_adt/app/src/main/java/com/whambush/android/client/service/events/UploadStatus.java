package com.whambush.android.client.service.events;

public class UploadStatus {

    private final int lastValue;

    public UploadStatus(int lastValue) {
        this.lastValue = lastValue;
    }

    public int getLastValue() {
        return lastValue;
    }
}
