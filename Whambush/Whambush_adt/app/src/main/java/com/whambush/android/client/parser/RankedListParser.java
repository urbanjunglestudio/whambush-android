package com.whambush.android.client.parser;

import com.whambush.android.client.domain.ranking.RankResult;


public class RankedListParser extends AbstractParser {
	
	public RankResult create(String json) {		
		return parse(json, RankResult.class);
	}
}
