package com.whambush.android.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.whambush.android.client.R;

public class ProfilePictureActivity extends AbstractWhambushCameraActivity {

	private static final String TAKE_PROFILE_PICTURE = "TakeProfilePicture";

	private String takenPicturePath;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_takepicture);
	}

	public void takePicture(View view) {
		takePicture();
	}

	public void cancelCapture(View view) {
		this.onBackPressed();
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return TAKE_PROFILE_PICTURE;
	}

	@Override
	public void pictureTaken(final String path) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				takenPicturePath = path;
				hideProgressBar();
				finish();
			}
		});
	}

	@Override
	public void finish() {
		if (takenPicturePath == null) {
			setResult(RESULT_CANCELED);
		} else {
			Intent data = new Intent();
			data.putExtra("PATH", takenPicturePath);
			setResult(RESULT_OK, data);
		}
		super.finish();
	}

	@Override
	public void recordingFailed() {
		;// Not needed in profile picture
	}
}
