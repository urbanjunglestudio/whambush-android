package com.whambush.android.client.view.list;

import roboguice.util.Ln;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.whambush.android.client.R;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.service.PaginateService;
import com.whambush.android.client.view.fragment.CountListener;
import com.whambush.android.client.view.listener.ScrollListener;

public class PaginatedListView<T> extends ListView {

	private Context context;
	private RelativeLayout footerView;
	protected PaginateService service;

	private Request baseRequest;

	protected WhambushActivity activity;
	private Request nextRequest;
	
	private CountListener listener;
	public int listItemCount;

	public PaginatedListView(Context context) {
		super(context);
		setup(context);
	}

	public PaginatedListView (Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context);
	}

	public PaginatedListView (Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context);
	}

	private void setup(Context context) {
		setOnScrollListener(new ScrollListener(this));
		init(context);
	}

	public void addPaginateLoader(PaginateService service) {
		this.service = service;
	}

	public void setActivity(WhambushActivity activity) {
		this.activity = activity;
	}
	
	public WhambushActivity getActivity() {
		return activity;
	}
	
	public void endOfListReached() {
		Ln.d("Checking is loading");
		if (service.isLoading()) {
			Ln.d("Still loading");
			return;
		}

		Ln.d("requesting more elements");
		if (!requestMoreElements())
			hideFooter();
		else
			showFooter();
	}

	public boolean requestMoreElements() {
		if (nextRequest == null) {
			Ln.d("next requesst == null");
			return false;
		}

		Ln.d("Request more elements %s", nextRequest.toString());
		service.get(nextRequest);

		return true;
	}

	public void init(Context context) {
		this.context = context;
		setFooterView((RelativeLayout) getInflater().inflate(R.layout.list_footer, this, false));
		addFooterView(getFooterView());
	}

	public void loadBase() {
		service.get(nextRequest==null?baseRequest:nextRequest);
	}

	public Context getParentContext() {
		return context;
	}

	protected LayoutInflater getInflater() {
		return (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	private RelativeLayout getFooterView() {
		return footerView;
	}

	public void setFooterView(RelativeLayout footerView) {
		Ln.d("**** Setting the footerview");
		this.footerView = footerView;
	}

	public void hideFooter() {
		Ln.d(footerView);
		removeFooterView(footerView);
	}

	public void showFooter() {
		removeFooterView(footerView);
		addFooterView(footerView);
	}

	public boolean isLoading() {
		return service.isLoading();
	}

	protected void scrollToTop() {
		ListView list = (ListView)findViewById(R.id.list);
		if (list != null)
			list.smoothScrollToPosition(0,0);
	}

	public void reload() {
		nextRequest = null;
		scrollToTop();
		showFooter();
		loadBase();
		requestLayout();
	}

	public void setBaseRequest(Request baseUrl) {
		Ln.d("Setting base url " + baseUrl);
		this.baseRequest = baseUrl;
		this.nextRequest = null;
	}

	/*@Override
	public void connectionError() {
		hideFooter();
		activity.connectionError();
	}

	@Override
	public void error(int httpResultCode) {
		hideFooter();
		activity.error(httpResultCode);
	}
*/

	public void paginateItems(WhambushList<T> items) {
		if (items != null) {
			listItemCount = items.getCount();
			Ln.d("GOT Paginated Items " + listItemCount);
			Ln.d("Next url %s", items.getNext());
			
			if (items.getNext() == null)
				nextRequest = null;
			else
				nextRequest = getNext(items);
			
			activity.hideProgressBar();
			activity.listDataReceived();
			
			updateCount(listItemCount);
		}
		hideFooter();
	}

	protected void updateCount(int count) {
		if (listener != null)
			listener.count(count);
	}

	protected Request getNext(WhambushList<T> items) {
		return service.parse(items.getNext(), true);
	}

	public CountListener getListener() {
		return listener;
	}

	public void setListener(CountListener listener) {
		this.listener = listener;
	}
}
