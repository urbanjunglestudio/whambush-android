package com.whambush.android.client.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.user.RegisterError;
import com.whambush.android.client.domain.user.UpdatedUser;
import com.whambush.android.client.http.requests.PutRequest;
import com.whambush.android.client.parser.RegisterFailureParser;
import com.whambush.android.client.parser.UserParser;
import com.whambush.android.client.service.events.UserInfoFailureReason;
import com.whambush.android.client.service.events.WBError;

public class UserUpdateService extends AbstractHTTPResultCallback {

	private static final String PROFILE_PICTURE = "profile_picture";
	private static final String USER_UPDATE_URL = "users/%s/";
	private static final String BIRTHDAY = "birthday";
	private static final String DESCRIPTION = "description";
	private static final String EMAIL = "email";
	private static final String COUNTRY = "country";

	private AbstractActivity context;

	public UserUpdateService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}

	public void updateBirthdate(String userid, int year, int month, int dayOfMonth) {
		update(userid, BIRTHDAY, String.format("%04d-%02d-%02d", year, month, dayOfMonth));
	}

	public void updateEmail(String userid, String email) {
		update(userid, EMAIL, email);
	}

	public void updateDescription(String userid, String newDescription) {
		update(userid, DESCRIPTION, newDescription);
	}
	
	public void updateCountry(String userid, String country) {
		update(userid, COUNTRY, country);
	}

	private void update(String userid, String key, String value) {
		PutRequest request = new PutRequest(context, String.format(USER_UPDATE_URL, userid), false);

		try {
			JSONObject obj = new JSONObject();
			obj.put(key, value);
			request.setJson(obj);
			send(context, request);
		} catch (Exception e) {
			Ln.e(e);
			EventBus.getDefault().post(new WBError(-1));
		}
	}

	public void updateProfilePicture(String userid, File picture) throws FileNotFoundException {
		PutRequest request = new PutRequest(context, String.format(USER_UPDATE_URL, userid), false);
		request.addFile(PROFILE_PICTURE, picture);
		send(context, request);
	}


	@Override
	public void result(int httpResultCode, String result) {

		Ln.d("Got result %d:%s", httpResultCode, result);

		if (httpResultCode == HttpURLConnection.HTTP_OK)
			EventBus.getDefault().post(parseUser(result));
		else if (httpResultCode == HttpURLConnection.HTTP_BAD_REQUEST || httpResultCode == HttpURLConnection.HTTP_NOT_FOUND)
			EventBus.getDefault().post(parseFailures(result));
		else
			EventBus.getDefault().post(new WBError(httpResultCode));
	}

	private UserInfoFailureReason[] parseFailures(String result) {

		List<UserInfoFailureReason> reasons = new ArrayList<>();

		RegisterError failures = new RegisterFailureParser().create(result);

		Ln.d("Error while registering user " + failures);

		if (failures.isEmail())
			reasons.add(UserInfoFailureReason.EMAIL_ALREADY_EXISTS);

		if (failures.isPassword())
			reasons.add(UserInfoFailureReason.PASSWORD_TOO_SHORT);

		return reasons.toArray(new UserInfoFailureReason[0]);
	}

	private UpdatedUser parseUser(String result) {
		return new UserParser().createUpdated(result);
	}

}
