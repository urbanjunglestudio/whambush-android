package com.whambush.android.client.service;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.video.VideoEntry;

import org.greenrobot.eventbus.EventBus;

public class LikeService extends LikeAndDislikeAbstractService {

	private static final String LIKE_URL = "like/video/";
	
	private static final String MISSIO_URL = "like/mission/";

	public LikeService(AbstractActivity context) {
		super(context, LIKE_URL, MISSIO_URL);
	}

	public void like(VideoEntry video) {
		doCall(video);
	}
	
	public void like(Mission mission) {
		doCall(mission);
	}

	@Override
	protected void result(LikesAndDislikes result) {
		EventBus.getDefault().post(result);
	}

}
