package com.whambush.android.client.service.events;

public class ErrorResult {

    private final int httpResultCode;

    public ErrorResult(int httpResultCode) {
        this.httpResultCode = httpResultCode;
    }

    public int getHttpResultCode() {
        return httpResultCode;
    }
}
