package com.whambush.android.client.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.adapter.holders.FollowHolder;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.view.ProfilePicture;

public class FollowAdapter extends WhambushAdapter<User> {

	private User currentUser;

	public FollowAdapter(Context context) {
		super(context);
		currentUser = Whambush.get(ConfigImpl.class).getLoginInfo().getUser();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		FollowHolder holder = null;

		if(row == null) {
			row = inflate(R.layout.list_follow_row, parent, false);

			holder = new FollowHolder();
			holder.username = (TextView)row.findViewById(R.id.username);
			holder.username.setTypeface(getFonts().getDescriptionFont());
			
			holder.profilePicture = (ProfilePicture)row.findViewById(R.id.profilePicture);

			holder.unfollow = (Button)row.findViewById(R.id.unfollow_button);
			holder.unfollow.setTypeface(getFonts().getTitleFont());
			holder.follow = (Button)row.findViewById(R.id.follow_button);
			holder.follow.setTypeface(getFonts().getTitleFont());

			row.setTag(holder);
		} else {
			holder = (FollowHolder)row.getTag();
		}

		populate(position, holder);

		return row;
	}

	private void populate(int position, FollowHolder holder) {
		User user = getItem(position);
		holder.username.setText(user.getUsername());

		holder.profilePicture.setTag(user);
		holder.follow.setTag(user);
		holder.unfollow.setTag(user);
		holder.username.setTag(user);
		
		setProfilePicture(holder.profilePicture, user);
		holder.user = user;

		
		if (Whambush.get(ConfigImpl.class).getLoginInfo().isGuest()) {
			holder.follow.setVisibility(View.GONE);
			holder.unfollow.setVisibility(View.GONE);
		} else if (isSelf(user)) {
			holder.follow.setVisibility(View.GONE);
			holder.unfollow.setVisibility(View.GONE);
		} else {
			if (user.isFollowing()) {
				holder.follow.setVisibility(View.GONE);
				holder.unfollow.setVisibility(View.VISIBLE);
			} else {
				holder.follow.setVisibility(View.VISIBLE);
				holder.unfollow.setVisibility(View.GONE);
			}
		}
	}

	private boolean isSelf(User user) {
		return user.getId() == currentUser.getId();
	}

	public User user(View view) {
		return (User)view.getTag();
	}

}
