package com.whambush.android.client.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.domain.comment.Comment;
import com.whambush.android.client.domain.comment.CommentList;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.util.DateAndTimeParser;
import com.whambush.android.client.view.ProfilePicture;

public class CommentListAdapter extends WhambushAdapter<Comment> {

	private int commentCount = 0;

	public CommentListAdapter(Context context, VideoEntry entry) {
		super(context);
		commentCount = entry.getCommentCount();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = getView(convertView);

		TextView title = getCommentTextView(R.id.comment_title, view);
		TextView posttime = getTextView(R.id.comment_posttime, view);
		TextView username = getTextView(R.id.username, view);
		TextView count = getTextView(R.id.count, view);
		ProfilePicture profile = (ProfilePicture)view.findViewById(R.id.profilePicture);

		if (position < getAll().size()) {
			Comment comment = getItem(position);
			count.setText((position + 1) + "/" + commentCount);
			username.setText(comment.getUser().getUsername()); 
			posttime.setText(DateAndTimeParser.getToAsNiceString(view.getResources(), comment.getPostDate()));
			title.setText(comment.getComment());
			setProfilePicture(profile, comment.getUser());
		}

		return view;
	}

	private TextView getCommentTextView(int id, View vi) {
		TextView view = (TextView)vi.findViewById(id);
		view.setTypeface(getFonts().getDescriptionFont());
		view.setText("");
		return view;
	}

	private TextView getTextView(int id, View vi) {
		TextView view = (TextView)vi.findViewById(id);
		if (view != null && view.getTypeface() != null)
			if (view.getTypeface().isBold())
				view.setTypeface(getFonts().getBodyBold());
			else
				view.setTypeface(getFonts().getDescriptionFont());
		view.setText("");
		return view;
	}

	@SuppressLint("InflateParams")
	private View getView(View convertView) {
		View vi=convertView;

		if(convertView==null)
			vi = inflate(R.layout.list_comments_row, null, false);
		return vi;
	}


	public void addComments(CommentList comments) {
		commentCount = comments.getCount();
		addAll(comments.getResults());
		notifyDataSetChanged();
	}

	public void clearComments() {
		clear();
		commentCount = 0;
		notifyDataSetChanged();
	}

	public Comment getCommentByUsername(String text) {
		for (Comment comment : getAll())
			if (comment.getUser().getUsername().equals(text))
				return comment;
		return null;
	}
}

