package com.whambush.android.client.parser;

import com.whambush.android.client.domain.pushnotification.PushNotification;


public class PushNotificationParser extends AbstractParser {
	
	public String[] parseArray(String string) {
		return parse(string, String[].class);
	}

	public PushNotification parse(String string) {
		return parse(string, PushNotification.class);
	}
}
