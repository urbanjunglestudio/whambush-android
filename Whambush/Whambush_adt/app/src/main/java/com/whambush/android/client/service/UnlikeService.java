package com.whambush.android.client.service;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.video.VideoEntry;

import org.greenrobot.eventbus.EventBus;

public class UnlikeService extends LikeAndDislikeAbstractService {

	private static final String UNLIKE_URL = "unlike/video/";
	private static final String UNLIKE_MISSION_URL = "unlike/mission/";

	public UnlikeService(AbstractActivity context) {
		super(context, UNLIKE_URL, UNLIKE_MISSION_URL);
	}

	public void unlike(VideoEntry video) {
		doCall(video);
	}

	public void unlike(Mission mission) {
		doCall(mission);
	}

	
	@Override
	protected void result(LikesAndDislikes result) {
		EventBus.getDefault().post(result);
	}

}
