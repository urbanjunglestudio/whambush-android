package com.whambush.android.client.parser;

import com.whambush.android.client.domain.likes.LikesAndDislikesError;

public class LikesAndDislikesErrorParser extends AbstractParser {
	
	public LikesAndDislikesError create(String json) {		
		return parse(json, LikesAndDislikesError.class);
	}

}
