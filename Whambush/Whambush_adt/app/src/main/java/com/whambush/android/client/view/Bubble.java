package com.whambush.android.client.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.FillType;

public class Bubble {

	private Path path;
	private final Paint paint = new Paint();
	private int color;
	private boolean leftBubble;
	
	public Bubble(int color) {
		this.color = color;
	}
	
	public Paint getPaint() {
		return paint;
	}
	
	
	public void drawBubble(int width, int height, Canvas canvas) {
		if (leftBubble)
			canvas.scale(-1f, 1f,
					width * 0.5f, height * 0.5f);
		
		Path path = getPath(width, height);

		paint.setColor(color);
		canvas.drawPath(path, paint);

		if (leftBubble)
			canvas.scale(-1f, 1f,
					width * 0.5f, height * 0.5f);
	}
	
	private Path getPath(int width, int height) {

		float W = width - 10;
		float H = height - 14; //
		float Y = 0;
		float X = 0;

		path = new Path();
		path.moveTo(W, Y+8);
		path.lineTo(W, H-8);
		path.lineTo(W+10, H-8);
		path.lineTo(W, H);
		path.cubicTo(W, H+4, W-4, H+8, W-8, H+8);
		path.lineTo(X+8, H+8);
		path.cubicTo(X+4, H+8, X,H+4, X, H);
		path.lineTo(X, Y+8);
		path.cubicTo(X, Y+4, X+4, Y, X+8, Y);
		path.lineTo(W-8, Y);
		path.cubicTo(W-4, Y, W, Y+4, W, Y+8);
		path.close();
		path.setFillType(FillType.WINDING);

		paint.setStyle(Paint.Style.FILL);
		paint.setStrokeCap(Paint.Cap.ROUND);

		paint.setStrokeWidth(1.0f);
		paint.setAntiAlias(true);
		
		return path;
	}

	public void setLeftBubble(boolean leftBubble) {
		this.leftBubble = leftBubble;
	}

	public boolean isLeftBubble() {
		return leftBubble;
	}
	
}