package com.whambush.android.client.camera;

public interface CameraListener {

	public void pictureTaken(String path);

	public void pictureTakeFailed();
	
	public void recordingFailed();
	
	public void cameraInitializationFailure();
	
}
