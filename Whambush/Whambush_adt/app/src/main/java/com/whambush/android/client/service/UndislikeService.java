package com.whambush.android.client.service;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.video.VideoEntry;

import org.greenrobot.eventbus.EventBus;

public class UndislikeService extends LikeAndDislikeAbstractService {

	private static final String DISLIKE_URL = "undislike/video/";
	private static final String DISLIKE_MISSION_URL = "undislike/mission/";


	public UndislikeService(AbstractActivity context) {
		super(context, DISLIKE_URL, DISLIKE_MISSION_URL);
	}
	
	public void undislike(VideoEntry video) {
		doCall(video);
	}
	
	public void undislike(Mission mission) {
		doCall(mission);
	}

	@Override
	protected void result(LikesAndDislikes result) {
		EventBus.getDefault().post(result);
	}	
}
