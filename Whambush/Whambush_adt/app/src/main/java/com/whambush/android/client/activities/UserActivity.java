package com.whambush.android.client.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.Selector;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.adapter.holders.RankedHolder;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.dialog.ProfilePictureDialog;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.activity.ActivityFeedEvent;
import com.whambush.android.client.domain.ranking.RankedUser;
import com.whambush.android.client.domain.user.UpdatedUser;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.domain.user.UserType;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.ChangeFollowService;
import com.whambush.android.client.service.UserService;
import com.whambush.android.client.service.UserUpdateService;
import com.whambush.android.client.service.events.ChangedFollow;
import com.whambush.android.client.service.events.ChangedUnFollow;
import com.whambush.android.client.service.events.ConnectionError;
import com.whambush.android.client.service.events.WBError;
import com.whambush.android.client.util.ShaderHelper;
import com.whambush.android.client.util.StringHelper;
import com.whambush.android.client.util.TextInputParameters;
import com.whambush.android.client.view.fragment.UserDetailsFragment;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class UserActivity extends WhambushActivity {

	private static final String ACTIVITY_ID = "Profile";

	private static final int SELECT_PICTURE = 1;
	private static final int TAKE_PICTURE = 2;
	private static final int CROP_PICTURE = 3;

	@InjectResource(R.string.USER_ADD_DESCRIPTION)
	private String USER_ADD_DESCRIPTION;

	@InjectResource(R.string.GENERAL_ERROR_UNKNOWN)
	private String GENERAL_ERROR_UNKNOWN;

	private User user;

	private UserDetailsFragment fragment;

	@InjectView(R.id.username)
	private TextView usernameView;

	@InjectView(R.id.usernameFirstLetter)
	TextView viewFirstLetter;

	@InjectView(android.R.id.content)
	FrameLayout rootLayout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		setupUser((User)getParameter(User.KEY));
		if (savedInstanceState != null && savedInstanceState.containsKey(User.KEY))
			user = (User)savedInstanceState.getSerializable(User.KEY);
		else
			user = getUser();

		super.onCreate(savedInstanceState, R.layout.activity_user, false);

		setupReloadImage();

		populateMissingProfileCount();

		fragment = (UserDetailsFragment)this.getSupportFragmentManager().findFragmentById(R.id.header_fragment);

		setUsername();

		if (!notSelf()) {
			profileShown = true;
			populateMissingProfileCount();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putSerializable(User.KEY, user);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		if (savedInstanceState.containsKey(User.KEY))
			user = (User)savedInstanceState.getSerializable(User.KEY);
	}

	@Override
	protected void onResume() {
		if (!isValidServiceAndLoggedIn()) {
			super.onResume();
			startActivity(SplashScreen.class);
			return;
		}

		if (!notSelf()) {
			setCurrentSelector(Selector.PROFILE);
			user = getUser();
		}

		fragment.setUser(user);
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		overridePendingTransition(R.anim.none, R.anim.none);
	}

	private void setupUser(User user) {
		if (this.user == null)
			this.user = Whambush.get(ConfigImpl.class).getLoginInfo().getUser();
	}

	private User getUser() {
		User parameter = (User)this.getParameter(User.KEY);
		if (parameter == null)
			parameter = Whambush.get(ConfigImpl.class).getLoginInfo().getUser();

		return parameter;
	}

	private void setUsername() {
		usernameView.setTypeface(getFonts().getTitleFont());
		viewFirstLetter.setTypeface(getFonts().getTitleFont());

		String username = user.getUsername();

		viewFirstLetter.setText(new String(new char[] {username.charAt(0)}).toUpperCase(getResources().getConfiguration().locale));

		usernameView.setText(StringHelper.getSmallCapsString(username.substring(1)));
		usernameView.getPaint().setShader(new ShaderHelper(this).getHeaderShader());
		viewFirstLetter.getPaint().setShader(new ShaderHelper(this).getHeaderShader());
	}

	@Override
	public void profileButtonPressed(View button) {
		if (!notSelf())
			return;

		if (getLoginInfo().isGuest()) {
			showLoginOrRegister();
		} else
			startActivity(UserActivity.class);
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		if (!notSelf())
			return ACTIVITY_ID;
		else
			return "UserFeed:user=" + user==null?"NULL":user.getId();
	}

	private boolean notSelf() {
		if (Whambush.get(ConfigImpl.class).getLoginInfo() == null || Whambush.get(ConfigImpl.class).getLoginInfo().getUser() == null)
			return true;
		return user == null || !user.getId().equals(Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getId());
	}
    
	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(UpdatedUser user) {
		hideProgressBar();
        UserActivity.this.user.setPicture(user.getPicture());
        fragment.setUser(UserActivity.this.user);
	}

	public void submitComment(View view) {
		String text = getTextView(R.id.newComment).getText().toString();
		new UserUpdateService(this).updateDescription(user.getId(), text);

		((UserDetailsFragment)fragment).setDescription(text);
		user.setDescription(text);
		hideTextInput();
		showProgressBar();
	}

	public void closeComment(View view) {
		hideTextInput();
	}

	public void changeDescription(View view) {
		if (notSelf())
			return;

		TextInputParameters params = new TextInputParameters();
		params.setDefaultText(user.getDescription());
		params.setMaxChars(90);
		params.setMaxLines(3);
		params.setTitle(USER_ADD_DESCRIPTION.toUpperCase(getResources().getConfiguration().locale));
		showTextInput(params);
	}

	@Override
	public void reload(View view) {
		fragment.refresh();
	}

	public void settingsClicked(View view) {
		fragment.settingsClicked(view);
	}

	public void userFollowClicked(View view) {
		new ChangeFollowService(this).follow(user);
		showProgressBar();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(WhambushList<User> items) {

	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(ChangedUnFollow follow) {
		user.setFollowing(false);
		fragment.setVisibilities();
		UserActivity.this.hideProgressBar();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(ChangedFollow follow) {
		user.setFollowing(true);
		fragment.setVisibilities();
		hideProgressBar();
	}

	public void userUnfollowClicked(View view) {
		new ChangeFollowService(this).unfollow(user);
		showProgressBar();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(WBError error) {
		hideProgressBar();
		getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(GENERAL_ERROR_UNKNOWN));
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(ConnectionError error) {
		hideProgressBar();
	}

	public void unfollowClicked(View view) {
		fragment.userUnfollowClicked(view);
	}

	public void followClicked(View view) {
		fragment.userFollowClicked(view);
	}

	public void activityClicked(View view) {
		ActivityFeedEvent event = (ActivityFeedEvent)view.getTag();

		activityClicked(event);
	}

	public void activityClicked(ActivityFeedEvent event) {
		if (event.getAction().equals("commented on") || event.getAction().equals("liked")) {
			Map<String, Serializable> params = new HashMap<String, Serializable>();
			params.put(VideoEntry.KEY, event.getVideo().getId());
			startActivity(SingleVideoActivity.class, params);
		} else if (event.getAction().equals("followed")) {
			Map<String, Serializable> params = new HashMap<String, Serializable>();
			params.put(User.KEY, (User)event.getUser());
			startActivity(OtherUserActivity.class, params);
		}
	}

	public void showRankedUser(View view) {
		Object tag = view.getTag();
		RankedUser user;
		if (tag instanceof RankedUser)
			user = (RankedUser)tag;
		else
			user = ((RankedHolder)tag).user;

		showProgressBar();
		new UserService(this).createLoadUserRequest(user.getUserId());
	}

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(User user) {
		hideProgressBar();
		Map<String, Serializable> params = new HashMap<>();
		params.put(User.KEY, user);
		startActivity(OtherUserActivity.class, params);
	}

	public void showUser(View view) {
		Map<String, Serializable> params = new HashMap<String, Serializable>();
		User user = (User)view.getTag();

		if (user.getUserType() == UserType.GUEST)
			return;

		params.put(User.KEY, user);
		startActivity(OtherUserActivity.class, params);
	}

	public void addProfilePicture(View view) {
		if (notSelf()) {
			return;
		}

		ProfilePictureDialog dialog = new ProfilePictureDialog();
		dialog.show(getFragmentManager(), "choosePicture");
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		Ln.d("result received %d %d", requestCode, resultCode);
		if (resultCode != Activity.RESULT_OK)
			return;

		Ln.d("DATA %s", data);

		if (requestCode == SELECT_PICTURE) {
			pictureSelected(data);
		} else if (requestCode == TAKE_PICTURE) {
			pictureTaken(data);
		} else if (requestCode == CROP_PICTURE) {
			sendProfilePic(data);
		}
	}

	private void sendProfilePic(Intent data) {
		if (data.hasExtra("PATH")) {
			Ln.d("CROPPED Path " + data.getExtras().getString("PATH"));
			try {
				new UserUpdateService(this).updateProfilePicture(user.getId(), new File(data.getExtras().getString("PATH")));
			} catch (FileNotFoundException e) {
				getDialogHelper().displayDialog(
						getDialogHelper().createErrorDialog(getString(R.string.GENERAL_ERROR_UNKNOWN)));
			}
		}
	}

	private void pictureTaken(Intent data) {
		if (data.hasExtra("PATH")) {
			cropImage(data.getExtras().getString("PATH"));
		}
	}

	private void pictureSelected(Intent data) {
		Uri selectedVideoLocation = data.getData();
		String realPath = selectedVideoLocation.getPath();
		realPath = getRealPathFromURI(this, selectedVideoLocation);
		boolean found = false;
		if(realPath != null) {
			found = new File(realPath).exists();
		}

		if (!found) {
			getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_UNKNOWN)));
			return;
		}

		selectedVideoLocation = Uri.parse(realPath);
		cropImage(selectedVideoLocation.getPath());
	}

	private void cropImage(String path) {
		Intent intent = new Intent(this, ImageActivity.class);
		Bundle bundle = new Bundle();
		bundle.putString("IMAGE", path);
		intent.putExtras(bundle);
		startActivityForResult(intent, CROP_PICTURE);
	}

	public String getRealPathFromURI(Context context, Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
			if (cursor != null) {
				cursor.moveToFirst();
				return cursor.getString(0);
			}

			return contentUri.getPath();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public void selectPicture() {
		Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
		mediaChooser.setType("image/*");
		mediaChooser.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
		startActivityForResult(mediaChooser, SELECT_PICTURE);
	}

	public void takePicture() {
		startActivityForResult(new Intent(this, ProfilePictureActivity.class), TAKE_PICTURE);
	}


}

