package com.whambush.android.client.domain.pushnotification;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class PushNotification implements Serializable{

	private static final long serialVersionUID = -619979270937136860L;

	@SerializedName("loc-args")
	private String[] args;
	
	@SerializedName("loc-key")
	private String key;
	
	@SerializedName("video")
	private String video;

	@SerializedName("user")
	private String user;
	
	public String[] getArgs() {
		return args;
	}

	public void setArgs(String[] args) {
		this.args = args;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
		
}
