package com.whambush.android.client.view.list;

import android.content.Context;
import android.util.AttributeSet;

import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.adapter.RankingListAdapter;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.ranking.RankedUser;
import com.whambush.android.client.domain.ranking.RankingList;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.service.RankingService;

public class RankingListView extends PaginatedListView<RankedUser> {	

	private RankingListAdapter adapter;

	private User user;
	private RankingList rankList;
	
	public RankingListView(Context context) {
		super(context);
	}

	public RankingListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public RankingListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void setActivity(WhambushActivity activity) {		
		super.setActivity(activity);
		addPaginateLoader(new RankingService(activity));
		setAdapter(adapter = new RankingListAdapter(activity));
		setBaseRequest(service.parse(null, false, user.getUsername(), user.getCountry().getCountry(), "1"));
		loadBase();
	}

	@Override
	public void reload() {
		adapter.clear();
		super.reload();
	}


	@Override
	public void paginateItems(final WhambushList<RankedUser> items) {
		super.paginateItems(items);
		if (getListener() != null)
			getListener().count(((RankingList)items).getUser().getLocalRank());
		
		adapter.addAll(items.getResults());
	}

	@Override
	protected void updateCount(int count) {
	}
	
	@Override
	protected Request getNext(WhambushList<RankedUser> items) {
		rankList = (RankingList)items;
		return service.parse(items.getNext(), true, user.getUsername(), user.getCountry().getCountry(), rankList.getNextPage());
	}

	public RankingListAdapter getRankedListAdapter() {
		return adapter;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getRanking() {
		return 10;
	}

}
