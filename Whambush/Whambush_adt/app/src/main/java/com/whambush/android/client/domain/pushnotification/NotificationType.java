package com.whambush.android.client.domain.pushnotification;

public enum NotificationType {

	NA(0, ""),
	NEW_MISSION(1, "PUSH_NEW_MISSION"),
	NEW_ICON_MISSION(2, "PUSH_NEW_ICON_MISSION"),
	ICON_MISSION_ENDING(3, "PUSH_ICON_MISSION_ENDING"),
	NEW_DARE_MISSION(4, "PUSH_NEW_DARE_MISSION"),
	NEW_COMMENT(5, "PUSH_NEW_COMMENT"),
	NEW_LIKE(6, "PUSH_NEW_LIKE"),
	NEW_FOLLOWER(7, "PUSH_NEW_FOLLOWER"),
	NEW_RANK(8, "PUSH_NEW_RANK");
	
	private final int id;
	private final String idStr;

	private NotificationType(int id, String idStr) {
		this.id = id;
		this.idStr = idStr;
	}

	public int getId() {
		return id;
	}

	public String getIdStr() {
		return idStr;
	}

	public static NotificationType getFromStr(String key) {
		
		for (NotificationType type : values())
			if (type.getIdStr().equals(key))
				return type;
		
		return NA;
	}
}
