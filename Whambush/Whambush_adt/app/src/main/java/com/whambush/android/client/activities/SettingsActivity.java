package com.whambush.android.client.activities;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.app.AlertDialog.Builder;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.dialog.LogoutDialog;
import com.whambush.android.client.dialog.OpenLinkDialog;
import com.whambush.android.client.domain.country.Countries;
import com.whambush.android.client.domain.country.Country;
import com.whambush.android.client.domain.user.ActivationStatus;
import com.whambush.android.client.domain.user.UpdatedUser;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.service.MailSendingService;
import com.whambush.android.client.service.UserUpdateService;
import com.whambush.android.client.service.events.UserInfoFailureReason;
import com.whambush.android.client.util.DateAndTimeParser;
import com.whambush.android.client.util.StringHelper;
import com.whambush.android.client.util.TextInputParameters;
import com.whambush.android.client.view.WBCheckBox;
import com.whambush.android.client.view.listener.CountrySelectionListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class SettingsActivity extends WhambushActivity {

	private static final String OPEN_LINK_DIALOG_FRAGMENT = "OpenLinkDialogFragment";

	private static final String ACTIVITY_ID = "Settings";

	@InjectResource(R.string.SETTINGS_EMAIL)
	private String SETTINGS_EMAIL;

	@InjectResource(R.string.GENERAL_ERROR_UNKNOWN)
	private String GENERAL_ERROR_UNKNOWN;

	@InjectResource(R.string.USER_ADD_BIRTHDAY)
	private String USER_ADD_BIRTHDAY;

	@InjectView(R.id.email)
	private TextView email;

	@InjectView(R.id.birthday)
	private TextView birthday;

	@InjectView(R.id.username)
	private TextView usernameView;

	@InjectView(R.id.usernameFirstLetter)
	private TextView viewFirstLetter;

	@InjectView(R.id.emailLabel)
	private TextView emailLabel;

	@InjectView(R.id.birthdayLabel)
	private TextView birthdayLabel;
	
	@InjectView(R.id.countryLabel)
	private TextView countryLabel;
	
	@InjectView(R.id.country)
	private TextView country;

	@InjectView(R.id.allowStatistics)
	private WBCheckBox statistic;

	@InjectView(android.R.id.content)
	private FrameLayout rootLayout;

	@InjectView(R.id.accountHeader)
	private TextView accountHeader;

	@InjectView(R.id.someHeader)
	private TextView someHeader;

	@InjectView(R.id.supportHeader)
	private TextView supportHeader;

	@InjectView(R.id.logoutButton)
	private Button logout;

	private Date date;
	private User user;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		super.onCreate(savedInstanceState, R.layout.activity_settings, false);

		profileShown = true;
		populateMissingProfileCount();

		setHeader();

		init();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("date", date);
		outState.putSerializable(User.KEY, user);

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.containsKey("date"))
			date = (Date)savedInstanceState.get("date");

		if (savedInstanceState.containsKey(User.KEY))
			user = (User)savedInstanceState.get(User.KEY);
	}

	private void setHeader() {
		usernameView.setTypeface(getFonts().getTitleFont());
		viewFirstLetter.setTypeface(getFonts().getTitleFont());

		String username = getString(R.string.USER_SETTINGS);

		viewFirstLetter.setText(username.substring(0, 1).toUpperCase(getResources().getConfiguration().locale));
		usernameView.setText(StringHelper.getSmallCapsString(username.substring(1)));

		usernameView.getPaint().setShader(getShader(usernameView));
		viewFirstLetter.getPaint().setShader(getShader(usernameView));
	}

	private Shader getShader(TextView view) {
		Shader textShader=new LinearGradient(0, 0, 0, 48,
				new int[]{getResources().getColor(R.color.top_color), getResources().getColor(R.color.bottom_color)},
				null, TileMode.CLAMP);
		return textShader;
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return ACTIVITY_ID;
	}

	private void init() {

		user = Whambush.get(ConfigImpl.class).getLoginInfo().getUser();

		setDefaultTypeface(email);
		setDefaultTypeface(birthday);
		setDefaultTypeface(emailLabel);
		setDefaultTypeface(birthdayLabel);
		setDefaultTypeface(countryLabel);
		setDefaultTypeface(country);

		setTitleFont(logout);
		setTitleFont(accountHeader);
		setTitleFont(supportHeader);
		setTitleFont(someHeader);

		email.setText(user.getEmail());
		updateDate(birthday, user.getBirthday());
		setCountry();

		if (Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getActivation() == ActivationStatus.NEW) {
			setNotActivated(email);
			showActivationDialog(user.getEmail());
		}

		statistic.setChecked(Whambush.get(ConfigImpl.class).getAllowStatistics());
	}

	private void setCountry() {
		country.setText(user.getCountry().getName());
	}

	private void setNotActivated(TextView email) {
		email.setTextColor(getResources().getColor(R.color.whambushRed));
	}

	@Override
	public void profileButtonPressed(View button) {
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		overridePendingTransition(R.anim.none, R.anim.none);
	}

	public void allowStatisticClicked(View view) {
		Ln.d("allow statistic clicked");
		statistic.setChecked(!statistic.isChecked());
		Whambush.get(ConfigImpl.class).setAllowStatistics(statistic.isChecked());
	}

	private void updateDate(TextView view, String birthdayStr) {
		try {
			if (birthdayStr != null) {
				view.setText(android.text.format.DateFormat.getDateFormat(this).format(DateAndTimeParser.parse(birthdayStr)));
				date = DateAndTimeParser.parse(birthdayStr);
				return;
			}
			else
				view.setText(USER_ADD_BIRTHDAY);
		} catch (ParseException e) {
			view.setText(USER_ADD_BIRTHDAY);
		}

		date = null;
	}

	public void logoutClicked(View view) {
        LogoutDialog logoutDialog = new LogoutDialog();
		logoutDialog.setActivity(this);
		logoutDialog.show(getFragmentManager(), "LogoutDialogFragment");
	}

	public void hideBirthdate(View view) {
		outDown.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				rootLayout.removeView(rootLayout.findViewById(R.id.main_view_list_header));
			}
		});
		rootLayout.findViewById(R.id.main_view_list_header).startAnimation(outDown);
	}


	private void showBirhdate() {
		View.inflate(this, R.layout.view_birthdate_chooser, rootLayout);
		rootLayout.findViewById(R.id.main_view_list_header).startAnimation(inDown);

		DatePicker datePicker = (DatePicker)findViewById(R.id.birthdatePicker);
		if (date != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			datePicker.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), new OnDateChangedListener() {

				@Override
				public void onDateChanged(DatePicker arg0, int arg1, int arg2, int arg3) {
				}
			});
		}
	}

	public void acceptBirthdate(View view) {
		DatePicker datePicker = (DatePicker)findViewById(R.id.birthdatePicker);

		hideBirthdate(view);

		if (!isSame(date, datePicker)) {
			new UserUpdateService(this).updateBirthdate(user.getId(), datePicker.getYear(), datePicker.getMonth()+1, datePicker.getDayOfMonth());
			updateDate(birthday, String.format("%04d-%02d-%02d", datePicker.getYear(), datePicker.getMonth()+1, datePicker.getDayOfMonth()));
		}
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(UpdatedUser user) {
        hideProgressBar();
        if (!user.getEmail().isEmpty()) {
            email.setText(user.getEmail());
            setNotActivated(email);
            this.user.setEmail(user.getEmail());
            showEmailChanged();
        }

        if (user.getCountry() != null || !user.getCountry().isEmpty()) {
            Countries countries = Whambush.get(ConfigImpl.class).getCountries();
            for (Country country : countries.getResults())
                if (country.getCountry().equals(user.getCountry()))
                    Whambush.get(ConfigImpl.class).getLoginInfo().getUser().setCountry(country);

            country.setText(user.getCountry());
        }
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(UserInfoFailureReason[] parseFailures) {
        hideProgressBar();
        if (parseFailures.length == 0) {
            hideProgressBar();
            getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(GENERAL_ERROR_UNKNOWN));
        } else {
            showUserInfoFailure(parseFailures, new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }

            });
        }
	}

	private boolean isSame(Date date, DatePicker datePicker) {

		if (date == null)
			return false;

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		if (cal.get(Calendar.YEAR) == datePicker.getYear() && cal.get(Calendar.MONTH) == datePicker.getMonth() && cal.get(Calendar.DAY_OF_MONTH) == datePicker.getDayOfMonth())
			return true;

		return false;
	}

	public void birthdayClicked(View view) {
		showBirhdate();
	}

	public void resetPassword(View view) {
		openLink(Uri.parse(geResetPasswordLink()));
	}

	public void faqClicked(View view) {
		openLink(Uri.parse(geFAQLink()));
	}

	public void instagramClicked(View view) {
		openLink(Uri.parse(getString(R.string.SETTINGS_SOME_INSTAGRAM_FI)));
	}

	public void facebookClicked(View view) {
		openLink(Uri.parse(getString(R.string.SETTINGS_SOME_FACEBOOK_FI)));
	}

	public void twitterClicked(View view) {
		openLink(Uri.parse(getString(R.string.SETTINGS_SOME_TWITTER_FI)));
	}

	private void openLink(Uri url) {
		OpenLinkDialog openDialog = new OpenLinkDialog(url);
		openDialog.show(getFragmentManager(), OPEN_LINK_DIALOG_FRAGMENT);
	}

	/* Cancel email change */
	public void closeComment(View view) {
		hideTextInput();
	}

	/* Accept new email */
	public void submitComment(View view) {

		String text = getTextView(R.id.newComment).getText().toString();

		hideTextInput();
		showProgressBar();


		new UserUpdateService(this).updateEmail(user.getId(), text);
	}

	private void showEmailChanged() {
		Builder builder = getDialogHelper().createNoteDialog(getString(R.string.SETTINGS_EMAIL_CHANGE_DONE), user.getEmail());

		builder.setPositiveButton(getResources().getString(R.string.GENERAL_OK), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				;
			}
		});
		getDialogHelper().displayDialog(builder);
	}

	public void emailSettinsClicked(View view) {
		TextInputParameters params = new TextInputParameters();
		params.setDefaultText(user.getEmail());
		params.setMaxChars(90);
		params.setMaxLines(1);
		params.setTitle(SETTINGS_EMAIL.toUpperCase(getResources().getConfiguration().locale));
		showTextInput(params);
	}

	public void countryClicked(View view) {
		showCountrySelection(new CountrySelectionListener() {
			
			@Override
			public void selected(final Country item) {
				showProgressBar();
				new UserUpdateService(SettingsActivity.this).updateCountry(user.getId(), item.getCountry());
			}
		});
	}
	
	public void sendSupportMail(View view) {
		new MailSendingService(this).createMailSender().send();
	}

	@Override
	public void reload(View view) {
		Ln.e("NOT IMPLEMENTED");
	}
}

