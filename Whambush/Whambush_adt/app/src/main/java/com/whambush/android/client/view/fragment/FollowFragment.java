package com.whambush.android.client.view.fragment;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.service.FollowService;
import com.whambush.android.client.service.PaginateService;


public class FollowFragment extends AbstractFollowFragment {

	public FollowFragment() {
		super();
	}

	@Override
	protected PaginateService getService() {
		return new FollowService((AbstractActivity)getActivity());
	}
}
