package com.whambush.android.client.service.events;

public class WBError {

    private final EventType eventType;
    private final int errorCode;

    public WBError(int errorCode) {
        eventType = EventType.NA;
        this.errorCode = errorCode;
    }

    public WBError(EventType type, int errorCode) {
        eventType = type;
        this.errorCode = errorCode;
    }

    public int getHttpResultCode() {
        return errorCode;
    }

    public EventType getEventType() {
        return eventType;
    }
}
