package com.whambush.android.client.view.fragment;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.viewpagerindicator.TitlePageIndicator;
import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.AbstractWhambushActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.activities.OtherUserActivity;
import com.whambush.android.client.activities.SettingsActivity;
import com.whambush.android.client.activities.UserActivity;
import com.whambush.android.client.adapter.UserPagerAdapter;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.country.Country;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.service.ChangeFollowService;
import com.whambush.android.client.service.events.ChangedFollow;
import com.whambush.android.client.service.events.ChangedUnFollow;
import com.whambush.android.client.service.events.ConnectionError;
import com.whambush.android.client.util.CountHelper;
import com.whambush.android.client.util.Fonts;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class UserDetailsFragment extends RoboFragment {

	@InjectView(R.id.user_unfollow_button)
	private TextView unfollow;

	@InjectView(R.id.user_follow_button)
	private TextView follow;

	@InjectView(R.id.settings_button)
	private TextView settings;

	@InjectView(R.id.bananaCount)
	private TextView bananaCount;

	@InjectView(R.id.bananaLabel)
	private TextView bananaLabel;

	@InjectView(R.id.description)
	private TextView description;

	@InjectView(R.id.titles)
	private TitlePageIndicator titleIndicator;

	@InjectView(R.id.pager)
	private ViewPager pager;

	@InjectView(R.id.profilePicture)
	ImageView profilePicture;

	@InjectView(R.id.addProfilePicture)
	ImageView addProfilePicture;

	@InjectView(R.id.userCountryFlag)
	private ImageView userCountrtyFlag;

	private User currentUser;

	private View view;

	private UserPagerAdapter userPagerAdapter;

	private Fonts fonts;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		Ln.d("Creating User Details Fragment");

		view = inflater.inflate(R.layout.view_user_details, container, false);

		userPagerAdapter = new UserPagerAdapter(getChildFragmentManager());

		setCurrentUser();

		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		pager.setOffscreenPageLimit(5);
		pager.setAdapter(userPagerAdapter);

		int color = getActivity().getResources().getColor(R.color.whambushTopBar);
		titleIndicator.setFooterLineColor(color);
		titleIndicator.setTypeface(getFonts().getDescriptionFont());
		titleIndicator.setViewPager(pager);

		setup();
	}

	private void setCurrentUser() {
		if (currentUser == null) {
			currentUser = Whambush.get(ConfigImpl.class).getLoginInfo().getUser();
		}
	}

	private boolean isSelf() {
		if (Whambush.get(ConfigImpl.class).getLoginInfo() == null || Whambush.get(ConfigImpl.class).getLoginInfo().getUser() == null)
			return false;

		return currentUser != null && currentUser.getId().equals(Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getId());
	}

	private void setup() {
		bananaCount = getTitleTextViewWithCompound(R.id.bananaCount);
		getTitleTextViewWithCompound(R.id.bananaLabel);
		description = getTextView(R.id.description);

		unfollow.setVisibility(View.GONE);
		unfollow.setTypeface(getFonts().getTitleFont());
		follow.setVisibility(View.GONE);
		follow.setTypeface(getFonts().getTitleFont());
		settings.setVisibility(View.GONE);
		settings.setTypeface(getFonts().getTitleFont());
	}


	protected TextView getTitleTextViewWithCompound(int id) {
		TextView view = getTitleTextView(id);

		Drawable[] drawables = view.getCompoundDrawables();
		for (Drawable drawable : drawables) {
			if (drawable != null)
				drawable.setBounds(0, -15, 
						(int)getActivity().getResources().getDimension(R.dimen.user_details_banana_size),
						(int)getActivity().getResources().getDimension(R.dimen.user_details_banana_size) - 15);	
		}
		view.setCompoundDrawables(drawables[0], drawables[1], drawables[2], drawables[3]);

		return view;
	}

	protected TextView getTitleTextView(int id) {
		TextView editText = (TextView) view.findViewById(id);
		editText.setTypeface(getFonts().getTitleFont());
		return editText;
	}

	protected TextView getTextView(int id) {
		TextView editText = (TextView) view.findViewById(id);
		editText.setTypeface(getFonts().getDescriptionFont());
		return editText;
	}

	public void setUser(User user) {
		this.currentUser = user;

		userPagerAdapter.setUser(user);
		userPagerAdapter.notifyDataSetChanged();

		WhambushActivity whambushActivity = (WhambushActivity)getActivity();
		bananaCount.setText(getLikeCount(user));
		setDescription(user.getDescription());
		setUserFlagImage(user.getCountry());

		setProfilePicture();

		whambushActivity.hideProgressBar();

		setVisibilities();
	}

	private void setUserFlagImage(Country country) {

		Ln.d("Set User Flag " + country);
		if (country.getImage() != null && !country.getImage().isEmpty()) {

			ImageLoader.getInstance().cancelDisplayTask(userCountrtyFlag);

			ImageLoader.getInstance().displayImage(country.getImage(), userCountrtyFlag, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					userCountrtyFlag.setImageBitmap(loadedImage);
				}
			});
		} else
			userCountrtyFlag.setImageResource(R.drawable.profile_badge_global);
	}

	public void setVisibilities() {
		if (Whambush.get(ConfigImpl.class).getLoginInfo().isGuest()) {
			follow.setVisibility(View.GONE);
			unfollow.setVisibility(View.GONE);
			settings.setVisibility(View.GONE);
			addProfilePicture.setVisibility(View.GONE);
		} else if (!isSelf()) {
			addProfilePicture.setVisibility(View.GONE);
			if (currentUser.isFollowing()) {
				follow.setVisibility(View.INVISIBLE);
				unfollow.setVisibility(View.VISIBLE);
			} else {
				unfollow.setVisibility(View.INVISIBLE);
				follow.setVisibility(View.VISIBLE);
			}
		} else {
			follow.setVisibility(View.GONE);
			unfollow.setVisibility(View.GONE);
			settings.setVisibility(View.VISIBLE);
		}
	}

	private CharSequence getLikeCount(User user) {
		return new CountHelper(getActivity()).countAsStr(user.getScore());
	}

	public void setDescription(String text) {
		if (text != null && text.length() > 0) {
			description.setText(text);
			Linkify.addLinks(description, Linkify.ALL);
		} else if (isSelf())
			description.setText(R.string.USER_ADD_DESCRIPTION);
		else
			description.setText(R.string.USER_DESCRIPTION_EMPTY);
	}

	public User getUser() {
		return currentUser;
	}

	private void handleError() {
		if (getActivity() == null)
			return;

		((WhambushActivity)getActivity()).hideProgressBar();
		((WhambushActivity)getActivity()).getDialogHelper().displayDialog(((WhambushActivity)getActivity()).getDialogHelper().createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_UNKNOWN)));
	}

	public void changeDescription(View view) {
	}

	public void userFollowClicked(final View view) {
		new ChangeFollowService((AbstractActivity)UserDetailsFragment.this.getActivity()).follow((User)view.getTag());
		((WhambushActivity)getActivity()).showProgressBar();
	}

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ChangedFollow follow) {
        ((User)view.getTag()).setFollowing(true);

        if (isSelf())
            currentUser.setFollowingCount(currentUser.getFollowingCount() + 1);
        else {
            currentUser.setFollowerCount(currentUser.getFollowerCount() + 1);
            setUser(currentUser);
            ((User)view.getTag()).setFollowing(true);
        }
        userPagerAdapter.redrawFollow();
        ((WhambushActivity)getActivity()).hideProgressBar();
    }

	public void userUnfollowClicked(final View view) {
		new ChangeFollowService((AbstractActivity)UserDetailsFragment.this.getActivity()).unfollow((User)view.getTag());
		((WhambushActivity)getActivity()).showProgressBar();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(ChangedUnFollow unFollow) {
		((User)view.getTag()).setFollowing(false);

		if (isSelf())
			currentUser.setFollowingCount(currentUser.getFollowingCount() - 1);
		else {
			currentUser.setFollowerCount(currentUser.getFollowerCount() - 1);
			((User)view.getTag()).setFollowing(false);
		}

		userPagerAdapter.redrawFollow();
		((WhambushActivity)getActivity()).hideProgressBar();
	}

	public void settingsClicked(View view) {
		((WhambushActivity)getActivity()).startActivity(SettingsActivity.class);
	}

	public void showUSer(View view) {

		Map<String, Serializable> params = new HashMap<>();
		User user = (User)view.getTag();
		if (user == null)
			return;

		if (((AbstractWhambushActivity)getActivity()).isSelf(user)) {
			((AbstractActivity)getActivity()).startActivity(UserActivity.class, params);
		} else {
			params.put(User.KEY, user);
			((AbstractActivity)getActivity()).startActivity(OtherUserActivity.class, params);
		}
	}

	public void refresh() {
		userPagerAdapter.refresh();
	}

	public void setProfilePicture() {
		if (currentUser.getPicture().isEmpty()) return;

		String uri = currentUser.getPicture(); //ResourceHelper.parseImageUrl();

		ImageLoader.getInstance().displayImage(uri, profilePicture, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View v, Bitmap loadedImage) {
				profilePicture.setImageBitmap(loadedImage);
				addProfilePicture.setVisibility(View.GONE);
			}
		});
	}

	public Fonts getFonts() {
		if (fonts == null)
			fonts = new Fonts();

		return fonts;
	}
}
