package com.whambush.android.client.adapter;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whambush.android.client.R;

public class FlagAdapter extends WhambushAdapter<String> {

	public FlagAdapter(Context context) {
		super(context);
		addAll(new ArrayList<String>(Arrays.asList(context.getResources().getStringArray(R.array.SINGLE_FLAG_REASON))));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if(row == null) {

			row = inflate(R.layout.list_flag_row, parent, false);
		} 

		TextView entry = (TextView)row.findViewById(R.id.flag_entry);
		entry.setTypeface(getFonts().getDescriptionFont());
		entry.setText((String)getItem(position));
		row.setTag((String)getItem(position));
		return row;
	}

	public int getId(View view) {
		String flag = (String)view.getTag();
		
		for (int i = 0; i < getAll().size(); i++) {
			if (getItem(i).equals(flag))
				return i+1;
		}
		
		return -1;
	}

}
