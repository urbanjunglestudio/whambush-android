package com.whambush.android.client.parser;

import com.whambush.android.client.domain.user.UpdatedUser;
import com.whambush.android.client.domain.user.User;

public class UserParser extends AbstractParser {
	
	public User create(String json) {		
		return parse(json, User.class);
	}

	public UpdatedUser createUpdated(String json) {
		return parse(json, UpdatedUser.class);
	}
}
