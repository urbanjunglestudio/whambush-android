package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.device.DeviceInfo;
import com.whambush.android.client.domain.user.InvalidLoginInfo;
import com.whambush.android.client.domain.user.LoginInfo;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.parser.LoginParser;
import com.whambush.android.client.service.events.ConnectionError;
import com.whambush.android.client.service.events.EventType;

import org.greenrobot.eventbus.EventBus;

public class LoginService extends AbstractHTTPResultCallback {

	private static final String WHAMBUSH_VERSION = "whambush_version";
	private static final String DEVICE = "device";
	private static final String OS_VERSION = "os_version";
	private static final String OS = "os";
	private static final String MANUFACTURER = "manufacturer";
	private static final String LOGIN_URL = "login/";
	private static final String PASSWORD = "password";
	private static final String USERNAME = "username";
	private static final String GUEST_ID = "guest_id";

	private AbstractActivity context;
	
	public LoginService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}
	
	public void createLoginRequest(String username, String password, String guestId, DeviceInfo info, String softwareVersion) {
		PostRequest request = new PostRequest(context, LOGIN_URL, false, false);
		request.addParameter(USERNAME, getNormalized(username, guestId));
		request.addParameter(PASSWORD, getNormalized(password, guestId));
		request.addParameter(MANUFACTURER, info.getManufacture());
		request.addParameter(GUEST_ID, guestId==null?"":guestId);
		request.addParameter(OS, info.getOs());
		request.addParameter(OS_VERSION, info.getVersion());
		request.addParameter(DEVICE, info.getDevice());
		request.addParameter(WHAMBUSH_VERSION, softwareVersion);
		
		send(context, request);
	}

	private String getNormalized(String field, String guestId) {
		if (guestId == null || guestId.isEmpty())
			return "";
		return field==null?"":field;
	}

	@Override
	public void result(int httpResultCode, String result) {
		if (httpResultCode == HttpURLConnection.HTTP_OK)
			EventBus.getDefault().post(parseUser(result));
		else if (httpResultCode == HttpURLConnection.HTTP_BAD_REQUEST)
			EventBus.getDefault().post(parseInvalidLogin(result));
		else
			EventBus.getDefault().post(new ConnectionError());
	}

	private InvalidLoginInfo parseInvalidLogin(String result) {
		return new LoginParser().createInvalidLogin(result);
	}

	private LoginInfo parseUser(String result) {
		return new LoginParser().create(result);
	}
}
