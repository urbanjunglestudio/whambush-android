package com.whambush.android.client.parser;

import com.whambush.android.client.domain.mission.Mission;

public class MissionParser extends AbstractParser {
	
	public Mission create(String json) {		
		return parse(json, Mission.class);
	}
}
