package com.whambush.android.client.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.adapter.holders.UserActivityHolder;
import com.whambush.android.client.domain.activity.ActivityFeedEvent;
import com.whambush.android.client.domain.user.UserType;
import com.whambush.android.client.util.DateAndTimeParser;
import com.whambush.android.client.view.ProfilePicture;

public class UserActivityAdapter extends WhambushAdapter<ActivityFeedEvent> {

	public UserActivityAdapter(Context context) {
		super(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		UserActivityHolder holder;

		if(convertView == null) {
			convertView = inflate(R.layout.list_user_activity_row, parent, false);
			holder = createHolderView(convertView);
			convertView.setTag(holder);
		} else
			holder = (UserActivityHolder)convertView.getTag();

		populateView(position, convertView, holder);

		return convertView;
	}

	private UserActivityHolder createHolderView(View convertView) {
		UserActivityHolder holder = new UserActivityHolder();

		holder.profile_picture = (ProfilePicture)convertView.findViewById(R.id.profilePicture);	
		
		holder.username = getTextView(R.id.username, convertView);
		holder.username.setTypeface(getFonts().getBodyBold());
		
		holder.message = getTextView(R.id.message, convertView);
		holder.message.setTypeface(getFonts().getDescriptionFont());
		
		holder.time = getTextView(R.id.activity_time, convertView);
		holder.time.setTypeface(getFonts().getDescriptionFont());
		
		holder.unread = (ImageView)convertView.findViewById(R.id.unread);

		return holder;
	}


	private void populateView(int position, View convertView, UserActivityHolder holder) {

		ActivityFeedEvent event = (ActivityFeedEvent)getItem(position);
		if (isValidVideo(event)) {
			setProfilePicture(holder, event);
			setUsername(holder, event);
			setMessage(holder, event);
			setTime(holder, event);
			setUnread(holder, event);
			holder.event = event;
		}
	}

	private void setProfilePicture(UserActivityHolder holder, ActivityFeedEvent event) {
		
		if (event.getUser().getUserType() != UserType.GUEST) {
			setProfilePicture(holder.profile_picture, event.getUser());
		} else
			holder.profile_picture.setImageResource(R.drawable.sheep);
		
		holder.profile_picture.setTag(event.getUser());
	}

	private void setUnread(UserActivityHolder holder, ActivityFeedEvent event) {
		holder.unread.setVisibility(event.isRead()?View.INVISIBLE:View.VISIBLE);
	}

	private void setMessage(UserActivityHolder holder, ActivityFeedEvent event) {
		holder.message.setText(getLocalizedMessage(event));
		holder.message.setTag(event);
	}
	
	private void setTime(UserActivityHolder holder, ActivityFeedEvent event) {
		holder.time.setText(DateAndTimeParser.getToAsNiceString(getContext().getResources(), event.getCreated_at()));
	}

	private String getLocalizedMessage(ActivityFeedEvent event) {
		
		if (event.getAction().equals("commented on")) {
			return getContext().getString(R.string.USER_ACTIVITY_COMMENTED);
		} else if (event.getUser().getUserType() != UserType.GUEST && event.getAction().equals("liked")) {
			return getContext().getString(R.string.USER_ACTIVITY_LIKED);
		} else if (event.getUser().getUserType() == UserType.GUEST && event.getAction().equals("liked")) {
			return getContext().getString(R.string.USER_ACTIVITY_LIKED_GUEST);
		} else if (event.getAction().equals("followed")) {
			return getContext().getString(R.string.USER_ACTIVITY_FOLLOWED);
		}
		
		return event.getAction();
	}

	private void setUsername(UserActivityHolder holder, ActivityFeedEvent event) {
		if (event.getUser().getUserType() == UserType.GUEST)
			holder.username.setVisibility(View.GONE);
		else
			holder.username.setVisibility(View.VISIBLE);
		holder.username.setText(event.getUser().getUsername());
		holder.username.setTag(event.getUser());
	}

	private boolean isValidVideo(ActivityFeedEvent event) {
		return event != null;
	}

	private TextView getTextView(int id, View vi) {
		TextView view = (TextView)vi.findViewById(id);
		return view;
	}

	public int getUnreadCount() {
		int count = 0;
		for (ActivityFeedEvent event : getAll()) {
			if (!event.isRead()) {
				count++;
			}
		}
		return count;
	}

	public void setAllUnread() {
		for (ActivityFeedEvent event : getAll())
			event.setRead(true);
		
		notifyDataSetChanged();
	}

}
