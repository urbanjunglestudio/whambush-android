package com.whambush.android.client.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.adapter.holders.ChannelViewHolderItem;
import com.whambush.android.client.domain.tv.Channel;

public class ChannelsListAdapter extends WhambushAdapter<Channel> {

	public ChannelsListAdapter(Context context) {
		super(context);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = getView(convertView);

		ChannelViewHolderItem holder = (ChannelViewHolderItem)view.getTag();

		if (position < getAll().size()) {
			Channel channel = getItem(position);
			holder.title.setText(channel.getName().toUpperCase());
			setImageView(holder.banner, channel.getPictureUrl(), R.drawable.placeholder_banner);
			holder.channel = channel;
		}

		return view;
	}

	@SuppressLint("InflateParams")
	private View getView(View convertView) {
		View view = convertView;

		if(view == null) {
			ChannelViewHolderItem holder = new ChannelViewHolderItem();
			view = inflate(R.layout.list_channel_row, null, false);
			holder.title = getTitleTextView(R.id.title, view);
			holder.banner = getImageView(R.id.banner, view);
			view.setTag(holder);
		}

		return view;
	}

	private TextView getTitleTextView(int id, View vi) {
		TextView view = (TextView)vi.findViewById(id);
		view.setTypeface(getFonts().getTitleFont());
		view.setText("");
		return view;
	}



	private ImageView getImageView(int id, View view) {
		return (ImageView)view.findViewById(id);
	}
}
