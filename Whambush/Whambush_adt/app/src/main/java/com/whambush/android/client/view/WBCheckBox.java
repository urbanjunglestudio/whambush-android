package com.whambush.android.client.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageButton;

import com.whambush.android.client.R;

public class WBCheckBox extends ImageButton {

	private boolean checked;
	private Drawable checkedImage;
	private Drawable uncheckedImage;


	public WBCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.WBCheckBox, 0, R.style.SettingsRow_SettingsButton_Checkbox);

		checkedImage = a.getDrawable(R.styleable.WBCheckBox_checked_image);
		uncheckedImage = a.getDrawable(R.styleable.WBCheckBox_unchecked_image);

		a.recycle();
	}

	public void setChecked(boolean checked){
		this.checked = checked;
		if(checked)
			setImageDrawable(checkedImage);
		else
			setImageDrawable(uncheckedImage);

	}

	public boolean isChecked() {
		return checked;
	}

}
