package com.whambush.android.client.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.country.Countries;
import com.whambush.android.client.domain.country.Country;

public class CountryAdapter extends WhambushAdapter<Country> {

	public CountryAdapter(Context context) {
		super(context);
		Countries countries = Whambush.get(ConfigImpl.class).getCountries();
		
		Country prev = null;
		for (int index = 0; index < countries.getResults().size(); index++) {
			Country c = countries.getResults().get(index);
			
			if (prev != null && prev.isSupported() && !c.isSupported()) {
				Country item = new Country();
				item.setSeparator(true);
				add(item);
			}
			
			c.setSeparator(false);
			add(c);
			prev = c;
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if(row == null) {
			row = inflate(R.layout.list_country_row, parent, false);
		} 
		
		TextView entry = (TextView)row.findViewById(R.id.country_entry);
		View separator = (View)row.findViewById(R.id.separator);
		entry.setTypeface(getFonts().getDescriptionFont());

		Country item = getItem(position);
		if (!item.isSeparator()) {
			entry.setText(item.getName());
			entry.setVisibility(View.VISIBLE);
			separator.setVisibility(View.GONE);
		} else {
			entry.setVisibility(View.GONE);
			separator.setVisibility(View.VISIBLE);
		}
			
		row.setTag(item);
		return row;
	}

	public int getId(View view) {
		Country country = (Country)view.getTag();
		
		for (int i = 0; i < getAll().size(); i++) {
			if (getItem(i).equals(country))
				return i+1;
		}
		
		return -1;
	}

}
