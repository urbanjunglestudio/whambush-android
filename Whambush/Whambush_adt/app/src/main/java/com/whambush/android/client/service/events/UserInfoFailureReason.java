package com.whambush.android.client.service.events;



public enum UserInfoFailureReason {

	USERNAME_ALREADY_TAKEN("username"),
	PASSWORD_TOO_SHORT("password1"),
	EMAIL_ALREADY_EXISTS("email"),
	PROMOCODE_REQUIRED("detail"),
	COUNTRY_MISSING("country"),
	GUEST_ID("guest_id");

	private String reasonCode;

	UserInfoFailureReason(String reasonCode) {
		this.setReasonCode(reasonCode);
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

}
