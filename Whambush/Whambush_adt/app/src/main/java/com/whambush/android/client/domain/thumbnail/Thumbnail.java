package com.whambush.android.client.domain.thumbnail;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Thumbnail implements Serializable {

	private static final long serialVersionUID = -1073393119456427625L;

	@SerializedName("url")
	private String url;
	
	@SerializedName("width")
	private int width;
	
	@SerializedName("height")
	private int height;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
}
