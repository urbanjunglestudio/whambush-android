package com.whambush.android.client.util;

import android.content.res.AssetManager;
import android.graphics.Typeface;

import com.whambush.android.client.Whambush;

public class Fonts {

	private static final String FONT_BODY = "font/Roboto 300.ttf";
	private static final String FONT_BODY_BOLD = "font/Roboto 700.ttf";
	private static final String FONT_TITLE = "font/leaguegothicregular.ttf";

	private Typeface body;
	private Typeface bodyBold;
	private Typeface title;
	
	public Typeface getTitleFont() {
		validate();
		return title;
	}

	public Typeface getDescriptionFont() {
		validate();
		return body;
	}
	
	public Typeface getBodyBold() {
		validate();
		return bodyBold;
	}

	public Typeface getTimeFont() {
		validate();
		return body;
	}

	public Typeface getOtherFont() {
		validate();
		return body;
	}

	public Typeface getUsernameFont() {
		validate();
		return body;
	}
	
	private void init() {
		title = getFont(FONT_TITLE);
		body = getFont(FONT_BODY);
		bodyBold = getFont(FONT_BODY_BOLD);
	}

	private Typeface getFont(String font) {
		return Typeface.createFromAsset(getAssets(), font);
	}

	private AssetManager getAssets() {
		return Whambush.getContext().getAssets();
	}

	private void validate() {
		if (title == null) {
			init();
		}
	}
}
