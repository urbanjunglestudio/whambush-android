package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.service.events.ActivitiesRead;
import com.whambush.android.client.service.events.WBError;

import org.greenrobot.eventbus.EventBus;

public class ActivityReadService extends AbstractHTTPResultCallback {

	private static final String ACTIVITY_READ_URL = "activities/read/";
	private AbstractActivity context;

	public ActivityReadService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}

	@Override
	public void result(int httpResultCode, String result) {
		super.result(httpResultCode, result);
		Ln.i("Got result %s", result);
		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			EventBus.getDefault().post(new ActivitiesRead());
		} else
			EventBus.getDefault().post(new WBError(httpResultCode));
	}

	public void markAllAsUnread() {
		GetRequest postRequest = new GetRequest(context, ACTIVITY_READ_URL, false);
		send(context, postRequest);
	}
}
