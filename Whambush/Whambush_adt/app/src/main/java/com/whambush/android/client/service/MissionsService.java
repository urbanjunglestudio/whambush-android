package com.whambush.android.client.service;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.feed.Feed;
import com.whambush.android.client.domain.feed.Feeds;
import com.whambush.android.client.domain.mission.Missions;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.parser.MissionsParser;
import com.whambush.android.client.service.events.WBError;

import org.greenrobot.eventbus.EventBus;

public class MissionsService extends SearchService {
	
	private static final String COUNTRY = "country";
	private static final String OLD_MISSION_ENDPOINT = "missions/?type=old";
	private static final String ACTIVE_MISSION_ENDPOINT = "missions/";
	private static final String MISSION_SLUG_URL = "missions/slug/%s/";
	private static final String NUMBER_OF_ENTRIES_IN_QUERY = "15";
	private static final String PAGE_SIZE_KEY = "page_size";
	
	private static final Object DEFAULT_MISSION_FEED = ACTIVE_MISSION_ENDPOINT;


	public MissionsService(AbstractActivity context) {
		super(context);
	}

	public Feeds getMissionFeeds() {
		Feed activeMission = createMissionFeed("ACTIVE_MISSION", ACTIVE_MISSION_ENDPOINT, 1, 1);
		Feed deactiveMission = createMissionFeed("OLD_MISSION", OLD_MISSION_ENDPOINT, 2, 2);

		List<Feed> feedList = new ArrayList<>();
		feedList.add(activeMission);
		feedList.add(deactiveMission);

		Feeds feeds = new Feeds();
		feeds.setResults(feedList);
		feeds.setDefaultFeedIndex("1");
		
		return feeds;
	}

	public void findActiveMissions(String country) {
		GetRequest request = new GetRequest(getContext(), ACTIVE_MISSION_ENDPOINT, false);
		request.addParameter(PAGE_SIZE_KEY, "1000");
		Ln.w("1. setting country " + country);
		request.addParameter(COUNTRY, country);
		get(request);
	}
	
	private Feed createMissionFeed(String name, String url, int icon, int order) {
		Feed missionFeed = new Feed();
		missionFeed.setName(name);
		missionFeed.setUrl(url); 
		missionFeed.setDefaultIcon(icon);
		missionFeed.setId("" + order);
		return missionFeed;
	}
	
	public Feed findDefaultMission(Feeds feeds) {
		for (Feed feed: feeds.getResults()) {
			if (feed.getName().equals(DEFAULT_MISSION_FEED))
				return feed;
		}
		
		return feeds.getResults().get(0);
	}

	public void findMissionSlug(String slug) {
		send(getContext(), new GetRequest(getContext(), String.format(MISSION_SLUG_URL, slug)));
	}
	
	@Override
	public Request getSearchRequest(String search) {
		Ln.e("** NOT IMPLEMENTED **");
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void get(Request request) {
		send(getContext(), request);
	}

	@Override
	public Request parse(String url, boolean fullUrl, String...args) {
		GetRequest request = new GetRequest(getContext(), url, fullUrl);
		request.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
		Ln.w("2. setting country " + args[0]);
		if (args.length > 0 && args[0] != null) {
			Ln.w("setting country " + args[0]);
			request.addParameter(COUNTRY, args[0]);
		}
		return request;
	}
	
	@Override
	public void result(int httpResultCode, String result) {
		super.result(httpResultCode, result);
				
		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			EventBus.getDefault().post(parseMissions(result));
		} else
			EventBus.getDefault().post(new WBError(httpResultCode));
	}

	private Missions parseMissions(String result) {
		return new MissionsParser().create(result);
	}

}
