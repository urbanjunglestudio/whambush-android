package com.whambush.android.client.view.list;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class CountryListView extends ListView {	

	public CountryListView(Context context) {
		this(context, null);
	}

	public CountryListView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public CountryListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
}
