package com.whambush.android.client.view.list;

import android.content.Context;
import android.util.AttributeSet;

import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.adapter.VideoListAdapter;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.domain.video.Videos;
import com.whambush.android.client.service.VideoFeedService;
import com.whambush.android.client.view.listener.PaginateListListener;

public class VideoListView extends PaginatedListView<VideoEntry> {	

	private VideoListAdapter adapter;
	private PaginateListListener listListener;

	public VideoListView(Context context) {
		super(context);
	}

	public VideoListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public VideoListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void setActivity(WhambushActivity activity) {
		super.setActivity(activity);
		addPaginateLoader(new VideoFeedService(activity));
		adapter = new VideoListAdapter(activity, false);
		setAdapter(adapter);
	}

	@Override
	public void paginateItems(final WhambushList<VideoEntry> items) {		
		VideoListView.super.paginateItems(items);
		Whambush.get(VideoEntryDao.class).addVideoEntries((Videos)items);
		adapter.addVideos(((WhambushList<VideoEntry>)items).getResults());
		if (listListener != null)
			listListener.loaded();
	}

	@Override
	public void endOfListReached() {
		super.endOfListReached();
	}

	@Override
	public void reload() {
		adapter.clear();
		super.reload();
	}

	public void setListener(PaginateListListener listListener) {
		this.listListener = listListener;
	}

	public VideoListAdapter getVideoListAdapter() {
		return adapter;
	}
}
