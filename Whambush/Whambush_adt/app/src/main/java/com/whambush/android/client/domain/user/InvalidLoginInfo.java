package com.whambush.android.client.domain.user;

import java.io.Serializable;
import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

public class InvalidLoginInfo implements Serializable {

	private static final long serialVersionUID = -5456442328848467307L;

	private static final String DISABLED = "LOGIN_ERROR_INACTIVE_USER";
	
	private static final String BANNED_USER = "LOGIN_ERROR_BANNED_USER";
	
	private static final String INVALID_LOGIN = "LOGIN_ERROR_WRONG_CREDENTIALS";
	
	@SerializedName("guest_id")
	private String guest_id_error;
	 
	@SerializedName("non_field_errors")
	private String[] details;
	
	public boolean isDisabled() {
		return (details[0].equals(DISABLED));
	}
	
	public boolean isInvalid() {
		return (details[0].equals(INVALID_LOGIN));
	}
	
	public boolean isBanned() {
		return (details[0].equals(BANNED_USER));
	}

	public boolean guestIDError() {
		return  (guest_id_error != null && !guest_id_error.isEmpty());
	}
	
	public void setDetails(String[] details) {
		this.details = details;
	}

	public boolean hasErrors() {
		return (details != null && details.length > 0);
	}

	@Override
	public String toString() {
		return "InvalidLoginInfo [details=" + Arrays.toString(details) + "]";
	}	
}

