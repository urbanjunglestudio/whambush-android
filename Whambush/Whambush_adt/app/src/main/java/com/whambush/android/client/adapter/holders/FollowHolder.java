package com.whambush.android.client.adapter.holders;

import android.widget.Button;
import android.widget.TextView;

import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.view.ProfilePicture;

public class FollowHolder {
	public TextView username;
	public ProfilePicture profilePicture;
	public Button follow;
	public Button unfollow;
	public User user;
}