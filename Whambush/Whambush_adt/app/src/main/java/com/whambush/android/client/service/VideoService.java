package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.http.requests.DeleteRequest;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.parser.VideoFeedEntryParser;
import com.whambush.android.client.service.events.VideoDeleted;
import com.whambush.android.client.service.events.WBError;

import org.greenrobot.eventbus.EventBus;

public class VideoService extends AbstractHTTPResultCallback {
	
	private static final String VIDEO_URL = "videos/%s/";
	private static final String VIDEO_SLUG_URL = "videos/slug/%s/";

	private AbstractActivity context;

	public VideoService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}
	
	public void load(String id) {
		send(context, new GetRequest(context, String.format(VIDEO_URL, id)));
	}
	
	public void delete(String id) {
		send(context, new DeleteRequest(context, String.format(VIDEO_URL, id)));
	}

	public void findVideoSlug(String slug) {
		send(context, new GetRequest(context, String.format(VIDEO_SLUG_URL, slug)));
	}
	
	@Override
	public void result(int httpResultCode, String result) {
		if (httpResultCode == HttpURLConnection.HTTP_OK)
			EventBus.getDefault().post(parseUser(result));
		if (httpResultCode == HttpURLConnection.HTTP_NO_CONTENT)
			EventBus.getDefault().post(new VideoDeleted());
		else
			EventBus.getDefault().post(new WBError(httpResultCode));
	}

	private VideoEntry parseUser(String result) {
		return new VideoFeedEntryParser().create(result);
	}

}
