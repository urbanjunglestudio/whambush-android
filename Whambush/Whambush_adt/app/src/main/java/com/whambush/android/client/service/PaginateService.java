package com.whambush.android.client.service;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.http.requests.Request;

public abstract class PaginateService extends AbstractHTTPResultCallback {

	private AbstractActivity context;

	public PaginateService(AbstractActivity context) {
		this.context = context;
		context.registerCallback(this);
	}

	@SuppressWarnings("rawtypes")
	public void get(Request url) {
		Ln.d("starting to load");
		send(context, url);
	}
	
	protected AbstractActivity getContext() {
		return context;
	}
	
	
	public abstract Request parse(String url, boolean fullUrl, String...args);

	public boolean isLoading() {
		Ln.d("Is loading " + loading);
		return loading;
	}
}
