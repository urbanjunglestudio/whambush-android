package com.whambush.android.client.adapter.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.whambush.android.client.domain.video.VideoEntry;

public class VideoViewHolderItem {

	public TextView title;
	public TextView description;
	public TextView time;
	public TextView username;
	public TextView commentCount;
	public TextView likeCount;
	public ImageView image;
	public View progress;
	public ImageView banana;
	public VideoEntry video;
	public View thumbnailHeader;
	public TextView viewCount;
	public TextView beingProcessed;
	public ImageView tvIcon;
	
}