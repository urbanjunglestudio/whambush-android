package com.whambush.android.client.util;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.provider.MediaStore.Video;
import android.provider.MediaStore.Video.VideoColumns;
import android.util.Log;


public class ThumbnailUtil {


	public Bitmap getLastVideoThumbnail(ContentResolver resolver, String bucketId) {
		Uri baseUri = Video.Media.EXTERNAL_CONTENT_URI;

		Uri query = baseUri.buildUpon().build();
		String[] projection = new String[] { VideoColumns._ID, MediaColumns.DATA, VideoColumns.DATE_TAKEN, VideoColumns.DURATION };
		//		String selection = VideoColumns.BUCKET_ID + '=' + bucketId;
		String order = VideoColumns.DATE_TAKEN + " DESC," + VideoColumns._ID + " DESC";

		Cursor cursor = null;
		try {
			cursor = resolver.query(query, projection, null, null, order);
			if (cursor != null && cursor.moveToFirst()) {
				while (cursor.moveToNext()) {
					Log.d("VIDEO", "getLastVideoThumbnail: " + cursor.getString(1));

					long id = cursor.getLong(0);
					ContentUris.withAppendedId(baseUri, id);

					Bitmap curThumb = MediaStore.Video.Thumbnails.getThumbnail(resolver, cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media._ID)), 
							MediaStore.Video.Thumbnails.MICRO_KIND, null);

					if (curThumb != null)
						return curThumb;
				}
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return null;
	}

}
