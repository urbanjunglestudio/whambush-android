package com.whambush.android.client.adapter;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.activities.MissionsActivity;
import com.whambush.android.client.adapter.holders.MissionHolder;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.util.CountHelper;
import com.whambush.android.client.util.DateAndTimeParser;
import com.whambush.android.client.view.ProfilePicture;

public class MissionListAdapter extends WhambushAdapter<Mission> {

	private final Map<String, Mission> elementMap = new HashMap<String, Mission>();
	private AbstractActivity activity;
	private String missionCountry;
	private String country;

	public MissionListAdapter(AbstractActivity activity) {
		super(activity);
		this.activity = activity;
	}

	public void setUserCountry(String country) {
		this.country = country;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		MissionHolder holder;

		if (view == null)
			view = createView(view, parent);

		holder = (MissionHolder)view.getTag();

		populate(view, holder, position);

		view.findViewById(R.id.missionListEntry).invalidate();
		view.invalidate();
		view.requestLayout();		
		return view;
	}

	private void populate(View view, MissionHolder holder, int position) {
		if (position != getCount()) {
			Mission mission = getItem(position);
			setBackgroundImage(mission, holder.bkgImg);
			setUsername(holder, mission);
			setTime(holder, mission);
			setTitle(holder, mission);
			setLikeCount(mission, holder.likeCount, holder.likeImage);
			setVideoCount(mission, holder.videoCount);
			setProfilePicture(holder.profilePicture, mission.getAddedBy());

			addClickListener(holder.accessImage, view);
			setAccessImage(holder, mission);

			holder.mission = mission.getId();
			elementMap.put(mission.getId(), mission);
		}		
	}

	private void setAccessImage(MissionHolder holder, Mission mission) {
		if (mission.isMissionSubmissionDone())
			holder.accessImage.setImageResource(R.drawable.country_select_done);
		else 
			holder.accessImage.setImageResource(R.drawable.country_select);
	}

	private void setTitle(MissionHolder holder, Mission mission) {
		holder.title.setText(mission.getName().toUpperCase(activity.getResources().getConfiguration().locale));
	}

	private void setTime(MissionHolder holder, Mission mission) {
		setTime(holder.time, holder.separator, DateAndTimeParser.getLeftAsNiceString(holder.time.getResources(), mission.getEndAt()));
	}

	private void setUsername(MissionHolder holder, Mission mission) {
		holder.username.setText(mission.getAddedBy().getUsername());
		holder.username.setTag(mission);
	}

	private View createView(View view, ViewGroup parent) {
		view = inflate(R.layout.list_missions_row, parent, false);
		MissionHolder holder = new MissionHolder();
		holder.title = getTextView(R.id.mission_title, view, getFonts().getTitleFont(), true);
		holder.time = getTextView(R.id.mission_time, view, getFonts().getTimeFont(), true);
		holder.separator = getTextView(R.id.separator, view, getFonts().getTimeFont(), false);
		holder.username = getTextView(R.id.username, view, getFonts().getTimeFont(), false);
		holder.bkgImg = (ImageView)view.findViewById(R.id.mission_image);
		holder.likeCount = getTextView(R.id.likeCount, view, getFonts().getDescriptionFont(), false);
		holder.videoCount = getTextView(R.id.videoCount, view, getFonts().getTimeFont(), false);
		holder.likeImage = (ImageView)view.findViewById(R.id.likeImage);
		holder.accessImage = (ImageView)view.findViewById(R.id.accessImage);
		holder.profilePicture = (ProfilePicture)view.findViewById(R.id.profilePicture);
		view.setTag(holder);

		return view;
	}

	private void setBackgroundImage(Mission mission, final ImageView bkgImg) {

		if (mission.getAddedBy().getId().equals("1")) {
			bkgImg.setImageResource(R.drawable.default1);
		} else
			bkgImg.setImageResource(R.drawable.default3);

		
		if (mission.getMissionImage1() != null && mission.getMissionImage1().length() > 0) {
			ImageLoader.getInstance().displayImage(mission.getMissionImage1(), bkgImg, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					bkgImg.setImageBitmap(loadedImage);
				}
			});
		} else if (mission.getVideo() != null) {
			ImageLoader.getInstance().displayImage(mission.getVideo().getThumbnail_url(), bkgImg, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					bkgImg.setImageBitmap(loadedImage);
				}
			});
		}
		bkgImg.setAlpha(0.17f);
	}

	private void setVideoCount(Mission mission, TextView videoCount) {
		videoCount.setText(String.format(getContext().getString(R.string.USER_VIDEO_COUNT), new CountHelper(this.getContext()).countAsStr(mission.getNumberOfSubmissions())));
	}

	private void setLikeCount(Mission mission, TextView likeCount, ImageView likeImage) {
		int bananas = mission.getLikeCount();
		bananas -= mission.getDislikeCount();

		if (bananas < 0)
			likeImage.setImageResource(R.drawable.shit_inactive);
		else
			likeImage.setImageResource(R.drawable.banana_inactive);
		
		likeCount.setText(new CountHelper(this.getContext()).countAsStr(Math.abs(bananas)));
	}

	public boolean wrongCountry() {
		return  !(country.equals(missionCountry));
	}

	private void addClickListener(final ImageView image, final View view) {
		image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((MissionsActivity)activity).browseMission(view);
			}
		});
	}

	private void setTime(TextView time, TextView separator, String timeAsStr) {
		if (timeAsStr != null && !timeAsStr.trim().isEmpty()) {
			time.setText(timeAsStr);
			time.setVisibility(View.VISIBLE);
			separator.setVisibility(View.VISIBLE);
		} else {
			time.setVisibility(View.GONE);
			separator.setVisibility(View.GONE);
		}
	}

	public void setCountry(String missionCountry) {
		this.missionCountry = missionCountry;
	}

	public boolean isOldMission(Mission mission) {
		try {
			Date date = DateAndTimeParser.parse(mission.getEndAt());
			return date.before(new Date(DateAndTimeParser.getNowInGMT()));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return false;
	}

	private TextView getTextView(int id, View vi, Typeface typeface, boolean clear) {
		TextView view = (TextView)vi.findViewById(id);
		view.setTypeface(typeface);
		if (clear)
			view.setText("");
		return view;
	}
	
	public Mission getMission(View view) {
		return elementMap.get(((MissionHolder)view.getTag()).mission);
	}

}

