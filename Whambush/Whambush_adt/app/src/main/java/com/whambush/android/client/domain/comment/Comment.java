package com.whambush.android.client.domain.comment;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.WhambushObject;
import com.whambush.android.client.domain.user.User;

public class Comment extends WhambushObject {

	private static final long serialVersionUID = 3380438760511527575L;

	@SerializedName("user")
	private User user;
     
	@SerializedName("comment")
	private String comment;
	
	@SerializedName("video_id")
	private String videoId;

	@SerializedName("post_date")
	private String postDate;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getVideoId() {
		return videoId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	
}
