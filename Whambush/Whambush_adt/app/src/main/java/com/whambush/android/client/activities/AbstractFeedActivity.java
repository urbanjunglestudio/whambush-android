package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.whambush.android.client.R;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.adapter.VideoListAdapter;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.events.ConnectionError;
import com.whambush.android.client.service.events.WBError;
import com.whambush.android.client.view.list.FeedListView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public abstract class AbstractFeedActivity extends WhambushActivity {

	private VideoListAdapter adapter;

	@SuppressWarnings("rawtypes")
	public void onCreate(Bundle savedInstanceState, int activity) {
		super.onCreate(savedInstanceState, activity, false);

		if (!isValidLogin())
			return;

		FeedListView list = getList();
		adapter = new VideoListAdapter(this, isTv());

		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				VideoEntry video = adapter.getVideo(view);

				if (video == null)
					return;

				if (video.isProcessed()) {
					showVideo(video);
				}
			}
		});

		setupReloadImage();
	}

	public boolean isTv() {
		return false;
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		if (!isValidServiceAndLoggedIn() || adapter == null) {
			startActivity(SplashScreen.class);
			return;
		}

		adapter.notifyDataSetChanged();
	}
	
	public void showVideo(VideoEntry video) {

		Map<String, Serializable> params = new HashMap<>();
		params.put(VideoEntry.KEY, video.getId());

		startActivity(SingleVideoActivity.class, params);
	}

	protected FeedListView<?> getList() {
		return (FeedListView<?>)findViewById(R.id.list);
	}

	@Override
	public void listDataReceived() {
		if (getList().getVisibility() != View.VISIBLE) {
			getList().startAnimation(inDown);
			getList().setVisibility(View.VISIBLE);
		}
	}

	@Override
	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(WBError resultCode) {
		hideProgressBar();
		stopReloadAnimation();
		getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_UNKNOWN)));
	}
	
	@Override
	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(ConnectionError connectionError) {
		stopReloadAnimation();
		super.onMessageEvent(connectionError);
	}
}
