package com.whambush.android.client.http.requests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import roboguice.util.Ln;
import android.content.Context;

import com.loopj.android.http.RequestParams;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.config.ConfigImpl;

public abstract class Request {

	private static final String APPLICATION_JSON = "application/json";
	
	List<Header> headers = new ArrayList<Header>();

	private static final String TOKEN_PREFIX = "Token ";
	private static final String TOKEN = "Authorization";

	private boolean addAuthToken;
	private String finalURL;
	private StringEntity entity;
	private MultipartEntity multiEntity;
	
	RequestParams params = new RequestParams();
	
	public Request(Context context, String url, boolean fullURL, boolean addAuthToken) {
		this.finalURL = createURL(url, fullURL);		
		this.addAuthToken = addAuthToken;
		setAuthorizationToken();
	}

	private String createURL(String url, boolean fullURL) {
		String created = url;
		if (!fullURL)
			created = createBase().append(url).toString();
		
		Ln.i("Created URL " + created);
		
		return created;
	}

	public void addHeader(String name, String value) {
		headers.add(new BasicHeader(name, value));
	}

	public Header[] getHeaders() {
		return headers.toArray(new Header[0]);
	}
	
	public RequestParams getParameters() {
		return params;
	}	

	protected void setAuthorizationToken() {
		if (!addAuthToken) 
			return;
		
		String authenticationToken = Whambush.get(ConfigImpl.class).getAuthenticationToken();
		if (authenticationToken != null)
			headers.add(new BasicHeader(TOKEN, TOKEN_PREFIX + authenticationToken));
	}

	public String getURL() {
		return finalURL;
	}

	protected StringBuilder createBase() {
		return new StringBuilder(Whambush.get(ConfigImpl.class).getURL());
	}

	public void addParameter(String key, String value) {
		params.add(key, value);
	}
	
	
	public void addFile(String key, File f) throws FileNotFoundException {
		MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE); 
		multipartEntity.addPart(key, new FileBody(f));
        this.multiEntity = multipartEntity;
	}
	
	public void setJson(JSONObject obj) throws UnsupportedEncodingException {
		setEntity(new StringEntity(obj.toString(), HTTP.UTF_8));		
	}

	public MultipartEntity getMultiEntity() {
		return multiEntity;
	}
	
	public StringEntity getEntity() {
		return entity;
	}

	public void setEntity(StringEntity entity) {
		Ln.d("entity %s", entity.toString());
		
		addHeader("Content-type", "application/json");
		addHeader("Accept", "application/json");
		
		this.entity = entity;
		this.entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, APPLICATION_JSON));
	}	
}
