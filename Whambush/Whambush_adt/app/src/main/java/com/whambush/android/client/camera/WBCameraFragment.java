package com.whambush.android.client.camera;

import com.whambush.android.client.R;

public class WBCameraFragment extends AbstractCameraFragment {

	public WBCameraFragment(boolean frontCamera) {
		super(frontCamera);
	}

	@Override
	protected int getCameraViewId() {
		return R.layout.view_profile_camera;
	}
	

}
