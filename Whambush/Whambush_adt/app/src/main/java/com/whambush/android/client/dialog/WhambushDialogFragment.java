package com.whambush.android.client.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.util.Fonts;

public class WhambushDialogFragment extends DialogFragment {

	Fonts fonts;
	
	protected void hideKeyboard(final TextView username) {
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(username.getWindowToken(), 0);
	}
	
	protected void setAnimation(Dialog dialog) {
		dialog.getWindow().getAttributes().windowAnimations = R.style.MyAnimation_Window;
	}
	
	protected void setButton(Button button) {
		button.setBackgroundColor(getResources().getColor(R.color.whambushBody));
		button.setTextColor(getResources().getColor(R.color.dialog_body_text_color));
	}
	
	
	protected Fonts getFonts() {
		if (fonts == null)
			fonts = new Fonts();

		return fonts;
	}
}
