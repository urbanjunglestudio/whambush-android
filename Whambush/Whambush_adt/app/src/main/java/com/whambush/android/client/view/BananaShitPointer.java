package com.whambush.android.client.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.whambush.android.client.R;

public class BananaShitPointer  extends View {

	private final Paint paint = new Paint();
	private int color = getResources().getColor(R.color.whambushGreen);
	private Path path;
	private int circleRadius;
	private float rotate = 0f;

	public BananaShitPointer(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public BananaShitPointer(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
	}

	public BananaShitPointer(Context context) {
		super(context);
	}

	private void makePaths() {
		if (path != null)
			return;

		path = new Path();
		float yy = getPadding();
		float width = getWidth() / 1.50f;
		float height = getHeight() - yy;
		float xx = getStart();
		float rr = ((getHeight()-yy) / 3f) / 2;

		circleRadius = (int)(getImageSize() / 2);
		
		path.moveTo(xx+rr, yy);
		path.lineTo(width-rr, yy);
		path.cubicTo(width-rr/2, yy, width, yy+rr/2, width, yy+rr);
		path.lineTo(width, height - rr);
		path.cubicTo(width,height-rr/2, width-rr/2, height, width - rr, height);
		path.lineTo(xx+rr, height);
		path.cubicTo(xx+rr/2,height, xx, height-rr/2, xx, height-rr);
		path.lineTo(xx, yy/2+height/2);
		path.lineTo(circleRadius*2 + dpToPx(2f), yy/2+height/2);
		path.lineTo(xx, yy/2+height/2);
		path.lineTo(xx, yy+rr);
		path.cubicTo(xx,yy+rr/2, xx+rr/2, yy, xx+rr, yy);

		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeCap(Paint.Cap.ROUND);

		paint.setStrokeWidth(3.0f);
		paint.setAntiAlias(true);

	}

	private float getStart() {
		return getImageSize() + getPadding() * 3;
	}

	private float getImageSize() {
		return getContext().getResources().getDimension(R.dimen.bananaShitCircleSize);
	}

	private float getPadding() {
		return getContext().getResources().getDimension(R.dimen.bananaShitPadding);
	}

	public void setLikeDislike(boolean like) {
		if (like) {
			color = getResources().getColor(R.color.whambushGreen);
			rotate = 0f;
		}
		else {
			color = getResources().getColor(R.color.whambushRed);
			rotate = 180f;
		}

		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		makePaths();

		canvas.rotate(rotate , getWidth()/2, getHeight()/2);

		paint.setColor(color);
		canvas.drawPath(path, paint);

		canvas.drawCircle(circleRadius + dpToPx(2f), getHeight() / 2, circleRadius, paint);

		paint.setColor(getResources().getColor(R.color.whambushBottomBar));
		canvas.drawCircle(getWidth()-(circleRadius + dpToPx(2f)), getHeight() / 2, circleRadius, paint);

	}

	public int dpToPx(float dp) {
	    DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
	    int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));       
	    return px;
	}

}
