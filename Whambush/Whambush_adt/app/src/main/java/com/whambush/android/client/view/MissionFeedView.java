package com.whambush.android.client.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.view.list.FeedListView;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class MissionFeedView extends FeedListView {

	public MissionFeedView(Context context) {
		super(context);
	}

	public MissionFeedView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MissionFeedView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void initHeader() {
		feedView = (RelativeLayout) getInflater().inflate(R.layout.header_mission_feed, this, false);

		if (!isInEditMode())
			((TextView)feedView.findViewById(R.id.mission_feed_header_textview)).setTypeface(getFonts().getDescriptionFont());
		addHeaderView(feedView);
	}

	public void setName(String name) {
		TextView view = (TextView)feedView.findViewById(R.id.mission_feed_header_textview);
		view.setText(name);
	}

	public void setMission(Mission mission) {
		setName(mission.getName());
	}
}
