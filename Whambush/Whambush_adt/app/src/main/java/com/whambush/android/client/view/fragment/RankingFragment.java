package com.whambush.android.client.view.fragment;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whambush.android.client.R;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.view.list.RankingListView;

public class RankingFragment extends RoboFragment {

	@InjectView(R.id.list)
	RankingListView list;

	private CountListener listener;

	private User user;

	public RankingFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);
		return inflater.inflate(R.layout.view_ranking, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setup();
	}	

	private void setup() {
		list.setUser(user);
		list.setActivity((WhambushActivity)getActivity());
		list.setListener(listener);
	}

	public void refresh() {
		if (list == null)
			return;
		list.reload();
	}


	public void setListener(CountListener countListener) {
		listener = countListener;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
