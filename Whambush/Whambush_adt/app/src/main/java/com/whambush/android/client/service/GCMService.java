package com.whambush.android.client.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import roboguice.util.Ln;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;

import com.google.android.gms.games.event.Event;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.http.requests.DeleteRequest;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.service.events.EventType;
import com.whambush.android.client.service.events.Registered;
import com.whambush.android.client.service.events.Unregistered;
import com.whambush.android.client.service.events.WBError;

public class GCMService extends AbstractHTTPResultCallback {

	private static final String REGISTRATION_ID = "registration_id";
	private static final String GCM = "gcm";
	private static final String TYPE = "type";

	private static final String DEVICE_URL = "devices/";

	private GoogleCloudMessaging gcm;

	private AbstractActivity context;
	
	public GCMService(AbstractActivity context) {
		super();
		
		this.context = context;
		context.registerCallback(this);
	}

	public void unregisterDevice() {
		String regid = Whambush.get(ConfigImpl.class).getGCMRequestID();
		if (regid.isEmpty())
			return;

		DeleteRequest request = new DeleteRequest(context, DEVICE_URL);

		try {
			JSONObject obj = new JSONObject();
			obj.put(TYPE, GCM);
			obj.put(REGISTRATION_ID, regid);
			request.setJson(obj);
			send(context, request);
		} catch (JSONException | UnsupportedEncodingException e) {
			errorResponse(e);
		}
	}

	private void registerDevice(String reqID) {
		PostRequest request = new PostRequest(context, DEVICE_URL);

		try {
			JSONObject obj = new JSONObject();
			obj.put(TYPE, GCM);
			obj.put(REGISTRATION_ID, reqID);
			request.setJson(obj);
			send(context, request);
		} catch (JSONException | UnsupportedEncodingException e) {
			errorResponse(e);
		}
	}

/*	public void unregisterDevice(String reqID) {
		EventBus.getDefault().post(new Unregistered());
	}
*/
	public void getReqID() {
		registerToGCM();
	}


	private void registerToGCM() {
		if (context.checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(context);
			String regid = getRegistrationId(getApplicationContext());

			if (regid.isEmpty()) {
				registerInBackground();
			} else {
				sendRegistrationIdToBackend(regid);
			}
		}
	}

	private String getRegistrationId(Context context) {
		String registrationId = Whambush.get(ConfigImpl.class).getGCMRequestID();

		if (registrationId.isEmpty()) return "";

		int registeredVersion = Whambush.get(ConfigImpl.class).getApplicationVersionForGCM();
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			return "";
		}

		return registrationId;
	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void registerInBackground() {

		AsyncTask task = new AsyncTask() {
			@Override
			protected String doInBackground(Object... params) {
				String msg = "";
				try {
					if (gcm == null)
						gcm = GoogleCloudMessaging.getInstance(getApplicationContext());

					Ln.d("GCM SenderID %s", Whambush.get(ConfigImpl.class).getGCMSenderID());
					String regid = gcm.register(Whambush.get(ConfigImpl.class).getGCMSenderID());
					msg = "Device registered, registration ID=" + regid;
					sendRegistrationIdToBackend(regid);
					storeRegistrationId(getApplicationContext(), regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					ex.printStackTrace();
				}
				return msg;
			}
		};


		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO)
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		else
			task.execute();
	}

	private Context getApplicationContext() {
		return context;
	}

	private void storeRegistrationId(Context context, String regId) {
		int appVersion = getAppVersion(context);
		Whambush.get(ConfigImpl.class).setGCMRequestID(regId);
		Whambush.get(ConfigImpl.class).setApplicationVersionForGCM(appVersion);
	}


	private void sendRegistrationIdToBackend(String regId) {
		registerDevice(regId);
	}


	@Override
	public void result(int httpResultCode, String result) {
		
		if (httpResultCode == HttpURLConnection.HTTP_CREATED)
			EventBus.getDefault().post(new Registered());
		else if (httpResultCode == HttpURLConnection.HTTP_OK)
			EventBus.getDefault().post(new Registered());
		else
			EventBus.getDefault().post(new WBError(EventType.GCM_REGISTRATION, httpResultCode));
	}
}
