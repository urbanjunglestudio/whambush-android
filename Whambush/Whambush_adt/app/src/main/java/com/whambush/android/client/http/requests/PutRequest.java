package com.whambush.android.client.http.requests;

import android.content.Context;

public class PutRequest extends Request {

	public PutRequest(Context context, String url) {
		this(context, url, false);
	}
	
	public PutRequest(Context context, String url, boolean fullURL) {
		super(context, url, fullURL, true);
	}
}
