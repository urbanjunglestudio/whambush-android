package com.whambush.android.client.domain.user;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.country.Country;


public class User implements Serializable {

	private static final long serialVersionUID = 4606514202403968873L;

	public static final String KEY = "WhambushUser";

	@SerializedName("rank")
	private String rank;

	@SerializedName("profile_picture")
	private String picture;

	@SerializedName("id")
	private String id;

	@SerializedName("url")
	private String url;

	@SerializedName("username")
	private String username;

	@SerializedName("email")
	private String email;

	@SerializedName("description")
	private String description;
	
	@SerializedName("birthday")
	private String birthday;
	
	@SerializedName("country")
	private Country country;
	
	@SerializedName("num_videos")
	private int submittedCount;

	@SerializedName("num_followers")
	private int followerCount;
	
	@SerializedName("num_followings")
	private int followingCount;

	@SerializedName("num_likes")
	private int likeCount;

	@SerializedName("num_dislikes")
	private int dislikeCount;

	@SerializedName("is_following")
	private boolean following;
	
	@SerializedName("user_type")
	private UserType userType;
	
	@SerializedName("userscore")
	private int score;
	
	@SerializedName("activation_state")
	private ActivationStatus activation;
	

	private transient String authToken;
	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	
	public int getSubmittedCount() {
		return submittedCount;
	}

	public void setSubmittedCount(int submittedCount) {
		this.submittedCount = submittedCount;
	}

	public int getFollowerCount() {
		return followerCount;
	}

	public void setFollowerCount(int followerCount) {
		this.followerCount = followerCount;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public int getDislikeCount() {
		return dislikeCount;
	}

	public void setDislikeCount(int dislikeCount) {
		this.dislikeCount = dislikeCount;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getRank() {
		return rank;
	}

	public String getId() {
		return id;
	}

	public String getUrl() {
		return url;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getAuthToken() {
		return authToken;
	}

	public boolean isFollowing() {
		return following;
	}

	public void setFollowing(boolean following) {
		this.following = following;
	}

	public int getFollowingCount() {
		return followingCount;
	}

	public void setFollowingCount(int followingCount) {
		this.followingCount = followingCount;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	@Override
	public String toString() {
		return "User [rank=" + rank + ", picture=" + picture + ", id=" + id
				+ ", url=" + url + ", username=" + username + ", email="
				+ email + ", description=" + description + ", birthday="
				+ birthday + ", country=" + country + ", submittedCount="
				+ submittedCount + ", followerCount=" + followerCount
				+ ", followingCount=" + followingCount + ", likeCount="
				+ likeCount + ", dislikeCount=" + dislikeCount + ", following="
				+ following + ", userType=" + userType + ", score=" + score + "]";
	}

	public ActivationStatus getActivation() {
		return activation;
	}

	public void setActivation(ActivationStatus activation) {
		this.activation = activation;
	}

	
	
}
