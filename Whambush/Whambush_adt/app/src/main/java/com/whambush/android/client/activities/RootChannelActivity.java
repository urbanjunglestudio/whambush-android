package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.whambush.android.client.R;
import com.whambush.android.client.Selector;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.adapter.ChannelsListAdapter;
import com.whambush.android.client.adapter.holders.ChannelViewHolderItem;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.tv.Channel;
import com.whambush.android.client.domain.tv.Channels;
import com.whambush.android.client.service.WBTVService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class RootChannelActivity extends WhambushActivity {

	public static final String SELECT_COUNTRY = "select_country";

	private static final String ACTIVITY_ID = "TvChannels";

	private List<Channel> channels;

	private List<Channel> baseChannels;
	
	@InjectView(R.id.list)
	ListView list;
	
	boolean selectCountry = false;

	ChannelsListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		super.onCreate(savedInstanceState, R.layout.activity_tvchannel, false);

		adapter = new ChannelsListAdapter(this);
		list.setAdapter(adapter);
		list.setOnItemClickListener(getOnItemClickListener());
		showBackButton();
		showProgressBar();

		selectCountry = getBooleanParameter(SELECT_COUNTRY);
		
		System.out.println("***** SELECT COUNTRY " + selectCountry);
		
		loadChannels();
	}


	private OnItemClickListener getOnItemClickListener() {
		return new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Channel channel = ((ChannelViewHolderItem)view.getTag()).channel;
				Ln.d("Clicked " + channel);
				if (channel != null) {
					if (isLeaf(channel))
						goToChannel(channel);
					else
						goToSubChannel(channel);
				}

			}

			private boolean isLeaf(Channel channel) {
				return channel.getEndpoint() != null && !channel.getEndpoint().isEmpty();
			}
		};
	}

	private void goToChannel(Channel channel) {
		Map<String, Serializable> params = getChannelParam(channel);
		startActivity(TVFeedActivity.class, params);
	}


	private void goToSubChannel(Channel channel) {
		Map<String, Serializable> params = getChannelParam(channel);
		startActivity(ChannelActivity.class, params);
	}


	private Map<String, Serializable> getChannelParam(Channel channel) {
		Map<String, Serializable> params = new HashMap<String, Serializable>();
		params.put(Channel.KEY, channel);
		return params;
	}


	protected void loadChannels() {
		new WBTVService(this).getTVRoot();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(Channels newChannels) {
		setChannels(findCountry(newChannels.getResults()));
	}


	protected List<Channel> findCountry(List<Channel> results) {
		baseChannels = results;
		if (selectCountry)
			return baseChannels;
		
		String country = Whambush.get(ConfigImpl.class).getGuestCountry();
		for (Channel channel : results) {
			if (channel.getCountry().equals(country))
				return channel.getSubChannels();
		}
		return results;
	}


	protected void setChannels(List<Channel> channels) {
		this.channels = channels;
		adapter.addAll(channels);
		list.setVisibility(View.VISIBLE);
		hideProgressBar();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(Channels.KEY, (java.io.Serializable)channels);
		outState.putSerializable("base", (java.io.Serializable)baseChannels);
		outState.putBoolean(SELECT_COUNTRY, selectCountry);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		if (savedInstanceState != null) {
			channels = (List<Channel>)savedInstanceState.getSerializable(Channels.KEY);
			baseChannels = (List<Channel>)savedInstanceState.getSerializable("base");
			if (savedInstanceState.containsKey(SELECT_COUNTRY))
				selectCountry = savedInstanceState.getBoolean(SELECT_COUNTRY);
		}
	}

	@Override
	protected void onResume() {
		setCurrentSelector(Selector.TV);
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		overridePendingTransition(R.anim.none, R.anim.none);
	}

	public void tvButtonPressed(View view) {
	}

	public boolean isTv() {
		return true;
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return ACTIVITY_ID;
	}

	public void currentClicked(View view) {
		;
	}

	public void selectCountry(View view) {
		Map<String, Serializable> map = new HashMap<String, Serializable>();
		map.put(SELECT_COUNTRY, true);
		
		startActivity(RootChannelActivity.class, map);
	}
	
	@Override
	public void reload(View view) {
		startReloadAnimation();
	}
}



