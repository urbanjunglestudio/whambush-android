package com.whambush.android.client.service;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.domain.ranking.RankResult;
import com.whambush.android.client.domain.ranking.RankingList;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.parser.RankedListParser;
import com.whambush.android.client.service.events.WBError;

public class RankingService extends PaginateService {

	private static final String RANK_URL = "http://tofuhead.eu/1/code/rank";

	public RankingService(AbstractActivity context) {
		super(context);
	}

	@Override
	public Request parse(String url, boolean fullUrl, String...args) {

		PostRequest request = new PostRequest(getContext(), RANK_URL, true, false);

		try {
			JSONObject obj = new JSONObject();
			obj.put("username", args[0]);
			if (args[1] != null)
				obj.put("country", args[1]);

			obj.put("page_number", Integer.parseInt(args[2]));

			request.setJson(obj);
		} catch (JSONException | UnsupportedEncodingException e) {
			Ln.e(e, "FAIL");
		}

		request.addHeader("X-Coronium-APP-ID", getContext().getString(R.string.CORONIUM_APP_ID));
		request.addHeader("X-Coronium-API-KEY", getContext().getString(R.string.CORONIUM_API_KEY));
		return request;
	}

	@Override
	public void result(int httpResultCode, String result) {
		Ln.d("*** GOT RESULT " + result);
		super.result(httpResultCode, result);
		
		if (isCancelled())
			return;

		if (httpResultCode == HttpURLConnection.HTTP_OK)
			EventBus.getDefault().post(parseRanked(result));
		else
			EventBus.getDefault().post(new WBError(httpResultCode));
	}

	private RankingList parseRanked(String result) {
		RankResult rank = new RankedListParser().create(result);

		if (rank.getResult().getNext() != null) {
			rank.getResult().setNextPage(rank.getResult().getNext());
			rank.getResult().setNext(RANK_URL);
		}
		rank.getResult().setPreviousPage(rank.getResult().getPrevious());
		rank.getResult().setPrevious(RANK_URL);

		return rank.getResult();
	}

}
