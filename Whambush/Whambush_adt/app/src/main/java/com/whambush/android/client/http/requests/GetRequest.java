package com.whambush.android.client.http.requests;

import android.content.Context;

public class GetRequest extends Request {

	public GetRequest(Context context, String url) {
		this(context, url, false, true);
	}

	public GetRequest(Context context, String url, boolean fullURL) {
		super(context, url, fullURL, true);
	}

	public GetRequest(Context context, String url, boolean fullURL,
			boolean addAuthToken) {
		super(context, url, fullURL, addAuthToken);
	}
}
