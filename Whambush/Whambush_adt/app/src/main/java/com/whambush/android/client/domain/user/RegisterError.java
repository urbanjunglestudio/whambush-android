package com.whambush.android.client.domain.user;

import java.io.Serializable;
import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

public class RegisterError implements Serializable {

	private static final long serialVersionUID = 4606514202403968873L;

	@SerializedName("username")
	private String[] username;
	
	@SerializedName("password1")
	private String[] password1;

	@SerializedName("email")
	private String[] email;
	
	@SerializedName("promocode")
	private String promocode;
	
	@SerializedName("guest_id")
	private String guestId;

	@SerializedName("country")
	private String[] country;
	
	public boolean isUsername() {
		return isError(username);
	}
	
	public boolean isEmail() {
		return isError(email);
	}
	
	public boolean isPassword() {
		return isError(password1);
	}

	public boolean isPromocode() {
		return isError(promocode);
	}

	public boolean isGuestId() {
		return isError(guestId);
	}
	
	public boolean isCountry() {
		return isError(country);
	}
	
	private boolean isError(Object field) {
		return field != null;
	}

	@Override
	public String toString() {
		return "RegisterError [username=" + Arrays.toString(username)
				+ ", password1=" + Arrays.toString(password1) + ", email="
				+ Arrays.toString(email) + ", promocode=" + promocode
				+ ", guestId=" + guestId + ", country=" + country + "]";
	}
	
}
