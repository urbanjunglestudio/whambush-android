package com.whambush.android.client.view;

import roboguice.RoboGuice;
import android.content.Context;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.util.DateAndTimeParser;
import com.whambush.android.client.util.Fonts;

public class SingleVideoDetails extends LinearLayout {

	private VideoEntry video;
	
	private Fonts fonts;


	public SingleVideoDetails(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public SingleVideoDetails(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
		init(context);
	}

	public SingleVideoDetails(Context context) {
		super(context);
		init(context);
	}

	private void init(Context context) {
		if (!isInEditMode()) { 
			RoboGuice.getInjector(context.getApplicationContext()).injectMembers(this);
			fonts = new Fonts();
		}
	}

	public void setVideo(VideoEntry video) {
		this.video = video;
		init();
	}
	
	public void init() {
		setTitle();
		setUsername();
		setDescription();
		setTags();
		setTime();
	}
	
	private void setTime() {
		TextView time = getTextView(R.id.movie_time);
		time.setTypeface(fonts.getTimeFont());
		time.setText(DateAndTimeParser.getToAsNiceString(getResources(), video.getCreated_at()));
	}

	private void setTitle() {
		TextView title = getTextView(R.id.movie_title);
		title.setTypeface(fonts.getTitleFont());
		title.setText(getTitleString(video));
	}

	private void setUsername() {
		TextView username = getTextView(R.id.username);
		username.setTypeface(fonts.getBodyBold());
		username.setText(video.getAddedBy().getUsername());
	}

	private void setTags() {
		TextView tags = getTextView(R.id.movie_tags);
		tags.setTypeface(fonts.getDescriptionFont());
		if (video.getTags() != null && video.getTags().length() > 0)
			tags.setText(getTagText(video.getTags()));
		else
			tags.setVisibility(View.GONE);
	}

	private void setDescription() {
		TextView description = getTextView(R.id.movie_description);
		description.setTypeface(fonts.getDescriptionFont());
		if (video.getDescription() != null && video.getDescription().length() > 0) {
			description.setText(video.getDescription());
			Linkify.addLinks(description, Linkify.ALL);
		} else
			description.setVisibility(View.GONE);
	}
	
	protected TextView getTextView(int id) {
		TextView editText = (TextView) findViewById(id);
		editText.setTypeface(fonts.getDescriptionFont());
		return editText;
	}

	private CharSequence getTagText(String tags) {
		return getResources().getString(R.string.SINGLE_TAG_PREFIX) + tags.toUpperCase(getResources().getConfiguration().locale);
	}
	
	private CharSequence getTitleString(VideoEntry video2) {
		return video.getName().toUpperCase(getResources().getConfiguration().locale);
	}
	
}
