package com.whambush.android.client;

import android.app.Application;
import android.content.Context;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.domain.country.Countries;
import com.whambush.android.client.http.RequestProxy;
import com.whambush.android.client.service.CountryService;
import com.whambush.android.client.service.VideoCreationService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import roboguice.util.Ln;

public class Whambush extends Application {

	private static Context context;

	private static final Map<Class<?>, Object> resources = new HashMap<Class<?>, Object>();

	public static void add(Object obj) {
		resources.put(obj.getClass(), obj);
	}

	@SuppressWarnings("unchecked")
	public static <E> E get(Class<E> resourceClass) {
		return (E)resources.get(resourceClass);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		setContext(getApplicationContext());
		createServices();
	}

	public static void createServices() {
		Ln.i("Creating Services");
		Whambush.add(new ConfigImpl());
		Whambush.add(new RequestProxy());
		Whambush.add(new VideoCreationService());
		Whambush.add(new VideoEntryDao());
		confImageCache();
		loadCountries();
	}


	private static void confImageCache() {
		if (ImageLoader.getInstance().isInited())
			return;

		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Whambush.getContext())
		.defaultDisplayImageOptions(defaultOptions)
		.build();
		ImageLoader.getInstance().init(config);
	}

	private static void loadCountries() {
		Ln.i("Starting to load countries");
		new CountryService(getContext()).getCountries();
	}

	public static Context getContext() {
		return context;
	}

	public static void setContext(Context context) {
		Whambush.context = context;
	}
}
