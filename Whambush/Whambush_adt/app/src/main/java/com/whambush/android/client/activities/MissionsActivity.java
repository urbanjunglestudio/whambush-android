package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import roboguice.util.Ln;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.whambush.android.client.R;
import com.whambush.android.client.Selector;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.country.Country;
import com.whambush.android.client.domain.feed.Feed;
import com.whambush.android.client.domain.feed.Feeds;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.mission.Missions;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.service.events.WBError;
import com.whambush.android.client.view.list.MissionFeedListView;
import com.whambush.android.client.view.listener.PaginateListListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MissionsActivity extends WhambushActivity implements PaginateListListener  {

	private static final int SELECT_COUNTRY = 1234;
	private static final String ACTIVITY_ID = "Mission";
	private Feed currentFeed;

	private String selectedCountry = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Ln.d("Starting to show Mission Activity");

		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		super.onCreate(savedInstanceState, R.layout.activity_missions, false);
		hideBackButton();
		setupReloadImage();
		setupCountryHolder();
		getList().setActivity(this);
		getList().setCountry(getSelectedCountry());
		checkCountry();
		getList().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				browseMission(view);
			}

		});
	}

	private void selectCountry() {
		if (selectedCountry.isEmpty()) {
			selectedCountry = getCountry();
			if (selectedCountry.isEmpty()) {
				selectedCountry = Whambush.get(ConfigImpl.class).getGuestCountry();
			}
		}

		if (selectedCountry.isEmpty())
			showCountrySelection();
		else {
			countrySelected(true);
		}
	}

	private void showCountrySelection() {
		Intent intent = new Intent(this, CountryChooserActivity.class);
		Bundle b = new Bundle();
		b.putString(Country.KEY, selectedCountry);
		b.putSerializable(Selector.KEY, Selector.MISSION);
		intent.putExtras(b);
		startActivityForResult(intent, SELECT_COUNTRY);
	}

	@Override
	protected void start() {
		selectCountry();
		super.start();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(Feeds feeds) {
		getList().onMessageEvent(feeds);
	}

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Missions missions) {
        getList().paginateItems(missions);
    }
	
	private void countrySelected(boolean addFeeds) {
		showProgressBar();

		getList().setCountry(selectedCountry);
		getList().setActivity(this);

		if (addFeeds)
			getList().loadFeeds();
		else
			getList().setAndLoadBase();

		getList().setListener(this);

		missionShown(true);
		populateActiveMissionCount();

		setupReloadImage();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(Feed.KEY, currentFeed);
		outState.putSerializable("country_id", selectedCountry);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.containsKey(Feed.KEY))
			currentFeed = (Feed)savedInstanceState.get(Feed.KEY);

		selectedCountry = savedInstanceState.getString("country_id");
	}

	protected MissionFeedListView getList() {
		return (MissionFeedListView)findViewById(R.id.list);
	}

	@Override
	protected void onResume() {
		setCurrentSelector(Selector.MISSION);
		super.onResume();


		if ((!isValidServiceAndLoggedIn()) || 
				getList() == null) {
			startActivity(SplashScreen.class);
			return;
		}
		getList().setUserCountry(getCountry());
	}

	private boolean updateCountry() {
		String country = getList().getCountry();
		Ln.d("**** UPDATE COUNTRY '" + country + "' '" + getCountry() + "'");
		if (!country.equals(getCountry())) {
			getList().setCountry(getCountry());
			return true;
		}

		return false;
	}

	@Override
	public void onBackPressed() {
		handleDoubleClickToExit();
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		if (currentFeed != null)
			return ACTIVITY_ID + ":feed="+currentFeed.getName();
		else
			return ACTIVITY_ID;
	}

	@Override
	public void listDataReceived() {
		if (getList().getVisibility() != View.VISIBLE) {
			getList().startAnimation(inDown);
			getList().setVisibility(View.VISIBLE);
		}
	}
	
	public void browseMission(View view) {
		Map<String, Serializable> params = new HashMap<>();
		Mission mission = getList().getMissionAdapter().getMission(view);
		params.put(Mission.KEY, mission);

		startActivity(MissionFeedActivity.class, params);
		overridePendingTransition(R.anim.activity_anim_back_out, R.anim.activity_anim_out);		
	}

	public void usernameClicked(View view) {
		startUserActivity(((Mission)view.getTag()).getAddedBy());
	}

	private void startUserActivity(User user) {
		Map<String, Serializable> params = new HashMap<>();

		if (isSelf(user)) {
			startActivity(UserActivity.class, params);
		} else {
			params.put(User.KEY, user);
			startActivity(OtherUserActivity.class, params);
		}
	}

	public void currentClicked(View view) {
		getList().startFeedSelection();
	}

	public void missionButtonPressed(View button) {

	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(WBError error) {
		hideProgressBar();
		getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_UNKNOWN)));
	}


	public void selectCountry(View view) {
		showCountrySelection();
	}

	@Override
	public void reload(View view) {
		startReloadAnimation();

		Ln.d("*** RELOAD " + getList().getCountry());
		
		if (updateCountry()) {
			Ln.d("*** RELOAD COUNTRY");
			getList().clear();
			getList().setAndLoadBase();
		} else
			getList().reload();
	}

	@Override
	public void loaded() {
		stopReloadAnimation();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		Ln.d("Get result from country selection " + requestCode + " " + resultCode);

		if (requestCode == SELECT_COUNTRY && resultCode != Activity.RESULT_OK) {
			Ln.d("NOK " + selectedCountry);
			if (selectedCountry.isEmpty())
				super.onBackPressed();
			return;
		} else if (requestCode == SELECT_COUNTRY) {
			selectedCountry = data.getStringExtra("COUNTRY");
			Whambush.get(ConfigImpl.class).setGuestCountry(selectedCountry);
			countrySelected(false);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
