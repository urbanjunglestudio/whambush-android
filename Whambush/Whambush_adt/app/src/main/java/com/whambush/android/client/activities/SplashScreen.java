package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import roboguice.util.Ln;
import android.app.Activity;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.whambush.android.client.AbstractWhambushActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.domain.country.Countries;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.user.InvalidLoginInfo;
import com.whambush.android.client.domain.user.LoginInfo;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.CountryService;
import com.whambush.android.client.service.DeviceInfoService;
import com.whambush.android.client.service.GCMService;
import com.whambush.android.client.service.LoginService;
import com.whambush.android.client.service.MissionsSlugService;
import com.whambush.android.client.service.SoftwareVersionService;
import com.whambush.android.client.service.UserService;
import com.whambush.android.client.service.VideoService;
import com.whambush.android.client.service.events.ConnectionError;
import com.whambush.android.client.service.events.EventType;
import com.whambush.android.client.service.events.Registered;
import com.whambush.android.client.service.events.Unregistered;
import com.whambush.android.client.service.events.WBError;
import com.whambush.android.client.util.Destination;
import com.whambush.android.client.util.URLParser;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class SplashScreen extends AbstractWhambushActivity {

	private static final String ACTIVITY_ID = "Splash";
	private Destination destination;

	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState, R.layout.splash_screen, true);
		} catch (RuntimeException ex) {
			finish();
			return;
		}

		if (!isValidServices())
			Whambush.createServices();

		logDeviceInfo();

		Uri data = getIntent().getData();
		if (data != null) {
			destination = URLParser.parse(data);
			Ln.d("*** INTENT data is NOT NULL ***");
		} else
			Ln.d("*** INTENT data is NULL");
		doLogin();
	}

	private void doLogin() {
		Ln.i("DO Login");
		ConfigImpl config = Whambush.get(ConfigImpl.class);
		String username = config.getUsername();
		String password = config.getPassword();

		LoginService loginService = new LoginService(this);

		loginService.createLoginRequest(username, password, config.getGuestId(), 
				new DeviceInfoService().getInfo(), new SoftwareVersionService(this).getVersion());
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return ACTIVITY_ID;
	}

	private void logDeviceInfo() {
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
	}

	@Override
	public void onBackPressed() {
		// Block the backbutton
	}

	@SuppressWarnings("unchecked")
	protected void moveToNextView() {

		Countries countries = Whambush.get(ConfigImpl.class).getCountries();
		if (countries == null) {
			new Timer().schedule(new TimerTask() {

				@Override
				public void run() {
					Ln.i("Waiting for countries");
					moveToNextView();
				}
			}, 500);
			return;
		}

		if (destination == null) {
			Ln.d("*** DESTINATION IS NULL ***");
			Class<? extends Activity> act = (Class<? extends Activity>)getParameter(TO_ACTIVITY);

			if (act != null) {
				startActivity(act, getIntent().getExtras(), true);
			} else {
				startActivity(MissionsActivity.class, true);
				setPendingTransition();
			}
		} else {
			Ln.d("*** DESTINATION IS NOT NULL ***");
			getTheSlugData();
		}
	}

	private void getTheSlugData() {
		Ln.d("*** DESTINATION IS " + destination.toString() + " ***");
		if (destination.getActionStr().equalsIgnoreCase("v")) {
			getVideo();
		} else if (destination.getActionStr().equalsIgnoreCase("u")) {
			getUser();
		} else if (destination.getActionStr().equalsIgnoreCase("m")) {
			getMission();
		} else {
			Ln.d("*** DESTINATION IS ???? ***");
			destination = null;
			moveToNextView();
		}
	}

	private void getMission() {
		Ln.d("*** GET THE MISSION ***");
		new MissionsSlugService(this).findMissionSlug(destination.getSlug());
	}

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Mission mission) {
        Map<String, Serializable> params = new HashMap<String, Serializable>();
        params.put(Mission.KEY, mission);

        startActivity(MissionFeedActivity.class, params, true);
        overridePendingTransition(R.anim.activity_anim_back_out, R.anim.activity_anim_out);
    }

	private void getUser() {
		Ln.d("*** GET THE MISSION USER ***");
		new UserService(this).findUserSlug(destination.getSlug());
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(User parsedUser) {
		Map<String, Serializable> params = new HashMap<String, Serializable>();

		if (isSelf(parsedUser)) {
			startActivity(UserActivity.class, params, true);
		} else {
			params.put(User.KEY, parsedUser);
			startActivity(OtherUserActivity.class, params, true);
		}
	}
	private void getVideo() {
		Ln.d("*** GET THE VIDEO ***");
		new VideoService(this).findVideoSlug(destination.getSlug());
	}

	@Override
	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(WBError error) {
		destination = null;
		if (error.getEventType() == EventType.GCM_REGISTRATION) {
			moveToNextView();
			return;
		}

        if (error.getEventType() == EventType.COUNTRY) {
            Ln.i("Starting to load countries AGAIN");
            new CountryService(this).getCountries();
            return;
        }

		getDialogHelper().displayDialog(
				getDialogHelper().createErrorDialog(getString(R.string.GENERAL_ERROR_UNKNOWN), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						moveToNextView();
					}
                })
        );
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(VideoEntry video) {
		Whambush.get(VideoEntryDao.class).add(video);

		Map<String, Serializable> params = new HashMap<String, Serializable>();
		params.put(VideoEntry.KEY, video.getId());

		startActivity(SingleVideoActivity.class, params, true);
	}

	@Override
	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(ConnectionError connectionError) {
		getDialogHelper().displayDialog(
				getDialogHelper().createErrorDialog(getString(R.string.GENERAL_ERROR_NO_NETWORK), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						moveToNextView();
					}

				}));
	}



	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(LoginInfo loginInfo) {
		Ln.d("Logged In %s", loginInfo);

		ConfigImpl config = Whambush.get(ConfigImpl.class);
		config.setGuestId(loginInfo.getGuestId());
		config.setLoginInfo(loginInfo);
		config.setAuthenticationToken(loginInfo.getAuthenticationToken());

		new GCMService(this).getReqID();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(Registered reg) {
		moveToNextView();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(Unregistered unreg) {
		moveToNextView();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(InvalidLoginInfo info) {
		getDialogHelper().displayDialog(
				getDialogHelper().createErrorDialog(getString(R.string.GENERAL_ERROR_UNKNOWN), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Ln.e("Invalid login in splashscreen");
						ConfigImpl config = Whambush.get(ConfigImpl.class);
						config.setGuestId("");
						config.setUsername("");
						config.setPassword("");
						doLogin();
					}

				}));
	}
}