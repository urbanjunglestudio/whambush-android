package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.activity.Activities;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.parser.ActivitiesParser;
import com.whambush.android.client.service.events.WBError;

import org.greenrobot.eventbus.EventBus;

public class ActivityService extends PaginateService {

	private static final String NUMBER_OF_ENTRIES_IN_QUERY = "30";
	private static final String PAGE_SIZE_KEY = "page_size";
	private static final String ACTIVITY_URL = "activities/";

	public ActivityService(AbstractActivity context) {
		super(context);
	}

	@Override
	public void result(int httpResultCode, String result) {
		super.result(httpResultCode, result);
		Ln.i("****** Got result %s", result);
		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			EventBus.getDefault().post(parseActivities(result));
		} else
			EventBus.getDefault().post(new WBError(httpResultCode));
	}

	private Activities parseActivities(String result) {
		return new ActivitiesParser().create(result);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void get(Request request) {

		send(getContext(), request);
	}

	
	@Override
	public Request parse(String url, boolean full, String...args) {
		GetRequest getRequest = new GetRequest(getContext(), url, full);
		getRequest.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
		return getRequest;
	}

	public Request getBase() {
		return parse(ACTIVITY_URL, false);
	}
}
