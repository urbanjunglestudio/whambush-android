package com.whambush.android.client.view.listener;

import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

import com.whambush.android.client.view.list.PaginatedListView;

public class ScrollListener implements OnScrollListener {
	
	private final PaginatedListView listView;

	public ScrollListener(PaginatedListView view) {
		this.listView = view;
	}
	
	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, final int totalItemCount) {
		if (totalItemCount > 0) {
			int lastInScreen = firstVisibleItem + visibleItemCount;
			if(lastInScreen == totalItemCount) {
				listView.endOfListReached();
			}
		}
	}
}

