package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.tv.Channels;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.parser.ChannelParser;
import com.whambush.android.client.service.events.WBError;

import org.greenrobot.eventbus.EventBus;

public class WBTVService extends AbstractHTTPResultCallback {

	private static final String NUMBER_OF_ENTRIES_IN_QUERY = "30";
	private static final String PAGE_SIZE_KEY = "page_size";
	private static final String TV_URL = "channels/";
	private AbstractActivity context;

	public WBTVService(AbstractActivity context) {
		this.context = context;
	}
		
	public Request getTVRoot() {
		GetRequest request = new GetRequest(context, TV_URL, false);
		
		request.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
		send(context, request);
		return request;
	}
	
	@Override
	public void result(int httpResultCode, String result) {
		super.result(httpResultCode, result);
		
		Ln.d("Received Channel info " + httpResultCode + " : " + result);
		
		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			EventBus.getDefault().post(parseChannels(result));
		} else
			EventBus.getDefault().post(new WBError(httpResultCode));
	}

	private Channels parseChannels(String result) {
		return new ChannelParser().create(result);
	}	
}
