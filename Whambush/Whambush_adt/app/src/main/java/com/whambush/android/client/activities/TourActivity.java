package com.whambush.android.client.activities;

import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.whambush.android.client.AbstractWhambushActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.domain.country.Country;
import com.whambush.android.client.view.listener.CountrySelectionListener;

public class TourActivity extends AbstractWhambushActivity {

	private static final String ACTIVITY_ID = "Tour";

	@InjectView(R.id.welcome_choose_country)
	TextView choose_country_label;
	
	@InjectView(R.id.welcome_choose_country_txt)
	TextView choose_country_text;
	
	@InjectView(R.id.country)
	TextView countryView;
	
	@InjectView(R.id.accept_country)
	ImageView accept;
	
	private Country country;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		super.onCreate(savedInstanceState, R.layout.activity_init_country, false);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if (country != null)
			countryView.setText(country.getName());
	}
	

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return ACTIVITY_ID;
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		if (country != null)
			outState.putSerializable(Country.KEY, country);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		
		if (savedInstanceState != null) {
			country = (Country)savedInstanceState.getSerializable(Country.KEY);
		}
	}

	
	public void selectCountry(View view) {
		showProgressBar();
		showCountrySelection(new CountrySelectionListener() {
			
			@Override
			public void selected(Country item) {
				country = item;
				countryView.setText(item.getName());
				accept.setImageResource(R.drawable.accept_profile_pic);
			}
		});
	}

	public void countryChoosed(View view) {
		if (country != null)
			finish();
	}
	
	@Override
	public void finish() {
		if (country == null) {
			setResult(RESULT_CANCELED);
		} else {
		
			Intent data = new Intent();
			data.putExtra(Country.KEY, country.getCountry());
			setResult(RESULT_OK, data);
		}
		
		super.finish();
	}
	
	
	@Override
	public void onBackPressed() {
		return;
	}

}

