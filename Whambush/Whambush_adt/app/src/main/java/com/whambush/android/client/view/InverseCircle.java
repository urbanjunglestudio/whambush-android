package com.whambush.android.client.view;

import roboguice.util.Ln;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.whambush.android.client.R;

public class InverseCircle extends View {

	Paint paint = new Paint();
	private float scale = 2f;
	
	public InverseCircle(Context context) {
		this(context, null);
	}
	
	public InverseCircle(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public InverseCircle(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		Bitmap bkg = getBackground(canvas);
		Bitmap circle = getCircle(canvas);
		canvas.drawBitmap(getMaskedBitmap(getContext().getResources(), bkg, circle), 0, 0, paint);
	}
	
	
	private Bitmap getBackground(Canvas canvas) {
		int w = canvas.getWidth(), h = canvas.getHeight();

		Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
		Bitmap bmp = Bitmap.createBitmap(w, h, conf);
		Canvas c = new Canvas(bmp);
		paint.setColor(getContext().getResources().getColor(R.color.image_background_color));
		c.drawRect(0, 0, w, h, paint);
		return bmp;
	}

	private Bitmap getCircle(Canvas canvas) {
		int w = getWidth(), h = getHeight();

		
		Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
		Bitmap bmp = Bitmap.createBitmap(w, h, conf);
		Canvas c = new Canvas(bmp);
		paint.setColor(getContext().getResources().getColor(R.color.black));
		c.drawCircle(w/2, h/2, w/scale, paint);
		return bmp;
	}

	
	public Bitmap getMaskedBitmap(Resources res, Bitmap source, Bitmap mask) {
		  BitmapFactory.Options options = new BitmapFactory.Options();
		  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		    options.inMutable = true;
		  }
		  options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		  Bitmap bitmap;
		  if (source.isMutable()) {
		    bitmap = source;
		  } else {
		    bitmap = source.copy(Bitmap.Config.ARGB_8888, true);
		    source.recycle();
		  }
		  bitmap.setHasAlpha(true);
		  Canvas canvas = new Canvas(bitmap);
		  Paint paint = new Paint();
		  paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
		  canvas.drawBitmap(mask, 0, 0, paint);
		  mask.recycle();
		  return bitmap;
		}

	public float scale(float scale) {
		this.scale = Math.min(Math.max(scale, 2f), 10f);
		invalidate();
		return this.scale;
	}

	public int getTopClipHeight() {
		int w = getWidth(), h = getHeight();
		Ln.d("%d,%d", w, h);
		return (h / 2) - (w / 2);
	}
	
	public int getBottomClipHeight() {
		return getTopClipHeight();
	}

	
}
