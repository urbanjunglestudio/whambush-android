package com.whambush.android.client.domain.user;

import com.google.gson.annotations.SerializedName;

public enum UserType {

	@SerializedName("10")
	GUEST,
	
	@SerializedName("0")
	NORMAL,
	
	@SerializedName("1")
	ADMIN;
	
}
