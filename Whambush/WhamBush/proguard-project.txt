# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:

-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}

-dontwarn java.awt.**,javax.swing.**,java.beans.**,javax.jnlp.**,javax.xml.bind.**,org.joda.**
-dontwarn org.w3c.dom.bootstrap.**,javax.ws.rs.**,javax.activation.DataHandler,net.spy.memcached.**
-dontwarn net.sf.ehcache.**,org.ietf.jgss.**,javax.servlet.**,org.apache.commons.**,org.jdesktop.application.**
-dontwarn com.coremedia.iso.gui.**,org.codehaus.jackson.xc.**,net.hockeyapp.android.UpdateActivity
-dontwarn org.apache.log.**
-dontwarn org.apache.log4j.**,org.apache.avalon.**,android.net.http.AndroidHttpClient


-keep public class com.whambush.android.client.domain.**
-keepclassmembers public class com.whambush.android.client.domain.** {
  *;
}

# HockeyApp specific config

-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable


-keep public class javax.net.ssl.**
-keepclassmembers public class javax.net.ssl.** {
  *;
}

-keep public class org.apache.http.**
-keepclassmembers public class org.apache.http.** {
  *;
}

-keepclassmembers class net.hockeyapp.android.UpdateFragment { 
  *;
}


# Play services

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}