package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.whambush.android.client.Whambush;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.VideoService;
import com.whambush.android.client.service.listeners.VideoServiceListener;

public class SingleVideoForwarderActivity extends ForwarderActivity {

	public static final String TO_THE_VIDEO_ID = "toVideoId";

	@Override
	protected void startLoading() {
		new VideoService(this).load((String)getParameter(TO_THE_VIDEO_ID), new VideoServiceListener() {

			@Override
			public void connectionError() {
				SingleVideoForwarderActivity. this.connectionError();

			}

			@Override
			public void videoData(VideoEntry video) {
				Map<String, Serializable> params = new HashMap<String, Serializable>();
				params.put(VideoEntry.KEY, video.getId());
				Whambush.get(VideoEntryDao.class).add(video);
				initBackToMainInActivity(params);

				startActivity(SingleVideoActivity.class, params);
			}

			@Override
			public void error(int httpResult) {
				SingleVideoForwarderActivity.this.error(httpResult);
			}

			@Override
			public void deleted() {
			}

		});
	}

	@Override
	protected void getParameters(Map<String, Serializable> params) {
		params.put(TO_THE_VIDEO_ID, (String)getParameter(TO_THE_VIDEO_ID));
	}
}
