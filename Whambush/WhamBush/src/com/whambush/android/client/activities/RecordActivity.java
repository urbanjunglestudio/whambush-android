package com.whambush.android.client.activities;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import roboguice.inject.InjectResource;
import roboguice.util.Ln;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.whambush.android.client.R;
import com.whambush.android.client.camera.AbstractCameraFragment;
import com.whambush.android.client.camera.VideoCombinerException;
import com.whambush.android.client.camera.WBCameraHost;
import com.whambush.android.client.camera.WBVideoCameraFragment;
import com.whambush.android.client.camera.WBVideoCameraHost;
import com.whambush.android.client.dialog.DialogButtons;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.util.FileUtil;
import com.whambush.android.client.util.ImageFilePath;

public class RecordActivity extends AbstractWhambushCameraActivity {

	private static final String ACTIVITY_ID = "Record";

	private static final int CHOOSE_FROM_GALLERY = 1;
	
	public static final String BACK_ACTIVITY = "BACK_ACTIVITY";
	public static final String DONE_ACTIVITY = "DONE_ACTIVITY";
	
	public static final String FILE = "RECORDED_FILE";
	
	private static final String CURRENT_DURATION = "CURRENT_DURATION";
	
	private static final long DEFAULT_DELAY = 1000;
	
	private static final int MAX_DURATION_IN_MSECONDS = 300;
	
	private int duration = 0;
	
	private TimerTask task;
	
	private Timer timer;
	
	private boolean stayInActivity = false;
		
	private Class<? extends Activity> backActivity;
	
	private Class<? extends Activity> doneActivity;

	private Mission mission;

	@InjectResource(R.string.GENERAL_ERROR_UNKNOWN)
	private String GENERAL_ERROR_UNKNOWN;

	private volatile boolean waitForAWhile = false;

	private Timer waitTimer;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		if (getIntent().getExtras() != null) {
			backActivity = (Class<? extends Activity>)getIntent().getExtras().get(BACK_ACTIVITY);
			doneActivity = (Class<? extends Activity>)getIntent().getExtras().get(DONE_ACTIVITY);
		}

		mission = (Mission)getParameter(Mission.KEY);
		
		super.onCreate(savedInstanceState, R.layout.activity_record2);
		
		startWaitTimer(true);
	}

	@Override
	public WBCameraHost getCameraHost() {
		if (wbCameraHost == null)
			wbCameraHost = new WBVideoCameraHost(this, this);
		
		return wbCameraHost;
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		getCameraHost().restore(savedInstanceState);

		duration = savedInstanceState.getInt(CURRENT_DURATION);
		savedInstanceState.get(Mission.KEY);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(Mission.KEY, mission);
		
		getCameraHost().saveInstanceState(outState);
		outState.putInt(CURRENT_DURATION, duration);
	}
	
	@Override
	protected String getDetailedActivityNameForAnalysis() {
		if (mission != null)
			return ACTIVITY_ID + ":mission=" + mission.getId();
		else
			return ACTIVITY_ID;
	}
	
	public void cameraClicked(View view) {
		
		if (isWaitForAWhile())
			return;
		
		if (isRecording())
			stopRecording();
		else
			startRecording();
	}
	
	public synchronized boolean isWaitForAWhile() {
		return waitForAWhile;
	}

	@Override
	protected void stopRecording() {
		startWaitTimer(true);
		
		super.stopRecording();

		task.cancel();
		timer.cancel();

		((WBVideoCameraHost)getCameraHost()).calculateLatestLength();
	
		duration = setVideoLength(((WBVideoCameraHost)getCameraHost()).getRecordingLengths());
		updateView(duration);
	}
	
	protected void setTime(int duration) {
		WBVideoCameraFragment current = (WBVideoCameraFragment)getCurrentCameraView();
		current.setTime(duration);
	}
	
	private int setVideoLength(List<Integer> recordingLengths) {
		WBVideoCameraFragment current = (WBVideoCameraFragment)getCurrentCameraView();
		return current.setDurations(recordingLengths);
	}

	@Override
	protected void startRecording() {
		if (duration >= MAX_DURATION_IN_MSECONDS)
			return;
		
		super.startRecording();
		
		startTimer();
		startWaitTimer(false);
	}
	
	private void startTimer() {
		timer = new Timer();
		task = new UpdateStatus();
		timer.scheduleAtFixedRate(task, 0, 100);
	}
	
	private void startWaitTimer(boolean showProgress) {
		if (showProgress)
			showProgressBar();

		setWaitForAWhile(true);
		waitTimer = new Timer();
		TimerTask slowdownTask = new WaitTimer();
		waitTimer.schedule(slowdownTask, DEFAULT_DELAY);
	}
	
	public synchronized void setWaitForAWhile(boolean waitForAWhile) {
		this.waitForAWhile = waitForAWhile;
	}
	
	@Override
	protected AbstractCameraFragment getNewCameraFragment(boolean b) {
		 return new WBVideoCameraFragment(b);
	}
	
	@Override
	public void recordingFailed() {
		getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(GENERAL_ERROR_UNKNOWN));
	}

	public void undo(View view) {
		((WBVideoCameraFragment)getCurrentCameraView()).startUndo();
	}

	public void doActualUndo(View view) {
		((WBVideoCameraHost)getCameraHost()).removeLast();

		((WBVideoCameraFragment)getCurrentCameraView()).updateView(((WBVideoCameraHost)getCameraHost()).getRecordingLengths());
		duration =  setVideoLength(((WBVideoCameraHost)getCameraHost()).getRecordingLengths());
	}

	public void back(View view) {
		onBackPressed();
	}
	
	@Override
	public void onBackPressed() {

		if (isWaitForAWhile())
			return;

		if (getCurrentCameraView().isRecording())
			stopRecording();

		if (duration > 0) {
			askToBeSure();
			return;
		}

		super.onBackPressed();
	}

	@Override
	public void enableAll() {
		if (stayInActivity) {
			stayInActivity = false;
			return;
		}

		if (backActivity == null)
			super.onBackPressed();
		else
			startActivity(backActivity);
	}

	
	private void askToBeSure() {

		DialogButtons buttons = new DialogButtons();
		buttons.add(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.GENERAL_YES), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {
				RecordActivity.super.onBackPressed();
			}
		});
		buttons.add(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.GENERAL_NO), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		getDialogHelper().displayDialog(getDialogHelper().createBasicDialog(buttons, getString(R.string.CAMERA_WARNING_VIDEO_NOT_UPLOADED), null));
	}
	
	
	public void browseFromGallery(View view) {
		showProgressBar();
		Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
		mediaChooser.setType("video/*");
		startActivityForResult(mediaChooser, CHOOSE_FROM_GALLERY);
	}
	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		hideProgressBar();
		if (requestCode == CHOOSE_FROM_GALLERY) {
			if (resultCode == Activity.RESULT_OK) {
				Uri selectedVideoLocation = data.getData();

				String realPath = ImageFilePath.getPath(this, selectedVideoLocation);
				Ln.d("Resolved!!! : " + realPath);
				File f = new File(realPath);
				if (!f.exists()) {
					realPath = new FileUtil().getRealPathFromURI(RecordActivity.this, selectedVideoLocation);
					Ln.d("Real path parsed " + realPath);
					boolean found = false;
					if(realPath != null) {
						found = new File(realPath).exists();
					}
					if (!found) {
						stayInActivity = true;
						getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_UNKNOWN)));
						return;
					}

					selectedVideoLocation = Uri.parse(realPath);
				}
				
				Integer length = ((WBVideoCameraHost)wbCameraHost).getMovieLength(realPath);
				if (length > MAX_DURATION_IN_MSECONDS) {
					stayInActivity = true;
					getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getResources().getString(R.string.CAMERA_WARNING_VIDEO_TOO_LONG)));
					return;
				} else if (length <= 0) {
					stayInActivity = true;
					getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_UNKNOWN)));
					return;
				}

				Map<String, Serializable> params = getActivityParameters();
				params.put(FILE, realPath);
				fwd(params);

			}
		}
	}
	
	public void doneClicked(View view) {
		Ln.d("Done Clicked");
		showProgressBar();
		finishRecording();
	}
	
	private void finishRecording() {
		try {
			((WBVideoCameraHost)getCameraHost()).finalizeVideos();
		} catch (VideoCombinerException e) {
			e.printStackTrace();
			getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_UNKNOWN)));
			return;
		} finally {
			hideProgressBar();
			cleanTempFiles();
		}

		if (doneActivity == null)
			super.onBackPressed();
		else
			startActivity(doneActivity, getActivityParameters());
	}

	private void cleanTempFiles() {
		FileUtil fileUtil = new FileUtil();
		fileUtil.emptyDirectory(fileUtil.getTempDirectory());
	}
	
	private void fwd(Map<String, Serializable> params) {
		if (doneActivity == null)
			onBackPressed();
		else
			startActivity(doneActivity, params);
	}
	
	private Map<String, Serializable> getActivityParameters() {
		Map<String, Serializable> params = new HashMap<String, Serializable>();

		if (mission != null)
			params.put(Mission.KEY, mission);

		params.put(FILE, ((WBVideoCameraHost)wbCameraHost).getFinalFilename());

		return params;
	}

	
	private class WaitTimer extends TimerTask {

		public void run() {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					setWaitForAWhile(false);
					hideProgressBar();
				}
			});
		}
	}
	
	private class UpdateStatus extends TimerTask {

		public void run() {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					setTime(duration);
					if (duration >= MAX_DURATION_IN_MSECONDS) { 
						stopRecording();
						return;
					}
				}
			});
			duration += 1;
		}
	}


	
}
