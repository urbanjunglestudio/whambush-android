package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.content.Context;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.whambush.android.client.R;
import com.whambush.android.client.dialog.OpenLinkDialog;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.view.TimeView;
import com.whambush.android.client.view.list.MissionVideoListView;
import com.whambush.android.client.view.listener.PaginateListListener;

public class MissionFeedActivity extends AbstractVideoPlayActivity implements PaginateListListener {

	private static final String TIME_DIFF = "TIME_DIFF";

	private static final String OPEN_LINK_DIALOG_FRAGMENT = "OpenLinkDialogFragment";
	
	private static final String ACTIVITY_ID = "MissionFeed";

	private Mission mission;
	
	@InjectView(R.id.list)
	MissionVideoListView list;
	
	@InjectView(R.id.timeView)
	TimeView time;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		super.onCreate(savedInstanceState, R.layout.activity_single_mission, false);

		setupReloadImage();

		showProgressBar();
		
		mission = (Mission)getParameter(Mission.KEY);
		list.setListener(this);

		list.setActivity(this, mission);
		list.setListener(this);
		
		registerClickListener();
		
		showBackButton();
		hideShareButton();
		time.setTime(mission.getEndAt());
	}
	
	private void registerClickListener() {
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				VideoEntry video = list.getVideoListAdapter().getVideo(view);

				if (video == null)
					return;

				if (video.isProcessed()) {
					showVideo(video);
				}
			}
		});	
	}
	
	public void showVideo(VideoEntry video) {

		Map<String, Serializable> params = new HashMap<String, Serializable>();
		params.put(VideoEntry.KEY, video.getId());

		startActivity(SingleVideoActivity.class, params);
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");		
		super.onSaveInstanceState(outState);
		outState.putSerializable(Mission.KEY, mission);
		outState.putLong(TIME_DIFF, time.getDiff());
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.containsKey(Mission.KEY))
			mission = (Mission)savedInstanceState.get(Mission.KEY);
		if (savedInstanceState.containsKey(TIME_DIFF))
			time.setDiff(savedInstanceState.getLong(TIME_DIFF));
	}

	protected void setPendingTransition() {
		overridePendingTransition(R.anim.activity_anim_in, R.anim.activity_anim_out);
	}

	@Override
	protected void onResume() {
		
		if (videoPlayer == null) {
			super.onResume();
			startActivity(SplashScreen.class);
			return;
		}
		if (mission != null)
			setupVideoView();
		
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mission != null && mission.getVideo() != null && videoPlayer != null)
			videoPlayer.release();
	}

	@Override
	public void onBackPressed() {
		if (mission != null && mission.getVideo() != null) {
			videoPlayer.stop();
			videoPlayer.release();
		}

		if (getParameter(SingleVideoForwarderActivity.BACK_TO_MAIN) != null)
			startActivity(MainActivity.class);
		else
			super.onBackPressed();

		overridePendingTransition(R.anim.activity_anim_out, R.anim.activity_anim_back_out);
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return ACTIVITY_ID + ":mission=" + mission.getId();
	}

	protected LayoutInflater getInflater() {
		return (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void usernameClicked(View view) {
		videoPlayer.stop();
		videoPlayer.release();

		startUserActivity(mission.getAddedBy());
	}

	private void startUserActivity(User user) {
		Map<String, Serializable> params = new HashMap<String, Serializable>();

		if (isSelf(user)) {
			startActivity(UserActivity.class, params);
		} else {
			params.put(User.KEY, user);
			startActivity(OtherUserActivity.class, params);
		}
	}

	private void setupVideoView() {
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		if (mission.getVideo() == null) {
			if (mission.getMissionImage1() != null && mission.getMissionImage1().length() > 0)
				setupThumbnail(mission.getMissionImage1());
			else if (mission.getAddedBy().getId().equals("1")) {
				setupThumbnail(R.drawable.default1);
			} else
				setupThumbnail(R.drawable.default3);
			
		} else
			setupVideo(mission.getVideo(), mission.getMissionImage1());
	}

	public void shitTheVideo(View view) {
		list.shitTheVideo(view);
		showProgressBar();
	}


	public void bananaTheVideo(View view) {
		list.bananaTheVideo(view);
		showProgressBar();
	}

	public void liked(LikesAndDislikes result) {
		list.liked(result);
		hideProgressBar();
	}

	public void unliked(LikesAndDislikes result) {
		list.unliked(result);
		hideProgressBar();
	}

	public void disliked(LikesAndDislikes result) {
		list.disliked(result);
		hideProgressBar();
	}

	public void undisliked(LikesAndDislikes result) {
		list.undisliked(result);
		hideProgressBar();
	}

	public void shareTheVideo(View view) {
		list.shareTheVideo(view);
	}

	@Override
	protected void setComponentVisibility(int visibility) {
		findViewById(R.id.headerLayout_ref).setVisibility(visibility);
		findViewById(R.id.list).setVisibility(visibility);
		findViewById(R.id.footer).setVisibility(visibility);
		time.setVisibility(visibility);
		hideProgressBar();
	}

	@Override
	public void reload(View view) {
		Ln.d("Start to reload the comments");
		startReloadAnimation();
		list.reload();
	}

	@Override
	public void loaded() {
		Ln.d("List loaded");
		stopReloadAnimation();
	}
	
	public void changeTime(View view) {
		time.change();
	}
	
	public void openRules(View view) {
		Uri url = Uri.parse(mission.getMissionUrl() + "/rules");
		OpenLinkDialog openDialog = new OpenLinkDialog(url);
		openDialog.show(getFragmentManager(), OPEN_LINK_DIALOG_FRAGMENT);
	}
	
	public void submitToMission(View view) {
		if (getVideoService().getStatus() == Status.FINISHED) {
			HashMap<String, Serializable> params = new HashMap<String, Serializable>();
			params.put(Mission.KEY, mission);
			goToRecordActivity(params);
		} else
			askToCancelUpload();		
	}
	
	@Override
	public void recordButtonPressed(View button) {
		submitToMission(button);
	}
}



