package com.whambush.android.client.activities;

import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.whambush.android.client.R;
import com.whambush.android.client.Selector;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.adapter.CountryListAdapter;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.country.Countries;
import com.whambush.android.client.domain.country.Country;
import com.whambush.android.client.view.list.CountryListView;

public class CountryChooserActivity extends WhambushActivity {

	private static final String ACTIVITY_ID = "CountryChooser";

	private Country selectedCountry;

	@InjectView(R.id.list)
	private CountryListView list;
	
	@InjectView(R.id.reloadHolder)
	private View reloadHolder;
	
	private CountryListAdapter adapter;

	private Selector selector;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Ln.d("Starting Country Chooser");

		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		super.onCreate(savedInstanceState, R.layout.activity_country_chooser, false);

		adapter = new CountryListAdapter(this);
		adapter.setCurrentlySelected((String)getParameter(Country.KEY));
		selector = (Selector)getParameter(Selector.KEY);
		list.setAdapter(adapter);

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				selectedCountry = adapter.getItem(position);
				Ln.d("country selected %s", selectedCountry);
				setResult();
				finish();
				CountryChooserActivity.this.overridePendingTransition(R.anim.none,R.anim.none);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		setCurrentSelector(selector);

		if (adapter.getCount() == 0) {

			Countries countries = Whambush.get(ConfigImpl.class).getCountries();

			for (Country country : countries.getResults()) {
				if (country.isSupported()) {
					adapter.add(country);
				}
			}

			adapter.add(getGlobalCountry());
			adapter.notifyDataSetChanged();
		}
		
		reloadHolder.setVisibility(View.GONE);
	}

	private Country getGlobalCountry() {
		Country country = new Country();

		country.setCountry("ZZ");
		country.setName("Global");

		return country;
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return ACTIVITY_ID;
	}


	@Override
	public void onBackPressed() {
		setResult();
		finish();
		CountryChooserActivity.this.overridePendingTransition(R.anim.none,R.anim.none);
	}
	
	private void setResult() {
		Ln.d("Finish the country selection with " + selectedCountry);
		if (selectedCountry == null) {
			setResult(RESULT_CANCELED);
		} else {
			Intent data = new Intent();
			data.putExtra("COUNTRY", selectedCountry.getCountry());
			setResult(RESULT_OK, data);
		}
	}

	@Override
	public void reload(View view) {

	}
}
