package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import roboguice.util.Ln;
import android.app.Activity;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.whambush.android.client.AbstractWhambushActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.domain.country.Countries;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.user.InvalidLoginInfo;
import com.whambush.android.client.domain.user.LoginInfo;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.DeviceInfoService;
import com.whambush.android.client.service.GCMService;
import com.whambush.android.client.service.LoginService;
import com.whambush.android.client.service.MissionsSlugService;
import com.whambush.android.client.service.SoftwareVersionService;
import com.whambush.android.client.service.UserService;
import com.whambush.android.client.service.VideoService;
import com.whambush.android.client.service.listeners.GCMServiceListener;
import com.whambush.android.client.service.listeners.LoginServiceListener;
import com.whambush.android.client.service.listeners.MissionsSlugServiceListener;
import com.whambush.android.client.service.listeners.UserServiceListener;
import com.whambush.android.client.service.listeners.VideoServiceListener;
import com.whambush.android.client.util.Destination;
import com.whambush.android.client.util.URLParser;


public class SplashScreen extends AbstractWhambushActivity implements LoginServiceListener {

	private static final String ACTIVITY_ID = "Splash";
	private Destination destination;

	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState, R.layout.splash_screen, true);
		} catch (RuntimeException ex) {
			finish();
			return;
		}

		if (!isValidServices())
			Whambush.createServices();

		logDeviceInfo();

		Uri data = getIntent().getData();
		if (data != null) {
			destination = URLParser.parse(data);
			Ln.d("*** INTENT data is NOT NULL ***");
		} else
			Ln.d("*** INTENT data is NULL");
		doLogin();
	}

	private void doLogin() {
		Ln.i("DO Login");
		ConfigImpl config = Whambush.get(ConfigImpl.class);
		String username = config.getUsername();
		String password = config.getPassword();

		LoginService loginService = new LoginService(this);

		loginService.createLoginRequest(username, password, config.getGuestId(), 
				new DeviceInfoService().getInfo(), new SoftwareVersionService(this).getVersion(), this);
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return ACTIVITY_ID;
	}

	private void logDeviceInfo() {
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
	}

	@Override
	public void onBackPressed() {
		// Block the backbutton
	}

	@SuppressWarnings("unchecked")
	protected void moveToNextView() {

		Countries countries = Whambush.get(ConfigImpl.class).getCountries();
		if (countries == null) {
			new Timer().schedule(new TimerTask() {

				@Override
				public void run() {
					Ln.i("Waiting for countries");
					moveToNextView();
				}
			}, 500);
			return;
		}

		if (destination == null) {
			Ln.d("*** DESTINATION IS NULL ***");
			Class<? extends Activity> act = (Class<? extends Activity>)getParameter(TO_ACTIVITY);

			if (act != null) {
				startActivity(act, getIntent().getExtras(), true);
			} else {
				startActivity(MissionsActivity.class, true);
				setPendingTransition();
			}
		} else {
			Ln.d("*** DESTINATION IS NOT NULL ***");
			getTheSlugData();
		}
	}

	private void getTheSlugData() {
		Ln.d("*** DESTINATION IS " + destination.toString() + " ***");
		if (destination.getActionStr().equalsIgnoreCase("v")) {
			getVideo();
		} else if (destination.getActionStr().equalsIgnoreCase("u")) {
			getUser();
		} else if (destination.getActionStr().equalsIgnoreCase("m")) {
			getMission();
		} else {
			Ln.d("*** DESTINATION IS ???? ***");
			destination = null;
			moveToNextView();
		}
	}

	private void getMission() {
		Ln.d("*** GET THE MISSION ***");
		new MissionsSlugService(this).findMissionSlug(destination.getSlug(), new MissionsSlugServiceListener() {

			@Override
			public void error(int httpResultCode) {
				destination = null;
				moveToNextView();
			}

			@Override
			public void connectionError() {
				SplashScreen.this.connectionError();
			}

			@Override
			public void mission(Mission mission) {
				Map<String, Serializable> params = new HashMap<String, Serializable>();
				params.put(Mission.KEY, mission);

				startActivity(MissionFeedActivity.class, params, true);
				overridePendingTransition(R.anim.activity_anim_back_out, R.anim.activity_anim_out);
			}
		});
	}

	private void getUser() {
		Ln.d("*** GET THE MISSION USER ***");
		new UserService(this).findUserSlug(destination.getSlug(), new UserServiceListener() {

			@Override
			public void error(int httpResultCode) {
				destination = null;
				moveToNextView();
			}

			@Override
			public void connectionError() {
				SplashScreen.this.connectionError();
			}

			@Override
			public void user(User parsedUser) {
				Map<String, Serializable> params = new HashMap<String, Serializable>();

				if (isSelf(parsedUser)) {
					startActivity(UserActivity.class, params, true);
				} else {
					params.put(User.KEY, parsedUser);
					startActivity(OtherUserActivity.class, params, true);
				}
			}
		});
	}

	private void getVideo() {
		Ln.d("*** GET THE VIDEO ***");
		new VideoService(this).findVideoSlug(destination.getSlug(), new VideoServiceListener() {

			@Override
			public void error(int httpResultCode) {
				destination = null;
				moveToNextView();
			}

			@Override
			public void connectionError() {
				SplashScreen.this.connectionError();
			}

			@Override
			public void videoData(VideoEntry video) {
				Whambush.get(VideoEntryDao.class).add(video);
				
				Map<String, Serializable> params = new HashMap<String, Serializable>();
				params.put(VideoEntry.KEY, video.getId());

				startActivity(SingleVideoActivity.class, params, true);
			}

			@Override
			public void deleted() {
				// NOT NEEDED
			}
		});
	}

	@Override
	public void connectionError() {
		getDialogHelper().displayDialog(
				getDialogHelper().createErrorDialog(getString(R.string.GENERAL_ERROR_NO_NETWORK), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						moveToNextView();
					}

				}));

	}


	@Override
	public void error(int httpResultCode) {
		getDialogHelper().displayDialog(
				getDialogHelper().createErrorDialog(getString(R.string.GENERAL_ERROR_UNKNOWN), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						moveToNextView();
					}

				}));

	}


	@Override
	public void loggedIn(LoginInfo loginInfo) {
		Ln.d("Logged In %s", loginInfo);

		ConfigImpl config = Whambush.get(ConfigImpl.class);
		config.setGuestId(loginInfo.getGuestId());
		config.setLoginInfo(loginInfo);
		config.setAuthenticationToken(loginInfo.getAuthenticationToken());

		new GCMService(this).getReqID(new GCMServiceListener() {

			@Override
			public void error(int httpResultCode) {
				moveToNextView();
			}

			@Override
			public void connectionError() {
				moveToNextView();
			}

			@Override
			public void unregistered() {
				moveToNextView();
			}

			@Override
			public void registered() {
				moveToNextView();
			}
		});
	}

	@Override
	public void invalidLogin(InvalidLoginInfo info) {
		getDialogHelper().displayDialog(
				getDialogHelper().createErrorDialog(getString(R.string.GENERAL_ERROR_UNKNOWN), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Ln.e("Invalid login in splashscreen");
						ConfigImpl config = Whambush.get(ConfigImpl.class);
						config.setGuestId("");
						config.setUsername("");
						config.setPassword("");
						doLogin();
					}

				}));
	}
}