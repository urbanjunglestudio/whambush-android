package com.whambush.android.client.activities;

import java.util.Timer;
import java.util.TimerTask;

import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.whambush.android.client.R;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.video.VideoPlayer;

public abstract class AbstractVideoPlayActivity extends WhambushActivity {

	private LayoutParams params;
	
	@InjectView(R.id.videoPlayer)
	protected VideoPlayer videoPlayer;
	
	private Timer timer = new Timer();

	private boolean videoPlayerSetup = false;

	@Override
	protected void onCreate(Bundle savedInstanceState, int activity, boolean fullScreen) {
		super.onCreate(savedInstanceState, activity, fullScreen);

		videoPlayer.inflate(this);
		fillWidthKeepAspect(videoPlayer);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (videoPlayerSetup && videoPlayer != null && !videoPlayer.isReleased())
			videoPlayer.pause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (videoPlayerSetup && videoPlayer != null && !videoPlayer.isReleased())
			videoPlayer.release();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("layout_y", params.height);
		outState.putInt("layout_x", params.width);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.containsKey("layout_x") && savedInstanceState.containsKey("layout_y"))
			params = new LayoutParams(savedInstanceState.getInt("layout_x"), savedInstanceState.getInt("layout_y"));
	}

	@Override
	protected void onResume() {
		Ln.d("Resuming " + videoPlayer );
		super.onResume();
		if (!isValidServiceAndLoggedIn()) {
			return;
		}
		
		if (videoPlayer == null) {
			videoPlayer = (VideoPlayer) findViewById(R.id.videoPlayer);
			videoPlayer.inflate(this);
			fillWidthKeepAspect(videoPlayer);
		}

		if (this.params == null) {
			fillWidthKeepAspect(videoPlayer);
		}
		
		doOnResume();
	}

	protected void doOnResume() {
		validateOrientation();
		startOrientationResetTimer();
	}

	private void startOrientationResetTimer() {
		TimerTask updateBall = new TimerTask() {

			@Override
			public void run() {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			}
		};
		timer.schedule(updateBall, 2000);

	}

	private void validateOrientation() {
		int orientation = getResources().getConfiguration().orientation;
		if (orientation == Configuration.ORIENTATION_LANDSCAPE)
			toFullScreen();
		else
			fromFullScreen();
	}

	protected void setupThumbnail(String url) {
		videoPlayer.setImageView(R.id.videoViewThumbnail, url);
	}
	
	protected void setupThumbnail(int resource) {
		videoPlayer.setImageView(R.id.videoViewThumbnail, resource);
	}
	
	protected void setupVideo(VideoEntry video, String thumbnail) {
		videoPlayerSetup = true;
		videoPlayer.setKeepScreenOn(true);
		videoPlayer.setup(video);
		if (thumbnail != null && thumbnail.length() > 0)
			videoPlayer.setImageView(R.id.videoViewThumbnail, thumbnail);
	}
	
	protected void setupVideo(VideoEntry video) {
		setupVideo(video, null);
	}

	protected void setVideoURI(Uri uri) {
		videoPlayer.setVideoURI(uri);
		videoPlayer.setup();
	}

	protected void setThumbnail(Bitmap bmThumbnail) {
		videoPlayer.setThumbnail(bmThumbnail);
	}

	protected void setVisibleBufferStatus(boolean visibility) {
		videoPlayer.setVisibleBufferStatus(visibility);
	}
	
	protected void setShareVisible(boolean visibility) {
		videoPlayer.setShareVisible(visibility);
	}
	
	protected void setPlayButton(int playButton) {
		videoPlayer.setPlayButton(playButton);
	}
	
	protected int getPlayButton() {
		return videoPlayer.getPlayButton();
	}
	
	protected void hideShareButton() {
		videoPlayer.setShareVisible(false);
	}

	protected int getPauseButton() {
		return videoPlayer.getPauseButton();
	}
	
	protected void setPauseButton(int pauseButton) {
		videoPlayer.setPauseButton(pauseButton);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);


		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			toFullScreen();
			videoPlayer.setSurfaceSize();
		}
		else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			fromFullScreen();
			videoPlayer.setSurfaceSize();
		}
	}

	private void toFullScreen() {
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		DisplayMetrics displaymetrics = getDefaultMetrics();
		setVideoplayerSize(displaymetrics.widthPixels, displaymetrics.heightPixels);

		videoPlayer.toFullScreen(true);

		setComponentVisibility(View.GONE);
	}

	private DisplayMetrics getDefaultMetrics() {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		return displaymetrics;
	}
	
	protected void fromFullScreen() {
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		if (params == null) {
			fillWidthKeepAspect(videoPlayer);
		}
		
		setVideoplayerSize(params.height, params.width); // X - Y Swapped due orientation
		fillWidthKeepAspect(videoPlayer);

		videoPlayer.toFullScreen(false);
		setComponentVisibility(View.VISIBLE);
	}


	private void fillWidthKeepAspect(View view) {
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int x = size.x;

		params = view.getLayoutParams();
		params.width = x;
		params.height = (int)(((float)x) / (16f/9f));
		view.setLayoutParams(params);
	}

	private void setVideoplayerSize(int width, int height) {
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)videoPlayer.getLayoutParams();
		params.width = width;
		params.height = height;
		params.setMargins(0, 0, 0, 0);
	}

	protected abstract void setComponentVisibility(int visibility);
}
