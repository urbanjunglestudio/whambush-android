package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.service.UserService;
import com.whambush.android.client.service.listeners.UserServiceListener;

public class UserFeedForwarderActivity extends ForwarderActivity {

	@Override
	protected void startLoading() {
		new UserService(this).createLoadUserRequest(new UserServiceListener() {

			@Override
			public void connectionError() {
				UserFeedForwarderActivity. this.connectionError();

			}

			@Override
			public void user(User user) {
				Map<String, Serializable> params = new HashMap<String, Serializable>();
				if (isSelf(user)) {
					startActivity(UserActivity.class, params);
				} else {
					params.put(User.KEY, user);
					startActivity(OtherUserActivity.class, params);
				}
			}

			@Override
			public void error(int httpResult) {
				UserFeedForwarderActivity.this.error(httpResult);
			}

		}, (String)getParameter(User.KEY));
	}

	@Override
	protected void getParameters(Map<String, Serializable> params) {
		params.put(User.KEY, (String)getParameter(User.KEY));
	}

}
