package com.whambush.android.client.activities;

import android.os.Bundle;
import android.view.View;

import com.whambush.android.client.R;
import com.whambush.android.client.Selector;
import com.whambush.android.client.view.list.VideoFeedListView;
import com.whambush.android.client.view.listener.PaginateListListener;

public class MainActivity extends AbstractFeedActivity implements PaginateListListener {

	private static final String ACTIVITY_ID = "Main";

	public static final String PROPERTY_REG_ID = "registration_id";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}
		
		super.onCreate(savedInstanceState, R.layout.activity_main);

		getList().enableSearch(true);

		showProgressBar();
		setupKeyboardListener();
		getList().setActivity(this);
		getList().setListener(this);
		checkCountry();
	}
	
	@Override
	protected void onResume() {
		setCurrentSelector(Selector.MAIN);		
		super.onResume();
	}
	
	@Override
	protected VideoFeedListView getList() {
		return (VideoFeedListView)super.getList();
	}
	
	@Override
	protected void start() {
		getList().loadFeeds();
		super.start();
	}
	
	public void mainButtonPressed(View view) {
		
	}
	
	@Override
	protected String getDetailedActivityNameForAnalysis() {
		if (getList() == null) {
			error();
			return ACTIVITY_ID;
		}
		
		if (getList().getCurrentFeed() != null)
			return ACTIVITY_ID + ":feed="+getList().getCurrentFeed().getName();
		else if (getList().getSearching() != null) {
			return ACTIVITY_ID + ":search=" + getList().getSearching();
		} else
			return ACTIVITY_ID;
	}

	public void currentClicked(View view) {
		getList().startFeedSelection();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();

		overridePendingTransition(R.anim.none, R.anim.none);
	}
	
	@Override
	public void reload(View view) {
		startReloadAnimation();
		getList().reload();
	}
	
	@Override
	public void loaded() {
		stopReloadAnimation();
	}
}
