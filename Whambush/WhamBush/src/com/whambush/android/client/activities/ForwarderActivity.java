package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;

import com.whambush.android.client.AbstractWhambushActivity;
import com.whambush.android.client.R;

public abstract class ForwarderActivity extends AbstractWhambushActivity {

	public static final String BACK_TO_MAIN = "back_to_main";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (!isValidServiceAndLoggedIn()) {
			startLogin();
			super.onCreate(savedInstanceState);
			return;
		}
		
		super.onCreate(savedInstanceState, R.layout.activity_fwd, false);

		showProgressBar();
		startLoading();
	}

	protected void startLogin() {
		Map<String, Serializable> params = new HashMap<String, Serializable>();
		params.put(TO_ACTIVITY, this.getClass());
		getParameters(params);
		startActivity(SplashScreen.class, params);
	}

	@Override
	public void onBackPressed() {
		; // Do nothing!
	}

	protected abstract void startLoading();
	protected abstract void getParameters(Map<String, Serializable> params);

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return null;
	}

	@Override
	protected boolean isStatisticActivity() {
		return false;
	}

	protected void initBackToMainInActivity(Map<String, Serializable> params) {
		params.put(UserFeedForwarderActivity.BACK_TO_MAIN, UserFeedForwarderActivity.BACK_TO_MAIN);
	}

}
