package com.whambush.android.client.activities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import com.whambush.android.client.AbstractWhambushActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.util.BitmapUtil;
import com.whambush.android.client.util.FileUtil;
import com.whambush.android.client.view.InverseCircle;
import com.whambush.android.client.view.TouchImageView;

public class ImageActivity extends AbstractWhambushActivity {

	private static final int MAX_WIDTH = 640;

	private static final String ACTIVITY_ID = "Image";

	private ScaleGestureDetector scaleDetector;
	private float scale = 2f;

	@InjectView(R.id.image)
	TouchImageView image;

	@InjectView(R.id.cover)
	InverseCircle cover;

	private String imageUrl;

	private Bitmap bitmapImage;

	private File theFilename;


	@Override
	public void onCreate(Bundle savedInstanceState) {

		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		super.onCreate(savedInstanceState, R.layout.activity_image, false);
		imageUrl = (String)getParameter("IMAGE");

		scaleDetector = new ScaleGestureDetector(this,new ScaleListener());
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		scaleDetector.onTouchEvent(ev);
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();

		try {
			Ln.d("THE PATH " + imageUrl);
			Bitmap bitImage = BitmapUtil.getCorrectlyOrientedImage(imageUrl);
			image.setImageBitmap(bitImage);

			int top = cover.getTopClipHeight();
			int bottom = cover.getBottomClipHeight();

			bitmapImage = BitmapUtil.addBorders(bitImage, top, 0, bottom, 0, getResources().getColor(R.color.whambushBody));

			image.setImageBitmap(bitmapImage);

		} catch (IOException e) {
			Ln.w(e);
			error(0);
		}
	}

	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("imageUrl", imageUrl);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState != null)
			imageUrl = savedInstanceState.getString("imageUrl");
	}
	
	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return ACTIVITY_ID;
	}


	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			scale /= detector.getScaleFactor();

			Ln.d("SCALE " + scale);

			scale = cover.scale(scale);
			return true;
		}
	}

	public void acceptTheSelection(View view) {

		showProgressBar();

		cropAndResize();

		hideProgressBar();

		finish();
	}

	private Bitmap resize(Bitmap theImg) {
		
		Bitmap createScaledBitmap = Bitmap.createScaledBitmap(theImg, 640, 640, false);
		
		theImg.recycle();
		
		return createScaledBitmap;

	}

	private void cropAndResize() {
		float zoom = image.getCurrentScale();
		float topClip = cover.getTopClipHeight();

		Ln.d("Zoom %f: clip %f", zoom, topClip);
		RectF cut = new RectF();
		cut.right = 0f;
		cut.left = 0f;
		cut.top = topClip * zoom;
		cut.bottom = topClip * zoom;

		Bitmap cropped = BitmapUtil.crop(image.getZoomedRect(), cut, bitmapImage);

		if (cropped.getWidth() > MAX_WIDTH)
			cropped = resize(cropped);
		
		File photoDir = new FileUtil().getPhotoDirectory();
		theFilename = new File(photoDir.getPath() + "/profile_" + System.currentTimeMillis() + ".jpg");
		try {
			OutputStream out = new FileOutputStream(theFilename);
			cropped.compress(Bitmap.CompressFormat.PNG, 80, out);
			out.close();
		} catch (IOException e) {
			getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getString(R.string.GENERAL_ERROR_UNKNOWN)));
		}
		cropped.recycle();
	}

	@Override
	public void finish() {
		if (theFilename == null) {
			setResult(RESULT_CANCELED);
		} else {
			Ln.d("The LENGTH " + theFilename.length());

			Intent data = new Intent();
			data.putExtra("PATH", theFilename.getPath());
			setResult(RESULT_OK, data);
		}
		super.finish();
	} 

	public void cancelTheSelection(View view) {
		finish();
	}

}
