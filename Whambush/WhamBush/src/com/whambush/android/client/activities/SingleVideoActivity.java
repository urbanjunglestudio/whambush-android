package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.adapter.FlagAdapter;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.dialog.DialogButtons;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.comment.Comment;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.CommentService;
import com.whambush.android.client.service.FlagService;
import com.whambush.android.client.service.VideoService;
import com.whambush.android.client.service.listeners.CommentServiceListener;
import com.whambush.android.client.service.listeners.FlagServiceListener;
import com.whambush.android.client.service.listeners.VideoServiceListener;
import com.whambush.android.client.util.TextInputParameters;
import com.whambush.android.client.view.list.CommentListView;
import com.whambush.android.client.view.listener.PaginateListListener;

public class SingleVideoActivity extends AbstractVideoPlayActivity implements VideoServiceListener, PaginateListListener {

	private static final String ACTIVITY_ID = "Single";

	@InjectView(R.id.list)
	CommentListView list;

	private VideoEntry video;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		super.onCreate(savedInstanceState, R.layout.activity_single_video, false);

		setupReloadImage();

		showProgressBar();
		video = Whambush.get(VideoEntryDao.class).getVideoEntry((String)getParameter(VideoEntry.KEY));

		list.setListener(this);

		if (video != null) {
			list.setActivity(this, video);
		} else {
			loadVideo((String)getParameter(VideoEntry.KEY));
		}
	}

	private void loadVideo(String parameter) {
		new VideoService(this).load((String)getParameter(VideoEntry.KEY), this);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
		super.onSaveInstanceState(outState);
		outState.putSerializable(VideoEntry.KEY, video);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.containsKey(VideoEntry.KEY))
			video = (VideoEntry)savedInstanceState.get(VideoEntry.KEY);

	}

	protected void setPendingTransition() {
		overridePendingTransition(R.anim.activity_anim_in, R.anim.activity_anim_out);
	}

	@Override
	protected void onResume() {
		if (videoPlayer == null) {
			super.onResume();
			startActivity(SplashScreen.class);
			return;
		}
		if (video != null)
			setupVideoView();
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (video != null && videoPlayer != null)
			videoPlayer.release();
	}

	@Override
	public void onBackPressed() {
		if (video != null) {
			videoPlayer.stop();
			videoPlayer.release();
		}

		if (getParameter(SingleVideoForwarderActivity.BACK_TO_MAIN) != null)
			startActivity(MainActivity.class);
		else
			super.onBackPressed();

		overridePendingTransition(R.anim.activity_anim_out, R.anim.activity_anim_back_out);
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		return ACTIVITY_ID + ":video=" + video.getId();
	}

	public void usernameInCommentClicked(View view) {

		videoPlayer.stop();
		videoPlayer.release();

		Comment comment = list.getCommentListAdapter().getCommentByUsername(((TextView)view).getText().toString());
		startUserActivity(comment.getUser());
	}

	public void usernameClicked(View view) {
		videoPlayer.stop();
		videoPlayer.release();

		startUserActivity(video.getAddedBy());
	}

	private void startUserActivity(User user) {
		Map<String, Serializable> params = new HashMap<String, Serializable>();

		if (isSelf(user)) {
			startActivity(UserActivity.class, params);
		} else {
			params.put(User.KEY, user);
			startActivity(OtherUserActivity.class, params);
		}
	}

	private void setupVideoView() {
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		setupVideo(video);
	}


	public void commentTheVideo(final View view) {
		if (getLoginInfo().isGuest()) {
			showLoginOrRegister();
		} else {
			TextInputParameters params = new TextInputParameters();
			params.setMaxChars(150);
			params.setMaxLines(6);
			params.setTitle(getResources().getString(R.string.SINGLE_ADD_COMMENT));
			showTextInput(params);
		}
	}

	public void submitComment(View view) {
		EditText text = getEditText(R.id.newComment);
		hideTextInput();
		new CommentService(this).create(video.getId(),	text.getText().toString(), new CommentServiceListener() {

			@Override
			public void error(int httpResultCode) {
				SingleVideoActivity.this.error(httpResultCode);
			}

			@Override
			public void connectionError() {
				SingleVideoActivity.this.connectionError();
			}

			@Override
			public void paginateItems(WhambushList<Comment> items) {
				;
			}

			@Override
			public void commentUploaded() {
				reload(null);
			}
		});
		showProgressBar();
	}

	public void shitTheVideo(View view) {
		list.shitTheVideo(view);
		showProgressBar();
	}


	public void bananaTheVideo(View view) {
		list.bananaTheVideo(view);
		showProgressBar();
	}

	public void liked(LikesAndDislikes result) {
		list.liked(result);
		hideProgressBar();
	}

	public void unliked(LikesAndDislikes result) {
		list.unliked(result);
		hideProgressBar();
	}

	public void disliked(LikesAndDislikes result) {
		list.disliked(result);
		hideProgressBar();
	}

	public void undisliked(LikesAndDislikes result) {
		list.undisliked(result);
		hideProgressBar();
	}

	public void acceptFlag(View view) {
		removeViewFromContent(view);
		hideProgressBar();
	}

	public void toggleOptions(View view) {
		list.toggleOptions(view);
	}

	public void shareTheVideo(View view) {
		list.shareTheVideo(view);
	}

	public void flagTheVideo(View v) {
		FrameLayout rootLayout = (FrameLayout)findViewById(android.R.id.content);
		View.inflate(this, R.layout.view_flag_chooser, rootLayout);

		final View view = rootLayout.findViewById(R.id.flag_chooser);
		view.setVisibility(View.INVISIBLE);
		ListView list = (ListView)findViewById(R.id.flagList);
		((TextView)findViewById(R.id.flagTitle)).setTypeface(getFonts().getTitleFont());
		final FlagAdapter adapter = new FlagAdapter(this);
		list.setAdapter(adapter);

		view.startAnimation(inDown);
		view.setVisibility(View.VISIBLE);

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				removeViewFromContent(view);
				showProgressBar();
				new FlagService(SingleVideoActivity.this).flag(new FlagServiceListener() {

					@Override
					public void connectionError() {
						connectionError();
					}

					@Override
					public void flagged() {
						SingleVideoActivity.this.flagged();
					}

					@Override
					public void error(int reason) {
						showError();
					}
				}, video.getId(), adapter.getId(view));
			}
		});
	}

	private void showError() {
		getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getResources().getString(R.string.SINGLE_ERROR_FLAG_FAIL)));
	}

	public void flagged() {
		hideProgressBar();

		DialogButtons buttons = new DialogButtons();
		buttons.add(DialogInterface.BUTTON_POSITIVE, getString(R.string.GENERAL_OK), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();

			}
		});
		getDialogHelper().displayDialog(getDialogHelper().createBasicDialog(buttons, null, getString(R.string.SINGLE_FLAG_DONE)));
	}

	public void hideFlagging(final View view) {
		outDown.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				removeViewFromContent(view);
			}
		});

		view.startAnimation(outDown);
		view.setVisibility(View.INVISIBLE);
	}

	public void closeComment(View view) {
		hideTextInput();
	}

	private void delete() {
		VideoService service = new VideoService(SingleVideoActivity.this);
		service.delete(video.getId(), SingleVideoActivity.this);
	}

	public void removeTheVideo(View view) {
		AlertDialog.Builder builder = getDialogHelper().createWarningDialog(getResources().getString(R.string.SINGLE_DELETE_VIDEO), R.string.GENERAL_YES);

		builder.setPositiveButton(getString(R.string.GENERAL_YES), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				delete();
			}
		});

		builder.setNegativeButton(getResources().getString(R.string.GENERAL_NO), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				;
			}
		});
		getDialogHelper().displayDialog(builder);
	}



	@Override
	public void deleted() {
		videoPlayer.stop();
		videoPlayer.release();
		startActivity(MainActivity.class);
	}

	@Override
	protected void setComponentVisibility(int visibility) {
		findViewById(R.id.headerLayout_ref).setVisibility(visibility);
		findViewById(R.id.list).setVisibility(visibility);
		findViewById(R.id.footer).setVisibility(visibility);
		hideProgressBar();
	}

	@Override
	public void reload(View view) {
		Ln.d("Start to reload the comments");
		startReloadAnimation();
		list.reload();
	}

	@Override
	public void loaded() {
		Ln.d("List loaded");
		stopReloadAnimation();
	}

	@Override
	public void videoData(final VideoEntry result) {
		Whambush.get(VideoEntryDao.class).add(result);
		if (video != null) {
			list.setActivity(this, result);
		}

		video = result;
		list.setVideo(video);
		list.updateLikesAndDisplikes();
	}
}

