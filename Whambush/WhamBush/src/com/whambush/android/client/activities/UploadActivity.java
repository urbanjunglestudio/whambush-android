package com.whambush.android.client.activities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.provider.MediaStore.Video.Thumbnails;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.adapter.VideoTypeAdapter;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.dialog.DialogButtons;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.mission.Missions;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.domain.video.VideoType;
import com.whambush.android.client.service.MissionsService;
import com.whambush.android.client.service.listeners.PaginateServiceListener;
import com.whambush.android.client.service.listeners.VideoCreationServiceListener;
import com.whambush.android.client.util.TagHelper;
import com.whambush.android.client.util.TextInputParameters;

public class UploadActivity extends AbstractVideoPlayActivity implements VideoCreationServiceListener {

	private static final int NONE = -1;

	private static final String ACTIVITY_ID = "Upload";

	private static final String GLOBAL = "ZZ";

	private int updatingId = NONE;

	private Mission mission;

	private Missions missions;

	private boolean alreadySet = false;

	@SuppressLint("UseSparseArrays")
	Map<Integer, String> tags = new HashMap<Integer, String>();

	@InjectView(R.id.titleView)
	private TextView titleView;

	@InjectView(R.id.tag1)
	private TextView tag1;

	@InjectView(R.id.tag2)
	private TextView tag2;

	@InjectView(R.id.tag3)
	private TextView tag3;

	@InjectView(R.id.tag4)
	private TextView tag4;

	@InjectView(R.id.tag5)
	private TextView tag5;

	@InjectView(R.id.tagView)
	private TextView tagView;

	@InjectView(R.id.descriptionView)
	private TextView descriptionView;

	@InjectView(R.id.videoTypeComboView)
	private TextView videoTypeComboView;

	public UploadActivity() {
		super();
		createEmptyTags();
	}

	private void createEmptyTags() {
		tags.put(R.id.tag1, "");
		tags.put(R.id.tag2, "");
		tags.put(R.id.tag3, "");
		tags.put(R.id.tag4, "");
		tags.put(R.id.tag5, "");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (!isValidServiceAndLoggedIn()) {
			startActivity(SplashScreen.class);
			super.onCreate(savedInstanceState);
			return;
		}

		super.onCreate(savedInstanceState, R.layout.activity_upload, false);

		setDefaultTypeface(titleView);
		videoTypeComboView.setTypeface(getFonts().getTitleFont());
		setDefaultTypeface(descriptionView);
		setDefaultTypeface(tagView);
		setDefaultTypeface(tag1);
		setDefaultTypeface(tag2);
		setDefaultTypeface(tag3);
		setDefaultTypeface(tag4);
		setDefaultTypeface(tag5);
		setVisibleBufferStatus(false);
		setShareVisible(false);
		mission = (Mission)getParameter(Mission.KEY);
		initializeFields();
		createThumbnail();
	}

	@Override
	protected void onResume() {
		super.onResume();
		setPauseButton(R.drawable.pause_upload);
		fromFullScreen();
		setPlayButton(R.drawable.play_upload);
		videoPlayer.readyToPlay();
		setPlayButton(R.drawable.play_upload);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(Mission.KEY, mission);
		outState.putSerializable(Missions.KEY, missions);
		outState.putBoolean("alreadySet", alreadySet);
		outState.putInt("updatingId", updatingId);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.containsKey(Mission.KEY))
			mission = (Mission)savedInstanceState.get(Mission.KEY);

		if (savedInstanceState.containsKey(Missions.KEY))
			missions = (Missions)savedInstanceState.get(Missions.KEY);

		if (savedInstanceState.containsKey("alreadySet"))
			alreadySet = savedInstanceState.getBoolean("alreadySet");

		if (savedInstanceState.containsKey("updatingId"))
			updatingId = savedInstanceState.getInt("updatingId");
	}

	@Override
	protected String getDetailedActivityNameForAnalysis() {
		if (mission != null)
			return ACTIVITY_ID + ":mission=" + mission.getId();
		else 
			return ACTIVITY_ID;
	}

	private void initializeFields() {
		setMissionField();

		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		if (!alreadySet) {
			setVideoURI(Uri.parse((String)getParameter(RecordActivity.FILE)));
			alreadySet = true;
		}
	}

	private void setMissionField() {
		if (mission != null) {
			videoTypeComboView.setText(R.string.UPLOAD_MISSION_VIDEO);
			titleView.setText(getTitle(mission.getName()));
			titleView.setTypeface(getFonts().getTitleFont());
			titleView.setEnabled(false);
		} else {
			videoTypeComboView.setText(R.string.UPLOAD_NORMAL_VIDEO);
			titleView.setTypeface(getFonts().getDescriptionFont());
			titleView.setText("");
			titleView.setEnabled(true);
		}
	}

	private CharSequence getTitle(String name) {
		String title = getString(R.string.MAIN_MISSION_PREFIX);
		return title += name; 
	}

	private void createThumbnail() {
		Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail((String)getParameter(RecordActivity.FILE), Thumbnails.MINI_KIND);
		setThumbnail(bmThumbnail);
	}

	public void uploadButtonPressed(View view) {

		if (getLoginInfo().isGuest()) {
			showLoginOrRegister();
			return;
		}


		try {
			String filename = (String)getParameter(RecordActivity.FILE);

			String title = titleView.getText().toString(); 
			if (mission != null)
				title  = mission.getName();

			String description = descriptionView.getText().toString();

			VideoType videoType = VideoType.NORMAL_VIDEO;

			if (title.trim().length() == 0) {
				showError(R.string.UPLOAD_ERROR_NO_TITLE);
				return;
			}

			showProgressBar();
			VideoEntry entry = new VideoEntry();
			entry.setName(title);
			entry.setDescription(description);
			entry.setTags(getTags());
			if (mission != null) 
				entry.setMissionId(mission.getId());

			Ln.d("Progressbar %s", upload);
			String username = Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getUsername();
			getVideoService().uploadVideo(this, filename, entry, upload, videoType, mission, username);
			goToMainView(R.string.UPLOAD_IN_PROGRESS);
		} catch (Exception e) {
			getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getResources().getString(R.string.UPLOAD_ERROR_UPLOAD_FAIL)));
		}
	}

	@Override
	protected void userLoggedIn() {
		showProgressBar();
		Ln.i("****** USER LOGGED IN " + Whambush.get(ConfigImpl.class).getLoginInfo().getUser());
		new MissionsService(this).findActiveMissions(new PaginateServiceListener<Mission>() {

			@Override
			public void error(int httpResultCode) {
				hideProgressBar();
				UploadActivity.this.showError(httpResultCode);
			}

			@Override
			public void connectionError() {
				hideProgressBar();
				UploadActivity.this.connectionError();
			}

			@Override
			public void paginateItems(WhambushList<Mission> items) {
				hideProgressBar();
				missions = (Missions)items;

				if (mission != null) {
					mission = findMission(mission, missions);
					if (mission.isMissionSubmissionDone())
						mission = null;
					initializeFields();
				}
			}
		}, getCountryForMission());
	}

	protected Mission findMission(Mission mission, Missions missions2) {
		for (Mission mis : missions2.getResults())
			if (mis.getId().equals(mission.getId()))
				return mis;

		return mission;
	}

	private void showError(int id) {
		getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getString(id)));
	}

	private String getTags() {
		StringBuilder concatTags = new StringBuilder();
		for (String tag : tags.values())
			if (tag.length() > 0)
				if (concatTags.length() > 0) {
					concatTags.append(", ").append(tag);
				} else
					concatTags.append(tag);


		return concatTags.toString();
	}

	public void descriptionClicked(View view) {
		updatingId = R.id.descriptionView;
		TextInputParameters params = new TextInputParameters();
		params.setMaxChars(150);
		params.setMaxLines(6);
		params.setTitle(getResources().getString(R.string.UPLOAD_ADD_DESCRIPTION).toUpperCase(getResources().getConfiguration().locale));
		params.setDefaultText(getTextView(updatingId).getText().toString());
		showTextInput(params);
	}

	public void videoTypeClicked(View view) {

		if (getLoginInfo().isGuest()) {
			showLoginOrRegister();
			return;
		}

		if (missions != null) {
			showVideoTypes();
			return;
		}

		loadMissions();
	}

	private void loadMissions() {
		showProgressBar();
		new MissionsService(this).findActiveMissions(new PaginateServiceListener<Mission>() {

			@Override
			public void error(int httpResultCode) {
				hideProgressBar();
				UploadActivity.this.showError(httpResultCode);
			}

			@Override
			public void connectionError() {
				hideProgressBar();
				UploadActivity.this.connectionError();
			}

			@Override
			public void paginateItems(WhambushList<Mission> items) {
				missions = (Missions)items;
				hideProgressBar(getHideProgressBarListener());
			}
		}, getCountryForMission());
	}

	private String getCountryForMission() {
		if (!Whambush.get(ConfigImpl.class).getLoginInfo().isGuest())
			return addGlobal(Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getCountry().getCountry());
		else
			return addGlobal(Whambush.get(ConfigImpl.class).getGuestCountry());
	}

	private String addGlobal(String guestCountry) {
		return guestCountry + "," + GLOBAL;
	}

	protected AnimationListener getHideProgressBarListener() {
		return new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				showVideoTypes();
			}
		}; 
	}

	protected void showVideoTypes() {

		FrameLayout rootLayout = (FrameLayout)findViewById(android.R.id.content);
		View.inflate(this, R.layout.view_video_chooser, rootLayout);
		final View view = rootLayout.findViewById(R.id.mission_chooser);
		view.setVisibility(View.INVISIBLE);
		ListView list = (ListView)findViewById(R.id.missionList);
		final VideoTypeAdapter adapter = new VideoTypeAdapter(this, getOnlyNonSubmitted(missions.getResults()));
		list.setAdapter(adapter);


		view.startAnimation(inDown);
		view.setVisibility(View.VISIBLE);

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view2,
					int position, long id) {

				mission = adapter.getMission(view2);
				initializeFields();

				hideChooser(view);
			}
		});
	}

	private List<Mission> getOnlyNonSubmitted(List<Mission> results) {
		ArrayList<Mission> remaining = new ArrayList<Mission>();
		for (Mission mission : results) {
			if (mission.getHasSubmitted() < mission.getMaxUserSubmissions() && mission.getNumberOfSubmissions() < mission.getMaxSubmissions())
				remaining.add(mission);
		}

		return remaining;
	}

	public void hideChooser(final View view) {
		outDown.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				removeViewFromContent(view);
			}
		});

		view.startAnimation(outDown);
		view.setVisibility(View.INVISIBLE);
	}

	public void titleClicked(View view) {
		updatingId = R.id.titleView;
		TextInputParameters params = new TextInputParameters();
		params.setAllCaps(true);
		params.setMaxChars(32);
		params.setMaxLines(1);
		params.setTitle(getResources().getString(R.string.UPLOAD_ADD_TITLE).toUpperCase(getResources().getConfiguration().locale));
		TextView view2 = (TextView)findViewById(updatingId);
		params.setDefaultText(view2.getText().toString());
		showTextInput(params);
	}

	public void tagClicked(View view) {
		updatingId = getNextFreeTag();
		if (updatingId == -1)
			return;

		TextInputParameters params = new TextInputParameters();
		params.setMaxChars(16);
		params.setMaxLines(1);
		params.setTitle(getResources().getString(R.string.UPLOAD_ADD_TAG).toUpperCase(getResources().getConfiguration().locale));
		params.setDefaultText(getTextView(updatingId).getText().toString());
		showTextInput(params);
	}

	public void tagNClicked(View view) {
		updatingId = view.getId();

		TextInputParameters params = new TextInputParameters();
		params.setDefaultText(getTextView(updatingId).getText().toString());
		showTextInput(params);
	}

	private int getNextFreeTag() {
		Set<Integer> keySet = tags.keySet();
		List<Integer> keys = new ArrayList<Integer>(keySet);
		Collections.sort(keys);

		for (Integer key : keys)
			if (tags.get(key).length() == 0)
				return key;

		return NONE;
	}

	public void closeComment(View view) {
		updatingId = NONE;
		hideTextInput();
	}

	public void submitComment(View view) {
		if (updatingId == NONE)
			return;
		EditText text = getEditText(R.id.newComment);
		if (!isTag()) {
			TextView textView = getTextView(updatingId);
			if (updatingId == R.id.titleView) {
				if (text.length() > 0)
					textView.setTypeface(getFonts().getTitleFont());
				else
					textView.setTypeface(getFonts().getDescriptionFont());
			}
			textView.setText(format(text));
		} else {
			finalizeSubmit(format(text));
			updateTags();
		}

		updatingId = NONE;
		hideTextInput();
	}

	private void updateTags() {
		if (NONE == getNextFreeTag()) {
			findViewById(R.id.tagView).setVisibility(View.GONE);
			TableRow.LayoutParams layoutParams = (TableRow.LayoutParams)findViewById(R.id.tag1).getLayoutParams();
			layoutParams.setMargins(0, (int)getResources().getDimension(R.dimen.upload_text_margin), 0, 0);
			findViewById(R.id.tag1).setLayoutParams(layoutParams);
		} else { 
			TableRow.LayoutParams layoutParams = (TableRow.LayoutParams)findViewById(R.id.tag1).getLayoutParams();
			layoutParams.setMargins((int)getResources().getDimension(R.dimen.upload_text_margin), (int)getResources().getDimension(R.dimen.upload_text_margin), 0, 0);
			findViewById(R.id.tagView).setVisibility(View.VISIBLE);
		}
	}

	private void finalizeSubmit(String tagText) {
		TextView textView = getTextView(updatingId);
		String[] parsed = parseTagValue(tagText);

		textView.setText(parsed[0]);
		updateTagValue(parsed[0]);
		textView.setVisibility(textView.getText().length() > 0?View.VISIBLE:View.GONE);

		for (int i = 1; i < parsed.length; i++) {
			updatingId = getNextFreeTag();
			if (updatingId == -1)
				break;
			textView = getTextView(updatingId);
			textView.setText(parsed[i]);
			updateTagValue(parsed[i]);
			textView.setVisibility(textView.getText().length() > 0?View.VISIBLE:View.GONE);
		}
	}

	private String[] parseTagValue(String string) {
		return TagHelper.strip(string);
	}

	private void updateTagValue(String newValue) {
		for (int key : tags.keySet())
			if (updatingId == key)
				tags.put(key, newValue);
	}

	private boolean isTag() {
		for (int key : tags.keySet())
			if (updatingId == key)
				return true;

		return false;
	}

	private String format(EditText text) {
		if (updatingId == R.id.titleView)
			return text.getText().toString().toUpperCase(getResources().getConfiguration().locale);

		return text.getText().toString();
	}

	@Override
	public void created() {
		startActivity(MissionsActivity.class);
	}

	@Override
	public void cannotCreate() {
		showError(R.string.UPLOAD_ERROR_UPLOAD_FAIL);
	}

	@Override
	public void onBackPressed() {
		if (getVideoService().getStatus() == Status.FINISHED)
			askToBeSure();
		else
			goToMainView();
	}

	private void askToBeSure() {

		DialogButtons buttons = new DialogButtons();
		buttons.add(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.GENERAL_YES), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {
				goToMainView();
			}
		});

		buttons.add(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.GENERAL_NO), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		getDialogHelper().displayDialog(getDialogHelper().createBasicDialog(buttons, getString(R.string.CAMERA_WARNING_VIDEO_NOT_UPLOADED), null));
	}

	protected void goToMainView() {
		startActivity(MissionsActivity.class);
	}

	protected void goToMainView(int id) {
		Map<String, Serializable> params = new HashMap<String, Serializable>();
		params.put(NOTE, getText(id).toString());

		startActivity(MissionsActivity.class, params);
	}

	@Override
	protected void setComponentVisibility(int visibility) {
		findViewById(R.id.headerLayout_ref).setVisibility(visibility);
		findViewById(R.id.titleView).setVisibility(visibility);
		findViewById(R.id.descriptionView).setVisibility(visibility);
		findViewById(R.id.tagTable).setVisibility(visibility);
		findViewById(R.id.footer).setVisibility(visibility);
	}

	@Override
	public void reload(View view) {
		Ln.e("NOT IMPLEMENTED");
	}

	@Override
	protected void updateUserIcon() {

	}

}

