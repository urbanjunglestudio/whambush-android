package com.whambush.android.client.service;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.service.listeners.PaginateServiceListener;

public abstract class PaginateService extends AbstractHTTPResultCallback {

	@SuppressWarnings("rawtypes")
	protected PaginateServiceListener listener;
	private AbstractActivity context;

	public PaginateService(AbstractActivity context) {
		this.context = context;
		context.registerCallback(this);
	}

	@SuppressWarnings("rawtypes")
	public void get(Request url, PaginateServiceListener listener) {
		Ln.d("starting to load");
		this.listener = listener;
		send(context, url);
	}
	
	protected AbstractActivity getContext() {
		return context;
	}
	
	
	public abstract Request parse(String url, boolean fullUrl, String...args);

	public boolean isLoading() {
		Ln.d("Is loading " + loading);
		return loading;
	}
}
