package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.country.Countries;
import com.whambush.android.client.service.AbstractServiceListener;

public interface CountryServiceListener extends AbstractServiceListener {

	public void countries(Countries countries);

}
