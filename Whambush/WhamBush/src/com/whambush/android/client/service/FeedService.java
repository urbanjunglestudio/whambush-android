package com.whambush.android.client.service;

import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.Comparator;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.feed.Feed;
import com.whambush.android.client.domain.feed.Feeds;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.parser.VideoFeedParser;
import com.whambush.android.client.service.listeners.FeedServiceListener;

public class FeedService extends AbstractHTTPResultCallback {

	private static final String VIDEOFEED_URL = "feeds/";
	private FeedServiceListener callback;
	private AbstractActivity context;

	public FeedService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}
	
	public void createLoadVideoRequest(FeedServiceListener callback) {
		this.callback = callback;
		
		send(context, new GetRequest(context, VIDEOFEED_URL));
	}
	
	@Override
	public void result(int httpResultCode, String result) {
		Ln.i("Got result %s", result);
		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			callback.feeds(parseVideoFeed(result));
		} else
			callback.error(httpResultCode);
	}

	private Feeds parseVideoFeed(String result) {
		return new VideoFeedParser().create(result);
	}

	public Feed findDefaultFeed(Feeds feeds) {
		Ln.d("Finding default feed from feeds " + feeds);
		for (Feed feed : feeds.getResults())
			if (feed.getId().equals(feeds.getDefaultFeedIndex())) {
				Ln.d("Found default feed " + feed.toString());
				return feed;
			}
		
		Collections.sort(feeds.getResults(), new DefaultFeedComparator());
		return feeds.getResults().get(0);
	}
	
	private class DefaultFeedComparator implements Comparator<Feed> {

		@Override
		public int compare(Feed arg0, Feed arg1) {
			return arg0.getId().compareTo(arg1.getId());
		}
		
	}

	public Feed findFeed(Feeds feeds, String selectedFeed) {
		for (Feed feed : feeds.getResults())
			if (feed.getId().equals(selectedFeed))
				return feed;
		
		return null;
	}

	
}
