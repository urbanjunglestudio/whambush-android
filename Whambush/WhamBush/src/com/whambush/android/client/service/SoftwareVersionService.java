package com.whambush.android.client.service;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

public class SoftwareVersionService {
	
	private Activity activity;

	public SoftwareVersionService(Activity activity) {
		this.activity = activity;
	}

	public String getVersion() {
		try {
			PackageInfo pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
			return pInfo.versionName;
		} catch (NameNotFoundException e) {
			return "?";
		}
	}

}
