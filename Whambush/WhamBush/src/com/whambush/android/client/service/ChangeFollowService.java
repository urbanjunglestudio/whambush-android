package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.follow.FollowList;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.http.requests.DeleteRequest;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.parser.FollowListParser;
import com.whambush.android.client.service.listeners.FollowServiceListener;

public class ChangeFollowService extends AbstractHTTPResultCallback {

	private static final String URL = "follows/?type=";
	private static final String FOLLOW_URL = "follows/";
	private static final String UNFOLLOW_URL = "follows/%s/";
	
	private static final String USER = "user";

	private static final String FOLLOWERS = "followers";
	private static final String FOLLOWING = "following";
	private static final String PAGE_SIZE_KEY = "page_size";
	private static final String NUMBER_OF_ENTRIES_IN_QUERY = "10000";

	private static final String FOLLOW_PARAM = "to_user";

	private FollowServiceListener callback;
	private AbstractActivity context;

	public ChangeFollowService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}
	
	public void getFollowers(FollowServiceListener callback, String userId) {
		
		GetRequest request = get(callback, FOLLOWERS);
		request.addParameter(USER, userId);
		send(context, request);
	}
	
	public void getFollowing(FollowServiceListener callback, String userId) {
		
		GetRequest request = get(callback, FOLLOWING);
		request.addParameter(USER, userId);
		send(context, request);
	}
	
	public void getFollowers(FollowServiceListener callback) {
		send(context, get(callback, FOLLOWERS));
	}

	public void follow(FollowServiceListener callback, User user) {
		this.callback = callback;

		PostRequest request = new PostRequest(context, FOLLOW_URL);
		request.addParameter(FOLLOW_PARAM, user.getId());
		send(context, request);
	}

	public void unfollow(FollowServiceListener callback, User user) {
		this.callback = callback;

		send(context, new DeleteRequest(context, String.format(UNFOLLOW_URL, user.getId())));
	}


	public void getFollowing(FollowServiceListener callback) {
		send(context, get(callback, FOLLOWING));
		
	}

	private GetRequest get(FollowServiceListener callback, String type) {
		this.callback = callback;

		GetRequest request = new GetRequest(context, URL + type);
		
		request.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
		return request;
	}

	@Override
	public void result(int httpResultCode, String result) {

		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			callback.paginateItems(parseList(result));
		} else if (httpResultCode == HttpURLConnection.HTTP_CREATED || 
				httpResultCode == HttpURLConnection.HTTP_NO_CONTENT) {
			callback.changedFollow();
		} else
			callback.error(httpResultCode);

	}

	private FollowList parseList(String result) {
		return new FollowListParser().create(result);
	}
	

}
