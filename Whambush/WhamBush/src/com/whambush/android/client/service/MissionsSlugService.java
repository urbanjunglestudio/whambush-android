package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.parser.MissionParser;
import com.whambush.android.client.service.listeners.MissionsSlugServiceListener;

public class MissionsSlugService extends AbstractHTTPResultCallback {


	private static final String MISSION_SLUG_URL = "missions/slug/%s/";

	private MissionsSlugServiceListener callback;

	private AbstractActivity context;

	public MissionsSlugService(AbstractActivity context) {
		super();
		this.context = context;
	}

	public void findMissionSlug(String slug,
			MissionsSlugServiceListener listener) {
		callback = listener;
		send(context, new GetRequest(context, String.format(MISSION_SLUG_URL, slug)));
	}

	@Override
	public void result(int httpResultCode, String result) {
		super.result(httpResultCode, result);

		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			callback.mission(parseMission(result));
		} else
			callback.error(httpResultCode);
	}

	private Mission parseMission(String result) {
		return new MissionParser().create(result);
	}

}
