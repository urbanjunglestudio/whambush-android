package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.user.UpdatedUser;
import com.whambush.android.client.service.AbstractServiceListener;

public interface UserUpdateServiceListener extends AbstractServiceListener {

	void updated(UpdatedUser user);

	void invalidRegister(UserInfoFailureReason[] parseFailures);

}
