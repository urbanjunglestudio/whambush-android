package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.activity.ActivityFeedEvent;


public interface ActivityServiceListener extends PaginateServiceListener<ActivityFeedEvent> {
	
	public void activitiesRead();
	
}
