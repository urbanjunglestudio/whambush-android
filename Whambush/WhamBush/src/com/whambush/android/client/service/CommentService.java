package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.comment.CommentList;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.parser.CommentListParser;
import com.whambush.android.client.service.listeners.CommentServiceListener;
import com.whambush.android.client.service.listeners.PaginateServiceListener;

public class CommentService extends PaginateService {

	private static final String COMMENT_URL = "comments/video/%s/";
	private static final String COMMENT = "comment";

	private CommentServiceListener listener;

	public CommentService(AbstractActivity context) {
		super(context);
	}

	public void load(String id, int count, CommentServiceListener listener) {
		this.listener = listener;

		String url = String.format(COMMENT_URL, id);

		send(getContext(), new GetRequest(getContext(), url));
	}

	public void load(String url, CommentServiceListener listener) {
		this.listener = listener;

		send(getContext(), new GetRequest(getContext(), url, true));
	}

	public void create(String id, String comment, CommentServiceListener listener) {
		this.listener = listener;

		String url = String.format(COMMENT_URL, id);
		PostRequest request = new PostRequest(getContext(), url);
		request.addParameter(COMMENT, comment);
		send(getContext(), request);
	}


	@Override
	public void result(int httpResultCode, String result) {
		super.result(httpResultCode, result);
		if (isCancelled())
			return;

		if (httpResultCode == HttpURLConnection.HTTP_OK)
			listener.paginateItems(parseComments(result));
		else if (httpResultCode == HttpURLConnection.HTTP_CREATED) 
			listener.commentUploaded();
		else
			listener.error(httpResultCode);
	}

	private CommentList parseComments(String result) {
		return new CommentListParser().create(result);
	}


	@SuppressWarnings("rawtypes")
	@Override
	public void get(Request url, PaginateServiceListener listener) {
		Ln.d("Starting to fetch more elements");
		this.listener = (CommentServiceListener)listener;
		send(getContext(), url);
	}

	@Override
	public Request parse(String url, boolean fullUrl, String...args) {

		String theUrl = url;
		if (url == null)
			theUrl = String.format(COMMENT_URL, (Object[])args);

		return new GetRequest(getContext(), theUrl, fullUrl);
	}
}
