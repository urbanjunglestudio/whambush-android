package com.whambush.android.client.service;

public interface AbstractServiceListener {

	void connectionError();
	
	public void error(int httpResultCode);
	
}
