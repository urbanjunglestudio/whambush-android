package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.user.User;

public interface FollowServiceListener extends PaginateServiceListener<User> {
	
	public void changedFollow();

}
