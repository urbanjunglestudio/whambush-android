package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import android.content.Context;

import com.whambush.android.client.domain.country.Countries;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.parser.CountriesParser;
import com.whambush.android.client.service.listeners.CountryServiceListener;

public class CountryService extends AbstractHTTPResultCallback {

	private static final String COUNTRIES_URL = "countries/";
	private static final String NUMBER_OF_ENTRIES_IN_QUERY = "300";
	private static final String PAGE_SIZE_KEY = "page_size";

	private CountryServiceListener callback;
	private Context context;

	public CountryService(Context context) {
		super();
		this.context = context;
	}

	public void getCountries(CountryServiceListener callback) {
		this.callback = callback;

		GetRequest request = new GetRequest(context, COUNTRIES_URL, false, true);
		request.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
		_send(context, request);
	}

	@Override
	public void result(int httpResultCode, String result) {
		super.result(httpResultCode, result);

		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			callback.countries(parseCountries(result));
		} else 
			callback.error(httpResultCode);
	}

	private Countries parseCountries(String result) {
		return new CountriesParser().create(result);
	}
	
}
