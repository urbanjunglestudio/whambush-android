package com.whambush.android.client.service.gcm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.whambush.android.client.domain.pushnotification.PushNotification;
import com.whambush.android.client.parser.PushNotificationParser;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
	
    @Override
    public void onReceive(Context context, Intent intent) {
    	try {
        // Start the service, keeping the device awake while it is launching.
    	Intent service = new Intent(context, GcmIntentService.class);
    	
    	PushNotification notification = new PushNotificationParser().parse(intent.getExtras().getString("message"));
    	
    	service.putExtra("NOTIFICATION", notification);
        startWakefulService(context, service);
        setResultCode(Activity.RESULT_OK);
    	} catch (Throwable th) {
    		th.printStackTrace();
    	}
    }
}