package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.tv.Channels;
import com.whambush.android.client.service.AbstractServiceListener;


public interface ChannelServiceListener extends AbstractServiceListener {
	
	public void received(Channels channels);
}
