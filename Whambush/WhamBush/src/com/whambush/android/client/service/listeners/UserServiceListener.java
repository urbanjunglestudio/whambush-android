package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.service.AbstractServiceListener;

public interface UserServiceListener extends AbstractServiceListener {

	void user(User parsedUser);

}
