package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.service.AbstractServiceListener;

public interface RegisterServiceListener extends AbstractServiceListener {

	public void registered(User user);
	
	public void invalidRegister(UserInfoFailureReason[] reasons);
	
	public void error();
	
}
