package com.whambush.android.client.service;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.listeners.UnlikeServiceListener;

public class UnlikeService extends LikeAndDislikeAbstractService {

	private static final String UNLIKE_URL = "unlike/video/";
	private static final String UNLIKE_MISSION_URL = "unlike/mission/";

	private UnlikeServiceListener listener;

	public UnlikeService(AbstractActivity context) {
		super(context, UNLIKE_URL, UNLIKE_MISSION_URL);
	}

	public void unlike(VideoEntry video, UnlikeServiceListener listener) {
		this.listener = listener;

		doCall(video, listener);
	}

	public void unlike(Mission mission, UnlikeServiceListener listener) {
		this.listener = listener;

		doCall(mission, listener);
	}

	
	@Override
	protected void result(LikesAndDislikes result) {
		listener.unliked(result);
	}

}
