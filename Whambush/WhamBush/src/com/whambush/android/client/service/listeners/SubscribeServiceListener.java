package com.whambush.android.client.service.listeners;

import com.whambush.android.client.service.AbstractServiceListener;

public interface SubscribeServiceListener extends AbstractServiceListener {

	public void subscribed();
	
	public void error();
	
}
