package com.whambush.android.client.service;

import java.net.HttpURLConnection;
import java.util.HashMap;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.parser.UserParser;
import com.whambush.android.client.service.listeners.UserServiceListener;

public class UserService extends AbstractHTTPResultCallback {
	
	private static final String USER_URL = "users/";
	private UserServiceListener callback;
	private AbstractActivity context;
	
	private static final HashMap<String, User> users = new HashMap<String, User>();

	public UserService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}
	
	public void createLoadUserRequest(UserServiceListener listener, String userid) {		
		this.callback = listener;
		send(context, new GetRequest(context, USER_URL+userid));
	}

	public void createLoadUserWithFullURL(UserServiceListener listener, String url, boolean forceLoad) {
		this.callback = listener;

		if (!forceLoad && users.containsKey(url)) {
			User user = users.get(url);
			callback.user(user);
			return;
		}

		send(context, new GetRequest(context, url, true));
	}

	public void findUserSlug(String slug,
			UserServiceListener listener) {
		callback = listener;
		GetRequest request = new GetRequest(context, USER_URL, true);
		request.addParameter("fields", "username");
		request.addParameter("query", slug);
		send(context, request);
	}
	
	
	@Override
	public void result(int httpResultCode, String result) {
		
		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			User parseUser = parseUser(result);
			users.put(parseUser.getUrl(), parseUser);
			callback.user(parseUser);
		} else
			callback.error(httpResultCode);
	}

	private User parseUser(String result) {
		return new UserParser().create(result);
	}

}
