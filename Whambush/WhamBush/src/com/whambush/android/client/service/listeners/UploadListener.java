package com.whambush.android.client.service.listeners;

public interface UploadListener {

	public void status(int presentage);

	public void error(Exception ex);
	
}
