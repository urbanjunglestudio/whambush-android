package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.comment.Comment;


public interface CommentServiceListener extends PaginateServiceListener<Comment> {
	
	public void commentUploaded();
}
