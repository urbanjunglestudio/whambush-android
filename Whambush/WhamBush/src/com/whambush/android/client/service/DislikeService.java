package com.whambush.android.client.service;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.listeners.DislikeServiceListener;

public class DislikeService extends LikeAndDislikeAbstractService {

	private static final String DISLIKE_URL = "dislike/video/";
	private static final String DISLIKE_MISSION_URL = "dislike/mission/";

	private DislikeServiceListener listener;

	public DislikeService(AbstractActivity context) {
		super(context, DISLIKE_URL, DISLIKE_MISSION_URL);
	}

	public void dislike(VideoEntry video, DislikeServiceListener listener) {
		this.listener = listener;

		doCall(video, listener);
	}
	
	public void dislike(Mission mission, DislikeServiceListener listener) {
		this.listener = listener;

		doCall(mission, listener);
	}

	@Override
	protected void result(LikesAndDislikes result) {
		listener.disliked(result);
	}
}
