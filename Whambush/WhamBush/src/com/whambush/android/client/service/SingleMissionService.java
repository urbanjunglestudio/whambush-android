package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.parser.MissionsParser;
import com.whambush.android.client.service.listeners.SingleMissionServiceListener;

public class SingleMissionService extends AbstractHTTPResultCallback {
	
	private static final String MISSION_BY_ID_ENDPOINT = "missions/%s/";
	
	private SingleMissionServiceListener callback;

	private AbstractActivity context;

	public SingleMissionService(AbstractActivity context) {
		super();
		this.context = context;
	}
	
	public void getMissionById(String id, SingleMissionServiceListener listener) {
		GetRequest request = new GetRequest(context, String.format(MISSION_BY_ID_ENDPOINT, id), false);
		get(request, listener);
	}
	
	public void get(Request request, SingleMissionServiceListener listener) {
		this.callback = listener;
		send(context, request);
	}
	
	@Override
	public void result(int httpResultCode, String result) {
		super.result(httpResultCode, result);
				
		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			callback.missionData(parseMission(result));
		} else
			callback.error(httpResultCode);
	}

	private Mission parseMission(String result) {
		return new MissionsParser().parseMission(result);
	}

}
