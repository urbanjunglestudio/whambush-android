package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.AbstractServiceListener;

public interface VideoServiceListener extends AbstractServiceListener {

	public void videoData(VideoEntry result);
	
	public void deleted();
	
}
