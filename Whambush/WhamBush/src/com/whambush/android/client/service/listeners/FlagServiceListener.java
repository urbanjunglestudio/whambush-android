package com.whambush.android.client.service.listeners;

import com.whambush.android.client.service.AbstractServiceListener;

public interface FlagServiceListener extends AbstractServiceListener {

	void flagged();
}
