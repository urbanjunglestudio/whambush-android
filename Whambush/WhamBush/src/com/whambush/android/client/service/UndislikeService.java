package com.whambush.android.client.service;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.listeners.UndislikeServiceListener;

public class UndislikeService extends LikeAndDislikeAbstractService {

	private static final String DISLIKE_URL = "undislike/video/";
	private static final String DISLIKE_MISSION_URL = "undislike/mission/";
	
	
	private UndislikeServiceListener listener;

	public UndislikeService(AbstractActivity context) {
		super(context, DISLIKE_URL, DISLIKE_MISSION_URL);
	}
	
	public void undislike(VideoEntry video, UndislikeServiceListener listener) {
		this.listener = listener;

		doCall(video, listener);
	}
	
	public void undislike(Mission mission, UndislikeServiceListener listener) {
		this.listener = listener;

		doCall(mission, listener);
	}

	@Override
	protected void result(LikesAndDislikes result) {
		listener.undisliked(result);
	}	
}
