package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.feed.Feeds;
import com.whambush.android.client.service.AbstractServiceListener;


public interface FeedServiceListener extends AbstractServiceListener {

	public void feeds(Feeds feeds);
	
}
