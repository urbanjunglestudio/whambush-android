package com.whambush.android.client.service;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.user.RegisterError;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.parser.RegisterFailureParser;
import com.whambush.android.client.parser.UserParser;
import com.whambush.android.client.service.listeners.RegisterServiceListener;
import com.whambush.android.client.service.listeners.UserInfoFailureReason;
import com.whambush.android.client.util.Validators;

public class RegisterService extends AbstractHTTPResultCallback {

	private static final int MINIMUM_PASSWORD_LENGTH = 4;
	private static final String REGISTER_URL = "users/";
	private static final String EMAIL = "email";
	private static final String USERNAME = "username";
	private static final String PASSWORD1 = "password1";
	private static final String PASSWORD2 = "password2";
	private static final String COUNTRY = "country";
	private static final String PROMOCODE = "promocode";
	
	private RegisterServiceListener callback;
	private AbstractActivity context;

	public RegisterService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}

	public boolean isValidEmail(CharSequence email) {
		return Validators.validateEmail(email.toString());
	}
	
	public boolean isPasswordTooShort(String password) {
		return password.length() < MINIMUM_PASSWORD_LENGTH;
	}
	
	public void createRegisterRequest(String username, String email, String password1, 
			String password2, String promocode, String country, RegisterServiceListener callback) {
		
		this.callback = callback;
	
		PostRequest request = new PostRequest(context, REGISTER_URL, false, false);
		
		request.addParameter(USERNAME, username);
		request.addParameter(EMAIL, email);
		request.addParameter(PASSWORD1, password1);
		request.addParameter(PASSWORD2, password2);
		request.addParameter(COUNTRY, country);
		if (promocode != null)
			request.addParameter(PROMOCODE, promocode);	

		send(context, request);
	}
	
	@Override
	public void result(int httpResultCode, String result) {		
		if (httpResultCode == HttpURLConnection.HTTP_CREATED)
			callback.registered(parseUser(result));
		else if (httpResultCode == HttpURLConnection.HTTP_BAD_REQUEST || httpResultCode == HttpURLConnection.HTTP_NOT_FOUND)
			callback.invalidRegister(parseFailures(result));
		else
			callback.error();
	}

	private UserInfoFailureReason[] parseFailures(String result) {
		
		List<UserInfoFailureReason> reasons = new ArrayList<UserInfoFailureReason>();

		RegisterError failures = new RegisterFailureParser().create(result);
		
		Ln.d("Error while registering user " + failures);
		
		if (failures.isEmail())
			reasons.add(UserInfoFailureReason.EMAIL_ALREADY_EXISTS);
		
		if (failures.isPassword())
			reasons.add(UserInfoFailureReason.PASSWORD_TOO_SHORT);
		
		if (failures.isPromocode())
			reasons.add(UserInfoFailureReason.PROMOCODE_REQUIRED);
		
		if (failures.isUsername())
			reasons.add(UserInfoFailureReason.USERNAME_ALREADY_TAKEN);
		
		if (failures.isCountry())
			reasons.add(UserInfoFailureReason.COUNTRY_MISSING);
		
		if (failures.isGuestId())
			reasons.add(UserInfoFailureReason.GUEST_ID);
		
		return reasons.toArray(new UserInfoFailureReason[0]);
	}
	
	private User parseUser(String result) {
		return new UserParser().create(result);
	}
}
