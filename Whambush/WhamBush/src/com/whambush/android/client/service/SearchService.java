package com.whambush.android.client.service;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.http.requests.Request;


public abstract class SearchService extends PaginateService {

	protected SearchService(AbstractActivity context) {
		super(context);
		
	}
	
	public abstract Request getSearchRequest(String search);
	
}
