package com.whambush.android.client.service.gcm;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.whambush.android.client.R;
import com.whambush.android.client.activities.SingleVideoForwarderActivity;
import com.whambush.android.client.activities.SplashScreen;
import com.whambush.android.client.activities.UserFeedForwarderActivity;
import com.whambush.android.client.domain.pushnotification.NotificationType;
import com.whambush.android.client.domain.pushnotification.PushNotification;
import com.whambush.android.client.domain.user.User;

public class GcmIntentService extends IntentService {

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (extras != null && !extras.isEmpty()) {  // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that GCM
			 * will be extended in the future with new message types, just ignore
			 * any message types you're not interested in, or that you don't
			 * recognize.
			 */

			if (GoogleCloudMessaging.
					MESSAGE_TYPE_MESSAGE.equals(messageType) || messageType == null) {


				try {					
					sendNotification((PushNotification)intent.getExtras().get("NOTIFICATION"));
				} catch (IllegalStateException ex) {
					ex.printStackTrace();
				}
			} 
		}

		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	private void sendNotification(PushNotification notification) {
		NotificationManager mNotificationManager = (NotificationManager)
				this.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent contentIntent = null;

		if (notification == null)
			return;

		// Incase of extra stuff like go to video.
		Intent intent;
		if (notification.getVideo() != null && !notification.getVideo().isEmpty()) {
			intent = new Intent(this, SingleVideoForwarderActivity.class);
			intent.putExtra(SingleVideoForwarderActivity.TO_THE_VIDEO_ID, notification.getVideo());
		} else if (notification.getUser() != null && !notification.getUser().isEmpty()) {
			intent = new Intent(this, UserFeedForwarderActivity.class);
			intent.putExtra(User.KEY, notification.getUser());
		} else
			intent = new Intent(this, SplashScreen.class);

		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);

		contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

		String msg = createMsg(notification.getKey(), notification.getArgs());

		if (msg == null)
			return;

		NotificationCompat.Builder mBuilder = createNotification(msg);
		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(getId(notification), mBuilder.build());
	}

	private int getId(PushNotification notification) {
		NotificationType notificationType = getNotificationType(notification.getKey());
		if (notificationType == NotificationType.NEW_COMMENT || notificationType == NotificationType.NEW_LIKE)
			return notification.getVideo().hashCode();
		else if (notificationType == NotificationType.NEW_FOLLOWER)
			return notification.getUser().hashCode();

		return notificationType.getId();
	}

	private NotificationType getNotificationType(String key) {
		return NotificationType.getFromStr(key);
	}

	private String createMsg(String type, String[] value) {
		switch (getNotificationType(type)) {
		case NEW_MISSION:
			return String.format(getString(R.string.PUSH_NEW_MISSION), value[0]);
		case NEW_ICON_MISSION:
			return String.format(getString(R.string.PUSH_NEW_ICON_MISSION), value[0]);
		case ICON_MISSION_ENDING:
			return String.format(getString(R.string.PUSH_ICON_MISSION_ENDING), value[0]);
		case NEW_DARE_MISSION:
			return String.format(getString(R.string.PUSH_NEW_DARE_MISSION), value[0]);
		case NEW_COMMENT:
			if (value.length < 2)
				break;
			return String.format(getString(R.string.PUSH_NEW_COMMENT), value[0], value[1]);
		case NEW_LIKE:
			if (value.length < 2)
				break;
			return String.format(getString(R.string.PUSH_NEW_LIKE), value[0], value[1]);
		case NEW_FOLLOWER:
			return String.format(getString(R.string.PUSH_NEW_FOLLOWER), value[0]);
		case NEW_RANK:
			return String.format(getString(R.string.PUSH_NEW_RANK));
		default:
			return null;
		}

		return null;
	}

	private NotificationCompat.Builder createNotification(String msg) {
		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(this)
		.setSmallIcon(R.drawable.icon)
		.setContentTitle(getString(R.string.GENERAL_APP_NAME).toUpperCase(getResources().getConfiguration().locale))
		.setStyle(new NotificationCompat.BigTextStyle()
		.bigText(msg))
		.setContentText(msg)
		.setAutoCancel(true)
		.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));


		return mBuilder;
	}
}
