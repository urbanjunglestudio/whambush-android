package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.likes.LikesAndDislikes;

public interface LikeServiceListener extends LikeAndDislikeServiceListener {

	void liked(LikesAndDislikes result);
}
