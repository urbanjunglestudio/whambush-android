package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.ranking.RankedUser;


public interface RankingServiceListener extends PaginateServiceListener<RankedUser> {
	
}
