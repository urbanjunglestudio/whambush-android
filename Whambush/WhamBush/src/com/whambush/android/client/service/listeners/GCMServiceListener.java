package com.whambush.android.client.service.listeners;

import com.whambush.android.client.service.AbstractServiceListener;


public interface GCMServiceListener extends AbstractServiceListener {

		public void registered();
		
		public void unregistered();

}
