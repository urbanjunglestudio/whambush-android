package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.service.AbstractServiceListener;

public interface SingleMissionServiceListener extends AbstractServiceListener {

	public void missionData(Mission result);
	
}
