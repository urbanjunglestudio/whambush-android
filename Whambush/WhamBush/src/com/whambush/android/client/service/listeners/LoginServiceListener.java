package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.user.InvalidLoginInfo;
import com.whambush.android.client.domain.user.LoginInfo;
import com.whambush.android.client.service.AbstractServiceListener;

public interface LoginServiceListener extends AbstractServiceListener {

	public void loggedIn(LoginInfo login);
	
	public void invalidLogin(InvalidLoginInfo info);

}
