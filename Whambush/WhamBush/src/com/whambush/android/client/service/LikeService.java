package com.whambush.android.client.service;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.service.listeners.LikeServiceListener;

public class LikeService extends LikeAndDislikeAbstractService {

	private static final String LIKE_URL = "like/video/";
	
	private static final String MISSIO_URL = "like/mission/";

	private LikeServiceListener listener;

	public LikeService(AbstractActivity context) {
		super(context, LIKE_URL, MISSIO_URL);
	}

	public void like(VideoEntry video, LikeServiceListener listener) {
		this.listener = listener;

		doCall(video, listener);
	}
	
	public void like(Mission mission, LikeServiceListener listener) {
		this.listener = listener;

		doCall(mission, listener);
	}

	@Override
	protected void result(LikesAndDislikes result) {
		listener.liked(result);
	}

}
