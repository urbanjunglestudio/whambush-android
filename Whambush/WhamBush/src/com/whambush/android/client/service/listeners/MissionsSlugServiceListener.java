package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.service.AbstractServiceListener;

public interface MissionsSlugServiceListener extends AbstractServiceListener {

	public void mission(Mission mission);
	
}
