package com.whambush.android.client.service.listeners;

import com.whambush.android.client.service.AbstractServiceListener;


public interface VideoCreationServiceListener extends AbstractServiceListener {

	public void created();

	public void cannotCreate();

}
