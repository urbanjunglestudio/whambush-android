package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.mission.Mission;

public interface MissionsServiceListener extends PaginateServiceListener<Mission> {

}
