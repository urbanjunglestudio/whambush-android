package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.likes.LikesAndDislikes;

public interface UnlikeServiceListener extends LikeAndDislikeServiceListener {

	void unliked(LikesAndDislikes result);
}
