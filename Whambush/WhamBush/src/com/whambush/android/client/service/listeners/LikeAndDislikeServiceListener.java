package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.likes.LikesAndDislikesError;
import com.whambush.android.client.service.AbstractServiceListener;

public interface LikeAndDislikeServiceListener extends AbstractServiceListener {
	
	void errorResult(LikesAndDislikesError error);
	
}
