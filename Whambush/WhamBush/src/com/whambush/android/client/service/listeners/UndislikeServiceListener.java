package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.likes.LikesAndDislikes;

public interface UndislikeServiceListener extends LikeAndDislikeServiceListener {

	void undisliked(LikesAndDislikes result);

}
