package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.follow.FollowList;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.parser.FollowListParser;
import com.whambush.android.client.service.listeners.PaginateServiceListener;

public class FollowingService extends PaginateService {

	private static final String URL = "follows/?type=";

	private static final String USER = "user";

	private static final String FOLLOWING = "following";
	private static final String PAGE_SIZE_KEY = "page_size";
	private static final String NUMBER_OF_ENTRIES_IN_QUERY = "30";

	private PaginateServiceListener<User> callback;

	public FollowingService(AbstractActivity context) {
		super(context);
	}

	@Override
	public void result(int httpResultCode, String result) {
		super.result(httpResultCode, result);
		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			callback.paginateItems(parseList(result));
		} else
			callback.error(httpResultCode);

	}

	private FollowList parseList(String result) {
		return new FollowListParser().create(result);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void get(Request request, PaginateServiceListener listener) {
		callback = listener;
		send(getContext(), request);

	}

	@Override
	public Request parse(String url, boolean full, String...args) {

		if (url == null || url.length() == 0) {
			GetRequest request = new GetRequest(getContext(), URL + FOLLOWING, full);
			request.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
			request.addParameter(USER, args[0]);
			return request;
		} else {
			return new GetRequest(getContext(), url, full);
		}
	}
}
