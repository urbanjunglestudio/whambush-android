package com.whambush.android.client.service;

import roboguice.util.Ln;
import android.content.Context;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.http.HTTPResultCallback;
import com.whambush.android.client.http.RequestProxy;
import com.whambush.android.client.http.requests.DeleteRequest;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.http.requests.PutRequest;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.service.listeners.GCMServiceListener;

public abstract class AbstractHTTPResultCallback implements HTTPResultCallback {

	private boolean cancelled = false;
	protected boolean loading = false;

	protected AbstractHTTPResultCallback() {
		super();
	}

	public RequestProxy getProxy() {
		return Whambush.get(RequestProxy.class);
	}
	
	protected void send(AbstractActivity context, Request request) {
		loading = true;
		
		if (request instanceof DeleteRequest)
			send(context, (DeleteRequest)request);
		else if (request instanceof GetRequest)
			_send(context, (GetRequest)request);
		else if (request instanceof PutRequest)
			send(context, (PutRequest)request);
		else if (request instanceof PostRequest)
			send(context, (PostRequest)request);
		else
			Ln.e("Invalid request type " + request);
		
		Ln.d("Request sent");
	}
	
	protected void send(AbstractActivity context, DeleteRequest request) {
		loading = true;
		getProxy().delete(context, request, this);
	}
	
	protected void _send(Context context, GetRequest request) {
		loading = true;
		getProxy().get(context, request, this);
	}
	
	protected void send(AbstractActivity context, PutRequest request) {
		loading = true;
		getProxy().put(context, request, this);
	}
	
	protected void send(AbstractActivity context, PostRequest request) {
		loading = true;
		getProxy().post(context, request, this);
	}

	protected boolean isCancelled() {
		loading = false;
		return cancelled;
	}
	
	public void cancel() {
		loading = false;
		cancelled = true;
	}
	
	@Override
	public void result(int httpResultCode, String result) {
		loading = false;
		Ln.d("RESULT %d : %s", httpResultCode, result);
	}

	protected void extracted(GCMServiceListener listener, Exception e) {
		Ln.e(e);
		listener.error(-1);
	}

}
