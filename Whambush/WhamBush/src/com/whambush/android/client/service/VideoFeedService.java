package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import roboguice.util.Ln;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.domain.video.Videos;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.parser.VideosParser;
import com.whambush.android.client.service.listeners.PaginateServiceListener;

public class VideoFeedService extends SearchService {

	private static final String NUMBER_OF_ENTRIES_IN_QUERY = "30";
	private static final String QUERY_KEY = "query";
	private static final String USER_KEY = "user";
	private static final String MISSION_KEY = "mission";
	private static final String PAGE_SIZE_KEY = "page_size";
	private static final String SEARCH_URL = "search/videos/";
	private PaginateServiceListener<VideoEntry> callback;

	public VideoFeedService(AbstractActivity context) {
		super(context);
	}
	
	@Override
	public Request getSearchRequest(String search) {
		Ln.d("Create Video feed URL for search " + search);
		GetRequest request = new GetRequest(getContext(), SEARCH_URL, false);
		
		request.addParameter(QUERY_KEY, search.trim());
		request.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
		return request;
	}
	
	public Request createSearchUserVideoUrl(String userid) {
		Ln.d("Create Video feed URL for userid " + userid);
		GetRequest request = new GetRequest(getContext(), SEARCH_URL, false);
		
		request.addParameter(USER_KEY, userid);
		request.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
		
		return request;
	}
	
	public Request createSearchForUserVideoURL(String username) {
		Ln.d("Create Video feed URL for username " + username);
		GetRequest request = new GetRequest(getContext(), SEARCH_URL, false);
		
		request.addParameter(USER_KEY, username.trim());
		request.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
		return request;
	}
	
	public Request createSearchForMissionVideoURL(String missionid) {
		Ln.d("Create Video feed URL for mission " + missionid);
		GetRequest request = new GetRequest(getContext(), SEARCH_URL, false);
		
		request.addParameter(MISSION_KEY, missionid);
		request.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
		
		return request;
	}
	
	@Override
	public void result(int httpResultCode, String result) {
		super.result(httpResultCode, result);
		
		if (httpResultCode == HttpURLConnection.HTTP_OK) {
			callback.paginateItems(parseVideoFeed(result));
		} else
			callback.error(httpResultCode);
	}

	private Videos parseVideoFeed(String result) {
		return new VideosParser().create(result);
	}

	@Override
	public Request parse(String url, boolean full, String...args) {
		GetRequest request = new GetRequest(getContext(), url, full);
		request.addParameter(PAGE_SIZE_KEY, NUMBER_OF_ENTRIES_IN_QUERY);
		return request;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void get(Request request, PaginateServiceListener listener) {
		Ln.d("get base " + request + " listener " + listener);
		callback = (PaginateServiceListener<VideoEntry>)listener;
		
		send(getContext(), request);
	}	
}
