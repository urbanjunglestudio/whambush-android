package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.likes.LikesAndDislikes;

public interface DislikeServiceListener extends LikeAndDislikeServiceListener {

	void disliked(LikesAndDislikes result);

}
