package com.whambush.android.client.service.listeners;

import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.service.AbstractServiceListener;


public interface PaginateServiceListener<T> extends AbstractServiceListener {

	public void paginateItems(WhambushList<T> items);
	
}
