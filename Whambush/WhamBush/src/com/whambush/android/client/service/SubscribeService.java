package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.service.listeners.SubscribeServiceListener;

public class SubscribeService extends AbstractHTTPResultCallback {

	private static final String SUBSCRIBE = "subscribe/waitinglist/";
	private static final String EMAIL = "email";
	
	private SubscribeServiceListener callback;
	private AbstractActivity context;

	public SubscribeService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}
	
	public void createSubscribeRequest(String email, SubscribeServiceListener callback) {
		this.callback = callback;
	
		PostRequest request = new PostRequest(context, SUBSCRIBE, false, false);
		
		request.addParameter(EMAIL, email);
		send(context, request);
	}
	
	@Override
	public void result(int httpResultCode, String result) {		
		if (httpResultCode == HttpURLConnection.HTTP_OK)
			callback.subscribed();
		else
			callback.error();
	}
}
