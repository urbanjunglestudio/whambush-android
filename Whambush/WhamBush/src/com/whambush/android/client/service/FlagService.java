package com.whambush.android.client.service;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.service.listeners.FlagServiceListener;

public class FlagService extends AbstractHTTPResultCallback {

	private static final String URL = "flags/video/%s/";
	private static final String REASON = "reason_code";
	private FlagServiceListener listener;
	private AbstractActivity context;

	public FlagService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}

	
	public void flag(FlagServiceListener callback, String videoId, int reason) {
		this.listener = callback;
		
		String url = String.format(URL, videoId);
		PostRequest request = new  PostRequest(context, url);
		
		try {
			JSONObject obj = new JSONObject();
			obj.put(REASON, reason);
			request.setJson(obj);
			send(context, request);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void result(int httpResultCode, String result) {
		
		if (httpResultCode == HttpURLConnection.HTTP_CREATED)
			listener.flagged();
		else
			listener.error(httpResultCode);
	}

}
