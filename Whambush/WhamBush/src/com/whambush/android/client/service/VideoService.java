package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.http.requests.DeleteRequest;
import com.whambush.android.client.http.requests.GetRequest;
import com.whambush.android.client.parser.VideoFeedEntryParser;
import com.whambush.android.client.service.listeners.VideoServiceListener;

public class VideoService extends AbstractHTTPResultCallback {
	
	private static final String VIDEO_URL = "videos/%s/";
	private static final String VIDEO_SLUG_URL = "videos/slug/%s/";
	
	private VideoServiceListener listener;

	private AbstractActivity context;

	public VideoService(AbstractActivity context) {
		super();
		this.context = context;
		context.registerCallback(this);
	}
	
	public void load(String id, VideoServiceListener listener) {
		this.listener = listener;

		send(context, new GetRequest(context, String.format(VIDEO_URL, id)));
	}
	
	public void delete(String id, VideoServiceListener listener) {
		this.listener = listener;

		send(context, new DeleteRequest(context, String.format(VIDEO_URL, id)));
	}

	public void findVideoSlug(String slug,
			VideoServiceListener listener) {
		this.listener = listener;
		send(context, new GetRequest(context, String.format(VIDEO_SLUG_URL, slug)));
	}
	
	@Override
	public void result(int httpResultCode, String result) {
		if (httpResultCode == HttpURLConnection.HTTP_OK)
			listener.videoData(parseUser(result));
		if (httpResultCode == HttpURLConnection.HTTP_NO_CONTENT)
			listener.deleted();
		else
			listener.error(httpResultCode);
	}

	private VideoEntry parseUser(String result) {
		return new VideoFeedEntryParser().create(result);
	}

}
