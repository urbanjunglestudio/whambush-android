package com.whambush.android.client.service;

import java.net.HttpURLConnection;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.likes.LikesAndDislikesError;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.http.requests.PostRequest;
import com.whambush.android.client.parser.LikesAndDislikesErrorParser;
import com.whambush.android.client.parser.LikesAndDislikesParser;
import com.whambush.android.client.service.listeners.LikeAndDislikeServiceListener;

public abstract class LikeAndDislikeAbstractService extends AbstractHTTPResultCallback {

	private static final String VIDEO_ID = "video_id";
	private static final String MISSION_ID = "mission_id";
	private final String url;
	private LikeAndDislikeServiceListener listener;
	private AbstractActivity context;
	private String missioUrl;

	public LikeAndDislikeAbstractService(AbstractActivity context, String url, String missioUrl) {
		super();
		this.url = url;

		this.context = context;
		this.missioUrl = missioUrl;
		context.registerCallback(this);
	}

	protected void doCall(VideoEntry video, LikeAndDislikeServiceListener listener) {
		this.listener = listener;

		PostRequest request = new PostRequest(context, url);

		request.addParameter(VIDEO_ID, video.getId());
		send(context, request);
	}
	
	protected void doCall(Mission mission, LikeAndDislikeServiceListener listener) {
		this.listener = listener;

		PostRequest request = new PostRequest(context, missioUrl);

		request.addParameter(MISSION_ID, mission.getId());
		send(context, request);
	}

	@Override
	public void result(int httpResultCode, String result) {

		if (httpResultCode == HttpURLConnection.HTTP_OK)
			result(parseLikesAndDislikes(result));
		else if (httpResultCode == HttpURLConnection.HTTP_BAD_REQUEST)
			errorResult(parseLikesAndDislikesError(result));
		else
			error(httpResultCode);
	}

	private LikesAndDislikes parseLikesAndDislikes(String result) {
		return new LikesAndDislikesParser().create(result);
	}

	private LikesAndDislikesError parseLikesAndDislikesError(String result) {
		return new LikesAndDislikesErrorParser().create(result);
	}

	protected void error(int httpResultCode) {
		listener.error(httpResultCode);
	}

	protected void errorResult(LikesAndDislikesError result) {
		listener.errorResult(result);
	}

	protected abstract void result(LikesAndDislikes result);

}

