package com.whambush.android.client.service;

import com.whambush.android.client.domain.device.DeviceInfo;

public class DeviceInfoService {

	private static final String ANDROID = "ANDROID";

	public DeviceInfo getInfo() { 
		return new DeviceInfo(getManufacurer(), ANDROID, getVersion(), detDevice());
	}

	private String detDevice() {
		return android.os.Build.MODEL;
	}

	private String getVersion() {
		return android.os.Build.VERSION.RELEASE;
	}

	private String getManufacurer() {
		return android.os.Build.MANUFACTURER;
	}
}
