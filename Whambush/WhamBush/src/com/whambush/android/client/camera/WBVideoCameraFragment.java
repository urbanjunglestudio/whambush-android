package com.whambush.android.client.camera;

import java.io.IOException;
import java.util.List;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.util.Fonts;
import com.whambush.android.client.util.ThumbnailUtil;
import com.whambush.android.client.view.RemainingTime;

public class WBVideoCameraFragment extends AbstractCameraFragment {


	private TextView infoText;
	private Button doneButton;
	private RemainingTime timeGraph;
	private ImageView undoView;
	private ImageView trashView;
	private ImageView browseIcon;
	private ImageView switchIcon;
	private RelativeLayout recordBar;
	private Animation fromLeftToIn;
	private Animation outRight;
	private Animation inRight;
	private Animation outLeft;
	private Animation fadeIn;

	public WBVideoCameraFragment(boolean frontCamera) {
		super(frontCamera);
	}

	@Override
	protected int getCameraViewId() {
		return R.layout.view_video_camera;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		infoText = (TextView)view.findViewById(R.id.info_text);
		doneButton = (Button)view.findViewById(R.id.doneButton);
		timeGraph = (RemainingTime)view.findViewById(R.id.remaining_time_graph);
		undoView = (ImageView)view.findViewById(R.id.undoView);
		trashView = (ImageView)view.findViewById(R.id.trashView);
		browseIcon = (ImageView)view.findViewById(R.id.browseIcon);
		recordBar = (RelativeLayout)view.findViewById(R.id.recordBar);
		switchIcon = (ImageView)view.findViewById(R.id.switchIcon);
		
		fromLeftToIn = AnimationUtils.loadAnimation(getActivity(), R.anim.from_left_in); 
		outRight = AnimationUtils.loadAnimation(getActivity(), R.anim.to_right_out); 
		outLeft = AnimationUtils.loadAnimation(getActivity(), R.anim.to_left_out); 
		inRight = AnimationUtils.loadAnimation(getActivity(), R.anim.from_right_in);
		fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein);
		
		doneButton.setVisibility(View.INVISIBLE);
		doneButton.setEnabled(false);

		infoText.setTypeface(new Fonts().getTitleFont());

		undoView.setVisibility(View.INVISIBLE);
		trashView.setVisibility(View.INVISIBLE);

		Bitmap thumbnail = new ThumbnailUtil().getLastVideoThumbnail(getActivity().getContentResolver(), "image/*");
		browseIcon.setImageBitmap(thumbnail);
	}

	@Override
	public void record() throws Exception {

		infoText.setVisibility(View.GONE);

		animToRightIfVisible(doneButton);

		animToLeftIfVisible(recordBar);

		animToRightIfVisible(undoView);
		animToRightIfVisible(trashView);

		super.record();
	}

	@Override
	public void stopRecording() throws IOException {
		browseIcon.setVisibility(View.INVISIBLE);

		recordBar.startAnimation(fromLeftToIn);
		recordBar.setVisibility(View.VISIBLE);

		undoView.startAnimation(inRight);
		undoView.setVisibility(View.VISIBLE);

		super.stopRecording();
		
		doneButton.startAnimation(inRight);
		doneButton.setVisibility(View.VISIBLE);
		doneButton.setEnabled(true);

		doneButton.requestLayout();
	}
	
	private void animToLeftIfVisible(View view) {
		animIfVisible(outLeft, view);
	}

	private void animToRightIfVisible(View view) {
		animIfVisible(outRight, view);
	}

	private void animIfVisible(Animation anim, View view) {
		if (view != null && view.getVisibility() != View.INVISIBLE) {
			view.startAnimation(anim);
			view.setVisibility(View.INVISIBLE);
		}
	}

	protected void fadeIn(View view) {
		view.startAnimation(fadeIn);
		view.setVisibility(View.VISIBLE);
	}

	protected void fromLeftToIn(View view) {
		view.startAnimation(fromLeftToIn);
		view.setVisibility(View.VISIBLE);
	}
	
	public int setDurations(List<Integer> recordingLengths) {
		return timeGraph.setRecordingTimes(recordingLengths);
	}

	public void setTime(int duration) {
		timeGraph.setTime(duration);
	}

	public void startUndo() {
		undoView.startAnimation(outRight);
		undoView.setVisibility(View.INVISIBLE);

		trashView.startAnimation(inRight);
		trashView.setVisibility(View.VISIBLE);		
	}

	public void updateView(List<Integer> recordingLengths) {
		if (recordingLengths.size() == 0) {

			animToRightIfVisible(undoView);
			animToRightIfVisible(trashView);
			animToRightIfVisible(doneButton);
			
			fromLeftToIn(switchIcon);
			fromLeftToIn(browseIcon);
			fadeIn(infoText);
		} else {
			trashView.startAnimation(outRight);
			trashView.setVisibility(View.INVISIBLE);

			undoView.startAnimation(inRight);
			undoView.setVisibility(View.VISIBLE);

			infoText.setVisibility(View.INVISIBLE);


			browseIcon.setVisibility(View.GONE);

			doneButton.setVisibility(View.VISIBLE);
			doneButton.setEnabled(true);
		}
	}


}
