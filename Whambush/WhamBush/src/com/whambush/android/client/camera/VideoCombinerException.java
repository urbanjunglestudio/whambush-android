package com.whambush.android.client.camera;

import java.io.IOException;

public class VideoCombinerException extends Exception {

	public VideoCombinerException(IOException ex) {
		initCause(ex);
	}

	private static final long serialVersionUID = 4326489679858520006L;

}
