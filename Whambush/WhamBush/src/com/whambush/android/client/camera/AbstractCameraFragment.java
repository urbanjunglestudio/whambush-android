package com.whambush.android.client.camera;

import android.hardware.Camera;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.commonsware.cwac.camera.CameraFragment;
import com.commonsware.cwac.camera.CameraView;
import com.whambush.android.client.R;

public abstract class AbstractCameraFragment extends CameraFragment {

	private boolean frontCamera;
	private View content;

	public AbstractCameraFragment(boolean frontCamera) {
		this.frontCamera = frontCamera;
	}

	protected boolean isFrontCamera() {
		return frontCamera;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		content = inflater.inflate(getCameraViewId(), container, false);

		CameraView cameraView = (CameraView)content.findViewById(R.id.cameraView);
		setCameraView(cameraView);
		
		ImageView switchImage = (ImageView)content.findViewById(R.id.switchIcon);
		if (isFrontCamera())
			switchImage.setImageResource(R.drawable.capture_cam2back);
		
		if (Camera.getNumberOfCameras() == 1) {			
			content.findViewById(R.id.switchIcon).setVisibility(View.GONE);
		}
		
		return content;
	}
	
	protected abstract int getCameraViewId();
	
	public ImageView getSwitchIcon() {
		return (ImageView)content.findViewById(R.id.switchIcon);
	}
	
}
