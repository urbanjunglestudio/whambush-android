package com.whambush.android.client.camera;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Bundle;

import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;
import com.whambush.android.client.AbstractWhambushActivity;
import com.whambush.android.client.util.FileUtil;

public class WBCameraHost extends SimpleCameraHost {

	private boolean frontCamera = false;
	private CameraListener listener;
	private AbstractWhambushActivity context;

	public WBCameraHost(AbstractWhambushActivity context, CameraListener listener) {
		super(context);
		this.context = context;
		this.listener = listener;
	}

	protected Context getContext() {
		return context;
	}
	
	@Override
	public boolean useSingleShotMode() {
		return true;
	}

	@Override
	public int getCameraId() {
		if (Camera.getNumberOfCameras() > 1 && frontCamera)
				return Camera.CameraInfo.CAMERA_FACING_FRONT;

		return Camera.CameraInfo.CAMERA_FACING_BACK;
	}

	public void setFrontCamera(boolean userFrontCamera) {
		this.frontCamera = userFrontCamera;
	}

	@Override
	protected File getPhotoDirectory() {
		return new FileUtil().getPhotoDirectory();
	}

	@Override
	public void onCameraFail(FailureReason reason) {
		listener.cameraInitializationFailure();
	}
	
	@Override
	public void saveImage(PictureTransaction xact, Bitmap bitmap) {
		File photo = getPhotoPath();
		String path = photo.getPath();
		if (photo.exists()) {
			photo.delete();
		}

		FileOutputStream fos;
		try {
			fos = new FileOutputStream(path);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
			fos.flush();
			fos.getFD().sync();

			listener.pictureTaken(path);
		} catch (IOException e) {
			listener.pictureTakeFailed();
		}
	}

	public void restore(Bundle savedInstanceState) {
	}

	public void saveInstanceState(Bundle outState) {
	}
}
