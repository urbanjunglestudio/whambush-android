package com.whambush.android.client.camera;

import java.io.File;
import java.util.List;

import roboguice.util.Ln;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;

import com.whambush.android.client.AbstractWhambushActivity;
import com.whambush.android.client.R;

public class WBVideoCameraHost extends WBCameraHost {

	private VideoCombiner videoCombiner;
	private static final int MAX_DURATION_IN_MSECONDS = 300;
	
	private static final int ONE_MSECOND = 100;
	private static final int MAX_DURATION = ONE_MSECOND * MAX_DURATION_IN_MSECONDS;
	private static final int[] QUALITIES = {CamcorderProfile.QUALITY_720P, CamcorderProfile.QUALITY_HIGH, 
		CamcorderProfile.QUALITY_480P, CamcorderProfile.QUALITY_LOW};
	private static final int BIT_RATE = 2500000;
	
	public WBVideoCameraHost(AbstractWhambushActivity context, CameraListener listener) {
		super(context, listener);
		videoCombiner = new VideoCombiner(context);
	}

	@Override
	protected File getVideoPath() {
		String nextFile = videoCombiner.getNextFile();
		
		Ln.d("Files in Array");
		videoCombiner.printFiles();
		
		return new File(nextFile);		
	}
	
	public List<Integer> getRecordingLengths() {
		return videoCombiner.getRecordingLengths();
	}

	public void calculateLatestLength() {
		videoCombiner.calculateLatestLength();		
	}
	
	@Override
	public void restore(Bundle savedInstanceState) {
		super.restore(savedInstanceState);
		
		if (savedInstanceState == null)
			return;
		
		if(videoCombiner == null)
			videoCombiner = new VideoCombiner(getContext());

		videoCombiner.restoreState(savedInstanceState);
	}
		
	@Override
	public void configureRecorderProfile(int cameraId, MediaRecorder recorder) {
		CamcorderProfile profile = null;

		for (int index = 0; index < QUALITIES.length; index++) {
			if (CamcorderProfile.hasProfile(cameraId, QUALITIES[index])) {
				profile = CamcorderProfile.get(cameraId, QUALITIES[index]);
				break;
			}
		}

		profile.duration = MAX_DURATION;
		if (!isCheapAssPhone()) {
			profile.videoBitRate = BIT_RATE;
		}
		
		recorder.setProfile(profile);
	}
		
	private boolean isCheapAssPhone() {
		String[] cheapPhones = getContext().getResources().getStringArray(R.array.bad_cameras);
		for (String string : cheapPhones) {
			if (android.os.Build.MODEL.equals(string))
				return true;	
		}
		return false;
	}
	
	@Override
	public void saveInstanceState(Bundle outState) {
		super.saveInstanceState(outState);
		
		if (videoCombiner != null)
			videoCombiner.saveState(outState);
	}

	public void removeLast() {
		videoCombiner.removeLast();
	}
	
	public Integer getMovieLength(String realPath) {
		return videoCombiner.getMovieLength(realPath);
	}

	public String getFinalFilename() {
		return videoCombiner.getFinalFilename();
	}

	public void finalizeVideos() throws VideoCombinerException {
		videoCombiner.finalizeVideos();
	}
	
}
