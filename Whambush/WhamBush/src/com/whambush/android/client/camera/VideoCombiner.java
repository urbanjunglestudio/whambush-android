package com.whambush.android.client.camera;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import roboguice.util.Ln;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import com.coremedia.iso.IsoFile;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.whambush.android.client.util.FileUtil;


public class VideoCombiner {

	private static final String FILENAME = "FILENAME";
	private static final String ALL_FILES = "ALL_FILES";
	private static final String COMBINED = "COMBINED";
	private static final String DURATIONS = "DURATIONS";

	private static final String VIDEO = "vide";
	private static final String SOUND = "soun";

	// Save to Bundle
	private ArrayList<String> allFiles = new ArrayList<String>();
	private ArrayList<Integer> durations = new ArrayList<Integer>();
	private String combined;
	private String finishedVideo;

	private final FileUtil fileUtil;

	private Context context;

	public VideoCombiner(Context context) {
		this.context = context;
		fileUtil = new FileUtil();
	}

	public String getNextFile() {
		String fName = fileUtil.getTempFile();
		addNewFilename(fName);
		return fName;
	}

	private void addNewFilename(String fName) {
		allFiles.add(fName);
	}

	class Result {
		int result = 0;
	}

	public void finalizeVideos() throws VideoCombinerException {

		combineVideos();

		finishedVideo = combined;
	}


	public String combineVideos() throws VideoCombinerException {
		FileChannel channel = null;

		try {
			if (allFiles.isEmpty() && combined != null)
				return combined;

			List<Movie> movies = new ArrayList<Movie>();

			printFiles();



			for (String movie : allFiles) {
				try {
					if (movie != null) {
						channel = new FileInputStream(movie).getChannel();
						if (channel != null)
							movies.add(MovieCreator.build(channel));
					}
				} catch (NullPointerException ex) {
					continue; // Incase of invalid video file.
				}
			}

			Movie[] inMovies = movies.toArray(new Movie[0]);

			List<Track> videoTracks = new LinkedList<Track>();
			List<Track> audioTracks = new LinkedList<Track>();

			for (Movie m : inMovies)
				for (Track t : m.getTracks())
					if (t.getHandler().equals(SOUND))
						audioTracks.add(t);
					else if (t.getHandler().equals(VIDEO))
						videoTracks.add(t);

			Movie result = new Movie();

			if (audioTracks.size() > 0) {
				AppendTrack appendTrack = new AppendTrack(audioTracks.toArray(new Track[0]));
				result.addTrack(appendTrack);
			}

			if (videoTracks.size() > 0)
				result.addTrack(new AppendTrack(videoTracks.toArray(new Track[0])));			

			IsoFile out = new DefaultMp4Builder().build(result);

			combined = fileUtil.getCombinedFileName();
			RandomAccessFile randomAccessFile = new RandomAccessFile(combined, "rw");
			FileChannel fc = randomAccessFile.getChannel();
			fc.position(0);
			out.getBox(fc);
			fc.close();
			randomAccessFile.close();

			removeTempFiles();
		} catch (IOException ex) {
			throw new VideoCombinerException(ex);
		} finally {
			if (channel != null) 
				try {
					channel.close();
				} catch (IOException ex) {
					;
				}
		}

		return combined;
	}

	public String getFinalFilename() {
		return finishedVideo;
	}

	public String getCombinedFilename() {
		return combined;
	}

	private void removeTempFiles() {
		for (String f : allFiles)
			(new File(f)).delete();

		allFiles.clear();
	}

	public void printFiles() {
		for (String f : allFiles)
			Ln.d(f);
	}

	public void saveState(Bundle outState) {
		outState.putStringArrayList(ALL_FILES, allFiles);
		outState.putString(FILENAME, finishedVideo);
		outState.putString(COMBINED, combined);
		outState.putIntegerArrayList(DURATIONS, durations);
	}

	public void restoreState(Bundle savedInstanceState) {
		allFiles = savedInstanceState.getStringArrayList(ALL_FILES);
		finishedVideo = savedInstanceState.getString(FILENAME);
		combined = savedInstanceState.getString(COMBINED);
		durations = savedInstanceState.getIntegerArrayList(DURATIONS);
	}

	public void removeLast() {
		if (allFiles.size() == 0) {
			durations.clear();
			return;
		}
		
		String file = allFiles.remove(allFiles.size()-1);
		durations.remove(durations.size()-1);
		new File(file).delete();
	}

	public void calculateLatestLength() {
		if (allFiles.size() == 0) {
			removeLast();
			return;
		}
			
		Integer movieLength = getMovieLength(allFiles.get(allFiles.size()-1));
		if (movieLength > 0)
			durations.add(movieLength);
		else {
			durations.add(0);
			removeLast();
		}
	}

	public List<Integer> getRecordingLengths() {
		return durations;
	}

	public Integer getMovieLength(String file) {
		File file2 = new File(file);
		if (!file2.exists())
			return 0;
		MediaPlayer player = MediaPlayer.create(context, Uri.fromFile(file2));
		if (player == null) {
			return -1;
		}
		int duration = player.getDuration() / 100;
		player.reset();
		player.release();
		player = null;
		return duration;
	}
}
