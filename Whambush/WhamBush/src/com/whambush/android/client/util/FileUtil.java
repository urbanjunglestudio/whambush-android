package com.whambush.android.client.util;

import java.io.File;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

public class FileUtil {

	private static final String BASENAME = "video";
	private static final String SUFFIX = ".mpg";
	private static final String OUTPUT_MP4 = "/%s%s.mp4";
	
	private static final String APPLICATION_WHAMBUSH = "/Application/whambush/";	
	
	public String getTempDirectory() {
		String tempDirectory = getBasePath() + "temp/";
		File f = new File(tempDirectory);
		f.mkdirs();
		return tempDirectory;
	}

	public String getBasePath() {
		String string = Environment.getExternalStorageDirectory().getPath() + APPLICATION_WHAMBUSH;
		new File(string).mkdirs();
		return string;
	}
	
	public String getCombinedPath() {
		String string = Environment.getExternalStorageDirectory().getPath() + APPLICATION_WHAMBUSH + "combined/";
		new File(string).mkdirs();
		return string;
	}
	
	public String getSerPath(String stateName) {
		return getTempDirectory() + "ser/" + stateName + ".ser";
	}
	
	public String getTempFile() {
		return getTempDirectory() + BASENAME + System.currentTimeMillis() + SUFFIX;
	}
	
	public String getVideoFileName() {
		return getBasePath() + String.format(OUTPUT_MP4, BASENAME, "" + System.currentTimeMillis());
	}
	
	public String getCombinedFileName() {
		return getCombinedPath() + String.format(OUTPUT_MP4, BASENAME, "" + System.currentTimeMillis());
	}

	public void emptyDirectory(String dirname) {
		File dir = new File(dirname);
		if (dir.isDirectory()) {
			for (File file : dir.listFiles()) {
				file.delete();
			}
		}
	}

	public File getPhotoDirectory() {
		String string = Environment.getExternalStorageDirectory().getPath() + APPLICATION_WHAMBUSH + "pictures/";
		File file = new File(string);
		file.mkdirs();
		return file;
	}
	
	public String getRealPathFromURI(Context context, Uri contentUri) {
		Cursor cursor = null;
		try { 
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
			if (cursor != null) {
				cursor.moveToFirst();
				return cursor.getString(0);
			}

			return contentUri.getPath();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	
}
