package com.whambush.android.client.util;

import roboguice.util.Ln;
import android.app.Activity;
import android.net.Uri;

import com.whambush.android.client.activities.MissionFeedActivity;
import com.whambush.android.client.activities.SingleVideoActivity;
import com.whambush.android.client.activities.UserActivity;

public class URLParser {

	public static Destination parse(Uri destination) {
		String path = destination.getPath();
		String[] splitted = path.split("/");
		for (String s : splitted)
			Ln.d("s = " + s);
		
		Destination dest = new Destination();
		dest.setActionStr(splitted[1]);
		dest.setAction(convert(splitted[1]));
		dest.setSlug(splitted[2]);
		
		return dest;
	}

	private static Class<? extends Activity> convert(String string) {
		if (string.equalsIgnoreCase("V"))
			return SingleVideoActivity.class;
		else if (string.equalsIgnoreCase("U"))
			return UserActivity.class;
		else if (string.equalsIgnoreCase("M"))
			return MissionFeedActivity.class;
		else 
			return null;
	}

}
