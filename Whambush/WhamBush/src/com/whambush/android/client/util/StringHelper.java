package com.whambush.android.client.util;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;

public class StringHelper {

	public static String normalize(String str) {
		return str.replaceAll("[^a-zA-Z0-9]", "");
	}

	private static final float SMALL_CAP = 0.8f;

	public static SpannableString getSmallCapsString(String input) {
		
		char[] chars = new String(input).toCharArray();
		int currentBlock = 0;
		int[] blockStarts = new int[chars.length];
		int[] blockEnds = new int[chars.length];
		boolean blockOpen = false;

		// record where blocks of lowercase letters start/end
		for (int i = 0; i < chars.length; ++i) {
			char c = chars[i];
			if (c >= 'a' && c <= 'z') {
				if (!blockOpen) {
					blockOpen = true;
					blockStarts[currentBlock] = i;
				}
				// replace with uppercase letters
				chars[i] = (char) (c - 'a' + '\u0041');
			} else {
				if (blockOpen) {
					blockOpen = false;
					blockEnds[currentBlock] = i;
					++currentBlock;
				}
			}
		}

		// add the string end, in case the last character is a lowercase letter
		blockEnds[currentBlock] = chars.length;
		
		// shrink the blocks found above
		SpannableString output = new SpannableString(new String(chars));
		
		if (currentBlock < chars.length)
			for (int i = 0; i < Math.min(blockStarts.length, blockEnds.length); ++i) {
				output.setSpan(new RelativeSizeSpan(SMALL_CAP), blockStarts[i], blockEnds[i], Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
			}

		return output;
	}

	public static SpannableString getEpisodeString(String name, String count) {
		String otherColor = count + " " + getEpisodes();
		String source = name + " " + getSeparator() + " " + otherColor;
		source = source.toUpperCase();
		SpannableString output = new SpannableString(source);
		output.setSpan(new ForegroundColorSpan(Whambush.getContext().getResources().getColor(R.color.whambushGreen)), 
			output.length() - otherColor.length(), output.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		return output;
	}

	private static String getSeparator() {
		return Whambush.getContext().getString(R.string.MAIN_TV_EPISODE_SEPARATOR);
	}
	
	private static String getEpisodes() {
		return Whambush.getContext().getString(R.string.MAIN_TV_EPISODE_STRING);
	}
	
}
