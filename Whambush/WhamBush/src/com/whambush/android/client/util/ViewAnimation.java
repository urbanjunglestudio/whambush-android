package com.whambush.android.client.util;

import android.support.v4.view.ViewPager.LayoutParams;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class ViewAnimation {

	private static final int MAX_DURATION = 1000;
	private static final int ANIMATION_SPEED_MULTIPLIER = 2;

	public static void expand(final View v) {
		v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		final int targtetHeight = v.getMeasuredHeight();

		v.getLayoutParams().height = 0;
		v.setVisibility(View.VISIBLE);
		Animation a = new Animation()
		{
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				v.getLayoutParams().height = interpolatedTime == 1
						? LayoutParams.WRAP_CONTENT
								: (int)(targtetHeight * interpolatedTime);
				v.requestLayout();
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		setDuration(a, (int)(targtetHeight / v.getContext().getResources().getDisplayMetrics().density));
		start(v, a);
	}

	private static void setDuration(Animation a, int i) {
		int duration = ANIMATION_SPEED_MULTIPLIER * i;
		if (duration > 1000)
			duration = MAX_DURATION;
		a.setDuration(duration);
	}

	public static void collapse(final View v) {
		final int initialHeight = v.getMeasuredHeight();

		Animation a = new Animation()
		{
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				if(interpolatedTime == 1){
					v.setVisibility(View.GONE);
				}else{
					v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		setDuration(a, (int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
		start(v, a);
	}

	private static void start(final View v, Animation a) {
		v.startAnimation(a);
	}

}
