package com.whambush.android.client.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;

import com.whambush.android.client.R;

@SuppressLint("SimpleDateFormat")
public class DateAndTimeParser {

	private static final String DEFAULT_TIMEZONE = "GMT";
	private static final String GMT_DATE_AND_TIME_CONVERT = "yyyy-MMM-dd HH:mm:ss";
	private static final String JSON_DATE = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd";
	
	private static final String DEFAULT_VISIBLE_DATE_FORMAT = "HH:mm dd.MM.yyyy";

	private static final long SECOND = 1000;

	private static final long MINUTE = SECOND * 60;
	private static final long HOUR = MINUTE * 60;
	private static final long TWO_HOURS = HOUR * 2;
	private static final long DAY = HOUR * 24;
	private static final long TWO_DAYS = DAY * 2;
	private static final long WEEK = DAY * 7;
	private static final long TWO_WEEKS = WEEK * 2;
	private static final long FOUR_WEEKS = WEEK * 4;

	public static Date parse( String input ) throws ParseException {

		try {
			return parse( JSON_DATE, input);
		} catch (ParseException ex) {
			return parse( SIMPLE_DATE_FORMAT, input);
		}

	}

	private static Date parse(String dateFormat, String input) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat( dateFormat );
		if (input != null)
			return df.parse(input);
		else 
			return new Date();
	}

	public static String getDateAsString(Context context, String input) {
		try {
			return android.text.format.DateFormat.getDateFormat(context).format(parse( input ));
		} catch (ParseException e) {
			return input;
		}
	}

	public static long getNowInGMT() {
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat(GMT_DATE_AND_TIME_CONVERT);
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone(DEFAULT_TIMEZONE));

		//Local time zone   
		SimpleDateFormat dateFormatLocal = new SimpleDateFormat(GMT_DATE_AND_TIME_CONVERT);

		//Time in GMT
		Date cal;
		try {
			cal = dateFormatLocal.parse( dateFormatGmt.format(new Date()) );
		} catch (ParseException e) {
			e.printStackTrace();
			return new Date().getTime();
		}

		return cal.getTime();
	}

	public static String getToAsNiceString( android.content.res.Resources resources, String strDate ) {
		Date when = new Date(getNowInGMT());
		try {
			when = parse(strDate);
		} catch (ParseException e) {
			return getString(resources, R.string.POSTED_SOMETIME);
		}

		long diff = calculateDiffToNow(when);

		String text = when.toString();
		if (diff < MINUTE)
			text = getString(resources, R.string.POSTED_JUST_NOW);
		else if (diff < HOUR)
			text = getString(resources, R.string.POSTED_NRO_MINUTES_AGO, getMinutes(diff));
		else if (diff < TWO_HOURS)
			text = getString(resources, R.string.POSTED_HOUR_AGO);
		else if (diff < DAY)
			text = getString(resources, R.string.POSTED_NRO_HOURS_AGO, getHours(diff));
		else if (diff < TWO_DAYS)
			text = getString(resources, R.string.POSTED_DAY_AGO);
		else if (diff < WEEK)
			text = getString(resources, R.string.POSTED_NRO_DAYS_AGO, getDays(diff));
		else if (diff < TWO_WEEKS)
			text = getString(resources, R.string.POSTED_WEEK_AGO);
		else if (diff < FOUR_WEEKS)
			text = getString(resources, R.string.POSTED_NRO_WEEKS_AGO, getWeeks(diff));
		else
			text = getString(resources, R.string.POSTED_LONG_TIME_AGO);

		return text;
	}


	public static String getLeftAsNiceString( android.content.res.Resources resources, String strDate ) {
		Date when = new Date(getNowInGMT());
		try {
			when = parse(strDate);
		} catch (ParseException e) {
			return getString(resources, R.string.ENDS_SOMETIME);
		}

		long diff = calculateDiffToNow(when);
		diff *= -1;

		if (diff < 0)
			return " ";

		String text = when.toString();
		if (diff < MINUTE)
			text = getString(resources, R.string.ENDS_LESS_THAN_MINUTE);
		else if (diff < HOUR)
			text = getString(resources, R.string.ENDS_AFTER_NRO_MINUTES, getMinutes(diff));
		else if (diff < TWO_HOURS)
			text = getString(resources, R.string.ENDS_AFTER_IN_A_HOUR);
		else if (diff < DAY)
			text = getString(resources, R.string.ENDS_AFTER_NRO_HOURS, getHours(diff));
		else if (diff < TWO_DAYS)
			text = getString(resources, R.string.ENDS_AFTER_A_DAY);
		else if (diff < WEEK)
			text = getString(resources, R.string.ENDS_AFTER_NRO_DAYS, getDays(diff));
		else if (diff < TWO_WEEKS)
			text = getString(resources, R.string.ENDS_AFTER_A_WEEK);
		else if (diff < FOUR_WEEKS)
			text = getString(resources, R.string.ENDS_AFTER_NRO_WEEKS, getWeeks(diff));
		else
			text = getString(resources, R.string.ENDS_NOT_ANY_TIME_SOON);

		return text;
	}

	
	public static String getAsTime(android.content.res.Resources resources, String date) {
		Date when = new Date(getNowInGMT());
		try {
			when = parse(date);
		} catch (ParseException e) {
			return getString(resources, R.string.ENDS_SOMETIME);
		}

		SimpleDateFormat dateFormatGmt = new SimpleDateFormat(DEFAULT_VISIBLE_DATE_FORMAT);
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone(DEFAULT_TIMEZONE));
		
		return  dateFormatGmt.format(when);
	}
	

	public static long calculateDiffToNow(Date when) {
		long diff = getNowInGMT() - when.getTime();
		return diff;
	}

	public static int getSeconds(long diff) {
		return (int)(diff / SECOND);
	}
	
	public static int getMinutes(long diff) {
		return (int)(diff / MINUTE);
	}

	public static int getHours(long diff) {
		return (int)(diff / HOUR);
	}

	public static int getDays(long diff) {
		return (int)(diff / DAY);
	}

	public static int getWeeks(long diff) {
		return (int)(diff / WEEK);
	}

	public static List<Integer> getDiff(long diff) {
		List<Integer> diffs = new ArrayList<Integer>();
		int day = getDays(diff);
		diffs.add(Math.abs(day));
		int hour = getHours(diff - (day * DAY));
		diffs.add(Math.abs(hour));
		int minute = getMinutes(diff - (day * DAY) - (hour * HOUR));
		diffs.add(Math.abs(minute));
		int seconds = getSeconds(diff - (day * DAY) - (hour * HOUR) - (minute * MINUTE));
		diffs.add(Math.abs(seconds));
		
		return diffs;
	}
	
	private static String getString(Resources resources, int id, int number) {
		return String.format(resources.getString(id), number);
	}

	private static String getString(Resources resources, int id) {
		return resources.getString(id);
	}
}
