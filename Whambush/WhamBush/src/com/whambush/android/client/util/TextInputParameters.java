package com.whambush.android.client.util;

public class TextInputParameters {

	private String defaultText = "";
	private String title = "";
	private int maxLines = -1;
	private int maxChars = -1;
	private boolean allCaps = false;
	
	public String getDefaultText() {
		return defaultText;
	}
	public void setDefaultText(String defaultText) {
		this.defaultText = defaultText;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getMaxLines() {
		return maxLines;
	}
	public void setMaxLines(int maxLines) {
		this.maxLines = maxLines;
	}
	public int getMaxChars() {
		return maxChars;
	}
	public void setMaxChars(int maxChars) {
		this.maxChars = maxChars;
	}
	public boolean isAllCaps() {
		return allCaps;
	}
	public void setAllCaps(boolean allCaps) {
		this.allCaps = allCaps;
	}
	public Object getInputType() {
		return new Object();
	}

}
