package com.whambush.android.client.util;

import java.io.Serializable;

import android.app.Activity;

public class Destination implements Serializable {

	private static final long serialVersionUID = 2431789699053123228L;
	
	private Class<? extends Activity> action;
	private String slug;

	private String actionStr;
	
	public String getSlug() {
		return slug;
	}
	
	public void setSlug(String id) {
		this.slug = id;
	}
	
	public Class<? extends Activity> getAction() {
		return action;
	}
	
	public void setAction(Class<? extends Activity> action) {
		this.action = action;
	}

	public String getActionStr() {
		return actionStr;
	}

	public void setActionStr(String actionStr) {
		this.actionStr = actionStr;
	}

	@Override
	public String toString() {
		return "Destination [action=" + action + ", slug=" + slug
				+ ", actionStr=" + actionStr + "]";
	}

}
