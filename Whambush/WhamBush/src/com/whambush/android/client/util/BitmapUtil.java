package com.whambush.android.client.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import roboguice.util.Ln;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;

public class BitmapUtil {

	private static final int MAX_SIZE = 640;

	public static Bitmap getCorrectlyOrientedImage(String pic) throws IOException {
		File photoFile = new File(pic);

		String imagePath = photoFile.getAbsolutePath();
		Bitmap myBitmap  = BitmapFactory.decodeFile(imagePath);

		final int maxSize = 1920;
		int outWidth;
		int outHeight;
		int inWidth = myBitmap.getWidth();
		int inHeight = myBitmap.getHeight();
		if(inWidth > inHeight){
			outWidth = maxSize;
			outHeight = (inHeight * maxSize) / inWidth; 
		} else {
			outHeight = maxSize;
			outWidth = (inWidth * maxSize) / inHeight; 
		}

		Bitmap resized = Bitmap.createScaledBitmap(myBitmap, outWidth, outHeight, true);
		return ExifUtil.rotateBitmap(pic, resized);
	}


	public static Bitmap crop(RectF size, RectF cut, Bitmap original) {
		
		Ln.d(size.toShortString());
		
		float imgWidth = original.getWidth();
		float imgHeight = original.getHeight();

		float top = (size.top * imgHeight) + cut.top;
		float left = (size.left * imgWidth) + cut.left;

		float height = (size.bottom * imgHeight) - top - cut.bottom;
		float width = (size.right * imgWidth) - left - cut.right;

		height = width = Math.min(width, height);

		Bitmap cropped = Bitmap.createBitmap(original,(int)left,(int)top, (int)width, (int)height);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		cropped.compress(Bitmap.CompressFormat.PNG, 100, stream);

		return cropped;
	}

	public static Bitmap toProfilePictureSize(Bitmap image) {
		return Bitmap.createScaledBitmap(image, MAX_SIZE, MAX_SIZE, false);
	}

	public static Bitmap addBorders(Bitmap original, int top, int right, int bottom, int left, int color) {
		float imgWidth = original.getWidth();
		float imgHeight = original.getHeight();

		RectF targetRect = new RectF(left, top, left + imgWidth, top + imgHeight);
		Bitmap dest = Bitmap.createBitmap((int)imgWidth + left + right, (int)imgHeight + top + bottom, original.getConfig());
		Canvas canvas = new Canvas(dest);
		canvas.drawColor(color);
		canvas.drawBitmap(original, null, targetRect, null);

		return dest;
	}


	public static Bitmap read(String path) {
		return BitmapFactory.decodeFile(path);
	}

}
