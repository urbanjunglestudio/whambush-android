package com.whambush.android.client.util;

import android.content.Context;

import com.whambush.android.client.R;

public class CountHelper {

	private Context context;

	public CountHelper(Context context) {
		this.context = context;
	}
	
	public String countAsStr(int count) {
		if (count < 0)
			count = 0;
		
		if (count >= 1000) {
			return Integer.toString(count / 1000) + context.getResources().getString(R.string.GENERAL_THOUSAND_SUFFIX);
		} else
			return Integer.toString(count);
	}
	
}
