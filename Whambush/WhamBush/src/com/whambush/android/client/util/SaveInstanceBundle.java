package com.whambush.android.client.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.whambush.android.client.domain.user.User;

public class SaveInstanceBundle implements Serializable {

	private static final long serialVersionUID = 3807990542819980044L;

	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	public static void saveBundle(String state, SaveInstanceBundle bundle) {
		ObjectOutputStream oos = null;
		FileOutputStream fout = null;
		try{
			fout = new FileOutputStream(new FileUtil().getSerPath(state));
			oos = new ObjectOutputStream(fout);
			oos.writeObject(bundle);
		} catch (Exception ex) {
			ex.printStackTrace();
		}finally {
			if(oos  != null){
				try {
					oos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
		}
	}

	public static SaveInstanceBundle restore(String state) {
		ObjectInputStream objectinputstream = null;
		try {
			FileInputStream streamIn = new FileInputStream(new FileUtil().getSerPath(state));
			objectinputstream = new ObjectInputStream(streamIn);
			return (SaveInstanceBundle) objectinputstream.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(objectinputstream != null){
				try {
					objectinputstream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
		}
		return null;
	}

}
