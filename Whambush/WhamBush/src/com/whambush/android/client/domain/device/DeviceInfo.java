package com.whambush.android.client.domain.device;

public class DeviceInfo {
	
	private final String manufacture;
	private final String os;
	private final String version;
	private final String device;
	
	
	public DeviceInfo(String manufacture, String os, String version, String device) {
		this.manufacture = manufacture;
		this.os = os;
		this.version = version;
		this.device = device;
	}
	
	public String getManufacture() {
		return manufacture;
	}
	public String getOs() {
		return os;
	}
	public String getVersion() {
		return version;
	}
	public String getDevice() {
		return device;
	}
	
	

}
