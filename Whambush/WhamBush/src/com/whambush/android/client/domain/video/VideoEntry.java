package com.whambush.android.client.domain.video;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.WhambushObject;

public class VideoEntry extends WhambushObject implements Serializable {

	private static final long serialVersionUID = -5118102293366464416L;

	public static final String KEY = "video_id";

	@SerializedName("external_id")
	private String external_id;

	@SerializedName("thumbnail_url")
	private String thumbnailURL;

	@SerializedName("is_processed")
	private boolean isProcessed;

	@SerializedName("tags")
	private String tags;	
	
	@SerializedName("like_count")
	private int likes;

	@SerializedName("dislike_count")
	private int dislikes;

	@SerializedName("has_liked")
	private boolean hasLiked;
	
	@SerializedName("has_disliked")
	private boolean hasDisliked;
	
	@SerializedName("comment_count")
	private int commentCount;
	
	@SerializedName("mission_id")
	private String missionId;
	
	@SerializedName("view_count")
	private long viewCount;
	
	@SerializedName("web_url")
	private String webUrl;

	@SerializedName("published_at")
	private String publishedAt;
	
	@SerializedName("video_type")
	private int videoType;

	public String getExternal_id() {
		return external_id;
	}

	public void setExternal_id(String external_id) {
		this.external_id = external_id;
	}

	public String getThumbnail_url() {
		return thumbnailURL;
	}

	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnailURL = thumbnail_url;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getDislikes() {
		return dislikes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}

	public boolean isHasLiked() {
		return hasLiked;
	}

	public void setHasLiked(boolean hasLiked) {
		this.hasLiked = hasLiked;
	}

	public boolean isHasDisliked() {
		return hasDisliked;
	}

	public void setHasDisliked(boolean hasDisliked) {
		this.hasDisliked = hasDisliked;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public boolean isProcessed() {
		return isProcessed;
	}

	public void setProcessed(boolean isProcessed) {
		this.isProcessed = isProcessed;
	}

	@Override
	public String toString() {
		return "VideoFeedEntry [external_id=" + external_id
				+ ", thumbnail_url=" + thumbnailURL + ", isProcessed="
				+ isProcessed + ", tags=" + tags + ", likes=" + likes
				+ ", dislikes=" + dislikes + ", hasLiked=" + hasLiked
				+ ", hasDisliked=" + hasDisliked + ", commentCount="
				+ commentCount + ", getId()="
				+ getId() + ", getHash()=" + getHash() + ", getName()="
				+ getName() + ", getDescription()=" + getDescription()
				+ ", getAddedBy()=" + getAddedBy() + ", getCreated_at()="
				+ getCreated_at() + ", getModified_at()=" + getModified_at()
				+ ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}

	public String getMissionId() {
		return missionId;
	}

	public void setMissionId(String missionId) {
		this.missionId = missionId;
	}

	public long getViewCount() {
		return viewCount;
	}

	public void setViewCount(long viewCount) {
		this.viewCount = viewCount;
	}

	public String getWebUrl() {
		return webUrl;
	}

	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}

	public String getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(String publishedAt) {
		this.publishedAt = publishedAt;
	}

	public int getVideoType() {
		return videoType;
	}
}
