package com.whambush.android.client.domain.mission;

import java.io.Serializable;

import com.whambush.android.client.domain.WhambushList;


public class Missions extends WhambushList<Mission> implements Serializable {

	private static final long serialVersionUID = -6221187789139129501L;
	public static final String KEY = "Missions";

}
