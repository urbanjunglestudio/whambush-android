package com.whambush.android.client.domain.user;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class LoginInfo implements Serializable {

	private static final long serialVersionUID = -5456442328848467307L;

	@SerializedName("token")
	private String authenticationToken;

	@SerializedName("num_active_missions")
	private int activeMissions;

	@SerializedName("incomplete")
	private int incompleteProfileCount;


	@SerializedName("user")
	private User user;

	@SerializedName("settings")
	private Settings settings;

	@SerializedName("guest_id")
	private String guestId;
	
	@SerializedName("is_guest")
	private boolean isGuest;
	
	@SerializedName("unread")
	private int unreadActivities;
	
	public String getGuestId() {
		return guestId;
	}

	public void setGuestId(String guestId) {
		this.guestId = guestId;
	}

	public boolean isGuest() {
		return isGuest;
	}

	public void setGuest(boolean isGuest) {
		this.isGuest = isGuest;
	}

	public int getActiveMissions() {
		return activeMissions;
	}

	public void setActiveMissions(int activeMissions) {
		this.activeMissions = activeMissions;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Settings getSettings() {
		return settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	public int getIncompleteProfileCount() {
		return incompleteProfileCount;
	}

	public void setIncompleteProfileCount(int incompleteProfileCount) {
		this.incompleteProfileCount = incompleteProfileCount;
	}

	@Override
	public String toString() {
		return "LoginInfo [authenticationToken=" + authenticationToken
				+ ", activeMissions=" + activeMissions
				+ ", incompleteProfileCount=" + incompleteProfileCount
				+ ", user=" + user + ", settings=" + settings + "]";
	}

	public int getUnreadActivities() {
		return unreadActivities;
	}

	public void setUnreadActivities(int unreadActivities) {
		this.unreadActivities = unreadActivities;
	}

}
