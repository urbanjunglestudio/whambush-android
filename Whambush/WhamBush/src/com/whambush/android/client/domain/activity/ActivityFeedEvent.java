package com.whambush.android.client.domain.activity;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.WhambushObject;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.domain.video.VideoEntry;

public class ActivityFeedEvent extends WhambushObject {

	private static final long serialVersionUID = -7565719589245806256L;

	@SerializedName("action")
	private String action;

	@SerializedName("user")
	private User user;
	
	@SerializedName("is_read")
	private boolean read;
	
	@SerializedName("target")
	private VideoEntry video;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public VideoEntry getVideo() {
		return video;
	}

	public void setVideo(VideoEntry video) {
		this.video = video;
	}

	@Override
	public String toString() {
		return "ActivityFeedEvent [action=" + action + ", user=" + user
				+ ", read=" + isRead() + ", video=" + video + "]";
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}	
}
