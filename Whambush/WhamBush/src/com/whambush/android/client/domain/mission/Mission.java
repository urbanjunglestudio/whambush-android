package com.whambush.android.client.domain.mission;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.WhambushObject;
import com.whambush.android.client.domain.video.VideoEntry;

public class Mission extends WhambushObject {

	private static final long serialVersionUID = -4155217568222614599L;
	public static final String KEY = "whambush_mission";
	
	@SerializedName("start_at")
	private String startAt;

	@SerializedName("end_at")
	private String endAt;
	
	@SerializedName("mission_type")
	private int missionType;
	
	private String country;
	
	@SerializedName("is_videos_ranked")
	private boolean isRanked;
	
	@SerializedName("linked_video")
	private VideoEntry video;
	
	@SerializedName("max_user_submissions")
	private int maxUserSubmissions;

	@SerializedName("max_submissions")
	private int maxSubmissions;
	
	@SerializedName("max_video_length")
	private int maxVideoLength;
	
	@SerializedName("mission_image_1")
	private String missionImage1;
	
	@SerializedName("mission_image_2")
	private String missionImage2;
	
	@SerializedName("has_submitted")
	private int hasSubmitted;
	
	@SerializedName("mission_url")
	private String missionUrl;
	
	@SerializedName("like_count")
	private int likeCount;

	@SerializedName("dislike_count")
	private int dislikeCount;

	@SerializedName("number_of_submissions")
	private int numberOfSubmissions;
	
	@SerializedName("has_liked")
	private boolean hasLiked;
	
	@SerializedName("has_disliked")
	private boolean hasDisliked;

	public String getStartAt() {
		return startAt;
	}

	public void setStartAt(String startAt) {
		this.startAt = startAt;
	}

	public String getEndAt() {
		return endAt;
	}

	public void setEndAt(String endAt) {
		this.endAt = endAt;
	}

	public int getMissionType() {
		return missionType;
	}

	public void setMissionType(int missionType) {
		this.missionType = missionType;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean isRanked() {
		return isRanked;
	}

	public void setRanked(boolean isRanked) {
		this.isRanked = isRanked;
	}

	public VideoEntry getVideo() {
		return video;
	}

	public void setVideo(VideoEntry video) {
		this.video = video;
	}

	public int getMaxUserSubmissions() {
		return maxUserSubmissions;
	}

	public void setMaxUserSubmissions(int maxUserSubmissions) {
		this.maxUserSubmissions = maxUserSubmissions;
	}

	public int getMaxSubmissions() {
		return maxSubmissions;
	}

	public void setMaxSubmissions(int maxSubmissions) {
		this.maxSubmissions = maxSubmissions;
	}

	public int getMaxVideoLength() {
		return maxVideoLength;
	}

	public void setMaxVideoLength(int maxVideoLength) {
		this.maxVideoLength = maxVideoLength;
	}

	public String getMissionImage1() {
		return missionImage1;
	}

	public void setMissionImage1(String missionImage1) {
		this.missionImage1 = missionImage1;
	}

	public String getMissionImage2() {
		return missionImage2;
	}

	public void setMissionImage2(String missionImage2) {
		this.missionImage2 = missionImage2;
	}

	public int getHasSubmitted() {
		return hasSubmitted;
	}

	public void setHasSubmitted(int hasSubmitted) {
		this.hasSubmitted = hasSubmitted;
	}

	public String getMissionUrl() {
		return missionUrl;
	}

	public void setMissionUrl(String missionUrl) {
		this.missionUrl = missionUrl;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public int getDislikeCount() {
		return dislikeCount;
	}

	public void setDislikeCount(int dislikeCount) {
		this.dislikeCount = dislikeCount;
	}

	public int getNumberOfSubmissions() {
		return numberOfSubmissions;
	}

	public void setNumberOfSubmissions(int numberOfSubmissions) {
		this.numberOfSubmissions = numberOfSubmissions;
	}

	public boolean isMissionSubmissionDone() {
		return getHasSubmitted() >= getMaxUserSubmissions() || (getMaxSubmissions() > 0 && getNumberOfSubmissions() >= getMaxSubmissions()) || isRanked();
	}

	public boolean isHasLiked() {
		return hasLiked;
	}

	public void setHasLiked(boolean liked) {
		hasLiked = liked;
	}
	
	public void setHasDisliked(boolean disLiked) {
		hasDisliked = disLiked;
	}
	
	public boolean isHasDisliked() {
		return hasDisliked;
	}
	
	@Override
	public String toString() {
		return "Mission [startAt=" + startAt + ", endAt=" + endAt
				+ ", missionType=" + missionType + ", country=" + country
				+ ", isRanked=" + isRanked + ", video=" + video + ", maxUserSubmissions="
				+ maxUserSubmissions + ", maxSubmissions=" + maxSubmissions
				+ ", maxVideoLength=" + maxVideoLength + ", missionImage1="
				+ missionImage1 + ", missionImage2=" + missionImage2
				+ ", hasSubmitted=" + hasSubmitted + ", missionUrl="
				+ missionUrl + ", likeCount=" + likeCount + ", dislikeCount="
				+ dislikeCount + ", numberOfSubmissions=" + numberOfSubmissions
				+ ", toString()=" + super.toString() + "]";
	}

}
