package com.whambush.android.client.domain.user;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Settings implements Serializable {

	private static final long serialVersionUID = 6213119547379059983L;

	@SerializedName("vzaar_key")
	private String vzaarKey;
	
	@SerializedName("vzaar_secret")
	private String vzaarSecret;

	public String getVzaarKey() {
		return vzaarKey;
	}

	public void setVzaarKey(String vzaarKey) {
		this.vzaarKey = vzaarKey;
	}

	public String getVzaarSecret() {
		return vzaarSecret;
	}

	public void setVzaarSecret(String vzaarSecret) {
		this.vzaarSecret = vzaarSecret;
	}
}
