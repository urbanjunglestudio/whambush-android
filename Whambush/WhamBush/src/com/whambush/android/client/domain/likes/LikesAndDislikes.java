package com.whambush.android.client.domain.likes;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class LikesAndDislikes implements Serializable {

	private static final long serialVersionUID = 6476231207348476989L;

	@SerializedName("success")
	private boolean success;
	
	@SerializedName("likes")
	private int likes;
	
	@SerializedName("dislikes")
	private int dislikes;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getDislikes() {
		return dislikes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}
	
}
