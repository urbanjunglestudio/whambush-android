package com.whambush.android.client.domain.feed;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.WhambushList;


public class Feeds extends WhambushList<Feed> {

	private static final long serialVersionUID = 6612230151108941688L;

	@SerializedName("default")
	private String defaultFeedIndex;

	public String getDefaultFeedIndex() {
		return defaultFeedIndex;
	}

	public void setDefaultFeedIndex(String defaultFeedIndex) {
		this.defaultFeedIndex = defaultFeedIndex;
	}

	@Override
	public String toString() {
		return "Feeds [defaultFeedIndex=" + defaultFeedIndex + ", getCount()="
				+ getCount() + "]";
	}
}
