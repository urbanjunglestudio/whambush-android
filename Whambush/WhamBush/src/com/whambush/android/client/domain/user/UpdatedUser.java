package com.whambush.android.client.domain.user;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.country.Country;


public class UpdatedUser implements Serializable {

	private static final long serialVersionUID = 4606514202403968873L;

	public static final String KEY = "UpdatedUser";

	@SerializedName("profile_picture")
	private String picture;

	@SerializedName("email")
	private String email;

	@SerializedName("description")
	private String description;
	
	@SerializedName("birthday")
	private String birthday;
	
	@SerializedName("country")
	private String country;

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "UpdatedUser [picture=" + picture + ", email=" + email
				+ ", description=" + description + ", birthday=" + birthday
				+ ", country=" + country + "]";
	}
	
	
			
}
