package com.whambush.android.client.domain.feed;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.WhambushObject;

public class Feed extends WhambushObject implements Serializable {

	private static final long serialVersionUID = -5118102293366464416L;

	public static final String KEY = "Feed";

	@SerializedName("icon_url")
	private String iconUrl;
	
	@SerializedName("default_icon")
	private int defaultIcon;
	
    @SerializedName("endpoint_url")
    private String url;
    
	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public int getDefaultIcon() {
		return defaultIcon;
	}

	public void setDefaultIcon(int defaultIcon) {
		this.defaultIcon = defaultIcon;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Feed [iconUrl=" + iconUrl + ", defaultIcon=" + defaultIcon
				+ ", url=" + url + ", getName()="
				+ getName() + "]";
	}	
}
