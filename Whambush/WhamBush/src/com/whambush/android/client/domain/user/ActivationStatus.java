package com.whambush.android.client.domain.user;

import com.google.gson.annotations.SerializedName;

public enum ActivationStatus {

	@SerializedName("ACTIVATED")
	ACTIVATED,
	
	@SerializedName("NEW")
	NEW;
	
}
