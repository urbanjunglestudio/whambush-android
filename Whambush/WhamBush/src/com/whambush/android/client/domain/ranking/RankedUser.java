package com.whambush.android.client.domain.ranking;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class RankedUser implements Serializable {
	
	private static final long serialVersionUID = 6767884131192095496L;

	@SerializedName("username")
	private String username;
	
	@SerializedName("rank")
	private int rank;
	
	@SerializedName("rank_local")
	private int localRank;
	
	@SerializedName("userId")
	private String userId;
	
	@SerializedName("country")
	private String country;
	
	@SerializedName("score")
	private int score;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getLocalRank() {
		return localRank;
	}

	public void setLocalRank(int localRank) {
		this.localRank = localRank;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
}
