package com.whambush.android.client.domain.country;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.whambush.android.client.domain.WhambushObject;

public class Country extends WhambushObject implements Serializable {

	private static final long serialVersionUID = -5118102293366464416L;

	public static final String KEY = "Country";

	@SerializedName("country")
	private String country;

	@SerializedName("flag_url")
	private String image;

	@SerializedName("banner_url")
	private String bigFlag;
	
	@SerializedName("supported")
	private boolean supported;

	private transient boolean separator = true;
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isSupported() {
		return supported;
	}

	public void setSupported(boolean supported) {
		this.supported = supported;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isSeparator() {
		return separator;
	}

	public void setSeparator(boolean separator) {
		this.separator = separator;
	}

	public String getBigFlag() {
		return bigFlag;
	}

	public void setBigFlag(String bigFlag) {
		this.bigFlag = bigFlag;
	}

	@Override
	public String toString() {
		return "Country [country=" + country + ", image=" + image + ", supported=" + supported + "]";
	}
}
