package com.whambush.android.client.domain.video;

public enum VideoType {

	NORMAL_VIDEO(0),
	PARTNER_VIDEO(1);
	
	private final int value;

	VideoType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
	
}
