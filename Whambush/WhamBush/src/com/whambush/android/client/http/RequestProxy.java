package com.whambush.android.client.http;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import roboguice.util.Ln;
import android.app.Activity;
import android.content.Context;
import android.os.Looper;

import com.google.gson.JsonSyntaxException;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.SyncHttpClient;
import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.http.requests.Request;

public class RequestProxy {

	private static final int TIMEOUT = 30000;

	private static final int RESPONSE_TIMEOUT = 30000;

	private static final String APPLICATION_JSON = "application/json";

	protected final static Logger LOGGER = Logger.getLogger(RequestProxy.class.getName());

	private final AsyncHttpClient syncSender = new SyncHttpClient();
	private final AsyncHttpClient aSyncSender = new AsyncHttpClient();

	private final HashMap<Context, List<RequestHandle>> handles = new HashMap<Context, List<RequestHandle>>();

	public RequestProxy() {
		setParams(syncSender);
		setParams(aSyncSender);
	}
	
	private void setParams(AsyncHttpClient sender) {
		sender.setTimeout(TIMEOUT);
		sender.setResponseTimeout(RESPONSE_TIMEOUT);
		sender.setMaxConnections(5);
		sender.getHttpClient().getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
	}

	public void get(Context context, Request request, HTTPResultCallback callback) {
		addToHandles(context, getSender().get(context, request.getURL(), request.getHeaders(), request.getParameters(), createCallback(callback)));
	}

	public void post(AbstractActivity context, Request request, HTTPResultCallback callback) {

		Ln.d("Sending Post %s : %s", request.getURL(), request.getEntity());
		
		if (request.getEntity() == null)
			addToHandles(context, getSender().post(context, request.getURL(), request.getHeaders(), request.getParameters(), null, createCallback(callback)));
		else
			addToHandles(context, getSender().post(context, request.getURL(), request.getHeaders(), request.getEntity(), null, createCallback(callback)));
	}

	public void delete(AbstractActivity context, Request request, HTTPResultCallback callback) {

		Ln.d("Sending delete " + request.getURL());

		if (request.getEntity() == null)
			addToHandles(context, getSender().delete(context, request.getURL(), request.getHeaders(), request.getParameters(), createCallback(callback)));
		else {
			HttpDeleteWithBody del = createRequest(request.getURL());
			del.setEntity(request.getEntity());
			send(context, del, callback);
		}
	}
	
	public void put(AbstractActivity context, Request request, HTTPResultCallback callback) {
		
		if(request.getEntity() != null && request.getEntity().getContentType().getValue().equals(APPLICATION_JSON))
			addToHandles(context, getSender().put(context, request.getURL(), request.getHeaders(), request.getEntity(), APPLICATION_JSON, createCallback(callback)));
		else
			addToHandles(context, getSender().put(context, request.getURL(), request.getHeaders(), request.getMultiEntity(), null, createCallback(callback)));
	}


	protected HttpDeleteWithBody createRequest(String url) {
		HttpDeleteWithBody request = new HttpDeleteWithBody(url);
		request.setHeader("Content-type", "application/json");
		request.setHeader("Accept", "application/json");

		return request;
	}


	class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {
		public static final String METHOD_NAME = "DELETE";
		public String getMethod() { return METHOD_NAME; }

		public HttpDeleteWithBody(final String uri) {
			super();
			setURI(URI.create(uri));
		}
		public HttpDeleteWithBody(final URI uri) {
			super();
			setURI(uri);
		}
		public HttpDeleteWithBody() { super(); }
	}

	public void send(final Activity context, final HttpRequestBase request, final HTTPResultCallback callback) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					HttpContext httpContext = new BasicHttpContext();
					HttpParams httpParameters = new BasicHttpParams(); 
					int timeoutConnection = 10000;
					HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
					int timeoutSocket = 10000;
					HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
					HttpClient httpClient = new DefaultHttpClient(httpParameters);
					final HttpResponse response = httpClient.execute(request, httpContext);
					
					context.runOnUiThread(new Runnable()
					{
						public void run()
						{
							callback.result(response.getStatusLine().getStatusCode(), getData(response));
						}
					});

				} catch (ClientProtocolException e) {
					Ln.e(this.getClass().getName(), "Unable to get http response " + request.getURI().toString(), e);
				} catch (IOException e) {
					Ln.e(this.getClass().getName(), "Unable to get http response " + request.getURI().toString(), e);
				}

			}
		}).start();
	}

	private String getData(HttpResponse response) {

		try {
			HttpEntity entity = response.getEntity();
			String page = EntityUtils.toString(entity);
			return page;
		} catch (Throwable e) {
			return "";
		}
	}

	private AsyncHttpResponseHandler createCallback(final HTTPResultCallback callback) {
		return new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				Ln.d("Success %d", arg0);
				Ln.d("Success content %d", arg2!=null?arg2.length:-1);

				String result = null;
				if (arg2 != null)
					result = new String(arg2);
				try {
					callback.result(arg0, result);
				} catch (JsonSyntaxException ex) {
					Ln.w(ex, "Invalid content received from server");
					callback.result(HttpURLConnection.HTTP_INTERNAL_ERROR, null);
				}
			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				String res = "";
				if (arg2 != null && arg2.length > 0)
					res = new String(arg2);
				Ln.w(arg3, "failed %d : %s", arg0, res);
				
				callback.result(arg0, res);

			}
		};
	}

	private void addToHandles(Context context, RequestHandle handle) {
		synchronized (handles) {
			List<RequestHandle> requests = handles.get(context);
			if (requests == null)
				requests = new ArrayList<RequestHandle>();

			requests.add(handle);
			handles.put(context, requests);
		}
	}

	public synchronized void cancelRequests(Context activity) {
		handles.remove(activity);
		getSender().cancelRequests(activity, true);
	}

	public AsyncHttpClient getSender() {
		if (Looper.myLooper() == null)
			return syncSender;
		return aSyncSender;
	}
}
