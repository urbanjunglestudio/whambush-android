package com.whambush.android.client.http.requests;

import android.content.Context;

public class PostRequest extends Request {

	public PostRequest(Context context, String url) {
		this(context, url, false, true);
	}

	public PostRequest(Context context, String url, boolean fullURL) {
		this(context, url, fullURL, true);
	}

	public PostRequest(Context context, String url, boolean fullURL, boolean addAuthToken) {
		super(context, url, fullURL, addAuthToken);
	}
}
