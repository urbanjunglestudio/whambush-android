package com.whambush.android.client.http.requests;

import android.content.Context;

public class DeleteRequest extends Request {

	public DeleteRequest(Context context, String url) {
		super(context, url, false, true);
	}

}
