package com.whambush.android.client.http;

public interface HTTPResultCallback {

	public static final HTTPResultCallback EMPTY = new DummyHTTPResultCallback();

	public void result(int httpResultCode, String result);
	
	
	public static class DummyHTTPResultCallback implements HTTPResultCallback {

		public void result(int httpResultCode, String result) {
			// Empty on purpose
		}
		
	}
}
