package com.whambush.android.client;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.inject.internal.Nullable;
import com.whambush.android.client.activities.MainActivity;
import com.whambush.android.client.activities.MissionsActivity;
import com.whambush.android.client.activities.RecordActivity;
import com.whambush.android.client.activities.RootChannelActivity;
import com.whambush.android.client.activities.SplashScreen;
import com.whambush.android.client.activities.TourActivity;
import com.whambush.android.client.activities.UploadActivity;
import com.whambush.android.client.activities.UserActivity;
import com.whambush.android.client.config.AppConfig;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.dialog.ActivationDialog;
import com.whambush.android.client.dialog.DialogButtons;
import com.whambush.android.client.dialog.LoginDialog;
import com.whambush.android.client.dialog.LoginOrRegisterDialog;
import com.whambush.android.client.dialog.RegisterDialog;
import com.whambush.android.client.domain.country.Country;
import com.whambush.android.client.domain.user.ActivationStatus;
import com.whambush.android.client.domain.user.InvalidLoginInfo;
import com.whambush.android.client.domain.user.LoginInfo;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.service.DeviceInfoService;
import com.whambush.android.client.service.GCMService;
import com.whambush.android.client.service.LoginService;
import com.whambush.android.client.service.RegisterService;
import com.whambush.android.client.service.SoftwareVersionService;
import com.whambush.android.client.service.SubscribeService;
import com.whambush.android.client.service.VideoCreationService;
import com.whambush.android.client.service.listeners.GCMServiceListener;
import com.whambush.android.client.service.listeners.LoginServiceListener;
import com.whambush.android.client.service.listeners.RegisterServiceListener;
import com.whambush.android.client.service.listeners.SubscribeServiceListener;
import com.whambush.android.client.service.listeners.UserInfoFailureReason;
import com.whambush.android.client.service.listeners.VideoCreationServiceListener;
import com.whambush.android.client.view.CountView;
import com.whambush.android.client.view.UploadProgressBar2;

public abstract class WhambushActivity extends AbstractWhambushActivity implements VideoCreationServiceListener, LoginServiceListener,
RegisterServiceListener, SubscribeServiceListener {

	protected static final String NOTE = "NOTE";
	private static final int SELECT_COUNTRY = 321;

	private static boolean missionsShown = false;
	protected static boolean profileShown = false;

	private VideoCreationService videoService;

	@InjectResource(R.string.UPLOAD_CANCEL_VIDEO_UPLOAD)
	private String UPLOAD_CANCEL_VIDEO_UPLOAD;

	@InjectResource(R.string.UPLOAD_ERROR_UPLOAD_FAIL)
	private String UPLOAD_ERROR_UPLOAD_FAIL;

	@Nullable
	@InjectView(R.id.missionCountView)
	private CountView missionCountView;

	@Nullable
	@InjectView(R.id.profileCountView)
	private CountView profileCountView;	

	@Nullable
	@InjectView(R.id.uploadProgressBarIndicator)
	protected ImageView uploadImage;

	@Nullable
	@InjectView(R.id.uploadProgressBar)
	protected UploadProgressBar2 upload;

	@InjectResource(R.string.GENERAL_NO)
	private String GENERAL_NO;

	@InjectResource(R.string.GENERAL_YES)
	private String GENERAL_YES;

	@InjectResource(R.anim.fadeout)
	private Animation fadeout;

	@InjectView(R.id.footer)
	RelativeLayout footer;

	@Nullable
	@InjectView(R.id.profileButton)
	ImageButton profileImage;

	@Nullable
	@InjectView(R.id.backHolder)
	private RelativeLayout backButton;

	private String promocode;

	private RegisterService registerService;

	@InjectResource(R.anim.rotate_full)
	private Animation rotate;

	@Nullable
	@InjectView(R.id.reloadHolder)
	private View reloadHolder;

	@Nullable
	@InjectView(R.id.countryHolder)
	private View countryHolder;

	@Nullable
	@InjectView(R.id.reload)
	private ImageView reload;

	@Nullable
	@InjectView(R.id.list)
	private ListView list;

	@Nullable
	@InjectView(R.id.mainSelector)
	private View mainSelector;

	@Nullable
	@InjectView(R.id.missionSelector)
	private View missionSelector;

	@Nullable
	@InjectView(R.id.tvSelector)
	private View tvSelector;

	@Nullable
	@InjectView(R.id.profileSelector)
	private View profileSelector;

	@InjectResource(R.string.LOGIN_ERROR_INACTIVE_USER) private String LOGIN_ERROR_INACTIVE_USER;
	@InjectResource(R.string.LOGIN_RESEND_ACTIVATION_LINK_PRODUCTION) private String LOGIN_RESEND_ACTIVATION_LINK_PRODUCTION;
	@InjectResource(R.string.LOGIN_RESEND_ACTIVATION_LINK) private String LOGIN_RESEND_ACTIVATION_LINK;
	@InjectResource(R.string.LOGIN_RESEND_ACTIVATION) private String LOGIN_RESEND_ACTIVATION;
	@InjectResource(R.string.LOGIN_ERROR_BANNED_USER) private String LOGIN_ERROR_BANNED_USER;
	@InjectResource(R.string.LOGIN_ERROR_WRONG_CREDENTIALS) private String INCORRECT_CREDENTIALS;
	@InjectResource(R.string.LOGIN_RESET_PASSWORD) private String RESET_PASSWORD;
	@InjectResource(R.string.LOGIN_RESET_PASSWORD_LINK_PRODUCTION) private String LOGIN_RESET_PASSWORD_LINK_PRODUCTION;
	@InjectResource(R.string.LOGIN_RESET_PASSWORD_LINK) private String LOGIN_RESET_PASSWORD_LINK;
	@InjectResource(R.string.GENERAL_LOGIN) private String LOGIN;
	@InjectResource(R.string.LOGIN_FAQ_LINK) private String FAQ;
	@InjectResource(R.string.LOGIN_FAQ_LINK_PRODUCTION) private String FAQ_PRODUCTION;

	@InjectResource(R.anim.upload)
	private Animation uploadAnimation;

	/* For the Log in functionality */
	private String password;
	private String email;
	private String username;
	private Dialog dialog;
	private String country;

	private static Selector currentSelector = Selector.MAIN;

	private Selector selector = null;


	@Override
	protected void onCreate(Bundle savedInstanceState, int activity, boolean fullScreen) {

		if (!isValidLogin()) {
			Ln.w("Invalid login detected to splashscreen");
			startActivity(SplashScreen.class);
			return;
		}

		super.onCreate(savedInstanceState, activity, fullScreen);

		if (!isValidServices()) {
			startActivity(SplashScreen.class);
			return;
		}

		setVideoService(Whambush.get(VideoCreationService.class)); 
		getVideoService().registerResultListener(this);

		updateUserIcon();
		setupUpload();
		populateActiveMissionCount();
		populateMissingProfileCount();
		showNote();
		checkPlayServices();
		hideSelectors();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("password", password);
		outState.putString("email", email);
		outState.putString("username", username);
		outState.putString("country", country);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState != null) {
			password = savedInstanceState.getString("password");
			email = savedInstanceState.getString("email");
			username = savedInstanceState.getString("username");
			country = savedInstanceState.getString("country", country);
		}
	}


	protected String getSelectedCountry() {
		String country = getCountry();
		if (country.isEmpty()) {
			country = Whambush.get(ConfigImpl.class).getGuestCountry();
		}
		return country;
	}

	protected void checkCountry() {
		String country = getSelectedCountry();
		if (country == null || country.isEmpty()) {
			Intent intent = new Intent(this, TourActivity.class);
			startActivityForResult(intent, SELECT_COUNTRY);
			return;
		}
		start();
	}

	protected void start() {
		if (!Whambush.get(ConfigImpl.class).getLoginInfo().isGuest() &&
				Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getActivation() == ActivationStatus.NEW) {
			showActivationDialog(Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getEmail());
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Ln.d("Returned from Tour " + requestCode + " " + resultCode);

		if (requestCode == SELECT_COUNTRY && resultCode != Activity.RESULT_OK) {
			checkCountry();
			return;
		} else if (requestCode == SELECT_COUNTRY) {
			Whambush.get(ConfigImpl.class).setGuestCountry(data.getStringExtra(Country.KEY));
			start();
		}
	}

	private void showSelector() {
		hideSelectors();
		switch (selector) {
		case MAIN:
			activateMainSelector();
			break;
		case MISSION:
			activateMissionSelector();
			break;
		case TV:
			activateTvSelector();
			break;
		case PROFILE:
			activateProfileSelector();
			break;
		}
	}

	private void hideSelectors() {
		hide(mainSelector);
		hide(profileSelector);
		hide(tvSelector);
		hide(missionSelector);
	}

	protected void hide(int id) {
		hide(findViewById(id));
	}

	protected void hide(View view) {
		if (view != null)
			view.setVisibility(View.INVISIBLE);
	}

	protected void onCreate(Bundle savedInstanceState, int activity) {
		super.onCreate(savedInstanceState, activity, true);
	}

	protected void updateUserIcon() {
		if (getLoginInfo().isGuest() && profileImage != null) {
			profileImage.setImageResource(R.drawable.profile_guest);
		} else if (profileImage != null)
			profileImage.setImageResource(R.drawable.profile);
	}

	private void showNote() {
		String note = (String)getParameter(NOTE);
		if (note != null)
			Toast.makeText(this, note, Toast.LENGTH_LONG).show();
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (!isValidServiceAndLoggedIn()) {
			return;
		}

		ConfigImpl configImpl = Whambush.get(ConfigImpl.class);
		if (!isValidLogin() && !configImpl.isTriedToLogin() &&
				configImpl.getUsername() != null && !configImpl.getUsername().isEmpty() &&
				configImpl.getPassword() != null && !configImpl.getPassword().isEmpty()) {
			doLogin(null, configImpl.getUsername(), configImpl.getPassword());
			configImpl.setTriedToLogin(true);	
		}

		if (selector == null)		
			selector = currentSelector;

		updateUserIcon();
		checkPlayServices();
		populateActiveMissionCount();
		populateMissingProfileCount();
		showSelector();
	}

	protected void populateActiveMissionCount() {
		if (missionCountView == null)
			return;

		if (missionsShown || getActiveMissionCount() == 0)
			missionCountView.setVisibility(View.GONE);
		else
			missionCountView.setCount(getActiveMissionCount());
	}

	protected void populateMissingProfileCount() {
		if (profileCountView == null)
			return;

		if (profileShown || getMissingProfileCount() == 0)
			profileCountView.setVisibility(View.GONE);
		else
			profileCountView.setCount(getMissingProfileCount());
	}

	private int getActiveMissionCount() {
		if (getLoginInfo() == null)
			return 0;
		return getLoginInfo().getActiveMissions();
	}

	private int getMissingProfileCount() {		
		return getLoginInfo().getUnreadActivities();
	}

	protected LoginInfo getLoginInfo() {
		return Whambush.get(ConfigImpl.class).getLoginInfo();
	}

	private void setupUpload() {
		Ln.d("The upload %s", upload);
		if (upload == null)
			return;

		if (videoService.getStatus() != Status.FINISHED) {
			videoService.setListener(upload);
			uploadAnimation.setRepeatCount(Animation.INFINITE);
			uploadImage.setAnimation(uploadAnimation);
			uploadAnimation.start();
			upload.setVisibility(View.VISIBLE);
			uploadImage.setVisibility(View.VISIBLE);
		} else {
			upload.setVisibility(View.INVISIBLE);
			uploadImage.setVisibility(View.INVISIBLE);
		}
	}

	protected void showLoginOrRegister() {
		DialogFragment dialog = new LoginOrRegisterDialog();
		dialog.show(getFragmentManager(), "LoginOrRegisterDialogFragment");
	}

	public void recordButtonPressed(View button) {

		if (videoService.getStatus() == Status.FINISHED)
			goToRecordActivity(new HashMap<String, Serializable>());
		else
			askToCancelUpload();
	}

	protected void goToRecordActivity(Map<String, Serializable> params) {
		if (videoService.getStatus() == Status.FINISHED) {
			params.put(RecordActivity.BACK_ACTIVITY, this.getClass());
			params.put(RecordActivity.DONE_ACTIVITY, UploadActivity.class);

			startActivity(RecordActivity.class, params);
		}
	}

	protected void askToCancelUpload() {
		DialogButtons buttons = new DialogButtons();
		buttons.add(Dialog.BUTTON_POSITIVE, GENERAL_YES, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {
				doCancelUpload();
				videoService.setMission(null);
			}
		});

		buttons.add(Dialog.BUTTON_NEGATIVE, GENERAL_NO, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});				
		AlertDialog.Builder alertDialog = getDialogHelper().createBasicDialog(buttons, UPLOAD_CANCEL_VIDEO_UPLOAD, null);

		AlertDialog dialog = alertDialog.show();
		setButton(dialog.getButton(DialogInterface.BUTTON_POSITIVE));
		setButton(dialog.getButton(DialogInterface.BUTTON_NEGATIVE));
	}

	private void setButton(Button button) {
		button.setBackgroundColor(getResources().getColor(R.color.whambushBody));
		button.setTextColor(getResources().getColor(R.color.dialog_body_text_color));
	}

	private void doCancelUpload() {
		getVideoService().cancelUpload();
		fadeoutUploads();
	}

	private void activateMainSelector() {
		if (mainSelector != null)
			mainSelector.setVisibility(View.VISIBLE);
	}

	private void activateTvSelector() {
		if (tvSelector != null)
			tvSelector.setVisibility(View.VISIBLE);
	}

	private void activateMissionSelector() {
		if (missionSelector != null)
			missionSelector.setVisibility(View.VISIBLE);
	}

	private void activateProfileSelector() {
		if (profileSelector != null)
			profileSelector.setVisibility(View.VISIBLE);
	}

	public void mainButtonPressed(View view) {		
		startActivity(MainActivity.class, Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
	}

	public void missionButtonPressed(View button) {
		Ln.d("Mission button pressed");
		startActivity(MissionsActivity.class, Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
	}

	public void profileButtonPressed(View button) {
		if (getLoginInfo().isGuest()) {
			showLoginOrRegister();
		} else
			startActivity(UserActivity.class, Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
	}

	public void tvButtonPressed(View view) {
		startActivity(RootChannelActivity.class, Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
	}

	public void logoClicked(View view) {
		scrollToTop();
	}

	protected void scrollToTop() {
		if (list != null)
			list.smoothScrollToPosition(0,0);
	}

	public VideoCreationService getVideoService() {
		return videoService;
	}

	public void setVideoService(VideoCreationService videoService) {
		this.videoService = videoService;
	}

	public void created() {
		fadeoutUploads();
		Toast.makeText(this, R.string.UPLOAD_SAVE_OK, Toast.LENGTH_SHORT).show();
	}

	public void cannotCreate() {
		fadeoutUploads();
		getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(UPLOAD_ERROR_UPLOAD_FAIL));
	}

	private void fadeoutUploads() {
		Animation fadeout2 = AnimationUtils.loadAnimation(this, R.anim.fadeout);
		uploadImage.startAnimation(fadeout2);
		upload.startAnimation(fadeout);
		upload.setVisibility(View.GONE);
		uploadImage.setVisibility(View.GONE);
	}

	public void goBack(View view) {
		onBackPressed();
	}

	protected void hideBackButton() {
		if (backButton != null)
			backButton.setVisibility(View.GONE);
	}

	protected void showBackButton() {
		if (backButton != null)
			backButton.setVisibility(View.VISIBLE);
	}


	@Override
	protected void showTextInput() {
		hideFooter();
		super.showTextInput();
	}

	public int getFooterVisibilty() {
		return footer.getVisibility();
	}

	public void hideFooter() {
		footer.setVisibility(View.GONE);
	}

	@Override
	protected void hideTextInput() {
		super.hideTextInput();
		footer.setVisibility(View.VISIBLE);
	}

	public void showFooter() {
		footer.setVisibility(View.VISIBLE);
	}


	@Override
	public void loggedIn(LoginInfo loginInfo) {
		hideProgressBar();
		closeDialog();

		Ln.d("Logged In %s", loginInfo);

		ConfigImpl config = Whambush.get(ConfigImpl.class);
		config.setUsername(username);
		config.setPassword(password);
		config.setGuestId(loginInfo.getGuestId());
		config.setLoginInfo(loginInfo);
		config.setAuthenticationToken(loginInfo.getAuthenticationToken());
		config.setSelectedSearchMainFeed(null);
		config.setSelectedMainFeed(null);


		new GCMService(this).getReqID(new GCMServiceListener() {

			@Override
			public void error(int httpResultCode) {
			}

			@Override
			public void connectionError() {
			}

			@Override
			public void unregistered() {
			}

			@Override
			public void registered() {
			}
		});
		updateUserIcon();
		resetActiveMission();
		populateActiveMissionCount();
		resetProfileCount();
		populateMissingProfileCount();
		userLoggedIn();
	}

	private void resetProfileCount() {
		if (profileCountView != null)
			profileCountView.setVisibility(View.VISIBLE);

		profileShown = false;
	}

	private void resetActiveMission() {
		if (missionCountView != null)
			missionCountView.setVisibility(View.VISIBLE);

		missionsShown = false;
	}

	private void closeDialog() {
		if (dialog != null) {
			dialog.dismiss();
			dialog = null;
		}
	}

	@Override
	public void invalidLogin(InvalidLoginInfo info) {
		hideProgressBar();
		if (info.hasErrors()) {
			if (info.isDisabled()) {
				showActivateAlert();
				return;
			} else if (info.isBanned()) {
				showBannedAlert();
				return;
			}
		}
		showResetAlert();
	}

	protected void showActivateAlert() {
		AlertDialog.Builder builder = getDialogHelper().createWarningDialog(LOGIN_ERROR_INACTIVE_USER);

		builder.setNegativeButton(LOGIN_RESEND_ACTIVATION, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResendActivationLink())); 
				startActivity(intent);
			}
		});
		getDialogHelper().displayDialog(builder);
	}

	private String getResendActivationLink() {
		if (AppConfig.DEBUG)
			return LOGIN_RESEND_ACTIVATION_LINK;
		else
			return LOGIN_RESEND_ACTIVATION_LINK_PRODUCTION;
	}

	protected void showBannedAlert() {
		AlertDialog.Builder builder = getDialogHelper().createWarningDialog(LOGIN_ERROR_BANNED_USER);
		getDialogHelper().displayDialog(builder);
	}


	protected void showResetAlert() {
		AlertDialog.Builder builder = getDialogHelper().createErrorDialog(INCORRECT_CREDENTIALS);

		builder.setNegativeButton(RESET_PASSWORD, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geResetPasswordLink())); 
				startActivity(intent);
			}
		}).setPositiveButton(LOGIN, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				DialogFragment loginDialog = new LoginDialog(username, password);
				loginDialog.show(getFragmentManager(), "LoginDialogFragment");
			}
		});
		getDialogHelper().displayDialog(builder);
	}

	protected String geResetPasswordLink() {
		if (AppConfig.DEBUG)
			return LOGIN_RESET_PASSWORD_LINK;
		else
			return LOGIN_RESET_PASSWORD_LINK_PRODUCTION;
	}

	protected String geFAQLink() {
		if (AppConfig.DEBUG)
			return FAQ;
		else
			return FAQ_PRODUCTION;
	}

	public void doLogin(Dialog dialog, String username, String password) {
		this.dialog = dialog;
		this.username = username;
		this.password = password;
		showProgressBar();
		ConfigImpl config = Whambush.get(ConfigImpl.class);
		LoginService loginService = new LoginService(this);

		loginService.createLoginRequest(username, password, config.getGuestId(), 
				new DeviceInfoService().getInfo(), new SoftwareVersionService(this).getVersion(), this);
	}

	public boolean doRegister(Dialog dialog, String username, String password,
			String rePassword, String email, String country) {

		this.dialog = dialog;
		this.username = username;
		this.password = password;
		this.email = email;
		this.country = country;

		if (containsEmptyFields(username, password, rePassword, email, country))
			return false;

		registerService = new RegisterService(this);

		if (!registerService.isValidEmail(email))
			showWarning(R.string.REGISTER_ERROR_BROKEN_EMAIL, getListenerForReregistration());
		else if (registerService.isPasswordTooShort(password) || registerService.isPasswordTooShort(rePassword))
			showWarning(R.string.REGISTER_ERROR_PASSWORD_TOO_SHORT, getListenerForReregistration());
		else if (!rePassword.equals(password))
			showWarning(R.string.REGISTER_ERROR_PASSWORD_MISMATCH, getListenerForReregistration());
		else {
			showProgressBar();
			registerService.createRegisterRequest(username, email, password, rePassword, promocode, country, this);
			return true;
		}

		return false;
	}

	private DialogInterface.OnClickListener getListenerForReregistration() {
		return new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				showRegisterDialog();
			}
		};
	}

	private boolean containsEmptyFields(String email, String rePassword, String password, String username, String country) {
		if (!isValid(email) ||
				!isValid(rePassword) || 
				!isValid(password) ||
				!isValid(username)) {
			showWarning(R.string.REGISTER_ERROR_EMPTY_FIELDS, getListenerForReregistration());
			return true;
		} else if (!isValid(country)) {
			showWarning(R.string.REGISTER_ERROR_EMPTY_COUNTRY, getListenerForReregistration());
			return true;
		}
		return false;
	}

	private boolean isValid(String field) {
		return field != null && !field.trim().isEmpty();
	}

	private void showWarning(int id, DialogInterface.OnClickListener listener) {
		showWarning(getResources().getString(id), listener);
	}

	private void showWarning(String text, DialogInterface.OnClickListener listener) {
		Builder createWarningDialog = getDialogHelper().createWarningDialog(text, listener);
		getDialogHelper().displayDialog(createWarningDialog);
	}


	@Override
	public void invalidRegister(UserInfoFailureReason[] reasons) {

		if (isPromocodeRequired(reasons)) {
			askPromocode();
			hideProgressBar();
		} else {
			Ln.d("Invalid registration reason others");
			hideProgressBar();

			showUserInfoFailure(reasons, getListenerForReregistration());
		}
	}

	protected void showUserInfoFailure(UserInfoFailureReason[] reasons, OnClickListener onClickListener) {
		String reason = formatReason(reasons);
		showWarning(reason, onClickListener);
	}

	private String formatReason(UserInfoFailureReason[] reasons) {
		String reason = "";

		for (int i = 0; i < reasons.length; i++) {
			UserInfoFailureReason registrationFailureReason = reasons[i];
			reason += translate(registrationFailureReason);
			if (i < (reasons.length - 1))
				reason += "\n";
		}
		return reason;
	}

	private void showRegisterDialog() {
		DialogFragment loginDialog = new RegisterDialog(username, email);
		loginDialog.show(getFragmentManager(), "RegisterDialogFragment");
	}

	private void askPromocode() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.REGISTER_ERROR_PROMO_CODE));

		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);


		LinearLayout ll = new LinearLayout(this);
		ll.setOrientation(LinearLayout.VERTICAL);

		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

		layoutParams.setMargins(12, 12, 12, 12);
		ll.addView(input, layoutParams);
		builder.setView(ll);

		// Set up the buttons
		builder.setPositiveButton(R.string.REGISTER_REGISTER_BUTTON, new DialogInterface.OnClickListener() { 
			@Override
			public void onClick(DialogInterface dialog, int which) {
				promocode = input.getText().toString();
				registerService.createRegisterRequest(username, email, password, password, promocode, country, WhambushActivity.this);
			}
		});
		builder.setNegativeButton(R.string.REGISTER_NOTIFY_WHEN_AVAILABLE, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				showProgressBar();
				new SubscribeService(WhambushActivity.this).createSubscribeRequest(email, WhambushActivity.this);
			}
		});

		builder.show();
	}

	private boolean isPromocodeRequired(UserInfoFailureReason[] reasons) {
		return reasons.length == 1 && reasons[0] == UserInfoFailureReason.PROMOCODE_REQUIRED;
	}

	private String translate(UserInfoFailureReason reason) {
		switch (reason) {
		case EMAIL_ALREADY_EXISTS:
			return getString(R.string.REGISTER_ERROR_EMAIL);
		case PASSWORD_TOO_SHORT:
			return getString(R.string.REGISTER_ERROR_PASSWORD_TOO_SHORT);
		case PROMOCODE_REQUIRED:
			return getString(R.string.REGISTER_ERROR_PROMO_CODE);
		case USERNAME_ALREADY_TAKEN:
			return getString(R.string.REGISTER_ERROR_USERNAME);
		case GUEST_ID:
			return getString(R.string.GENERAL_ERROR_UNKNOWN);
		case COUNTRY_MISSING:
			return getString(R.string.REGISTER_ERROR_EMPTY_COUNTRY);
		}
		return "";
	}

	@Override
	public void registered(User user) {
		hideProgressBar();

		Builder builder = getDialogHelper().createNoteDialog(getResources().getString(R.string.REGISTER_NEW_USER_INFO), email);
		builder.setPositiveButton(getResources().getString(R.string.GENERAL_OK), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				doLogin(WhambushActivity.this.dialog, username, password);
			}
		});
		getDialogHelper().displayDialog(builder);
	}

	@Override
	public void error() {
		getDialogHelper().displayDialog(getDialogHelper().createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_UNKNOWN)));
	}

	@Override
	public void subscribed() {
	}

	/**
	 * Override this if activity wants notification about logged in event
	 */
	protected void userLoggedIn() {

	}

	public void linkClicked(View view) {
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getTermsAndConditionLink())); startActivity(intent);
	}

	private String getTermsAndConditionLink() {
		if (AppConfig.DEBUG)
			return getResources().getString(R.string.REGISTER_TERMS_CONDITIONS_LINK);
		else
			return getResources().getString(R.string.REGISTER_TERMS_CONDITIONS_LINK_PRODUCTION);
	}

	protected void setupReloadImage() {
		reload.setVisibility(View.VISIBLE);
		reloadHolder.setVisibility(View.VISIBLE);
	}

	protected void setupCountryHolder() {
		countryHolder.setVisibility(View.VISIBLE);


	}

	public void startReloadAnimation() {
		rotate.setRepeatCount(Animation.INFINITE);
		reload.startAnimation(rotate);
	}

	public void stopReloadAnimation() {
		if (rotate.hasStarted()) {
			rotate.cancel();
			rotate.reset();
		}
	}

	protected void missionShown(boolean b) {
		missionsShown = true;
	}

	protected void setupKeyboardListener() {

		final View activityRootView = findViewById(R.id.themainlayout);
		activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
				if (heightDiff > 100)
					hideFooter();
				else
					showFooter();
			}
		});
	}


	public abstract void reload(View view);

	public void listDataReceived() {

	}

	public Selector getCurrentSelector() {
		return currentSelector;
	}

	public void setCurrentSelector(Selector selector) {
		currentSelector = selector;
	}

	protected void showActivationDialog(String email) {
		ActivationDialog activationDialog = new ActivationDialog(email);
		activationDialog.show(getFragmentManager(), "ActivationDialogFragment");
	}

	protected String getCountry() {	
		User user = Whambush.get(ConfigImpl.class).getLoginInfo().getUser();

		Ln.d(Whambush.get(ConfigImpl.class).getLoginInfo());

		if (user != null && user.getCountry() != null)
			return user.getCountry().getCountry();
		else if (getLoginInfo().isGuest()) {
			return Whambush.get(ConfigImpl.class).getGuestCountry();
		}

		return "";
	}
}
