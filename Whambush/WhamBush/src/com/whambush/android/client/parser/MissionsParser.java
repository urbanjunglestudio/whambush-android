package com.whambush.android.client.parser;

import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.mission.Missions;

public class MissionsParser extends AbstractParser {
	
	public Missions create(String json) {		
		return parse(json, Missions.class);
	}

	public Mission parseMission(String json) {
		return parse(json, Mission.class);
	}
}
