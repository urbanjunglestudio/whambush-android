package com.whambush.android.client.parser;

import com.whambush.android.client.domain.user.RegisterError;


public class RegisterFailureParser extends AbstractParser {
	
	public RegisterError create(String json) {		
		return parse(json, RegisterError.class);
	}
}
