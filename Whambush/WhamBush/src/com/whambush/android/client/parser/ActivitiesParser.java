package com.whambush.android.client.parser;

import com.whambush.android.client.domain.activity.Activities;

public class ActivitiesParser extends AbstractParser {
	
	public Activities create(String json) {		
		return parse(json, Activities.class);
	}
}
