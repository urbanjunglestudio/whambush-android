package com.whambush.android.client.parser;

import com.whambush.android.client.domain.tv.Channels;


public class ChannelParser extends AbstractParser {

	public Channels create(String result) {
		return parse(result, Channels.class);
	}
}
