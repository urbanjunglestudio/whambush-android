package com.whambush.android.client.parser;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public abstract class AbstractParser {

	private final Gson gson;

	protected AbstractParser() {
		gson = new Gson(); // Basic parser no need for GsonBuilder. Maybe in future???
	}

	public <T> T parse(String json, Class<T> type) {
		try {
			return gson.fromJson(json, type);
		} catch (JsonSyntaxException ex) {
			throw ex;
		}
	}
}
