package com.whambush.android.client.parser;

import roboguice.util.Ln;

import com.whambush.android.client.domain.user.InvalidLoginInfo;
import com.whambush.android.client.domain.user.LoginInfo;


public class LoginParser extends AbstractParser {
	
	public LoginInfo create(String json) {
		Ln.d("Creating LoginInfo %s", json);
		return parse(json, LoginInfo.class);
	}
	
	public InvalidLoginInfo createInvalidLogin(String json) {		
		return parse(json, InvalidLoginInfo.class);
	}
}
