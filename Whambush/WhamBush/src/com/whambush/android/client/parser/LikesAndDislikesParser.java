package com.whambush.android.client.parser;

import com.whambush.android.client.domain.likes.LikesAndDislikes;

public class LikesAndDislikesParser extends AbstractParser {
	
	public LikesAndDislikes create(String json) {		
		return parse(json, LikesAndDislikes.class);
	}

}
