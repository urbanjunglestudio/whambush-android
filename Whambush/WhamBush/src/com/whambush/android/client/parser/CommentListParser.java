package com.whambush.android.client.parser;

import com.whambush.android.client.domain.comment.CommentList;

public class CommentListParser extends AbstractParser {
	
	public CommentList create(String json) {		
		return parse(json, CommentList.class);
	}
}
