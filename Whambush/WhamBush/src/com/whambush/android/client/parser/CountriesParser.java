package com.whambush.android.client.parser;

import com.whambush.android.client.domain.country.Countries;

public class CountriesParser extends AbstractParser {
	
	public Countries create(String json) {		
		return parse(json, Countries.class);
	}
}
