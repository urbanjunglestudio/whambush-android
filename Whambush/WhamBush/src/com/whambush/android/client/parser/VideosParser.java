package com.whambush.android.client.parser;

import com.whambush.android.client.domain.video.Videos;


public class VideosParser extends AbstractParser {

	public Videos create(String result) {
		return parse(result, Videos.class);
	}
}