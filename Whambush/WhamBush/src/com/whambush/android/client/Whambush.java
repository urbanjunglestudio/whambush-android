package com.whambush.android.client;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender.Method;
import org.acra.sender.HttpSender.Type;

import roboguice.util.Ln;
import android.app.Application;
import android.content.Context;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.whambush.android.client.config.AppConfig;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.domain.country.Countries;
import com.whambush.android.client.http.RequestProxy;
import com.whambush.android.client.service.CountryService;
import com.whambush.android.client.service.VideoCreationService;
import com.whambush.android.client.service.listeners.CountryServiceListener;

@ReportsCrashes(
		formKey = "",
		httpMethod = Method.PUT,
		reportType = Type.JSON,
		formUri = "http://whambush.iriscouch.com/acra-myapp/_design/acra-storage/_update/report",
		formUriBasicAuthLogin = "whambush",
		formUriBasicAuthPassword = "WhamTheBush#",
		excludeMatchingSharedPreferencesKeys={"password_key", "auth_token_key", "GCM_REQ_ID"},
		maxNumberOfRequestRetries = 1,

		mode = ReportingInteractionMode.SILENT,
		resDialogText = R.string.crash_dialog_text,
		resDialogIcon = android.R.drawable.ic_dialog_info,
		resDialogTitle = R.string.crash_dialog_title,
		resDialogCommentPrompt = R.string.crash_dialog_comment_prompt,
		resDialogOkToast = R.string.crash_dialog_ok_toast
		)
public class Whambush extends Application {

	private static final int LOAD_INTERVAL = 5000;

	private static Context context;

	private static final Map<Class<?>, Object> resources = new HashMap<Class<?>, Object>();

	private static CountryService countryLoader;

	public static void add(Object obj) {
		resources.put(obj.getClass(), obj);
	}

	@SuppressWarnings("unchecked")
	public static <E> E get(Class<E> resourceClass) {
		return (E)resources.get(resourceClass);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		setContext(getApplicationContext());
		createServices();
		initCrashReport();
		
	}

	public static void createServices() {
		Ln.i("Creating Services");
		Whambush.add(new ConfigImpl());
		Whambush.add(new RequestProxy());
		Whambush.add(new VideoCreationService());
		Whambush.add(new VideoEntryDao());
		confImageCache();
		loadCountries();
	}


	private static void confImageCache() {
		if (ImageLoader.getInstance().isInited())
			return;

		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Whambush.getContext())
		.defaultDisplayImageOptions(defaultOptions)
		.build();
		ImageLoader.getInstance().init(config);
	}

	static boolean successLoad = false;
	
	private static void loadCountries() {
		Ln.i("Starting to load countries");
		getCountryLoader().getCountries(new CountryServiceListener() {

			@Override
			public void error(int httpResultCode) {
				postLoad();
			}

			@Override
			public void connectionError() {
				postLoad();
			}

			@Override
			public void countries(Countries countries) {
				if (successLoad) {
					get(ConfigImpl.class).setCountries(countries);
					return;
				}
				successLoad = true;
				postLoad();
			}
		});
	}	
	
	private static CountryService getCountryLoader() {
		if (countryLoader == null)
			countryLoader = new CountryService(getContext());
		return countryLoader;
	}

	private static void postLoad() {
		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				Ln.i("Starting to post load countries");
				loadCountries();
			}
		}, LOAD_INTERVAL);

	}

	private void initCrashReport() {
		if (!AppConfig.DEFAULT_BUG_REPORT)
			ACRA.init(this);
	}

	public static Context getContext() {
		return context;
	}

	public static void setContext(Context context) {
		Whambush.context = context;
	}
}
