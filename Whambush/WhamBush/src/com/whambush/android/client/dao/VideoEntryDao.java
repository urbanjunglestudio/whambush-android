package com.whambush.android.client.dao;

import java.util.HashMap;
import java.util.Map;

import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.domain.video.Videos;

public class VideoEntryDao {

	Map<String, VideoEntry> storage = new HashMap<String, VideoEntry>();

	public void addVideoEntries(Videos videos) {
		for (VideoEntry video : videos.getResults())
			storage.put(video.getId(), video);
	}

	public VideoEntry getVideoEntry(String id) {
		return storage.get(id);
	}

	public void add(VideoEntry video) {
		storage.put(video.getId(), video);
	}

	public void clear() {
		storage.clear();
	}

}
