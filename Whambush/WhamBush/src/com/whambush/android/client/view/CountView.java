package com.whambush.android.client.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.whambush.android.client.R;

public class CountView extends View {

	Paint p = new Paint();

	private int backgroundColor;

	public CountView(Context context) {
		this(context, null);
	}

	public CountView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public CountView(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CountView, count, R.style.CountView);

		backgroundColor = a.getColor(R.styleable.CountView_backgroundColor, R.color.whambushRed);
		a.recycle();
	}

	public void setCount(int count) {
		
	}

	@Override
	protected void onDraw(Canvas canvas) {
		p.setAntiAlias(true);

		drawInnerCircle(canvas);
	}


	private void drawInnerCircle(Canvas canvas) {
		p.setStyle(Paint.Style.FILL);
		p.setColor(backgroundColor);
		int circleWidth = getCircleWidth();
		canvas.drawCircle(getCircleX(), getCircleY(), circleWidth, p);
	}

	private int getCircleY() {
		return getCircleWidth();
	}

	private int getCircleX() {
		return getWidth() - (getCircleWidth());
	}

	private int getCircleWidth() {
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
	}

}
