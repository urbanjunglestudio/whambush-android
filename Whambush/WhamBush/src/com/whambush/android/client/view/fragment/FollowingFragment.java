package com.whambush.android.client.view.fragment;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.service.FollowingService;
import com.whambush.android.client.service.PaginateService;


public class FollowingFragment extends AbstractFollowFragment {

	public FollowingFragment() {
		super();
	}

	@Override
	protected PaginateService getService() {
		return new FollowingService((AbstractActivity)getActivity());
	}

}
