package com.whambush.android.client.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.whambush.android.client.R;

public class BubbleRelativeLayout extends RelativeLayout {

	private Bubble bubble;


	public BubbleRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		createBubble();
	}

	public BubbleRelativeLayout(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
		createBubble();
	}

	public BubbleRelativeLayout(Context context) {
		super(context);
		createBubble();
	}
	
	private void createBubble() {
		bubble = new Bubble(getResources().getColor(R.color.bubble));		
	}
	
	@Override
    protected void dispatchDraw(Canvas canvas) {

		bubble.drawBubble(getWidth(), getHeight(), canvas);
		super.dispatchDraw(canvas);

	}

	public boolean isLeftBubble() {
		return bubble.isLeftBubble();
	}

	public void setLeftBubble(boolean leftBubble) {
		bubble.setLeftBubble(leftBubble);
	}

}
