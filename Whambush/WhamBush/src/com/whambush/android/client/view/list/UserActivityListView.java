package com.whambush.android.client.view.list;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;

import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.activities.UserActivity;
import com.whambush.android.client.adapter.UserActivityAdapter;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.activity.ActivityFeedEvent;
import com.whambush.android.client.service.ActivityService;

public class UserActivityListView extends PaginatedListView<ActivityFeedEvent> {	

	private UserActivityAdapter adapter;

	public UserActivityListView(Context context) {
		super(context);
	}

	public UserActivityListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public UserActivityListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void setActivity(final WhambushActivity activity) {
		super.setActivity(activity);

		setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (adapter.getCount() > 0)
					((UserActivity)activity).activityClicked((ActivityFeedEvent)adapter.getItem(position));
			}
		});
		
		ActivityService service = new ActivityService(activity);
		addPaginateLoader(service);
		setBaseRequest(service.getBase());

		adapter = new UserActivityAdapter(activity);
		setAdapter(adapter);
		loadBase();
	}

	@Override
	public void paginateItems(final WhambushList<ActivityFeedEvent> items) {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			public void run() {
				adapter.addAll(items.getResults());
				UserActivityListView.super.paginateItems(items);
				requestLayout();
			}
		});
	}

	@Override
	public void endOfListReached() {
		super.endOfListReached();
	}

	@Override
	public void reload() {
		adapter.clear();
		super.reload();
	}

	public int getUnreadCount() {
		return adapter.getUnreadCount();
	}

	public void setAllUnread() {
		adapter.setAllUnread();
	}
}
