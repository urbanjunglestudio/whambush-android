package com.whambush.android.client.view;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.util.DateAndTimeParser;

public class TimeView extends TextView {


	private static final int ONE_SECOND_DELAY = 1000;
	private Date time;
	private boolean showRemain = true;
	private String dateStr;
	private long diff;

	public TimeView(Context context) {
		super(context);
	}

	public TimeView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TimeView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	private void startTimer() {
		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				Handler mainHandler = new Handler(getContext().getMainLooper());

				Runnable myRunnable = new Runnable() {
					@Override 
					public void run() {
						updateTime();
					}
				};
				mainHandler.post(myRunnable);
			}
		}, ONE_SECOND_DELAY);

	}

	protected void updateTime() {
		diff = DateAndTimeParser.calculateDiffToNow(time);
		if (diff < 0) {
			if (showRemain) {
				List<Integer> diffs = DateAndTimeParser.getDiff(diff);
				if (diffs.get(0) < 30)
					setText(String.format(getContext().getString(R.string.MISSIONS_TIMER_TEXT), diffs.get(0), diffs.get(1), diffs.get(2), diffs.get(3)));
				else
					setText(getContext().getString(R.string.ENDS_NOT_ANY_TIME_SOON));

				startTimer();
			} else
				setText(DateAndTimeParser.getAsTime(getResources(), dateStr));
		} else {
			setText(getContext().getString(R.string.ENDS_NOT_RUNNING));
			setTextColor(getContext().getResources().getColor(R.color.whambushRed));
		}
	}


	public void change() {
		showRemain = !showRemain;
		invalidate();
		startTimer();
	}

	public void setTime(String date) {
		this.dateStr = date;
		try {
			time = DateAndTimeParser.parse(date);
			updateTime();
			startTimer();
		} catch (ParseException e) {
			setText("?");
		}
	}

	public long getDiff() {
		return diff;
	}

	public void setDiff(long newDiff) {
		diff= newDiff;
	}

}
