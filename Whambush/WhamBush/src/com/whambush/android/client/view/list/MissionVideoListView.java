package com.whambush.android.client.view.list;

import roboguice.util.Ln;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.adapter.VideoListAdapter;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.likes.LikesAndDislikesError;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.domain.video.Videos;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.service.DislikeService;
import com.whambush.android.client.service.LikeService;
import com.whambush.android.client.service.UndislikeService;
import com.whambush.android.client.service.UnlikeService;
import com.whambush.android.client.service.VideoFeedService;
import com.whambush.android.client.service.listeners.DislikeServiceListener;
import com.whambush.android.client.service.listeners.LikeServiceListener;
import com.whambush.android.client.service.listeners.UndislikeServiceListener;
import com.whambush.android.client.service.listeners.UnlikeServiceListener;
import com.whambush.android.client.util.CountHelper;
import com.whambush.android.client.view.BananaShitPointer;
import com.whambush.android.client.view.SingleMissionDetails;
import com.whambush.android.client.view.listener.PaginateListListener;

public class MissionVideoListView extends PaginatedListView<VideoEntry> implements UndislikeServiceListener, DislikeServiceListener, 
UnlikeServiceListener, LikeServiceListener {	

	private Mission mission;
	private boolean isShitting = false;
	private boolean isBananing = false;
	private PaginateListListener listListener;
	private VideoListAdapter adapter;

	public MissionVideoListView(Context context) {
		super(context);
	}

	public MissionVideoListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MissionVideoListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void setActivity(WhambushActivity activity, Mission entry) {
		this.mission = entry;
		super.setActivity(activity);
		addPaginateLoader(new VideoFeedService(activity));
		this.addHeaderView(getHeader());
		setAdapter(adapter = new VideoListAdapter(activity, false));

		adapter.setRanked(false);
		updateLikesAndDisplikes();
		setBaseRequest(((VideoFeedService)service).createSearchForMissionVideoURL(mission.getId()));
		loadBase();
	}
	
	@SuppressLint("InflateParams")
	private View getHeader() {
		LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		SingleMissionDetails details = (SingleMissionDetails)inflater.inflate(R.layout.header_single_mission_details, null);
		details.setMission(mission);
		return details;
	}

	@SuppressWarnings("deprecation")
	public void shareTheVideo(View view) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		i.putExtra(Intent.EXTRA_TITLE, activity.getString(R.string.GENERAL_APP_NAME));
		i.putExtra(Intent.EXTRA_SUBJECT,  String.format(activity.getString(R.string.MISSIONS_SHARE_MESSAGE), mission.getName()));
		i.putExtra(Intent.EXTRA_TEXT, mission.getMissionUrl());
		activity.startActivity(Intent.createChooser(i, activity.getString(R.string.GENERAL_APP_NAME)));		
	}

	public void updateLikesAndDisplikes() {
		ImageView banana = activity.getImageView(R.id.banana);
		if (mission.isHasLiked())
			banana.setImageResource(R.drawable.banana_active);
		else
			banana.setImageResource(R.drawable.banana_default);

		ImageView shit = activity.getImageView(R.id.shit);
		if (mission.isHasDisliked())
			shit.setImageResource(R.drawable.shit_active);
		else
			shit.setImageResource(R.drawable.shit_inactive);

		setLikeCount();
	}

	private void setLikeCount() {
		TextView commentCount = activity.getTextView(R.id.likeCount);
		commentCount.setTypeface(activity.getFonts().getOtherFont());
		int likeCount = getLikeCount();
		commentCount.setText(new CountHelper(activity).countAsStr(likeCount));

		BananaShitPointer view = (BananaShitPointer)findViewById(R.id.bubleLayer);

		if (mission.getLikeCount() == 0 && mission.getDislikeCount() == 0) {
			view.setVisibility(View.INVISIBLE);
			commentCount.setTextColor(getResources().getColor(R.color.whambushBottomBar));
		} else {
			view.setVisibility(View.VISIBLE);
			if (likeCount >= 0) {
				view.setLikeDislike(true);
				commentCount.setTextColor(getResources().getColor(R.color.whambushGreen));
			} else {
				view.setLikeDislike(false);
				commentCount.setTextColor(getResources().getColor(R.color.whambushRed));
			}
		}
	}

	private int getLikeCount() {
		return mission.getLikeCount() - mission.getDislikeCount();
	}


	@Override
	public void reload() {
		adapter.clear();
		super.reload();
	}

	public void shitTheVideo(View view) {
		if (isShitting)
			return;
		isShitting = true;
		if (mission.isHasDisliked()) {
			new UndislikeService(activity).undislike(mission, this);
		} else {
			new DislikeService(activity).dislike(mission, this);
		}
	}

	public void bananaTheVideo(View view) {
		if (isBananing)
			return;

		isBananing = true;
		if (mission.isHasLiked()) {
			new UnlikeService(activity).unlike(mission, this);
		} else {
			new LikeService(activity).like(mission, this);
		}
	}

	@Override
	public void liked(LikesAndDislikes result) {
		mission.setHasLiked(true);
		mission.setHasDisliked(false);
		update(result);
		isBananing = false;
		activity.hideProgressBar();
	}

	@Override
	public void unliked(LikesAndDislikes result) {
		mission.setHasLiked(false);
		update(result);
		isBananing = false;
		activity.hideProgressBar();
	}

	@Override
	public void disliked(LikesAndDislikes result) {
		mission.setHasDisliked(true);
		mission.setHasLiked(false);
		update(result);
		isShitting = false;
		activity.hideProgressBar();
	}

	@Override
	public void undisliked(LikesAndDislikes result) {
		mission.setHasDisliked(false);
		update(result);
		isShitting = false;
		activity.hideProgressBar();
	}

	private void update(LikesAndDislikes result) {
		mission.setLikeCount(result.getLikes());
		mission.setDislikeCount(result.getDislikes());
		update();
	}

	private void update() {
		updateLikesAndDisplikes();
	}

	@Override
	public void errorResult(final LikesAndDislikesError error) {
		activity.getDialogHelper().displayDialog(activity.getDialogHelper().createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_UNKNOWN)));
	}

	@Override
	public void paginateItems(final WhambushList<VideoEntry> items) {
		Ln.d("Received comments");
		
		super.paginateItems(items);
		Whambush.get(VideoEntryDao.class).addVideoEntries((Videos)items);
		adapter.addVideos(items.getResults());
		listListener.loaded();

	}

	@Override
	protected Request getNext(WhambushList<VideoEntry> items) {
		return service.parse(items.getNext(), true); 
	}

	public void setListener(PaginateListListener listListener) {
		this.listListener = listListener;
	}

	public VideoListAdapter getVideoListAdapter() {
		return adapter;
	}

}
