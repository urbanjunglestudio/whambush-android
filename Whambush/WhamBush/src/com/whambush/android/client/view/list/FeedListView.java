package com.whambush.android.client.view.list;

import roboguice.util.Ln;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.WhambushObject;
import com.whambush.android.client.domain.feed.Feed;
import com.whambush.android.client.domain.feed.Feeds;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.service.FeedService;
import com.whambush.android.client.service.SearchService;
import com.whambush.android.client.service.listeners.FeedServiceListener;
import com.whambush.android.client.util.Fonts;
import com.whambush.android.client.util.RightDrawableOnTouchListener;
import com.whambush.android.client.util.ViewAnimation;
import com.whambush.android.client.view.FeedSelectionListener;

public abstract class FeedListView<T> extends PaginatedListView<T> implements FeedServiceListener, FeedSelectionListener {

	protected RelativeLayout feedView;
	private boolean searchEnabled = true;

	private String searching;

	protected Feed currentFeed;

	protected Feed selectedFeed;
	protected Feeds feeds;
	private SearchService searchService;

	private int paddingBottom;
	private Fonts fonts;

	public FeedListView(Context context) {
		super(context);
	}

	public FeedListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FeedListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void addPaginateLoader(SearchService service) {
		super.addPaginateLoader(service);
		this.searchService = service;
	}

	@Override
	public void init(Context context) {
		super.init(context);

		initHeader();
	}

	/**
	 * Override this if you want to change the header
	 */
	protected void initHeader() {
		Ln.d("Init the header");
		feedView = (RelativeLayout) getInflater().inflate(R.layout.header_feed_list, this, false);

		setParams(feedView, R.id.searchText, R.drawable.search);

		final TextView search = (TextView)findView(R.id.searchText);
		search.setOnTouchListener(new RightDrawableOnTouchListener(search) {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (activity != null) {
					int padd = getPaddingBottom();
					if (padd > 0)
						paddingBottom = padd;
					setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), 0);
					activity.hideFooter();
				}
				return super.onTouch(v, event);
			}

			@Override
			public boolean onDrawableTouch(final MotionEvent event) {
				return onClickSearch(event, search);
			}

			private boolean onClickSearch(MotionEvent event, final View view) {
				search.setText("");
				event.setAction(MotionEvent.ACTION_CANCEL);
				return false;

			}
		});

		addSearchListener(feedView);
		addHeaderView(feedView);
		feedView.setVisibility(View.INVISIBLE);
	}

	public void setHeaderVisibility(int visibility) {
		feedView.setVisibility(visibility);
	}

	@Override
	public void setVisibility(int visibility) {
		super.setVisibility(visibility);
		feedView.setVisibility(visibility);
	}

	public void enableSearch(boolean enabled) {
		this.searchEnabled = enabled;
		changeSearchState();
	}

	private void changeSearchState() {
		View view = feedView.findViewById(R.id.searchText);
		if (searchEnabled)
			view.setVisibility(View.VISIBLE);
		else
			view.setVisibility(View.GONE);
	}

	private void addSearchListener(RelativeLayout feedView2) {
		EditText text = (EditText)feedView2.findViewById(R.id.searchText);
		text.setOnEditorActionListener(createListener());
	}

	private TextView.OnEditorActionListener createListener() {
		TextView.OnEditorActionListener exampleListener = new TextView.OnEditorActionListener(){

			@Override
			public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					String string = view.getText().toString();
					if (string.trim().length() > 0) {
						hideKeyboard();
						if (activity != null) {
							activity.showFooter();
							addPadding();
						}
						stopFeedSelection();
						search(string);
						setCurrentToSearch(string);
					}
				}
				return true;
			}
		};
		return exampleListener;
	}

	private void addPadding() {
		Ln.d("Setting adding " + paddingBottom);
		if (paddingBottom > 0)
			setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), paddingBottom);
	}

	private int getColor(int color) {
		return getParentContext().getResources().getColor(color);
	}

	protected Drawable getDrawable(int id) {
		return getParentContext().getResources().getDrawable(id);
	}

	private void highlightFeed(View view) {
		if (view != null)
			view.setBackgroundColor(getColor(R.color.feed_selection_hightlight));
	}

	private String getString(int id) {
		return getParentContext().getString(id);
	}

	private String getLocalizedName(Feed feed) {
		String name = feed.getName();

		if (name.equals("MAIN_FEED_LATEST"))
			name = getString(R.string.MAIN_FEED_LATEST);
		else if (name.equals("MAIN_FEED_MY_VIDEOS"))
			name = getString(R.string.MAIN_FEED_MY_VIDEOS);
		else if (name.equals("MAIN_FEED_FOLLOW"))
			name = getString(R.string.MAIN_FEED_FOLLOW);
		else if (name.equals("MAIN_FEED_FEATURED"))
			name = getString(R.string.MAIN_FEED_FEATURED);
		else if (name.equals("ACTIVE_MISSION"))
			name = getString(R.string.MISSIONS_ACTIVE_FEED);
		else if (name.equals("OLD_MISSION"))
			name = getString(R.string.MISSIONS_ARCHIVE_FEED);
		else if (name.equals("MAIN_FEED_RANDOM"))
			name = getString(R.string.MAIN_FEED_RANDOM);

		return name;
	}

	private void setImage(final Feed feed, final TextView view) {

		Drawable drawable = null;

		switch (feed.getDefaultIcon()) {
		case 1:
			drawable = getDrawable(R.drawable.following);
			break;
		case 2: 
			drawable = getDrawable(R.drawable.popular);
			break;
		case 3:
			drawable = getDrawable(R.drawable.latest);
			break;
		case 4:
			drawable = getDrawable(R.drawable.myvideos);
			break;
		case 5:
			drawable = getDrawable(R.drawable.star);
			break;
		case 6:
			drawable = getDrawable(R.drawable.eye);
			break;
		case 7:
			drawable = getDrawable(R.drawable.banana_active);
			break;
		case 8:
			drawable = getDrawable(R.drawable.shit_active);
			break;
		case 10:
			drawable = getDrawable(R.drawable.random);
			break;
		case 11:
			drawable = getDrawable(R.drawable.tv_feed);
			break;
		case 12:
			drawable = getDrawable(R.drawable.share_active);
			break;
		case 13:
			drawable = getDrawable(R.drawable.comment);
			break;
		case 14:
			drawable = getDrawable(R.drawable.flag_inactive);
			break;
		default:
			drawable = getDrawable(R.drawable.default_icon);
		}

		doSetImage(drawable, view);


		if (feed.getIconUrl() != null && feed.getIconUrl().length() > 0) {
			ImageLoader.getInstance().displayImage(feed.getIconUrl(), new ImageView(getContext()), new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View v, Bitmap loadedImage) {
					Drawable drawable = new BitmapDrawable(FeedListView.this.getResources(), loadedImage);
					drawable.setBounds(0, 0, (int)getResources().getDimension(R.dimen.feed_icon_size), (int)getResources().getDimension(R.dimen.feed_icon_size));
					Drawable[] drawables = view.getCompoundDrawables();
					view.setCompoundDrawables(drawable, drawables[1], drawables[2], drawables[3]);				
				}
			});
		}
	}


	public void setCurrentToSearch(String search) {
		TextView view = (TextView)findViewById(R.id.current);
		view.setTypeface(getFonts().getDescriptionFont());
		view.setText(search);
		doSetImage(R.drawable.search, view);
		clearCurrent();
		currentFeed = null;
	}

	protected void updateCurrentView(Feed feed) {
		TextView view = (TextView)findViewById(R.id.current);
		view.setTypeface(getFonts().getDescriptionFont());
		view.setText(getLocalizedName(feed));
		setImage(feed, view);
		highlightFeed(view);
	}


	private View create(Feed feed) {
		TextView view = (TextView) getInflater().inflate(R.layout.list_feed_entry, this, false);

		view.setTag(feed);
		view.setId(feed.getId().hashCode());
		view.setOnClickListener(getClickListener());
		view.setTypeface(getFonts().getDescriptionFont());
		view.setText(getLocalizedName(feed));
		setImage(feed, view);

		return view;
	}

	private void stopFeedSelection() {
		ViewAnimation.expand(findViewById(R.id.current));
		ViewAnimation.collapse(findViewById(R.id.selection));
	}

	private void doSetImage(int drawable, TextView textView) {
		Drawable editTextDrawable = getParentContext().getResources().getDrawable(drawable);
		doSetImage(editTextDrawable, textView);
	}

	public void startFeedSelection() {
		if (isFeedSelectionEnabled()) {
			ViewAnimation.collapse(findViewById(R.id.current));
			ViewAnimation.expand(findViewById(R.id.selection));
			changeSearchState();
		}
	}

	private void clearCurrent() {
		if (currentFeed != null) {
			findView(currentFeed.getId().hashCode()).setBackgroundColor(getColor(R.color.whambushFeedBody));
			currentFeed = null;
		}
	}

	private OnClickListener getClickListener() {
		return new OnClickListener() {

			@Override
			public void onClick(View view) {
				hideKeyboard();
				if (activity != null) {
					activity.showFooter();
					addPadding();
				}
				stopFeedSelection();

				Feed feed = (Feed)view.getTag();
				if (feed != currentFeed) {
					clearCurrent();
					highlightFeed(view);
					setCurrentFeed(feed);
					selected(feed);
				}
			}
		};
	}


	private View findView(int id) {
		return feedView.findViewById(id);
	}

	private void populateFeeds(Feeds feeds, Feed defaultFeed) {
		LinearLayout layout = (LinearLayout)findView(R.id.selection);

		for (Feed feed : feeds.getResults()) {
			View view = create(feed);
			layout.addView(view);

			if (defaultFeed == feed)
				highlightFeed(view);
		}

	}

	protected void doSetImage(Drawable leftDrawable, TextView textView) {
		leftDrawable.setBounds(0, 0, (int)getResources().getDimension(R.dimen.feed_icon_size), (int)getResources().getDimension(R.dimen.feed_icon_size));

		Drawable[] drawables = textView.getCompoundDrawables();


		if (textView instanceof EditText) {
			if (drawables[2] != null)
				drawables[2].setBounds(0,0,(int)getResources().getDimension(R.dimen.feed_icon_size),(int)getResources().getDimension(R.dimen.feed_icon_size));
		} else {
			if (drawables[2] != null)
				drawables[2].setBounds(0,0,(int)getResources().getDimension(R.dimen.feed_clear_search_width),(int)getResources().getDimension(R.dimen.feed_clear_search_height));
		}

		textView.setCompoundDrawables(leftDrawable, drawables[1], drawables[2], drawables[3]);
	}

	public void setFeeds(Feeds feeds, Feed defaultFeed) {		
		populateFeeds(feeds, defaultFeed);
		setCurrentFeed(defaultFeed);
	}

	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager)getParentContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getWindowToken(), 0);
	}

	protected void setParams(RelativeLayout mRefreshView, int id, int drawable) {
		setFont(mRefreshView, id);
		setImage(mRefreshView, id, drawable);
	}

	private void setImage(RelativeLayout mRefreshView, int myvideos, int drawable) {
		TextView textView = (TextView)mRefreshView.findViewById(myvideos);
		doSetImage(drawable, textView);
	}

	private void setFont(RelativeLayout mRefreshView, int id) {
		if (!this.isInEditMode())
			((TextView)mRefreshView.findViewById(id)).setTypeface(getFonts().getDescriptionFont());
	}

	public void setCurrentFeed(Feed feed) {
		this.currentFeed = feed;
		updateCurrentView(feed);
	}

	public Fonts getFonts() {
		if (fonts == null)
			fonts = new Fonts();

		return fonts;
	}

	public void setSearchText(String text) {
		TextView search = (TextView)findView(R.id.searchText);
		search.setText(text);
	}

	public WhambushObject getCurrentFeed() {
		return currentFeed;
	}

	public String getSearching() {
		return searching;
	}


	public void loadFeeds() {

	}

	@Override
	public void feeds(final Feeds feeds) {
		setFeeds(feeds, true);
	}

	protected void setFeeds(final Feeds feeds, boolean updateFeedsView) {
		this.feeds = feeds;

		Ln.d("Got feeds %s", feeds.toString());

		FeedService feedService = new FeedService(activity);

		Ln.d("Else Feed");
		selectedFeed = feedService.findDefaultFeed(feeds);
		currentFeed = selectedFeed;
		if (updateFeedsView)
			setFeeds(feeds, selectedFeed);
		Ln.d("default feed URL : %s", selectedFeed.getUrl());
		setAndLoadBase();
	}

	public void setAndLoadBase() {
		setBaseRequest(service.parse(selectedFeed.getUrl(), false, getExtras()));
		loadBase();
		activity.viewChanged();
	}

	protected String[] getExtras() {
		return null;
	}


	public void selectWithRequest(String name, Request request) {
		setCurrentToSearch(name);
		setBaseRequest(request);
		loadBase();
	}
	
	public void selectWithRequest(Request request) {
		hideFeedHeader();
		setBaseRequest(request);
		loadBase();
	}

	private void hideFeedHeader() {
		findView(R.id.main_view_list_header).setVisibility(View.GONE);
		//feedView.setVisibility(View.GONE);
	}

	@Override
	public void selected(Feed feed) {
		ConfigImpl config = Whambush.get(ConfigImpl.class);
		config.setSelectedMainFeed(feed.getId());
		config.setSelectedSearchMainFeed(null);
		currentFeed = feed;
		searching = null;
		activity.viewChanged();
		Ln.d("Feed URL : %s", feed.getUrl());

		setBaseRequest(service.parse(feed.getUrl(), false, getExtras()));
		loadBase();
		showFooter();
		addPadding();
	}

	@Override
	public void search(String text) {
		Ln.d("Search %s", text);
		ConfigImpl config = Whambush.get(ConfigImpl.class);
		config.setSelectedSearchMainFeed(text);
		config.setSelectedMainFeed(null);
		currentFeed = null;
		searching = text;
		activity.viewChanged();
		Request url = searchService.getSearchRequest(text);
		setBaseRequest(url);
		loadBase();
		showFooter();
		addPadding();
	}

	public boolean isFeedSelectionEnabled() {
		return !isLoading();
	}

	public Feeds getFeeds() {
		return feeds;
	}

	public void hideIcons() {
		TextView view = (TextView)findViewById(R.id.current);
		view.setCompoundDrawables(null, null, null, null);
	}

	public View getHeader() {
		return feedView;
	}
}
