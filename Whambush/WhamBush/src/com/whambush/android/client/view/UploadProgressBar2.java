package com.whambush.android.client.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

import com.whambush.android.client.R;
import com.whambush.android.client.service.listeners.UploadListener;

public class UploadProgressBar2 extends View implements UploadListener {

	private int progress = 0;
	
	Paint p = new Paint();

	public UploadProgressBar2(Context context) {
		this(context, null);
	}
	
	public UploadProgressBar2(Context context, AttributeSet attrs) {
		this(context, attrs, 0); 
	}

	public UploadProgressBar2(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
	}

	@Override
	public void status(final int d) {
		new Handler(getContext().getMainLooper()).post(new Runnable() {
			public void run() {
				UploadProgressBar2.this.setProgress(d);
				invalidate();
			}
		});
	}

	@Override
	protected void onDraw(Canvas canvas) {
		p.setAntiAlias(true);
		drawPRogressbar(canvas);
	}

	private void drawPRogressbar(Canvas canvas) {
		p.setColor(getContext().getResources().getColor(R.color.whambushRed));
		p.setStyle(Paint.Style.FILL); 
		canvas.drawRect(0, 0, calculateWidth(), getHeight(), p);
	}

	private float calculateWidth() {
		return (((float)getWidth()) / 100f) * getProgressInNumber();
	}
	
	public float getProgressInNumber() {
		if (progress < 0)
			return 0;
		else if (progress > 100)
			return 100;
		else
			return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	@Override
	public void error(Exception ex) {
		status(Integer.MIN_VALUE);
	}

}
