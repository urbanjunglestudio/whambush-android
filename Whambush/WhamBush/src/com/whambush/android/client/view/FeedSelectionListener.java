package com.whambush.android.client.view;

import com.whambush.android.client.domain.feed.Feed;

public interface FeedSelectionListener {

	public void selected(Feed feed);

	public void search(String text);

	public boolean isFeedSelectionEnabled();
	
}
