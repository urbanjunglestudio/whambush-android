package com.whambush.android.client.view.fragment;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.activities.SingleVideoActivity;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.service.VideoFeedService;
import com.whambush.android.client.view.list.VideoListView;

public class UserVideoFeedFragment extends RoboFragment {

	@InjectView(R.id.list)
	VideoListView list;
	
	private User user;

	private CountListener listener; 
	
	public UserVideoFeedFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		super.onCreateView(inflater, container, savedInstanceState);
		return inflater.inflate(R.layout.view_video_list, container, false);
	}

	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		setup();
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Ln.d(list.getAdapter().getClass().getName());
				VideoEntry video = list.getVideoListAdapter().getVideo(view);

				if (video == null)
					return;

				if (video.isProcessed()) {
					showVideo(video);
				}
			}
		});

	}
	
	public void showVideo(VideoEntry video) {

		if (video == null)
			return;

		Map<String, Serializable> params = new HashMap<String, Serializable>();
		params.put(VideoEntry.KEY, video.getId());

		((AbstractActivity)getActivity()).startActivity(SingleVideoActivity.class, params);
	}	
	
	public void setup() {
		list.setActivity((WhambushActivity)getActivity());
		Request request = new VideoFeedService((WhambushActivity)getActivity()).createSearchForUserVideoURL(user.getId());
		list.setBaseRequest(request);
		list.setListener(listener);
		list.reload();
	}

	public void refresh() {
		if (list == null)
			return;
		list.reload();
	}

	public void setListener(CountListener countListener) {
		listener = countListener;
	}

	public void setUser(User user2) {
		user = user2;
	}
	
}
