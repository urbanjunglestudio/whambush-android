package com.whambush.android.client.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.whambush.android.client.util.Fonts;

public class RemainingTime extends View {

	private static final int START_B = 0x34;
	private static final int START_G = 0xC2;
	private static final int START_R = 0xAD;

	private final static double MAX_TIME_MSECONDS = 300.0;

	private static final double HALF_OF_TIME = MAX_TIME_MSECONDS / 2.0;

	private final ArrayList<Integer> timeStamps = new ArrayList<Integer>();

	//G ADC234
	//Y F5D42A
	float rFromGtoY = ((float)(0xAD - 0xF5)) / ((float)HALF_OF_TIME);
	float gFromGtoY = ((float)(0xC2 - 0xD4)) / ((float)HALF_OF_TIME);
	float bFromGtoY = ((float)(0x34 - 0x2A)) / ((float)HALF_OF_TIME);

	//R #CD554A
	float rFromYtoR = ((float)(0xF5 - 0xCD)) / ((float)HALF_OF_TIME);
	float gFromYtoR = ((float)(0xD4 - 0x55)) / ((float)HALF_OF_TIME);
	float bFromYtoR = ((float)(0x2A - 0x4A)) / ((float)HALF_OF_TIME);

	float currentR = START_R;
	float currentG = START_G;
	float currentB = START_B;

	private long time = 0;
	private Paint p = new Paint();
	
	private Fonts fonts;

	public RemainingTime(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public RemainingTime(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
	}

	public RemainingTime(Context context) {
		super(context);
	}

	public void setTime(long time) {
		this.time = time;
		updateColor();
		invalidate();	
	}

	@Override
	protected void onDraw(Canvas canvas) {

		RectF rectF = getRect();
		p.setAntiAlias(true);

		drawInnerCircle(canvas);
		drawArc(canvas, rectF);
		drawTime(canvas);
		drawStamps(canvas);
	}

	private void drawTime(Canvas canvas) {
		p.setStyle(Paint.Style.FILL);
		setColor();

		if (getFonts() != null)
			p.setTypeface(getFonts().getTitleFont());
		String t = "" + (int)((MAX_TIME_MSECONDS - time)/10);
		p.setTextSize(getWidth()/2); 
		p.setStrokeWidth(1);

		float textWidth = p.measureText(t);

		int xPos = (int)((getWidth() / 2.0) - (textWidth/2));
		int yPos = (int) ((getHeight() / 2) - ((p.descent() + p.ascent()) / 2)) ; 

		canvas.drawText(t, xPos, yPos, p);
	}

	private void drawArc(Canvas canvas, RectF rectF) {
		setColor();
		p.setStyle(Paint.Style.STROKE); 
		p.setStrokeWidth(getWidth() / 8);
		int startRarius = getStartRarius();
		int endRadius = getEndRadius();
		canvas.drawArc (rectF, startRarius, endRadius, false, p);
	}

	private void drawInnerCircle(Canvas canvas) {
		p.setStyle(Paint.Style.FILL); 
		p.setColor(Color.argb(255, 41, 43, 53));
		int circleWidth = (getWidth() / 4) + (getWidth() / 16);
		canvas.drawCircle((getWidth() / 2), (getHeight() / 2), circleWidth+1, p);
	}

	private void drawStamps(Canvas canvas) {

		Integer total = 0;
		for (Integer tStamp : timeStamps) {
			total += tStamp;
			RectF rectF = getRect();
			p.setColor(Color.rgb(0xff, 0xff, 0xff));
			int radius = (int)(((double)total) * (360.0/MAX_TIME_MSECONDS));
			p.setStyle(Paint.Style.STROKE); 
			p.setStrokeWidth(getWidth() / 8);
			canvas.drawArc (rectF, radius, 3, false, p);
		}
	}

	private void setColor() {
		p.setColor(Color.rgb((int)currentR, (int)currentG, (int)currentB));
	}

	/**
	 * FIX THIS
	 */
	private void updateColor() {

		currentR = START_R;
		currentB = START_B;
		currentG = START_G;
		
		for (int i = 0; i < time; i++) {
			if (i <= HALF_OF_TIME) {
				currentR -= rFromGtoY;
				currentG -= gFromGtoY;
				currentB -= bFromGtoY;
			} else {
				currentR -= rFromYtoR;
				currentG -= gFromYtoR;
				currentB -= bFromYtoR;
			}
		}
	}

	private RectF getRect() {
		int width = getWidth();
		int height = getHeight();

		RectF rectF = new RectF(getWidth() / 8, getWidth() / 8, width-(getWidth() / 8), height-(getWidth() / 8));

		return rectF;
	}

	private int getStartRarius() {
		return (int)(time * (360.0/MAX_TIME_MSECONDS));
	}

	private int getEndRadius() {
		return 360-getStartRarius();
	}

	public Integer setRecordingTimes(List<Integer> recordingLengths) {
		timeStamps.clear();
		timeStamps.addAll(recordingLengths);
		int total = 0;
		for (Integer integer : recordingLengths) {
			total += integer;
		}

		setTime(total);
		return total;
	}

	public Fonts getFonts() {
		if (fonts == null)
			fonts = new Fonts();
		
		return fonts;
	}
}
