package com.whambush.android.client.view;

import roboguice.RoboGuice;
import roboguice.util.Ln;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.whambush.android.client.R;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.util.CountHelper;
import com.whambush.android.client.util.Fonts;

public class SingleMissionDetails extends LinearLayout {

	private Mission mission;

	private Fonts fonts;


	public SingleMissionDetails(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public SingleMissionDetails(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
		init(context);
	}

	public SingleMissionDetails(Context context) {
		super(context);
		init(context);
	}

	private void init(Context context) {
		if (!isInEditMode()) { 
			RoboGuice.getInjector(context.getApplicationContext()).injectMembers(this);
			fonts = new Fonts();
		}
	}

	public void setMission(Mission mission) {
		this.mission = mission;
		init();
	}

	public void init() {
		setTitle();
		setUsername();
		setProfilePicture();
		setDescription();
		setSubmissionCount();
		setRecordLabel();
	}

	private void setRecordLabel() {
		TextView send = getTextView(R.id.sendVideoViewSingle);
		TextView done = getTextView(R.id.missionCompleted);
		
		if (mission.isMissionSubmissionDone()) {
			send.setVisibility(View.GONE);
		} else {
			send.setText(getContext().getString(R.string.MISSIONS_CREATE_MISSION_VIDEO));
			
			if (mission.getMaxUserSubmissions() > 1 && (mission.getMaxUserSubmissions() - mission.getHasSubmitted()) > 1) {
				send.setText(send.getText() +  
						String.format(getContext().getString(R.string.MISSIONS_SUBMISSIONS_LEFT), (mission.getMaxUserSubmissions() - mission.getHasSubmitted())));
			}
				
			done.setVisibility(View.GONE);
		}
	}

	private void setSubmissionCount() {
		TextView title = getTextView(R.id.submissionBody);
		title.setTypeface(fonts.getDescriptionFont());
		
		if (mission.getMaxSubmissions() == 0)
			title.setText(String.format(getContext().getString(R.string.MISSIONS_VIDEO_COUNT), new CountHelper(this.getContext()).countAsStr(mission.getNumberOfSubmissions())));
		else
			title.setText(String.format(getContext().getString(R.string.MISSIONS_VIDEO_COUNT_COUNT), new CountHelper(this.getContext()).countAsStr(mission.getNumberOfSubmissions()), 
					new CountHelper(this.getContext()).countAsStr(mission.getMaxSubmissions())));
	}

	private void setTitle() {
		TextView title = getTextView(R.id.mission_title);
		title.setTypeface(fonts.getTitleFont());
		title.setText(getTitleString(mission));
	}

	private void setProfilePicture() {
		ProfilePicture picture = (ProfilePicture)findViewById(R.id.profilePicture);
		setProfilePicture(picture, mission.getAddedBy());
	}

	protected void setProfilePicture(final ProfilePicture view, final User user) {

		view.setDefaultPicture();
		ImageLoader.getInstance().cancelDisplayTask(view);

		if (isValidUser(user)) {
			Ln.d("valid user " + user);
			ImageLoader.getInstance().displayImage(user.getPicture(), view, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View v, Bitmap loadedImage) {
					Ln.d("loaded image for user " + user);
					view.setImageBitmap(loadedImage);
				}
			});
		} else {
			Ln.d("not valid user " + user);
		}
	}

	private boolean isValidUser(User user) {
		return user != null && user.getPicture() != null && !user.getPicture().isEmpty();
	}

	private void setUsername() {
		TextView username = getTextView(R.id.username);
		username.setTypeface(fonts.getBodyBold());
		username.setText(mission.getAddedBy().getUsername());
	}

	private void setDescription() {
		TextView description = getTextView(R.id.description);
		description.setTypeface(fonts.getDescriptionFont());
		if (mission.getDescription() != null && mission.getDescription().length() > 0) {
			description.setText(mission.getDescription());
			Linkify.addLinks(description, Linkify.ALL);
		} else
			description.setVisibility(View.GONE);
	}

	protected TextView getTextView(int id) {
		TextView editText = (TextView) findViewById(id);
		editText.setTypeface(fonts.getDescriptionFont());
		return editText;
	}

	private CharSequence getTitleString(Mission mission) {
		return mission.getName().toUpperCase(getResources().getConfiguration().locale);
	}

}
