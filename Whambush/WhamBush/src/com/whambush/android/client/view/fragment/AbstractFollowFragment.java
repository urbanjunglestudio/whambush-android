package com.whambush.android.client.view.fragment;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whambush.android.client.R;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.adapter.FollowAdapter;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.service.PaginateService;
import com.whambush.android.client.view.list.FollowListView;

public abstract class AbstractFollowFragment extends RoboFragment {

	@InjectView(R.id.list)
	FollowListView list;

	protected FollowAdapter adapter;

	private View view;

	private User user;

	private CountListener listener;

	public AbstractFollowFragment() {
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);

		view = inflater.inflate(R.layout.view_follow_list, container, false);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setup();
		refresh();
	}

	private void setup() {
		list.setUser(user);
		list.setListener(listener);
		list.setActivity((WhambushActivity)getActivity());
		PaginateService service = getService();
		list.setBaseRequest(service.parse(null, false, user.getId()));
		list.addPaginateLoader(service);
		adapter = new FollowAdapter(getActivity());
		list.setAdapter(adapter);
	}
	
	public void setListener(CountListener countListener) {
		listener = countListener;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public void refresh() {
		if (adapter == null || list  == null)
			return;
		adapter.clear();
		list.showFooter();
		list.loadBase();
	}
	
	public void redraw() {
		if (adapter == null || list  == null)
			return;
		adapter.notifyDataSetChanged();
	}
	
	protected abstract PaginateService getService();

	
	
}
