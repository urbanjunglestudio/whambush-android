package com.whambush.android.client.view;

import roboguice.RoboGuice;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

import com.whambush.android.client.R;
import com.whambush.android.client.service.listeners.UploadListener;
import com.whambush.android.client.util.Fonts;

public class UploadProgressBar extends View implements UploadListener {

	private static final int STROKE_WIDTH = 9;

	private final static float FULL = 100.0f;
	private static final float HALF = FULL / 2.0f;

	private boolean modal = false;

	//Green ADC234
	//Yellow F5D42A
	private float rFromYtoG = ((float)(0xF5 - 0xAD)) / HALF;
	private float gFromYtoG = ((float)(0xD4 - 0xC2)) / HALF;
	private float bFromYtoG = ((float)(0x2A - 0x34)) / HALF;

	//Red #CD554A
	private float rFromRtoY = ((float)(0xCD - 0xF5)) / HALF;
	private float gFromRtoY = ((float)(0x55 - 0xD4)) / HALF;
	private float bFromRtoY = ((float)(0x4A - 0x2A)) / HALF;

	private float currentR = 0xCD;
	private float currentG = 0x55;
	private float currentB = 0x4A;

	private int progress = 50;
	Paint p = new Paint();
	private View recordButton = new View(this.getContext()); // The empty view is for Graph editor
	private int radius = 45;
	
	private int backgroundColor;
	private int textColor;
	
	private Fonts fonts;

	public UploadProgressBar(Context context) {
		this(context, null);
	}
	
	public UploadProgressBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0); 
	}

	public UploadProgressBar(Context context, AttributeSet attrs, int count) {
		super(context, attrs, count);
		if (!isInEditMode()) {
			RoboGuice.getInjector(context.getApplicationContext()).injectMembers(this);
			fonts = new Fonts();
		}
		
		getModal(context, attrs);
		getColors(context, attrs);
	}

	private void getColors(Context context, AttributeSet attrs) {
		TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.UploadProgressBar);
		backgroundColor = arr.getColor(R.styleable.UploadProgressBar_upBackgroundColor, R.color.uploadBackground);
		textColor = arr.getColor(R.styleable.UploadProgressBar_android_textColor, R.color.uploadBackground);
		arr.recycle();
	}

	private void getModal(Context context, AttributeSet attrs) {
		TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.Modality);
		boolean modal_cs = arr.getBoolean(R.styleable.Modality_modal, false);
		setModal(modal_cs);
		arr.recycle();
	}

	public void status(final int d) {
		new Handler(getContext().getMainLooper()).post(new Runnable() {
			public void run() {
				UploadProgressBar.this.setProgress(d);
				updateColor();
				invalidate();
			}
		});
	}

	private void updateColor() {
		if (getProgressInNumber() >= HALF) {
			currentR -= rFromYtoG;
			currentG -= gFromYtoG;
			currentB -= bFromYtoG;
		} else {
			currentR -= rFromRtoY;
			currentG -= gFromRtoY;
			currentB -= bFromRtoY;
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {

		calculateSizes();

		p.setAntiAlias(true);

		if (isModal())
			drawBackground(canvas);
		
		drawInnerCircle(canvas);
		drawArc(canvas);
		drawTime(canvas);
	}

	private void calculateSizes() {
		radius = (recordButton.getHeight()  - STROKE_WIDTH) /2;
	}

	private void drawBackground(Canvas canvas) {
		p.setColor(backgroundColor);
		p.setStyle(Paint.Style.FILL); 
		canvas.drawRect(0, 0, getWidth(), getHeight(), p);
	}

	private RectF getRect() {
		int centerX = getCenterX();
		int centerY = getCenterY();

		RectF rectF = new RectF(centerX - radius, centerY - radius, centerX + radius, centerY + radius);

		return rectF;
	}

	private void drawInnerCircle(Canvas canvas) {
		p.setStyle(Paint.Style.FILL);
		p.setColor(Color.argb(255, 41, 43, 53));
		int centerX = getCenterX();
		int centerY = getCenterY();

		RectF rect = new RectF(centerX - radius, centerY - radius, centerX + radius, centerY + radius);

		canvas.drawArc (rect, 0, 360, false, p);
	}

	private int getCenterY() {
		int centerY = recordButton.getHeight() / 2;

		return centerY;
	}

	private int getCenterX() {
		int centerX = recordButton.getWidth() / 2;

		return centerX;
	}

	private void drawTime(Canvas canvas) {
		p.setStyle(Paint.Style.FILL);
		p.setColor(textColor);

		if (fonts != null)
			p.setTypeface(fonts.getTitleFont());
		String t = getProgress() + "%";
		p.setTextSize(getHeight() / 2.0f); 
		p.setStrokeWidth(1);

		float textWidth = p.measureText(t);

		int xPos = (int)(getCenterX() - (textWidth/2));
		int yPos = (int) (getCenterY() - ((p.descent() + p.ascent()) / 2)) ; 

		canvas.drawText(t, xPos, yPos, p);
	}


	private void drawArc(Canvas canvas) {
		setArcColor();
		p.setStyle(Paint.Style.STROKE);
		RectF rectF = getRect();
		p.setStrokeWidth(STROKE_WIDTH);
		int startRarius = getStartRarius();
		int endRadius = getEndRadius();
		canvas.drawArc (rectF, startRarius, endRadius, false, p);
	}

	private void setArcColor() {
		p.setColor(Color.rgb((int)currentR, (int)currentG, (int)currentB));
	}

	private int getStartRarius() {
		return (int)(getProgressInNumber() * (360.0/FULL));
	}

	private int getEndRadius() {
		return 360-getStartRarius();
	}

	public boolean isModal() {
		return modal;
	}

	public void setModal(boolean modal) {
		this.modal = modal;
	}

	public String getProgress() {
		if (progress == Integer.MIN_VALUE)
			return "!";
		
		return String.valueOf(getProgressInNumber());
	}
	
	public int getProgressInNumber() {
		if (progress < 0)
			return 0;
		else if (progress > 100)
			return 100;
		else
			return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public void setCoverableArea(View recordButton) {
		this.recordButton = recordButton;
	}

	@Override
	public void error(Exception ex) {
		status(Integer.MIN_VALUE);
	}

}
