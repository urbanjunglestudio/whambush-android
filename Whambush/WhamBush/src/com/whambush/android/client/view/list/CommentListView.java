package com.whambush.android.client.view.list;

import roboguice.util.Ln;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.adapter.CommentListAdapter;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.WhambushList;
import com.whambush.android.client.domain.comment.Comment;
import com.whambush.android.client.domain.comment.CommentList;
import com.whambush.android.client.domain.likes.LikesAndDislikes;
import com.whambush.android.client.domain.likes.LikesAndDislikesError;
import com.whambush.android.client.domain.mission.Mission;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.http.requests.Request;
import com.whambush.android.client.service.CommentService;
import com.whambush.android.client.service.DislikeService;
import com.whambush.android.client.service.LikeService;
import com.whambush.android.client.service.SingleMissionService;
import com.whambush.android.client.service.UndislikeService;
import com.whambush.android.client.service.UnlikeService;
import com.whambush.android.client.service.listeners.CommentServiceListener;
import com.whambush.android.client.service.listeners.DislikeServiceListener;
import com.whambush.android.client.service.listeners.LikeServiceListener;
import com.whambush.android.client.service.listeners.SingleMissionServiceListener;
import com.whambush.android.client.service.listeners.UndislikeServiceListener;
import com.whambush.android.client.service.listeners.UnlikeServiceListener;
import com.whambush.android.client.util.CountHelper;
import com.whambush.android.client.view.BananaShitPointer;
import com.whambush.android.client.view.SingleVideoDetails;
import com.whambush.android.client.view.listener.PaginateListListener;

public class CommentListView extends PaginatedListView<Comment> implements UndislikeServiceListener, DislikeServiceListener, 
UnlikeServiceListener, LikeServiceListener, CommentServiceListener {	

	private VideoEntry video;
	private boolean isShitting = false;
	private boolean isBananing = false;
	private PaginateListListener listListener;
	private CommentListAdapter adapter;

	public CommentListView(Context context) {
		super(context);
	}

	public CommentListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CommentListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void setActivity(WhambushActivity activity, VideoEntry entry) {
		this.video = entry;
		super.setActivity(activity);
		addPaginateLoader(new CommentService(activity));
		this.addHeaderView(getHeader());
		setAdapter(adapter = new CommentListAdapter(activity, entry));

		isRemoveableVideo();


		getVideoOperations().setVisibility(View.GONE);
		updateLikesAndDisplikes();
		setBaseRequest(service.parse(null, false, video.getId()));
		loadBase();
	}

	@SuppressLint("InflateParams")
	private View getHeader() {
		LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		SingleVideoDetails details = (SingleVideoDetails)inflater.inflate(R.layout.header_single_video_details, null);
		details.setVideo(video);
		return details;
	}

	private View getVideoOperations() {
		return findViewById(R.id.videoOperations);
	}

	private void isRemoveableVideo() {
		if ( Whambush.get(ConfigImpl.class).getLoginInfo().isGuest()) {
			hideRemove();
			return;
		}

		String currentUserID = getUserId();
		String addedBy = video.getAddedBy().getId();
		Ln.d("current %s addedBy %s", currentUserID, addedBy);

		boolean removable = addedBy.equals(currentUserID);

		if (removable) {
			if (video.getMissionId() == null || video.getMissionId().isEmpty()) {
				showRemove();
				return;
			} else {
				getMission(video.getMissionId());
			}
		} 

		hideRemove();
	}

	private void hideRemove() {
		findViewById(R.id.removeHolder).setVisibility(View.GONE);
	}

	private void showRemove() {
		findViewById(R.id.removeHolder).setVisibility(View.VISIBLE);
	}

	private void getMission(String missionId) {
		new SingleMissionService(getActivity()).getMissionById(missionId, new SingleMissionServiceListener() {

			@Override
			public void connectionError() {
				getActivity().connectionError();
			}

			@Override
			public void error(int httpResultCode) {
				getActivity().error(httpResultCode);
			}

			@Override
			public void missionData(Mission result) {
				if (result.isRanked())
					showRemove();
				else
					hideRemove();
			}
		});

	}

	private String getUserId() {
		return Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getId();
	}


	public void toggleOptions(View view) {
		View videoOperations = getVideoOperations();
		if (videoOperations.getVisibility() == View.VISIBLE) {
			activity.getImageView(R.id.options).setImageResource(R.drawable.options);
			findViewById(R.id.optionsHolder).setBackgroundColor(getResources().getColor(R.color.single_video_notification_area_background));
			videoOperations.startAnimation(activity.collapse);
			videoOperations.setVisibility(View.GONE);
		} else {
			activity.getImageView(R.id.options).setImageResource(R.drawable.foptions_active);
			findViewById(R.id.optionsHolder).setBackgroundColor(getResources().getColor(R.color.single_video_notification_option_background));
			videoOperations.startAnimation(activity.expand);
			videoOperations.setVisibility(View.VISIBLE);
		}
	}

	public void shareTheVideo(View view) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		i.putExtra(Intent.EXTRA_TITLE, activity.getString(R.string.SINGLE_SHARE_TITLE));
		i.putExtra(Intent.EXTRA_SUBJECT,  String.format(activity.getString(R.string.SINGLE_SHARE_MESSAGE), video.getName()));
		i.putExtra(Intent.EXTRA_TEXT, video.getWebUrl());
		activity.startActivity(Intent.createChooser(i, activity.getString(R.string.SINGLE_SHARE_TITLE)));
	}

	public void updateLikesAndDisplikes() {
		ImageView banana = activity.getImageView(R.id.banana);
		if (video.isHasLiked())
			banana.setImageResource(R.drawable.banana_active);
		else
			banana.setImageResource(R.drawable.banana_default);

		ImageView shit = activity.getImageView(R.id.shit);
		if (video.isHasDisliked())
			shit.setImageResource(R.drawable.shit_active);
		else
			shit.setImageResource(R.drawable.shit_inactive);

		setLikeCount();
	}

	private void setLikeCount() {
		TextView commentCount = activity.getTextView(R.id.likeCount);
		commentCount.setTypeface(activity.getFonts().getOtherFont());
		int likeCount = getLikeCount();
		commentCount.setText(new CountHelper(activity).countAsStr(likeCount));

		BananaShitPointer view = (BananaShitPointer)findViewById(R.id.bubleLayer);

		if (video.getLikes() == 0 && video.getDislikes() == 0) {
			view.setVisibility(View.INVISIBLE);
			commentCount.setTextColor(getResources().getColor(R.color.whambushBottomBar));
		} else {
			view.setVisibility(View.VISIBLE);
			if (likeCount >= 0) {
				view.setLikeDislike(true);
				commentCount.setTextColor(getResources().getColor(R.color.whambushGreen));
			} else {
				view.setLikeDislike(false);
				commentCount.setTextColor(getResources().getColor(R.color.whambushRed));
			}
		}
	}

	private int getLikeCount() {
		return video.getLikes() - video.getDislikes();
	}


	@Override
	public void reload() {
		adapter.clearComments();
		super.reload();
	}

	public void shitTheVideo(View view) {
		if (isShitting)
			return;
		isShitting = true;
		if (video.isHasDisliked()) {
			new UndislikeService(activity).undislike(video, this);
		} else {
			new DislikeService(activity).dislike(video, this);
		}
	}

	public void bananaTheVideo(View view) {
		if (isBananing)
			return;

		isBananing = true;
		if (video.isHasLiked()) {
			new UnlikeService(activity).unlike(video, this);
		} else {
			new LikeService(activity).like(video, this);
		}
	}

	@Override
	public void liked(LikesAndDislikes result) {
		video.setHasLiked(true);
		video.setHasDisliked(false);
		update(result);
		isBananing = false;
		activity.hideProgressBar();
	}

	@Override
	public void unliked(LikesAndDislikes result) {
		video.setHasLiked(false);
		update(result);
		isBananing = false;
		activity.hideProgressBar();
	}

	@Override
	public void disliked(LikesAndDislikes result) {
		video.setHasDisliked(true);
		video.setHasLiked(false);
		update(result);
		isShitting = false;
		activity.hideProgressBar();
	}

	@Override
	public void undisliked(LikesAndDislikes result) {
		video.setHasDisliked(false);
		update(result);
		isShitting = false;
		activity.hideProgressBar();
	}

	private void update(LikesAndDislikes result) {
		video.setLikes(result.getLikes());
		video.setDislikes(result.getDislikes());
		update();
	}

	private void update() {
		updateLikesAndDisplikes();
	}

	@Override
	public void errorResult(final LikesAndDislikesError error) {
		activity.getDialogHelper().displayDialog(activity.getDialogHelper().createErrorDialog(getResources().getString(R.string.GENERAL_ERROR_UNKNOWN)));
	}

	public void setVideo(VideoEntry video) {
		this.video = video;
	}

	@Override
	public void paginateItems(final WhambushList<Comment> items) {
		Ln.d("Received comments");

		super.paginateItems(items);
		adapter.addComments((CommentList)items);
		listListener.loaded();

	}

	@Override
	public void commentUploaded() {
		Ln.e("NOT IMPLEMENTED");
	}

	@Override
	protected Request getNext(WhambushList<Comment> items) {
		return service.parse(items.getNext(), true); 
	}

	public void setListener(PaginateListListener listListener) {
		this.listListener = listListener;
	}

	public CommentListAdapter getCommentListAdapter() {
		return adapter;
	}

}
