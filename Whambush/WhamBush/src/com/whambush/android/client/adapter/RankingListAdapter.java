package com.whambush.android.client.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.adapter.holders.RankedHolder;
import com.whambush.android.client.domain.ranking.RankedUser;
import com.whambush.android.client.domain.user.User;

public class RankingListAdapter extends WhambushAdapter<RankedUser> {

	public RankingListAdapter(Context context) {
		super(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RankedHolder holder = null;

		if(row == null) {
			row = inflate(R.layout.list_rank_row, parent, false);

			holder = new RankedHolder();
			holder.bananaCount = (TextView)row.findViewById(R.id.banana_count);
			holder.bananaCount.setTypeface(getFonts().getTitleFont());
			
			holder.rank = (TextView)row.findViewById(R.id.rank);
			holder.rank.setTypeface(getFonts().getTitleFont());
			
			holder.username = (TextView)row.findViewById(R.id.username);
			holder.username.setTypeface(getFonts().getDescriptionFont());

			row.setTag(holder);
		} else {
			holder = (RankedHolder)row.getTag();
		}

		populate(position, holder);

		return row;
	}

	private void populate(int position, RankedHolder holder) {
		RankedUser user = getItem(position);
		
		float size = getContext().getResources().getDimension(R.dimen.titleFontSize);		
		if (position < 3) {
			size = getContext().getResources().getDimension(R.dimen.rank_top_3_size);
		}
		
		holder.username.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
		holder.bananaCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
		holder.rank.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
		
		holder.username.setText(user.getUsername());
		holder.username.setTag(user);

		holder.bananaCount.setText(String.format("%d", user.getScore()));
		holder.bananaCount.setTag(user);
		
		holder.rank.setText(String.format("%d", position + 1));
		holder.rank.setTag(user);
		
		holder.user = user;
	}

	public User user(View view) {
		return (User)view.getTag();
	}

}
