package com.whambush.android.client.adapter.holders;

import com.whambush.android.client.domain.ranking.RankedUser;

import android.widget.TextView;

public class RankedHolder {
	public TextView rank;
	public TextView username;
	public TextView bananaCount;
	public RankedUser user;
}