package com.whambush.android.client.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.Html;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.config.ConfigImpl;
import com.whambush.android.client.domain.user.User;
import com.whambush.android.client.view.fragment.ActivityFragment;
import com.whambush.android.client.view.fragment.CountListener;
import com.whambush.android.client.view.fragment.FollowFragment;
import com.whambush.android.client.view.fragment.FollowingFragment;
import com.whambush.android.client.view.fragment.RankingFragment;
import com.whambush.android.client.view.fragment.UserVideoFeedFragment;

public class UserPagerAdapter extends FragmentPagerAdapter {

	private static final int SELF_FRAGMENT_COUNT = 5;
	private static final int OTHER_FRAGMENT_COUNT = 3;
	private ActivityFragment activity;
	private FollowingFragment following;
	private FollowFragment follow;
	private UserVideoFeedFragment videos;
	private RankingFragment ranking;

	private User user;
	private int followingCount = 0;
	private int followersCount = 0;
	private int videoCount = 0;
	private int activityCount = 0;
	private int rankingCount = 0;

	public UserPagerAdapter(FragmentManager fragmentManager) {
		super(fragmentManager);
	}

	public void setUser(User user) {
		this.user = user;

		getFollowing();
		getFollow();
		getVideos();

		if (isSelf())
			getActivityFragment();
	}

	private boolean isSelf() {
		if (Whambush.get(ConfigImpl.class).getLoginInfo() == null || Whambush.get(ConfigImpl.class).getLoginInfo().getUser() == null)
			return false;

		return user != null && user.getId().equals(Whambush.get(ConfigImpl.class).getLoginInfo().getUser().getId());
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
		case 0:
			return getVideos();
		case 1:
			return getFollow();
		case 2:
			return getFollowing();
		case 3:
			return getActivityFragment();
		case 4:
		default:
			return getRankingFragment();
		}
	}

	private RankingFragment getRankingFragment() {
		if (ranking == null)
			ranking = new RankingFragment();

		ranking.setUser(user);
		ranking.setListener(new CountListener() {

			@Override
			public void count(int count) {
				setRankingCount(count);
			}
		});
		return ranking;
	}

	private FollowingFragment getFollowing() {
		if (following == null)
			following = new FollowingFragment();

		following.setUser(user);
		following.setListener(new CountListener() {

			@Override
			public void count(int count) {
				setFollowingCount(count);
			}

		});

		return following;
	}

	private FollowFragment getFollow() {
		if (follow == null)
			follow = new FollowFragment();
		follow.setUser(user);

		follow.setListener(new CountListener() {

			@Override
			public void count(int count) {
				setFollowersCount(count);
			}

		});

		return follow;
	}

	private UserVideoFeedFragment getVideos() {
		if (videos == null)
			videos = new UserVideoFeedFragment();

		videos.setUser(user);

		videos.setListener(new CountListener() {

			@Override
			public void count(int count) {
				setVideoCount(count);
			}

		});

		return videos;
	}

	private ActivityFragment getActivityFragment() {
		if (activity == null)
			activity = new ActivityFragment();
		activity.setListener(new CountListener() {

			@Override
			public void count(int count) {
				setActivityCount(activity.getUnreadCount());
			}

		});

		setActivityCount(Whambush.get(ConfigImpl.class).getLoginInfo().getUnreadActivities());
		return activity;
	}

	@Override
	public int getCount() {		
		if (isSelf())
			return SELF_FRAGMENT_COUNT;
		else
			return OTHER_FRAGMENT_COUNT;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return getCountString(R.string.USER_VIDEO_COUNT, R.string.USER_COUNT, getVideoCount());
		case 1:
			return getCountString(R.string.USER_FOLLOWERS_COUNT, R.string.USER_COUNT, getFollowersCount());
		case 2:
			return getCountString(R.string.USER_FOLLOWINGS_COUNT, R.string.USER_COUNT, getFollowingCount());
		case 3:
			return getCountString(R.string.USER_ACTIVITY, R.string.USER_COUNT, getActivityCount());
		case 4:
			return getCountString(R.string.USER_RANK, R.string.USER_COUNT, getRankingCount());
		default:
			return "";
		}
	}

	private CharSequence getCountString(int main, int sub, int count) {
		String countString = Whambush.getContext().getString(sub);
		String subString = String.format("<font color=#FF0000>" + countString + "</font>", count);
		String mainString = Whambush.getContext().getString(main);

		return Html.fromHtml("<font color=#FFFFFF>" + String.format(mainString, subString) + "</font>");
	}

	private int getRankingCount() {
		return rankingCount;
	}

	private int getActivityCount() {
		return activityCount;
	}

	private int getVideoCount() {
		return videoCount;
	}

	private int getFollowersCount() {
		return followersCount;
	}

	private int getFollowingCount() {
		return followingCount;
	}

	public void updateFollow() {
		getFollowing().refresh();
		getFollow().refresh();
	}

	public void refresh() {
		updateFollow();
		getVideos().refresh();
		if (isSelf())
			getActivityFragment().refresh();
	}

	public void redrawFollow() {
		getFollowing().redraw();
		getFollow().redraw();
	}

	private void setRankingCount(int count) {
		rankingCount = count;
		notifyDataSetChanged();
	}

	public void setFollowingCount(int followingCount) {
		this.followingCount = followingCount;
		notifyDataSetChanged();

	}

	public void setFollowersCount(int followersCount) {
		this.followersCount = followersCount;
		notifyDataSetChanged();
	}

	public void setVideoCount(int videoCount) {
		this.videoCount = videoCount;
		notifyDataSetChanged();
	}

	public void setActivityCount(int activityCount) {
		this.activityCount = activityCount;
		notifyDataSetChanged();
	}
}
