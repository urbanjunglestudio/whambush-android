package com.whambush.android.client.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.domain.mission.Mission;

public class VideoTypeAdapter extends WhambushAdapter<String> {

	private List<Mission> missions;

	public VideoTypeAdapter(Context context, List<Mission> values) {
		super(context);
		
		add(context.getString(R.string.UPLOAD_NORMAL_VIDEO));
		missions = values;

		for (Mission mission : values)
			add(mission.getName());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if(row == null)
			row = inflate(R.layout.list_video_type_row, parent, false);

		TextView entry = (TextView)row.findViewById(R.id.video_type_entry);
		entry.setTypeface(getFonts().getDescriptionFont());
		entry.setText(getItem(position));
		row.setTag(getItem(position));
		return row;
	}

	public Mission getMission(View view) {
		String tag = (String)view.getTag();

		for (int i = 0; i < getAll().size(); i++) {
			if (getItem(i).equals(tag)) {
				int location = i-1;
				if (location < 0)
					return null;
				else
					return missions.get(location);
			}
		}

		return null;
	}

}
