package com.whambush.android.client.adapter.holders;

import android.widget.ImageView;
import android.widget.TextView;

import com.whambush.android.client.domain.activity.ActivityFeedEvent;
import com.whambush.android.client.view.ProfilePicture;

public class UserActivityHolder {
	public TextView username;
	public TextView message;
	public TextView time;
	public ImageView unread;
	public ActivityFeedEvent event;
	public ProfilePicture profile_picture;
}