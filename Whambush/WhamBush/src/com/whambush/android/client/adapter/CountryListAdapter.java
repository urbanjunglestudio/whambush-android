package com.whambush.android.client.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.whambush.android.client.R;
import com.whambush.android.client.domain.country.Country;

public class CountryListAdapter extends WhambushAdapter<Country> {

	private String countryCodeString;

	public CountryListAdapter(Context context) {
		super(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if(row == null) {
			row = inflate(R.layout.list_country_flag_row, parent, false);
		} 

		TextView entry = getCountryLabel(row);
		ImageView banner = getBanner(row);
		ImageView chooser = getChooser(row);

		Country item = getItem(position);
		entry.setText(item.getName());
		entry.setVisibility(View.VISIBLE);
		setBigFlag(banner, item);

		if (item.getCountry().equals(countryCodeString))
			chooser.setImageResource(R.drawable.country_select_done);
		else
			chooser.setImageResource(R.drawable.country_select);

		row.setTag(item);
		return row;
	}

	private void setBigFlag(final ImageView banner, Country item) {
		banner.setImageResource(R.drawable.placeholder_banner);
		
		if (item.getBigFlag() != null && !item.getBigFlag().isEmpty()) {
			ImageLoader.getInstance().cancelDisplayTask(banner);

			ImageLoader.getInstance().displayImage(item.getBigFlag(), banner, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					banner.setImageBitmap(loadedImage);
				}
			});
		} else {
			banner.setImageResource(R.drawable.flag_selection_global);
		}
	}

	public void setCurrentlySelected(String countryCodeString) {
		this.countryCodeString = countryCodeString;
	}

	private ImageView getChooser(View row) {
		return (ImageView)row.findViewById(R.id.image);
	}

	private ImageView getBanner(View row) {
		return (ImageView)row.findViewById(R.id.flag_image);
	}

	private TextView getCountryLabel(View row) {
		TextView entry = (TextView)row.findViewById(R.id.country_entry);
		entry.setTypeface(getFonts().getTitleFont());
		return entry;
	}

	public int getId(View view) {
		Country country = (Country)view.getTag();

		for (int i = 0; i < getAll().size(); i++) {
			if (getItem(i).equals(country))
				return i+1;
		}

		return -1;
	}
}
