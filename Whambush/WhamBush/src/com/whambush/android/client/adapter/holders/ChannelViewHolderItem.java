package com.whambush.android.client.adapter.holders;

import com.whambush.android.client.domain.tv.Channel;

import android.widget.ImageView;
import android.widget.TextView;

public class ChannelViewHolderItem {

	public ImageView banner;
	public TextView title;
	public Channel channel;
}