package com.whambush.android.client.adapter;

import java.util.List;

import roboguice.util.Ln;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.whambush.android.client.AbstractActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;
import com.whambush.android.client.adapter.holders.VideoViewHolderItem;
import com.whambush.android.client.dao.VideoEntryDao;
import com.whambush.android.client.domain.video.VideoEntry;
import com.whambush.android.client.util.CountHelper;
import com.whambush.android.client.util.DateAndTimeParser;

public class VideoListAdapter extends WhambushAdapter<String> {

	private boolean ranked;
	private boolean tvAdapter;
	private AbstractActivity activity;

	public VideoListAdapter(AbstractActivity activity, boolean isTvAdapter) {
		super(activity);
		this.activity = activity;
		this.tvAdapter = isTvAdapter;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		VideoViewHolderItem holder;

		if(convertView == null) {
			convertView = inflate(R.layout.list_movie_row, parent, false);
			holder = createHolderView(convertView);
			convertView.setTag(holder);
		} else
			holder = (VideoViewHolderItem)convertView.getTag();

		populateView(position, convertView, holder);

		return convertView;
	}

	private void populateView(int position, View convertView, VideoViewHolderItem holder) {

		VideoEntry video = getVideoEntry(position);
		if (video != null)
			Ln.d("Draw video " + video.toString());
		else
			Ln.d("Draw video null at position " + position);
		if (isValidVideo(video)) {

			setTime(convertView, holder, video);
			setDescription(holder, video);
			setTitle(convertView, holder, video, position+1);
			SetUsername(holder, video);
			setCommentCount(holder, video);
			setLikeCount(holder, video);
			setBanana(holder, video);
			setImageAsync(holder, video);
			setViewCount(holder, video);
			setTv(holder, video);
			holder.video = video;
		}
	}

	private void setTv(VideoViewHolderItem holder, VideoEntry video) {
		if (video.getVideoType() != 2 || tvAdapter)
			holder.tvIcon.setVisibility(View.GONE);
		else
			holder.tvIcon.setVisibility(View.VISIBLE);
	}

	private void setViewCount(VideoViewHolderItem holder, VideoEntry video) {
		holder.viewCount.setText(""+video.getViewCount());
	}

	private VideoEntry getVideoEntry(int position) {
		if (position < getCount())
			return Whambush.get(VideoEntryDao.class).getVideoEntry(getItem(position));

		return null;
	}

	private void setBanana(VideoViewHolderItem holder, VideoEntry video) {
		if ((video.getLikes() - video.getDislikes()) < 0)
			holder.banana.setImageResource(R.drawable.shit_dark_small);
		else
			holder.banana.setImageResource(R.drawable.banana_dark_small);
	}

	private void setLikeCount(VideoViewHolderItem holder, VideoEntry video) {
		holder.likeCount.setText(new CountHelper(activity).countAsStr(video.getLikes() - video.getDislikes()));
	}

	private void setCommentCount(VideoViewHolderItem holder, VideoEntry video) {
		holder.commentCount.setText(new CountHelper(activity).countAsStr(video.getCommentCount()));
	}

	private void SetUsername(VideoViewHolderItem holder, VideoEntry video) {
		holder.username.setText(video.getAddedBy().getUsername());
	}

	private void setTitle(View convertView, VideoViewHolderItem holder,
			VideoEntry video, int index) {
		String title = getTitle(convertView.getResources(), video);
		if (ranked) {
			holder.title.setText(String.format("%d. %s", index, title));
		} else
			holder.title.setText(title);
	}

	private void setTime(View convertView, VideoViewHolderItem holder,
			VideoEntry video) {
		holder.time.setText(DateAndTimeParser.getToAsNiceString(convertView.getResources(), video.getPublishedAt()));
	}

	private void setDescription(VideoViewHolderItem holder, VideoEntry video) {
		if (containsValidDescription(video)) {
			holder.description.setVisibility(View.VISIBLE);
			holder.description.setText(video.getDescription());
		} else
			holder.description.setVisibility(View.GONE);
	}

	private boolean isValidVideo(VideoEntry video) {
		return video != null;
	}

	private boolean containsValidDescription(VideoEntry video) {
		return video.getDescription() != null && video.getDescription().length() > 0;
	}

	private String getTitle(Resources resources, VideoEntry video) {
		String title = getMissionId(video).length() > 0?resources.getString(R.string.MAIN_MISSION_PREFIX):"";
		title += video.getName().toUpperCase(activity.getResources().getConfiguration().locale); 
		return title;
	}

	private String getMissionId(VideoEntry video) {
		return video.getMissionId()==null?"":video.getMissionId();
	}

	private VideoViewHolderItem createHolderView(View convertView) {
		VideoViewHolderItem holder = new VideoViewHolderItem();

		holder.title = getTextView(R.id.movie_title, convertView);
		holder.title.setTypeface(getFonts().getTitleFont());
		holder.description = getTextView(R.id.movie_description, convertView);
		holder.description.setTypeface(getFonts().getDescriptionFont());
		holder.time = getTextView(R.id.movie_time, convertView);
		holder.time.setTypeface(getFonts().getTimeFont());
		holder.username = getTextView(R.id.usernameLabel, convertView);
		holder.username.setTypeface(getFonts().getDescriptionFont());
		holder.commentCount = getTextView(R.id.comment_count, convertView);
		holder.commentCount.setTypeface(getFonts().getOtherFont());
		holder.likeCount = getTextView(R.id.like_count, convertView);
		holder.likeCount.setTypeface(getFonts().getOtherFont());
		holder.progress = convertView.findViewById(R.id.progressLayout);
		holder.banana = getBanana(convertView);
		holder.image = getImageView(convertView, R.id.thumbnail);
		holder.thumbnailHeader = convertView.findViewById(R.id.thumbnailHeader);
		holder.viewCount = getTextView(R.id.viewcountLabel, convertView);
		holder.viewCount.setTypeface(getFonts().getUsernameFont());
		holder.beingProcessed = getTextView(R.id.beingProcessed, convertView);
		holder.beingProcessed.setTypeface(getFonts().getTitleFont());
		holder.tvIcon = getImageView(convertView, R.id.tv_icon);

		int x = getImageX();

		LayoutParams params = holder.image.getLayoutParams();
		params.width = (x / 2) - (int)activity.getResources().getDimension(R.dimen.movie_entry_thumbnail_right_padding);
		params.height = (int)(((float)params.width) / (1.70));
		holder.image.setLayoutParams(params);
		holder.progress.setLayoutParams(params);
		holder.beingProcessed.setLayoutParams(params);

		params = holder.thumbnailHeader.getLayoutParams();
		params.width = (x / 4);
		holder.thumbnailHeader.setLayoutParams(params);


		return holder;
	}

	@SuppressWarnings("deprecation")
	private int getImageX() {
		Display display;

		if (android.os.Build.VERSION.SDK_INT >= 13) {
			WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
			display = wm.getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			return size.x;

		} else {
			display = activity.getWindowManager().getDefaultDisplay();
			return display.getWidth();
		}
	}

	private void setImageAsync(final VideoViewHolderItem holder, VideoEntry video) {

		if (video.isProcessed()) {
			String url = createUrl(video);

			holder.image.setVisibility(View.INVISIBLE);
			holder.progress.setVisibility(View.VISIBLE);

			ImageLoader.getInstance().cancelDisplayTask(holder.image);

			ImageLoader.getInstance().displayImage(url, holder.image, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					holder.image.setImageBitmap(loadedImage);
					holder.image.setVisibility(View.VISIBLE);
					holder.progress.setVisibility(View.INVISIBLE);
				}
			});
		} else {
			holder.image.setVisibility(View.INVISIBLE);
			holder.progress.setVisibility(View.INVISIBLE);
			holder.beingProcessed.setVisibility(View.VISIBLE);
		}
	}

	private String createUrl(VideoEntry video) {
		StringBuilder builder = new StringBuilder("http://view.vzaar.com/");
		builder.append(video.getExternal_id());
		builder.append("/image");		
		return builder.toString();
	}

	private ImageView getImageView(View vi, int id) {
		return (ImageView)vi.findViewById(id);
	}


	private ImageView getBanana(View view) {
		ImageView image = (ImageView)view.findViewById(R.id.banana);
		image.setImageBitmap(null);
		return image;
	}

	private TextView getTextView(int id, View vi) {
		TextView view = (TextView)vi.findViewById(id);
		return view;
	}

	public VideoEntry getVideo(View view) {
		if (view == null || view.getTag() == null)
			return null;
		return ((VideoViewHolderItem)view.getTag()).video;
	}


	public void setRanked(boolean ranked) {
		this.ranked = ranked;
	}

	public void addVideos(List<VideoEntry> tempVideos) {
		for (VideoEntry v : tempVideos)
			add(v.getId());
		
		notifyDataSetChanged();
	}
	
}

