package com.whambush.android.client.dialog;

import roboguice.util.Ln;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.WhambushActivity;
import com.whambush.android.client.domain.country.Country;
import com.whambush.android.client.view.listener.CountrySelectionListener;

public class RegisterDialog extends WhambushDialogFragment {

	private String email;
	private String username;
	private String country;
	
	private View view;
	private AlertDialog dialog;


	public RegisterDialog() {
		email = "";
		username = "";
	}
	
	public RegisterDialog(String username, String email) {
		this.username = username;
		this.email = email;		
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		outState.putString("EMAIL", email);
		outState.putString("USERNAME", username);
		outState.putString("COUNTRY", country);
	}

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		if (savedInstanceState != null) {
			username = savedInstanceState.getString("username");
			country = savedInstanceState.getString("country");
			email = savedInstanceState.getString("email");
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		view = inflater.inflate(R.layout.dialog_register, null);
		builder.setView(view);

		builder.setPositiveButton(R.string.REGISTER_REGISTER_BUTTON, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				final TextView username = (TextView)((Dialog)dialog).findViewById(R.id.username);
				final TextView password = (TextView)((Dialog)dialog).findViewById(R.id.password);
				final TextView rePassword = (TextView)((Dialog)dialog).findViewById(R.id.rePassword);
				final TextView email = (TextView)((Dialog)dialog).findViewById(R.id.email);
				hideKeyboard(username);
				((WhambushActivity)getActivity()).doRegister((Dialog)dialog, username.getText().toString(), password.getText().toString(), 
						rePassword.getText().toString(), email.getText().toString(), country);
			}
		})
		.setNegativeButton(R.string.GENERAL_CANCEL, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Ln.d("Cancel");
				final TextView username = (TextView)((Dialog)dialog).findViewById(R.id.username);
				hideKeyboard(username);
			}
		});

		dialog = builder.create();
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setAnimation(dialog);
		dialog.show();

		TextView username = (TextView)view.findViewById(R.id.username);
		username.setTypeface(getFonts().getDescriptionFont());
		username.setText(this.username);
		
		((TextView)view.findViewById(R.id.country)).setTypeface(getFonts().getDescriptionFont());
		
		((TextView)view.findViewById(R.id.country)).setClickable(true);
		((TextView)view.findViewById(R.id.country)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				handleCountrySelection();
				
			}
		});
		
		((TextView)view.findViewById(R.id.password)).setTypeface(getFonts().getDescriptionFont());
		((TextView)view.findViewById(R.id.rePassword)).setTypeface(getFonts().getDescriptionFont());
		
		TextView email = (TextView)view.findViewById(R.id.email);
		email.setTypeface(getFonts().getDescriptionFont());
		email.setText(this.email);

		((TextView)view.findViewById(R.id.register_title)).setTypeface(getFonts().getTitleFont());

		setButton(dialog.getButton(DialogInterface.BUTTON_POSITIVE));
		setButton(dialog.getButton(DialogInterface.BUTTON_NEGATIVE));
		
		
		
		return dialog;
	}

	public void handleCountrySelection() {
		dialog.hide();
		
		((WhambushActivity)getActivity()).showCountrySelection(new CountrySelectionListener() {
			
			@Override
			public void selected(Country item) {
				Ln.d("Selected " + item);
				country = item.getCountry();
				((TextView)view.findViewById(R.id.country)).setText(item.getName());
				
				dialog.show();
			}
		});
	}

}
