package com.whambush.android.client.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.WhambushActivity;

@SuppressLint("InflateParams")
public class LoginDialog extends WhambushDialogFragment {

	private final String username;
	private final String password;


	public LoginDialog() {
		this.username = "";
		this.password = "";
	}
	
	public LoginDialog(String username, String password) {
		this.username = username;
		this.password = password;
	}
	

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setPositiveButton(R.string.GENERAL_LOGIN, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				final TextView username = (TextView)((Dialog)dialog).findViewById(R.id.username);
				final TextView password = (TextView)((Dialog)dialog).findViewById(R.id.password);
				hideKeyboard(username);
				((WhambushActivity)getActivity()).doLogin((Dialog)dialog, username.getText().toString(), password.getText().toString());
			}
		})
		.setNegativeButton(R.string.GENERAL_CANCEL, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				final TextView username = (TextView)((Dialog)dialog).findViewById(R.id.username);
				hideKeyboard(username);
			}
		});
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_login, null);
		builder.setView(view);

		
		AlertDialog dialog = builder.create();
		setAnimation(dialog);
		
		dialog.show();
		setButton(dialog.getButton(DialogInterface.BUTTON_POSITIVE));
		setButton(dialog.getButton(DialogInterface.BUTTON_NEGATIVE));
		
		TextView username = (TextView)view.findViewById(R.id.username);
		username.setTypeface(getFonts().getDescriptionFont());
		username.setText(this.username);
		
		TextView password = (TextView)view.findViewById(R.id.password);
		password.setTypeface(getFonts().getDescriptionFont());
		password.setText(this.password);
		
		((TextView)view.findViewById(R.id.login_title)).setTypeface(getFonts().getTitleFont());
		
		return dialog;
	}
}
