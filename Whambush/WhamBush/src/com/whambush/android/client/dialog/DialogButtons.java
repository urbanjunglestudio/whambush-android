package com.whambush.android.client.dialog;

import java.util.ArrayList;
import java.util.List;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class DialogButtons {

	List<DialogButton> buttons = new ArrayList<DialogButton>();
	
	public void add(int type, String text, DialogInterface.OnClickListener listener) {
		buttons.add(new DialogButton(type, text, listener));
	}
	
	public OnClickListener getListener(int id) {
		return buttons.get(id).getListener();
	}
	
	public CharSequence getText(int id) {
		return buttons.get(id).getText();
	}
	
	public boolean hasPositiveButton() {
		return has(DialogInterface.BUTTON_POSITIVE);
	}
	
	public int getPositiveId() {
		return getId(DialogInterface.BUTTON_POSITIVE);
	}
	
	public boolean hasNeutralButton() {
		return has(DialogInterface.BUTTON_NEUTRAL);
	}
	
	public int getNeutralId() {
		return getId(DialogInterface.BUTTON_NEUTRAL);
	}
	
	public boolean hasNegativeButton() {
		return has(DialogInterface.BUTTON_NEGATIVE);
	}
	
	public int getNegativeId() {
		return getId(DialogInterface.BUTTON_NEGATIVE);
	}
	
	public int getId(int buttonType) {
		for (int i = 0; i < buttons.size(); i++)
			if (buttons.get(i).getType() == buttonType)
				return i;
		return -1;
	}

	
	
	private boolean has(int buttonType) {
		for (DialogButton button : buttons)
			if (button.getType() == buttonType)
				return true;
		
		return false;
	}

	private class DialogButton {
	
		private final int type;
		private final String text;
		private final DialogInterface.OnClickListener listener;
		
		public DialogButton(int type, String text, OnClickListener listener) {
			this.type = type;
			this.text = text;
			this.listener = listener;
		}
		
		private int getType() {
			return type;
		}

		private String getText() {
			return text;
		}

		private DialogInterface.OnClickListener getListener() {
			return listener;
		}
	}

}
