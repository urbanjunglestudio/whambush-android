package com.whambush.android.client.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.activities.UserActivity;

public class ProfilePictureDialog extends WhambushDialogFragment {

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setPositiveButton(R.string.USER_SETTINGS_PROFILE_PICTURE_GALLERY, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int id) {
				((UserActivity)getActivity()).selectPicture();
			}
		})
		.setNeutralButton(R.string.USER_SETTINGS_PROFILE_PICTURE_CAMERA, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				((UserActivity)getActivity()).takePicture();
			}
		})
		.setNegativeButton(R.string.GENERAL_CANCEL, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		});

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_add_profile_picture, null);
		builder.setView(view);

		((TextView)view.findViewById(R.id.add_picture_title)).setTypeface(getFonts().getTitleFont());


		AlertDialog dialog = builder.create();
		setAnimation(dialog);
		dialog.show();

		setButton(dialog.getButton(DialogInterface.BUTTON_POSITIVE));
		setButton(dialog.getButton(DialogInterface.BUTTON_NEGATIVE));
		setButton(dialog.getButton(DialogInterface.BUTTON_NEUTRAL));

		return dialog;
	}
}
