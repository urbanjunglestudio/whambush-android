package com.whambush.android.client.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.whambush.android.client.R;
import com.whambush.android.client.Whambush;

@SuppressLint("InflateParams")
public class ActivationDialog extends WhambushDialogFragment {

	private static final String EMAIL_KEY = "EMAIL";
	private String email;

	public ActivationDialog(String email) {
		this.email = email;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(EMAIL_KEY, email);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		if (savedInstanceState != null && savedInstanceState.containsKey(EMAIL_KEY))
			email = savedInstanceState.getString(EMAIL_KEY);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setPositiveButton(R.string.GENERAL_OK, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		})
		.setNegativeButton(R.string.SETTINGS_EMAIL_RESEND_ACTIVATION, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Whambush.getContext().getString(R.string.SETTINGS_RESEND_ACTIVATION_LINK))); 
				startActivity(intent);
			}
		});
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_activation, null);
		builder.setView(view);

		
		AlertDialog dialog = builder.create();
		setAnimation(dialog);
		
		dialog.show();
		setButton(dialog.getButton(DialogInterface.BUTTON_POSITIVE));
		setButton(dialog.getButton(DialogInterface.BUTTON_NEGATIVE));
		
		TextView textView = (TextView)view.findViewById(R.id.activation_text);
		textView.setText(String.format(textView.getText().toString(), email));
		((TextView)view.findViewById(R.id.activation_title)).setTypeface(getFonts().getTitleFont());
		
		return dialog;
	}	
}
