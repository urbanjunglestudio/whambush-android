package com.whambush.android.client.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.whambush.android.client.R;

@SuppressLint("InflateParams")
public class OpenLinkDialog extends WhambushDialogFragment {

	private Uri url;

	public OpenLinkDialog() {
	}
	
	public OpenLinkDialog(Uri url) {
		this.url = url;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("URL", url.getPath());
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		restoreState(savedInstanceState);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setPositiveButton(R.string.GENERAL_OK, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Intent intent = new Intent(Intent.ACTION_VIEW, url); 
				startActivity(intent);
			}
		})
		.setNegativeButton(R.string.GENERAL_CANCEL, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			}
		});
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_openlink, null);
		builder.setView(view);

		AlertDialog dialog = builder.create();
		setAnimation(dialog);
		
		dialog.show();
		setButton(dialog.getButton(DialogInterface.BUTTON_POSITIVE));
		setButton(dialog.getButton(DialogInterface.BUTTON_NEGATIVE));
		
		((TextView)view.findViewById(R.id.openlink_title)).setTypeface(getFonts().getTitleFont());
		
		return dialog;
	}

	private void restoreState(Bundle savedInstanceState) {
		if (savedInstanceState == null)
			return;
		
		String urlStr = savedInstanceState.getString("URL");
		if (urlStr != null)
			url = Uri.parse(urlStr);
	}
}
