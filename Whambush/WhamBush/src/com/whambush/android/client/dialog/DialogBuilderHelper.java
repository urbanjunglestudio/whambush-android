package com.whambush.android.client.dialog;

import roboguice.util.Ln;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.whambush.android.client.AbstractWhambushActivity;
import com.whambush.android.client.R;
import com.whambush.android.client.util.Fonts;

@SuppressLint("InflateParams")
public class DialogBuilderHelper {

	private final AbstractWhambushActivity activity;

	public DialogBuilderHelper(AbstractWhambushActivity activity) {
		this.activity = activity;
	}


	public AlertDialog.Builder createErrorDialog(String description) {
		return createBasicDialog(R.string.GENERAL_ERROR_TITLE, description);
	}
	
	public AlertDialog.Builder createErrorDialog(String description, DialogInterface.OnClickListener listener) {
		return createBasicDialog(listener, R.string.GENERAL_ERROR_TITLE, description);
	}

	public AlertDialog.Builder createWarningDialog(String id) {
		Ln.d("createWarningDialog : %s", id);
		return createBasicDialog(R.string.GENERAL_WARNING_TITLE, id);
	}

	public AlertDialog.Builder createWarningDialog(String id, DialogInterface.OnClickListener listener) {
		Ln.d("createWarningDialog : %s", id);
		return createBasicDialog(listener, R.string.GENERAL_WARNING_TITLE, id);
	}

	public AlertDialog.Builder createWarningDialog(String id, int okButtonId) {
		Ln.d("createWarningDialog : %s (buttons : %d)", id, okButtonId);
		return createBasicDialog(R.string.GENERAL_WARNING_TITLE, okButtonId, id);
	}

	public AlertDialog.Builder createNoteDialog(String id, Object ... args) {
		Ln.d("createNoteDialog : %s %d", id, (args==null?0:args.length));
		return createBasicDialog(R.string.GENERAL_NOTE_TITLE, id, args);
	}

	public AlertDialog.Builder createBasicDialog(int type, String description, Object...args) {
		Ln.d("createWarningDialog : %s %d %d", type, type, (args==null?0:args.length));
		return createBasicDialog(type, R.string.GENERAL_OK, description, args);
	}

	@SuppressLint("InflateParams")
	protected AlertDialog.Builder createBasicDialog(int type, int buttonId, String description, Object...args) {
		return createBasicDialog(null, type, description, args);
	}

	public AlertDialog.Builder createBasicDialog(DialogInterface.OnClickListener listener, int type, String description, Object...args) {
		Ln.d("createWarningDialog : %s %d %d", type, type, (args==null?0:args.length));
		return createBasicDialog(listener, type, R.string.GENERAL_OK, description, args);
	}

	@SuppressLint("InflateParams")
	public AlertDialog.Builder createBasicDialog(DialogInterface.OnClickListener listener, int type, int buttonId, String description, Object...args) {
		return createBasicDialog(listener, activity.getString(type), buttonId, description, args);
	}

	@SuppressLint("InflateParams")
	public AlertDialog.Builder createBasicDialog(DialogInterface.OnClickListener listener, String title, int buttonId, String description, Object...args) {
		Ln.d("createBasicDialog %s %d %s [args : %d]", title, buttonId, description, (args==null?0:args.length));
		activity.hideProgressBar();
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);

		View view = activity.getLayoutInflater().inflate(R.layout.dialog_basic, null);
		builder.setView(view);

		Fonts fonts = new Fonts();
		
		TextView txt = ((TextView)view.findViewById(R.id.dialog_message));
		txt.setText(String.format(description, args));
		txt.setTypeface(fonts.getDescriptionFont());

		txt = ((TextView)view.findViewById(R.id.dialog_title));
		txt.setText(title);
		txt.setTypeface(fonts.getTitleFont());

		builder.setCancelable(false);

		if (listener == null)
			builder.setPositiveButton(activity.getResources().getString(buttonId), getDefaultListener());
		else
			builder.setPositiveButton(activity.getResources().getString(buttonId), listener);
		return builder;
	}

	public void displayDialog(Builder builder) {
		if (activity.isFinishing()) {
			return;
		}

		AlertDialog alert = builder.create();
		Ln.d("Dialog created " + alert);
		if (alert != null) {
			alert.getWindow().getAttributes().windowAnimations = R.style.MyAnimation_Window;
			alert.show();

			setButton(alert.getButton(DialogInterface.BUTTON_POSITIVE));
			setButton(alert.getButton(DialogInterface.BUTTON_NEGATIVE));
			setButton(alert.getButton(DialogInterface.BUTTON_NEUTRAL));
		}
	}

	protected void setButton(Button button) {
		if (button == null)
			return;

		button.setBackgroundColor(activity.getResources().getColor(R.color.whambushBody));
		button.setTextColor(activity.getResources().getColor(R.color.dialog_body_text_color));
	}



	private DialogInterface.OnClickListener getDefaultListener() {
		return new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				activity.enableAll();
			}};
	}


	public Builder createBasicDialog(DialogButtons buttons,
			String title, String description, Object...args) {
		Ln.d("createBasicDialog %s %s [args : %d]", title, description, (args==null?0:args.length));
		activity.hideProgressBar();
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);

		View view = activity.getLayoutInflater().inflate(R.layout.dialog_basic, null);
		builder.setView(view);

		TextView txt = ((TextView)view.findViewById(R.id.dialog_message));
		Fonts fonts = new Fonts();
		
		if (description != null) {
			txt.setText(String.format(description, args));
			txt.setTypeface(fonts.getDescriptionFont());
		} else txt.setVisibility(View.GONE);

		txt = ((TextView)view.findViewById(R.id.dialog_title));
		if (title != null) {
			txt.setText(title);
			txt.setTypeface(fonts.getTitleFont());
		} else txt.setVisibility(View.GONE);

		builder.setCancelable(false);

		if (buttons.hasPositiveButton()) {
			int id = buttons.getPositiveId();
			builder.setPositiveButton(buttons.getText(id), getListener(buttons, id));
		}

		if (buttons.hasNeutralButton()) {
			int id = buttons.getNeutralId();
			builder.setNeutralButton(buttons.getText(id), getListener(buttons, id));
		}

		if (buttons.hasNegativeButton()) {
			int id = buttons.getNegativeId();
			builder.setNegativeButton(buttons.getText(id), getListener(buttons, id));
		}

		return builder;
	}


	private OnClickListener getListener(DialogButtons buttons, int id) {
		OnClickListener listener = buttons.getListener(id);
		if (listener == null)
			return getDefaultListener();

		return listener;
	}

}
