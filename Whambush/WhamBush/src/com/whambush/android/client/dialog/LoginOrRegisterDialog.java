package com.whambush.android.client.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.whambush.android.client.R;

public class LoginOrRegisterDialog extends WhambushDialogFragment {
	
	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setPositiveButton(R.string.GENERAL_LOGIN, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				DialogFragment loginDialog = new LoginDialog();
				loginDialog.show(getFragmentManager(), "LoginDialogFragment");
			}
		})
		.setNeutralButton(R.string.REGISTER_REGISTER_BUTTON, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				DialogFragment loginDialog = new RegisterDialog();
				loginDialog.show(getFragmentManager(), "RegisterDialogFragment");
			}
		})
		.setNegativeButton(R.string.GENERAL_CANCEL, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			}
		});
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_login_or_register, null);
		builder.setView(view);
		
		((TextView)view.findViewById(R.id.login_title)).setTypeface(getFonts().getTitleFont());
		
		
		AlertDialog dialog = builder.create();
		setAnimation(dialog);
		dialog.show();
		
		setButton(dialog.getButton(DialogInterface.BUTTON_POSITIVE));
		setButton(dialog.getButton(DialogInterface.BUTTON_NEGATIVE));
		setButton(dialog.getButton(DialogInterface.BUTTON_NEUTRAL));
		
		return dialog;
	}
}
