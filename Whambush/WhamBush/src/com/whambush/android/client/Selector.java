package com.whambush.android.client;

public enum Selector {

	MAIN,
	MISSION,
	TV,
	PROFILE;

	
	public static final String KEY = "Selector";
	
}
